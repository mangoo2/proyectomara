DROP TABLE IF EXISTS `ventashistoricas_proyeccion_venta`;
CREATE TABLE IF NOT EXISTS `ventashistoricas_proyeccion_venta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idventashistoricas` bigint(20) NOT NULL,
  `opcion` tinyint(4) NOT NULL,
  `porcentaje_a` decimal(10,2) NOT NULL,
  `porcentaje_b` decimal(10,2) NOT NULL,
  `num_tabla` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `anio` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

