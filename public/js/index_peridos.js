var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	get_table();

    ///// Agregar perido
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
 
            }else{
                event.preventDefault();
                Swal.fire({
                    title: '<strong>¿Estás seguro de agregar el periodo?</strong>',
                    icon: 'info',
                    showCloseButton: true,
                    showCancelButton: true,
                    focusConfirm: false,
                    confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Agregar periodo',
                    confirmButtonAriaLabel: 'Thumbs up, great!',
                    cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
                    cancelButtonAriaLabel: 'Thumbs down',
                    customClass: {
                      confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
                      cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
                    },
                    buttonsStyling: false,
                    preConfirm: login => {
                        $('.btn_registro').attr('disabled',true);
                        var datos = $('#form_registro').serialize();
                        $.ajax({
                            type:'POST',
                            url: base_url+'Periodos/add_data',
                            data: datos,
                            statusCode:{
                                404: function(data){
                                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                                },
                                500: function(){
                                    Swal.fire("Error!", "500", "error");
                                }
                            },
                            success:function(data){
                                Swal.fire({
                                    icon: 'success',
                                    title: 'El periodo ha sido agregado',
                                    showConfirmButton: false,
                                    timer: 3000,
                                    buttonsStyling: false
                                });
                                setTimeout(function(){ 
                                    $('.btn_registro').attr('disabled',false);
                                    tabla.ajax.reload();
                                    agregar_periodo();
                                }, 2000);
                            }
                        });  
                        
                    },
                });

            }

            form.classList.add('was-validated');
          },
          false
        );
    });
});

function agregar_periodo(){
    $.ajax({
        type:'POST',
        url: base_url+'Periodos/get_form_periodo',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_periodo').html(data);
        }
    });  
}

function reload_registro(){
    tabla.destroy();
    get_table();
}

function get_table(){
	tabla = $('.dt-responsive').DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Periodos/getlist_data",
            type: "post",
            data:{
            	anio:$('#anio').val(),
            	mes:$('#mes').val()
            },
            error: function(){
               $(".dt-responsive").css("display","none");
            }
        },
      columns: [
        { data: 'id' },
        { data: 'nombre' },
        { data: 'anio' },
        {"data": null,
            "render": function ( data, type, row, meta ){
            var html='';
            if(row.mes==1){
                html='ENERO';
            }else if(row.mes==2){
                html='FEBRERO';
            }else if(row.mes==3){
                html='MARZO';
            }else if(row.mes==4){
                html='ABRIL';
            }else if(row.mes==5){
                html='MAYO';
            }else if(row.mes==6){
                html='JUNIO';
            }else if(row.mes==7){
                html='JULIO';
            }else if(row.mes==8){
                html='AGOSTO';
            }else if(row.mes==9){
                html='SEPTIEMBRE';
            }else if(row.mes==10){
                html='OCTUBRE';
            }else if(row.mes==11){
                html='NOVIEMBRE';
            }else if(row.mes==12){
                html='DICIEMBRE';
            }
            return html;
            }
        },
      ],
      language: {
        url: base_url+'public/es-ES.json',
      },
    });
}


/*function agregar_periodo(){
	Swal.fire({
        title: '<strong>¿Estás seguro de agregar el periodo?</strong>',
        icon: 'info',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Agregar periodo',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
        	$.ajax({
                type:'POST',
                url: base_url+'Periodos/add_data',
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
				        icon: 'success',
				        title: 'El periodo ha sido agregado',
				        showConfirmButton: false,
				        timer: 3000,
				        buttonsStyling: false
				    });
                }
            });  
            
        },
    });
}*/