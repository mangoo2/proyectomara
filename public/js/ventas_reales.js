var base_url = $('#base_url').val();
var idcliente = $('#idcliente').val();
var anio = $('#anio').val();
$(document).ready(function($) {
	$('.mes_num_1').click();
});
function get_infomes(mes){
	var mesname=$('.mes_num_'+mes).data('mesname');
	$.ajax({
        type:'POST',
        url: base_url+'Ventas_reales/obtener_info_mes',
        data: {
        	mes:mes,
        	mesn:mesname,
        	cli:idcliente,
        	anio:anio
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.info_mes_'+mes).html(data);
        }
    });
    infogeneralmeses();
}
function updatemontos(id){
	var monto=$('.input_monto_'+id).val();
	$.ajax({
        type:'POST',
        url: base_url+'Ventas_reales/updatemontos',
        data: {
        	id:id,
        	monto:monto
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
    calculartotales();
    infogeneralmeses();
}
function calculartotales(){
	var total=0;
	var montosmes = $(".tablepagos tbody > tr");
	montosmes.each(function(){                                
        var can = $(this).find("input[class*='input_monto']").val();
        if(can>0){
        	total=parseFloat(total)+parseFloat(can);
        }
    });
    var totalhtml = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(total)
    $('.tota_montos').html(totalhtml);
}
function infogeneralmeses(){
	$.ajax({
        type:'POST',
        url: base_url+'Ventas_reales/infogeneralmeses',
        data: {
        	cli:idcliente,
        	anio:anio
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        	console.log(data);
        	$('.info_mes_general').html(data);
        }
    });
}