var base_url = $('#base_url').val();
$(document).ready(function() {
    
});

function vista_tipo(tipo){
    $('.link_data').removeClass('active');
    $('.vista_txt').hide(500);
    $('.btn_txt').show(500);
    //$('.pestanas_text').html('');
    //$('.servicios_text').html('');
    if(tipo==1){
        $('.link1').addClass('active');
        $('.vista_txt_1').show(500);
        get_oportunidad();
        get_amenaza();
        get_tabla_guia_oportunidades();
    }else if(tipo==2){
        $('.link2').addClass('active');
        $('.vista_txt_2').show(500);
        get_fortaleza();
        get_debilidad();
        get_tabla_guiafortaleza();
    }else if(tipo==3){
        $('.link3').addClass('active');
        $('.vista_txt_3').show(500);
        get_tabla_guiacliente();
        tabla_guiacliente_instrucciones();
    }else if(tipo==4){  
        $('.link4').addClass('active');
        $('.vista_txt_4').show(500);
    }else if(tipo==5){  
        $('.link5').addClass('active');
        $('.vista_txt_5').show(500);
    }else{
    }
    setTimeout(function(){ 

    }, 1000);
}

function get_oportunidad(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_oportunidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_oportunidades').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}

function get_amenaza(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_amenazas',
        type: 'POST',
        success: function(data){ 
            $('.txt_amenazas').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}

function get_fortaleza(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_fortalezas',
        type: 'POST',
        success: function(data){ 
            $('.txt_fortaleza').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}

function get_debilidad(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_debilidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_debilidades').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}

function guardar_competidor(){
    var nom=$('#nombrec').val();
    if(nom!=''){
        $('.btn_registro_c').attr('disabled',true);
        $.ajax({
            type: 'POST',
            url: base_url+'Guia_detectar_oportunidades/add_competidor',
            data: {
                nombre:$('#nombrec').val(),
            },
            async: false,
            statusCode: {
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success: function(data) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro editado correctamente',
                    showConfirmButton: false,
                    buttonsStyling: false
                });
                
                setTimeout(function(){ 
                    $('#nombrec').val('')
                    $('.btn_registro_c').attr('disabled',false);
                    get_tabla_guia_oportunidades();
                }, 2000);
            }
        });
    }else{
        Swal.fire("¡Atención!", "El campo esta vacio", "error");
    }
}

function get_tabla_guia_oportunidades(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia').html(data);
        }
    });
}

function eliminar_competidor(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este registro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Guia_detectar_oportunidades/delete_record_competidor',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      get_tabla_guia_oportunidades();
                  }, 1000); 
                }
            });  
            
        },
    });
}
var idreg_c=0;
function edit_competidor(id){
    idreg_c=id;
    var nom=$('.row_c_'+id).data('competidor');
    $('#nombrec').val(nom);
    $('.btn_edit').html('<button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="editar_competidor('+id+')">\
      <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
    </button>');
}

function editar_competidor(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_data_competidor',
        data: {
            nombre:$('#nombrec').val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            setTimeout(function(){ 
                get_tabla_guia_oportunidades();
                $('.btn_edit').html('<button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="guardar_competidor()">\
                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span></button>');
            }, 1000); 
        }
    });
}

function pregunta_competidor(idcompetidor,idpregunta){
    var resp=$('#pre_'+idcompetidor+'_'+idpregunta+' option:selected').val();
    if(idpregunta==1){
        if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='0'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==2){
        if(resp=='0'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==5){
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==6){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==7){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==8){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==11){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==14){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==17){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==19){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==21){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==24){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==25){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==26){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==31){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==32){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }else if(idpregunta==33){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','white');
        }
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta',
        data:{id:idcompetidor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function pregunta_competidor_input(idcompetidor,idpregunta){
    var resp=$('#pre_'+idcompetidor+'_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta',
        data:{id:idcompetidor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function get_tabla_guiafortaleza(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_fortaleza',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_fortaleza').html(data);
        }
    });
}

function pregunta_fortaleza(id){
    var resp=$('#pre_f_'+id+' option:selected').val();
    if(resp==1){
        $('#pre_f_'+id).css('background','#70ad47');
    }else{
        $('#pre_f_'+id).css('background','#e31a1a');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_fortaleza',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function pregunta_fortaleza_input(id){
    var resp=$('#pre_f_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_fortaleza',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}


function get_tabla_guiacliente(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_cliente',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_cliente').html(data);
        }
    });
}


function pregunta_cliente(id){
    var resp=$('#pre_c_'+id+' option:selected').val();
    if(resp==1){
        $('#pre_c_'+id).css('background','#70ad47');
    }else{
        $('#pre_c_'+id).css('background','#e31a1a');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_cliente',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function tabla_guiacliente_instrucciones(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_cliente_instrucciones',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_cliente_instrucciones').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}


function pregunta_cliente_formulario(id){
    var resp=$('#pre_cf_'+id+'').val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_cliente',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}


function adjustTextareaHeight(textarea) {
    $(textarea).css('height', 'auto'); // Resetea la altura
    $(textarea).css('height', textarea.scrollHeight + 'px'); // Ajusta la altura según el contenido
}
function tamaniotextarea(){
    console.log('se ejecuta');
    setTimeout(function(){ 
        $('textarea.js-auto-size').each(function() {
            adjustTextareaHeight(this); // Ajusta la altura inicial si ya hay contenido
            $(this).on('input', function() {
                adjustTextareaHeight(this);
            });
        });
    }, 1000);
    
}