var base_url = $('#base_url').val();
var tipo_m=$('#tipo_m').val();
$(document).ready(function() {
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_competidores',
        type: 'POST',
        success: function(data){ 
            vista_tipo(tipo_m);
        }
    });

    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
            }else{
                event.preventDefault();
                
                $('.btn_registro').attr('disabled',true);
                var datos = $('#form_registro').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Analisisfoda/add_data',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        add_tabla_fortaleza();
                        $('#concepto').val('');
                        setTimeout(function(){ 
                            $('.btn_registro').attr('disabled',false);
                            //get_fortaleza();
                        }, 500);
                    }
                }); 
            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    /**/
    const bsValidationFormso = document.querySelectorAll('#form_registro_o');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationFormso).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
            }else{
                event.preventDefault();
                $('.btn_registro_o').attr('disabled',true);
                var datos = $('#form_registro_o').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Analisisfoda/add_data_o',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        add_tabla_oportunidades();
                        $('#concepto_o').val('');
                        setTimeout(function(){ 
                            $('.btn_registro_o').attr('disabled',false);
                            //add_tabla_oportunidades();
                        }, 500);
                    }
                }); 
            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    /**/
    const bsValidationForms_c = document.querySelectorAll('#form_registro_d');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms_c).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
            }else{
                event.preventDefault();

                $('.btn_registro_d').attr('disabled',true);
                var datos = $('#form_registro_d').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Analisisfoda/add_data_d',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        add_tabla_debilidad();
                        $('#concepto_d').val('');
                        setTimeout(function(){ 
                            $('.btn_registro_d').attr('disabled',false);
                            //get_debilidad();
                        }, 500);
                    }
                }); 
            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    /**/
    const bsValidationFormsa = document.querySelectorAll('#form_registro_a');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationFormsa).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
            }else{
                event.preventDefault();

                $('.btn_registro_a').attr('disabled',true);
                var datos = $('#form_registro_a').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Analisisfoda/add_data_a',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        add_tabla_amenaza();
                        $('#concepto_a').val('');
                        setTimeout(function(){ 
                            $('.btn_registro_a').attr('disabled',false);
                            //get_amenaza();
                        }, 500);
                    }
                }); 
            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    
});


function vista_tipo(tipo){
    $('.link_data').removeClass('active');
    $('.vista_txt').hide(500);
    $('.btn_txt').show(500);
    $('.img_li').removeClass('img_color');
    $('.link_c').removeClass('link_color');
    $('.text_mercado').css('display','none');
    if(tipo==1){
        $('.link1').addClass('active');
        $('.link1_c').addClass('link_color');
        $('.linkimg1').addClass('img_color');
        //$('.vista_txt_1').show(500);
        general(1,1);
        //alert(1);
        //get_oportunidad();
        //get_amenaza();
        //get_tabla_guia_oportunidades();get_tabla_guiamecardo();
        $('.text_mercado1').css('display','block');
    }else if(tipo==2){
        //$('.link2').addClass('active');
        //$('.vista_txt_2').show(500);
        $('.link2_c').addClass('link_color');
        $('.linkimg2').addClass('img_color');
        //general(1,2);
        //get_fortaleza();
        //get_debilidad();
        //get_tabla_guiafortaleza();get_tabla_guiafortalezas();
        $('.text_analisisinterno').css('display','none');
        $('.text_analisisinterno1').css('display','block');
        general(2,3);
        //get_tabla_guiafortaleza();
    }else if(tipo==3){
        //$('.link3').addClass('active');
        //$('.vista_txt_3').show(500);
        $('.link3_c').addClass('link_color');
        $('.linkimg3').addClass('img_color');
        //get_tabla_guiacliente();
        //tabla_guiacliente_instrucciones();
        $('.text_analisis_cliente').css('display','none');
        $('.text_analisis_cliente1').css('display','block');
        general(3,5);
    }else if(tipo==4){  
        //$('.link4').addClass('active');
        //$('.vista_txt_4').show(500);
        $('.link4_c').addClass('link_color');
        $('.linkimg4').addClass('img_color');
        $('.text_analisis_estado').css('display','none');
        $('.text_analisis_estado1').css('display','block');
        get_tabla_fortaleza_pregunta_seleccionestado_resultados();
        //get_tabla_fortaleza_pregunta_seleccionestado_resultados();
    }else if(tipo==5){  
        $('.link5').addClass('active');
        $('.vista_txt_5').show(500);
        get_tabla_fortaleza_pregunta_seleccionresumenestrategias();
    }else{
    }
    setTimeout(function(){ 

    }, 1000);
}

function add_tabla_oportunidades(){
    var concepto_o=$('#concepto_o').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_o+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_oportunidades').append(html);
    get_oportunidad();
}
function get_oportunidad(){
    $.ajax({
        url: base_url+'Analisisfoda/get_oportunidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_oportunidades').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            //tamaniotextarea();
        }
    });
}

function add_tabla_amenaza(){
    var concepto_a=$('#concepto_a').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_a+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_amenazas').append(html);
    get_amenaza();
}
function get_amenaza(){
    $.ajax({
        url: base_url+'Analisisfoda/get_amenazas',
        type: 'POST',
        success: function(data){ 
            $('.txt_amenazas').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            //tamaniotextarea();
        }
    });
}

function add_tabla_fortaleza(){
    var concepto=$('#concepto').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_fortaleza').append(html);
    get_fortaleza()
}


function get_fortaleza(){
    $.ajax({
        url: base_url+'Analisisfoda/get_fortalezas',
        type: 'POST',
        success: function(data){ 
            $('.txt_fortaleza').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}

function add_tabla_debilidad(){
    var concepto_d=$('#concepto_d').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_d+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_debilidad').append(html);
     get_debilidad()
}

function get_debilidad(){
    $.ajax({
        url: base_url+'Analisisfoda/get_debilidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_debilidades').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            tamaniotextarea();
        }
    });
}
function guardar_competidor(){
    var nom=$('#nombrec').val();
    if(nom!=''){
        $('.btn_registro_c').attr('disabled',true);
        $.ajax({
            type: 'POST',
            url: base_url+'Guia_detectar_oportunidades/add_competidor',
            data: {
                nombre:$('#nombrec').val(),
            },
            async: false,
            statusCode: {
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success: function(data) {
                Swal.fire({
                    icon: 'success',
                    title: 'Registro editado correctamente',
                    showConfirmButton: false,
                    buttonsStyling: false
                });
                
                setTimeout(function(){ 
                    $('#nombrec').val('')
                    $('.btn_registro_c').attr('disabled',false);
                    get_tabla_guia_oportunidades();
                }, 2000);
            }
        });
    }else{
        Swal.fire("¡Atención!", "El campo esta vacio", "error");
    }
}
function get_tabla_guia_oportunidades(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia').html(data);
            $('textarea.js-auto-size').textareaAutoSize();

            var button = $('.encabezado');
            var originalOffsetTop = button.offset().top;
            $(window).scroll(function() {
                var scrollTop = $(window).scrollTop();
                console.log(scrollTop + 110);
                if (scrollTop + 79 >= originalOffsetTop) {
                    button.addClass('fixed-button');
                } else {
                    button.removeClass('fixed-button');
                }
            });

            var scrollableDiv = $('.tabla_guia_com');
            var originalOffsetLeft = scrollableDiv.offset().left;

            scrollableDiv.scroll(function() {
                var scrollLeft = scrollableDiv.scrollLeft();
                console.log(scrollLeft); // Muestra el desplazamiento horizontal en la consola
                $("#encabezado").css('margin-left', '-'+scrollLeft+'px');
            });
        }
    });
}
function eliminar_competidor(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este registro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Guia_detectar_oportunidades/delete_record_competidor',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      get_tabla_guia_oportunidades();
                  }, 1000); 
                }
            });  
            
        },
    });
}
var idreg_c=0;
function edit_competidor(id){
    idreg_c=id;
    var nom=$('.row_c_'+id).data('competidor');
    $('#nombrec').val(nom);
    $('.btn_edit').html('<button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="editar_competidor('+id+')">\
      <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
    </button>');
}
function editar_competidor(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_data_competidor',
        data: {
            nombre:$('#nombrec'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            /*Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            setTimeout(function(){ 
                get_tabla_guia_oportunidades();
                $('.btn_edit').html('<button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="guardar_competidor()">\
                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span></button>');
            }, 1000); */
        }
    });
}
function pregunta_competidor(idcompetidor,idpregunta){
    var resp=$('#pre_'+idcompetidor+'_'+idpregunta+' option:selected').val();
    if(idpregunta==1){
        if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='0'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==2){
        if(resp=='0'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==5){
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==6){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==7){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==8){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==11){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==14){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==17){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==19){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==21){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==24){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==25){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==26){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else if(resp=='0'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==29){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==31){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==32){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idpregunta==33){ 
        if(resp=='2'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='1'){
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#e31a1a');
        }else{
            $('#pre_'+idcompetidor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta',
        data:{id:idcompetidor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function pregunta_competidor_input(idcompetidor,idpregunta){
    var resp=$('#pre_'+idcompetidor+'_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta',
        data:{id:idcompetidor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function get_tabla_guiafortaleza(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_fortaleza',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_fortaleza').html(data);
        }
    });
}
function pregunta_fortaleza(id){
    var resp=$('#pre_f_'+id+' option:selected').val();
    if(resp==1){
        $('#pre_f_'+id).css('background','#70ad47');
    }else{
        $('#pre_f_'+id).css('background','#e31a1a');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_fortaleza',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_fortaleza_input(id){
    var resp=$('#pre_f_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_fortaleza',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function get_tabla_guiacliente(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_cliente',
        type: 'POST',
        success: function(data){ 
            console.log(data);
            $('.tabla_guia_cliente').html(data);
        }
    });
}
function pregunta_cliente(id){
    var resp=$('#pre_c_'+id+' option:selected').val();
    if(resp==1){
        $('#pre_c_'+id).css('background','#70ad47');
    }else{
        $('#pre_c_'+id).css('background','#e31a1a');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_cliente',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function tabla_guiacliente_instrucciones(){
    $.ajax({
        url: base_url+'Guia_detectar_oportunidades/get_tabla_guia_cliente_instrucciones',
        type: 'POST',
        success: function(data){ 
            console.log(data);
            $('.tabla_guia_cliente_instrucciones').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
            //tamaniotextarea();
        }
    });
}
function pregunta_cliente_formulario(id){
    var resp=$('#pre_cf_'+id+'').val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_cliente',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function pregunta_cliente_formulario_ckeck(id,v){
    $('.radioimgr'+id).removeClass('radioimgr');
    $('.radioimgr'+id).removeClass('radioimgv');
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_pregunta_cliente',
        data:{id:id,respuesta:v},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function adjustTextareaHeight(textarea) {
    $(textarea).css('height', 'auto'); // Resetea la altura
    $(textarea).css('height', textarea.scrollHeight + 'px'); // Ajusta la altura según el contenido
}
function tamaniotextarea(){
    console.log('se ejecuta');
    setTimeout(function(){ 
        $('textarea.js-auto-size').each(function() {
            adjustTextareaHeight(this); // Ajusta la altura inicial si ya hay contenido
            $(this).on('input', function() {
                adjustTextareaHeight(this);
            });
        });
    }, 1000);  
}
function general(tipo,tiposub){
    $('.txt_oportunidades').html('');
    $('.txt_amenazas').html('');
    $('.tabla_guia').html('');
    $('.tabla_guia_mercado').html('');
    $('.tabla_guia_cliente').html('');
    $('.tabla_guia_cliente_instrucciones').html('');
    $('.tabla_guia_fortaleza').html('');
    $('.tabla_guia_fortalezas').html('');
    $('.tabla_guia_cliente_gsf').html('');
    $('.tabla_guia_estados_resultados').html('');
    $('.tabla_guia_resumen_estrategias').html('');

    if(tiposub==1){
        //var ariaExpanded = $('.generar_1').attr('aria-expanded');
        //console.log('tiposub: '+tiposub+' ariaExpanded:'+ariaExpanded);
        //if(ariaExpanded=='true'){
            get_oportunidad();// $('.txt_oportunidades').html(data);
            get_amenaza();//$('.txt_amenazas').html(data);
            get_tabla_guia_oportunidades();//$('.tabla_guia').html(data);
        //}
    }
    
    if(tiposub==2){
        //var ariaExpanded = $('.generar_2').attr('aria-expanded');
        //console.log('tiposub: '+tiposub+' ariaExpanded:'+ariaExpanded);
        //if(ariaExpanded=='true'){
            get_tabla_guiamecardo();//$('.tabla_guia_mercado').html(data);
            /*setTimeout(function(){
                clonalencabezado(1,'tabla_datos1','clonetr_data1');
                //=====================
                    var button = $('#clonetr_data1');
                        var originalOffsetTop = button.offset().top;
                        $(window).scroll(function() {
                            var scrollTop = $(window).scrollTop();
                            console.log(scrollTop + 110);
                            if (scrollTop + 79 >= originalOffsetTop) {
                                button.addClass('fixed-button');
                            } else {
                                button.removeClass('fixed-button');
                            }
                        });
                //=====================
            }, 1500);*/
        //}
    }
    if(tiposub==3){
        //var ariaExpanded = $('.generar_3').attr('aria-expanded');
        //if(ariaExpanded=='true'){
            get_fortaleza();
            get_debilidad();
            get_tabla_guiafortaleza();

            //get_tabla_guiacliente();//$('.tabla_guia_cliente').html(data);
            //tabla_guiacliente_instrucciones();//$('.tabla_guia_cliente_instrucciones').html(data);
        //}
    }
    if(tiposub==4){
        /*var ariaExpanded = $('.generar_4').attr('aria-expanded');
        if(ariaExpanded=='true'){
            console.log('tiposub: '+tiposub+' ariaExpanded:'+ariaExpanded);*/
            get_tabla_guiafortalezas();//$('.tabla_guia_fortalezas').html(data);
            /*setTimeout(function(){ 
                clonalencabezado(1,'tabla_datos2','clonetr_data2');
                //=====================
                    var button = $('#clonetr_data2');
                        var originalOffsetTop = button.offset().top;
                        $(window).scroll(function() {
                            var scrollTop = $(window).scrollTop();
                            console.log(scrollTop + 110);
                            if (scrollTop + 79 >= originalOffsetTop) {
                                button.addClass('fixed-button');
                            } else {
                                button.removeClass('fixed-button');
                            }
                        });
                //=====================
            }, 1000);*/
        //}
    }
    if(tiposub==5){
        //var ariaExpanded = $('.generar_5').attr('aria-expanded');
        //if(ariaExpanded=='true'){
            //console.log('tiposub: '+tiposub+' ariaExpanded:'+ariaExpanded);
            get_tabla_guiacliente();//$('.tabla_guia_cliente').html(data);
            tabla_guiacliente_instrucciones();//$('.tabla_guia_cliente_instrucciones').html(data);
            
        //}
    }
    if(tiposub==6){
        //var ariaExpanded = $('.generar_6').attr('aria-expanded');
        //if(ariaExpanded=='true'){
            //console.log('tiposub: '+tiposub+' ariaExpanded:'+ariaExpanded);
            get_tabla_fortaleza_pregunta_seleccioncliente();//$('.tabla_guia_cliente_gsf').html(data);
            
        //}
    }
    tamaniotextarea();

}


function pregunta_compe_deta(idpregunta){
    var resp=$('#pre_com_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/edit_compe_detalle',
        data:{idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function siguiente_competidores_ocultar(id){
    $('.text_mercado').css('display','none');
    $('.text_mercado'+id).css('display','block');

    setTimeout(function(){ 
        if(id==3){
            get_oportunidad();
            get_amenaza();
            $('textarea.js-auto-size').textareaAutoSize();
        }else if(id==2){
            general(1,2);
            // validar si exite oportunidades
            validar_oportunidades();
        }else if(id==1){
            vista_tipo(1);
        }
    }, 1000); 
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);      
}

function validar_oportunidades(){
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/validar_oportunidades_existe',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            add_tabla_oportunidades_mercado();
        }
    }); 
}


function siguiente_analisis_ocultar(id){
    $('.text_analisisinterno').css('display','none');
    $('.text_analisisinterno'+id).css('display','block');
    setTimeout(function(){ 
        if(id==1){
            vista_tipo(2);
        }else if(id==2){
            general(2,4);
            // validar si exite fortaleza
            validar_fortaleza();
        }else if(id==3){
            //
            get_fortaleza();
            get_debilidad();
        }
    }, 1000); 
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);   
}

function validar_fortaleza(){
    $.ajax({
        type:'POST',
        url: base_url+'Guia_detectar_oportunidades/validar_fortaleza_existe',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            add_tabla_fortaleza_mejorar();
        }
    }); 
}

function siguiente_analisis_cliente_ocultar(id){
    $('.text_analisis_cliente').css('display','none');
    $('.text_analisis_cliente'+id).css('display','block');
    if(id==2){
        general(3,6);
    }else if(id==1){
        vista_tipo(3);
    }
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);
}

function siguiente_analisis_estado_ocultar(id){
    $('.text_analisis_estado').css('display','none');
    $('.text_analisis_estado'+id).css('display','block');
    if(id==2){
        get_tabla_fortaleza_pregunta_seleccionestado_resultados();
    }else{
        
    }
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);
}


function btn_editar_f(id){
    $('.btn_f_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_f('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
    $('#concepto_f_'+id).attr('disabled',false);

}

function btn_editar_o(id){
    $('.btn_o_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_o('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
    $('#concepto_o_'+id).attr('disabled',false);

}

function btn_editar_d(id){
    $('.btn_d_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_d('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
    $('#concepto_d_'+id).attr('disabled',false);

}

function btn_editar_a(id){
    $('.btn_a_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_a('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
    $('#concepto_a_'+id).attr('disabled',false);

}

function eliminar_registro_fo(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_f',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_fortaleza();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_op(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_o',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_oportunidad();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_de(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_d',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_debilidad();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_am(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_a',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_amenaza();
                    }, 2000);
                }
            });  
            
        },
    });
}

function btn_guardar_f(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_f',
        data: {
            concepto:$('#concepto_f_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            $('.btn_f_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_f_'+id+'" onclick="btn_editar_f('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
            $('#concepto_f_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_o(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_o',
        data: {
            concepto:$('#concepto_o_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            $('.btn_o_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_o_'+id+'" onclick="btn_editar_o('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
            $('#concepto_o_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_d(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_d',
        data: {
            concepto:$('#concepto_d_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            $('.btn_d_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_d_'+id+'" onclick="btn_editar_d('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
            $('#concepto_d_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_a(id){
    $.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_a',
        data: {
            concepto:$('#concepto_a_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            Swal.fire({
                icon: 'success',
                title: 'Registro editado correctamente',
                showConfirmButton: false,
                buttonsStyling: false
            });
            $('.btn_a_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_a_'+id+'" onclick="btn_editar_a('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
            $('#concepto_a_'+id).attr('disabled',true);
        }
    });
}
