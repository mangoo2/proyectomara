var base_url = $('#base_url').val();
var idpro;
var idser_pro;
$(document).ready(function($) {
	$('#adddescript').click(function(event) {
		var form = $('#form_descrit');
		if (form.valid()) {
			console.log('idpro: '+idpro);
			$.ajax({
		        type:'POST',
		        url: base_url+'CostoVentas/adddescript',
		        data: {
		        	idpro:idpro,
		        	idser_pro:idser_pro,
		        	descrit:$('#namepro').val()
		        },
		        statusCode:{
		            404: function(data){
		                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
		            },
		            500: function(){
		                Swal.fire("Error!", "500", "error");
		            }
		        },
		        success:function(data){
		        	form[0].reset();
		        	$('#adddescript').html('Agregar');
		        	$('#tabs_2').click();
		        	viewdescript(idser_pro);

		        }
		    });
		}
	});
});
function obtener_info_tables(tipo,idser,anio,tipot){
	$('.info_table').html('');
	if(tipo==3){
		$('.info_table_'+tipot+'_'+idser).html('<h5 style="text-align: center;">Procesando...</h5>');
	}else{
		$('.info_table_'+tipo+'_'+idser).html('<h5 style="text-align: center;">Procesando...</h5>');
	}
	
	$.ajax({
        type:'POST',
        url: base_url+'CostoVentas/obtener_info_tables',
        data: {
        	tipo:tipo,
        	idser:idser,
        	anio:anio
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        	
        	if(tipo==0){
        		$('.info_table_'+tipo+'_'+idser).html(data);
        		calcular();
        	}
        	if (tipo==2) {
        		$('.info_table_'+tipo+'_'+idser).html(data);
        		calcular2();
        	}
        	if(tipo==3){
        		$('.info_table_'+tipot+'_'+idser).html(data);
        		v_calcular();
        	}
        	
        }
    });
}
function update_v_table0(id,anio,mes){
	var valor = $('#v_'+id).val();
	$.ajax({
        type:'POST',
        url: base_url+'CostoVentas/update_v_table0',
        data: {
        	id:id,
        	valor:valor
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        	calcular();
        }
    });
}
function calcular(){
	//==========promedios===============
		$(".cal_pro").each(function() {
	        var mes = $(this).data('mes');
	        var anio = $(this).data('anio');
	        var datapro = $(this).data('datapro');
	        var valor=$('.c_am_'+anio+'_'+mes).val();
	        if(valor>0 && datapro>0){
	          var porc=parseFloat(valor)/parseFloat(datapro);
	            console.log(porc);
	            $(this).data('vprom',porc);
	            porc=porc*100;
	            porc=porc.toFixed(0)
	          console.log(porc);
	          $(this).html(porc+' %');
	        }else{
	        	$(this).data('vprom',0);
	        	$(this).html('0 %');
	        }
	    });
	//=========================
		totalesgenerales('anio_prev_5');
		totalesgenerales('anio_prev_4');
		totalesgenerales('anio_prev_3');
		totalesgenerales('anio_prev_2');
		totalesgenerales('anio_prev_1');
		//totalesgenerales('anio_actual');
	//=============promedio============
		
		pro_meses(1);
		pro_meses(2);
		pro_meses(3);
		pro_meses(4);
		pro_meses(5);
		pro_meses(6);
		pro_meses(7);
		pro_meses(8);
		pro_meses(9);
		pro_meses(10);
		pro_meses(11);
		pro_meses(12);
	//=========================
	//=========================
}
function pro_meses(mes){
	var pro_meses=[]
	$(".col_m_"+mes).each(function() {
		var valor=$(this).val();
		if(valor==''){
			//pro_meses.push(valor);
		}else{
			pro_meses.push(parseFloat(valor));
		}
		
	});
	if (pro_meses.length === 0) {
	    console.log('El arreglo está vacío, no se puede calcular el promedio.');
	    var promedio=0;
	    var promedio_l=0;
	} else {
	    var suma = pro_meses.reduce((acumulador, valorActual) => acumulador + valorActual, 0);
	    var promedio = suma / pro_meses.length;
	    //console.log(promedio);
	    var promedio_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(promedio.toFixed(0));
	}
	$('.cal_promedio_'+mes).data('prome',promedio).html(promedio_l);
	//=======================================igual se calculara el porcentaje
		var valoranterior=$('.cal_porc_'+mes).data('anioacvalor');
		var porcen=parseFloat(promedio)/parseFloat(valoranterior);
		var porcen_l= parseFloat(porcen)*100;
		$('.cal_porc_'+mes).data('valor',porcen).html(porcen_l.toFixed(0)+' %');
	//====================================================

}
function totalesgenerales(classv){
	var aniov = $('.'+classv).data('anio');
	var valort=0;
	$('.col_a_'+aniov).each(function() {
		var subvalor = $(this).val();
		valort += Number(subvalor);
	});
	$('.tg_'+aniov).data('valor_tg',valort);
	var valort_name=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(valort);
	$('.tg_'+aniov).html(valort_name);

	//===========Promedios generales=========================
		var vapro=0
		$(".prom_"+aniov).each(function() {
	        
	        var datapro = $(this).data('datapro');
	        vapro+=Number(datapro);
	    });
		if(valort>0 && vapro>0){

		    var promg=parseFloat(valort)/parseFloat(vapro);
		    $('.pg_'+aniov).data('valor_pg',promg);
		    promg=promg*100;
	            promg=promg.toFixed(0)
	          $('.pg_'+aniov).html(promg+' %');

		}else{
			$('.pg_'+aniov).data('valor_pg',0);
			$('.pg_'+aniov).html('0 %');
		}
		
	//=============Total promedio generales=======================
		setTimeout(function(){ 
			//=========================================================
				var vaprom=0
				$(".cal_prom").each(function() {
			        
			        var datapro = $(this).data('prome');
			        vaprom+=Number(datapro);
			    });
			    var vaprom_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(vaprom.toFixed(0));
			
				$('.cal_promedio_t').data('valor',vaprom).html(vaprom_l);
				var val_cal_prom_t=$('.cal_promedio_porc').data('valor');
				var cpp=parseFloat(vaprom)/parseFloat(val_cal_prom_t);
					cpp=cpp*100;
					$('.cal_promedio_porc').html(cpp.toFixed()+' %');
			//=========================================================
					var tg_v1=0;
					$(".tg_v1").each(function() {
			        
				        var datav = $(this).data('valor');
				        tg_v1+=Number(datav);
				    });
				    var tg_v1_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tg_v1.toFixed(0));
				    $('.r_tg_v1').data('valor',tg_v1).html(tg_v1_l);

				    var v_r_tg_p1 = $('.r_tg_p1').data('value');
				    var r_tg_p1=parseFloat(tg_v1)/parseFloat(v_r_tg_p1);
				    	r_tg_p1=r_tg_p1.toFixed();
				    	$('.r_tg_p1').html(r_tg_p1+' %');


				//======================================================
				    var tg_v2=0;
					$(".tg_v2").each(function() {
			        
				        var datav = $(this).data('valor');
				        tg_v2+=Number(datav);
				    });
				    var tg_v2_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tg_v2.toFixed(0));
				    $('.r_tg_v2').data('valor',tg_v2).html(tg_v2_l);

				    var v_r_tg_p2 = $('.r_tg_p2').data('value');
				    var r_tg_p2=parseFloat(tg_v2)/parseFloat(v_r_tg_p2);
				    	r_tg_p2=r_tg_p2.toFixed();
				    	$('.r_tg_p2').html(r_tg_p2+' %');
				//======================================================
				    var tg_v3=0;
					$(".tg_v3").each(function() {
			        
				        var datav = $(this).data('valor');
				        tg_v3+=Number(datav);
				    });
				    var tg_v3_l=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tg_v3.toFixed(0));
				    $('.r_tg_v3').data('valor',tg_v3).html(tg_v3_l);


				    var v_r_tg_p3 = $('.r_tg_p3').data('value');
				    var r_tg_p3=parseFloat(tg_v3)/parseFloat(v_r_tg_p3);
				    	r_tg_p3=r_tg_p3.toFixed();
				    	$('.r_tg_p3').html(r_tg_p3+' %');
			//=========================================================

		}, 2000);
		
}
function agregardescrip(idser){
	idser_pro=idser;
	idpro=0;
	$('#descripModal').modal('show');
	viewdescript(idser);
}
function viewdescript(idser){
	$.ajax({
        type:'POST',
        url: base_url+'CostoVentas/viewdescript',
        data: {
        	idser:idser
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        	$('.table_tb_descrip').html(data);
        }
    });
}
function delete_descrit(id){
	
	Swal.fire({
	  title: "Desea confirmar la eliminacion?",
	  text: "¡No podrá revertirse!",
	  icon: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#3085d6",
	  cancelButtonColor: "#d33",
	  confirmButtonText: "Confirmar!"
	}).then((result) => {
	  if (result.isConfirmed) {
	  	/***/
	    	$.ajax({
		        type:'POST',
		        url: base_url+'CostoVentas/delete_descrit',
		        data: {
		        	id:id
		        },
		        statusCode:{
		            404: function(data){
		                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
		            },
		            500: function(){
		                Swal.fire("Error!", "500", "error");
		            }
		        },
		        success:function(data){
		        	viewdescript(idser_pro);
		        	Swal.fire({
				      title: "eliminado!",
				      text: "Registro eliminado.",
				      icon: "success"
				    });
		        }
		    });
	    /***/
	  }
	});
	
}

function edit_descrit(id){
	$('#adddescript').html('Actualizar');
	idpro=id;
	var des = $('.edit_descrit_'+id).data('namepro');

	$('#idpro').val(id);
	$('#namepro').val(des);
}
//================================================================
function update_v_table2(id,anio,mes){
	var valor = $('#vc_'+id).val();
	$.ajax({
        type:'POST',
        url: base_url+'CostoVentas/update_v_table2',
        data: {
        	id:id,
        	valor:valor
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        	calcular2();
        }
    });
}
function calcular2(){
	calcular2_sum_mes(1);
	calcular2_sum_mes(2);
	calcular2_sum_mes(3);
	calcular2_sum_mes(4);
	calcular2_sum_mes(5);
	calcular2_sum_mes(6);
	calcular2_sum_mes(7);
	calcular2_sum_mes(8);
	calcular2_sum_mes(9);
	calcular2_sum_mes(10);
	calcular2_sum_mes(11);
	calcular2_sum_mes(12);


	var totam_1=$('.t_c_v_1').data('valor');
	var totam_2=$('.t_c_v_2').data('valor');
	var totam_3=$('.t_c_v_3').data('valor');
	var totam_4=$('.t_c_v_4').data('valor');
	var totam_5=$('.t_c_v_5').data('valor');
	var totam_6=$('.t_c_v_6').data('valor');
	var totam_7=$('.t_c_v_7').data('valor');
	var totam_8=$('.t_c_v_8').data('valor');
	var totam_9=$('.t_c_v_9').data('valor');
	var totam_10=$('.t_c_v_10').data('valor');
	var totam_11=$('.t_c_v_11').data('valor');
	var totam_12=$('.t_c_v_12').data('valor');
	var t_c_v=0;
	$(".t_c_v").each(function() {
		var valor = $(this).data('valor');
		t_c_v += Number(valor);
	});

	var totalesgenerales=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(t_c_v);
	$('.totalesgenerales').html(totalesgenerales);

}
function calcular2_sum_mes(mes){
	console.log('calcular2_sum_mes');
	var vc_m_v=0;
	$(".col_m_"+mes).each(function() {
		var valor = $(this).val();
		vc_m_v += Number(valor);
	});

	var valort_tcv1=new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(vc_m_v);
	$('.t_c_v_'+mes).html(valort_tcv1);
	$('.t_c_v_'+mes).data('valor',vc_m_v);

	//===========promedio=====================
	var total_prom=0;
	$('.col_m_'+mes).each(function() {
		var anio = $(this).data('anio');
		var mes = $(this).data('mes');
		var idpro = $(this).data('idpro');

		var valor = $(this).val();
		 if(valor>0){}else{valor=0;}
		if(valor>0){
		 	var promedio=parseFloat(valor)/parseFloat(vc_m_v);
		 	total_prom+=Number(promedio);
		 	$('.promc_'+anio+'_'+mes+'_'+idpro).data('prom',promedio);
		 		promedio=promedio*100;
		 		promedio=promedio.toFixed(1);
		 		$('.promc_'+anio+'_'+mes+'_'+idpro).html(promedio+' %');

		 	
		}else{
			$('.promc_'+anio+'_'+mes+'_'+idpro).data('prom',0);
			$('.promc_'+anio+'_'+mes+'_'+idpro).html('0 %');
		}
	});
	if(total_prom>0){
		$('.t_c_prom_'+mes).data('prom',total_prom);
		total_prom=total_prom*100;
		total_prom=total_prom.toFixed(1);
		$('.t_c_prom_'+mes).html(total_prom+' %');
	}else{
		$('.t_c_prom_'+mes).data('prom',0);
		$('.t_c_prom_'+mes).html('0 %');
	}
	if(vc_m_v>0){
		var valortotalanterior=$('.vr_t_'+mes).data('valor');
		var porc_co_ve=parseFloat(vc_m_v)/parseFloat(valortotalanterior);
		$('.co_ve_'+mes).data('valor',porc_co_ve);
		porc_co_ve=porc_co_ve*100;
		porc_co_ve=porc_co_ve.toFixed(2);
		$('.co_ve_'+mes).html(porc_co_ve+' %');
	}else{
		$('.co_ve_'+mes).data('valor',0);
		$('.co_ve_'+mes).html('0 %');
	}
	
}
//================================================================
function update_v_table3(id,anio,mes,idpro){
	calcular3(mes);
	calculartotales3();

	var porcentaje = $('#v_'+id).val();

	var valor = $('.v_'+mes+'_'+idpro).data('value');

	if(porcentaje>=0 && porcentaje<=100){
		$.ajax({
	        type:'POST',
	        url: base_url+'CostoVentas/update_v_table3',
	        data: {
	        	id:id,
	        	porcentaje:porcentaje,
	        	valor:valor
	        },
	        statusCode:{
	            404: function(data){
	                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	            },
	            500: function(){
	                Swal.fire("Error!", "500", "error");
	            }
	        },
	        success:function(data){
	        	
	        }
	    });
	}else{
		Swal.fire("Error!", "Fuera de rango solo se admite de 0 a 100!", "error");
	}
}
function v_calcular(){
	calcular3(1);
	calcular3(2);
	calcular3(3);
	calcular3(4);
	calcular3(5);
	calcular3(6);
	calcular3(7);
	calcular3(8);
	calcular3(9);
	calcular3(10);
	calcular3(11);
	calcular3(12);
	calculartotales3();
		
}
function calcular3(mes){
	var val_proy = $('.ven_proy_'+mes).data('value');
	//var val_proy_temp=val_proy;
	var totalmontos=0;
	var totalporcentaje=0;
	$(".class_idpro_"+mes).each(function() {
		//$(this).css("background-color", "red");
		var idpro = $(this).data('idpro');
		var valor0 =$('.col_m_'+mes+'_idpro_'+idpro).val();
		if(idpro>0){
			val_proy=$('.v_'+mes+'_0').data('value');
		}
		if(valor0>0){
			valor0=parseFloat(valor0)/100;
			var prom_proy = parseFloat(val_proy)*parseFloat(valor0);
				prom_proy=prom_proy.toFixed(2);
				if(idpro>0){
					totalmontos+=Number(prom_proy);
					totalporcentaje+=Number(valor0);
				}
				//val_proy_temp=prom_proy;

			var prom_proy_l = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(prom_proy);
			$('.v_'+mes+'_'+idpro).data('value',prom_proy).html(prom_proy_l);
		}else{
			$('.v_'+mes+'_'+idpro).data('value',0).html('');
		}
	});
		totalmontos=totalmontos.toFixed(0);
	var totalmontos_l = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(totalmontos);
	var totalporcentaje_l=parseFloat(totalporcentaje)*100;
	$('.tot_vent_'+mes).data('value',totalmontos).html(totalmontos_l);
	$('.tot_porc_'+mes).data('value',totalporcentaje).html(totalporcentaje_l);
	//================================
	if(totalporcentaje>1){
		$('.rest_m_'+mes).html('Error total mayoe 100%');
	}else{
		$('.rest_m_'+mes).html('OK');
	}
	/*
	var val_proy = $('.ven_proy_'+mes).data('value');
	var valor0 =$('.col_m_'+mes+'_idpro_0').val();
	if(valor0>0){
		valor0=parseFloat(valor0)/100;
		var prom_proy = parseFloat(val_proy)*parseFloat(valor0);
		var prom_proy_l = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(prom_proy);
		$('.v_'+mes+'_0').data('value',prom_proy).html(prom_proy_l);
	}else{
		$('.v_'+mes+'_0').data('value',0).html('');
	}
	*/
	
}
function calculartotales3(){
	var tot_vent=0;
	$(".tot_vent").each(function() {
		var montox = $(this).data('value');
		console.log('tv: '+montox);
		tot_vent+=Number(montox);
	});
		var tot_vent_l = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tot_vent);
		$('.tot_vent_general').html(tot_vent_l);

	var tot_tp=0;
	$(".c_t_p").each(function() {
		var montox = $(this).data('value');
		//console.log('tp: '+montox);
		tot_tp+=Number(montox);
	});
		var tot_tp_l = new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tot_tp);
		$('.c_t_pt').html(tot_tp_l);
}