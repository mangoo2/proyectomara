var base_url = $('#base_url').val();
var idreg = $('#idreg').val();
var validar_pass=0;
var validar_user=0;
$(document).ready(function() {
    var avatar1 = new KTImageInput('kt_image_1');
    /// funcion de guardado inicio  
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submissionalert(1);
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
            }else{
                event.preventDefault();
                if(validar_pass==0 && validar_user==0){
                    $('.btn_registro').attr('disabled',true);
                    var datos = $('#form_registro').serialize();
                    $.ajax({
                        type:'POST',
                        url: base_url+'Usuarios/add_data',
                        data: datos,
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                            var idr=parseFloat(data);
                            guardar_registro_usuario(idr);
                            add_file(idr);
                            Swal.fire({
                                icon: 'success',
                                title: 'Registro guardado correctamente',
                                showConfirmButton: false,
                                buttonsStyling: false
                            });
                            
                        }
                    }); 
                }

            }
            form.classList.add('was-validated');
          },
          false
        );
    });
});

function guardar_registro_usuario(id){
    $.ajax({
        type:'POST',
        url: base_url+'Usuarios/add_data_usuarios',
        data: {
            personalId:id,
            UsuarioID:$('#UsuarioID').val(),
            perfilId:$('#perfilId option:selected').val(),
            usuario:$('#usuario').val(),
            contrasena:$('#contrasena').val(),
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            setTimeout(function(){ 
                window.location = base_url+'Usuarios';
            }, 2000);
        }
    });
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Usuarios/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                $('.txtusuario').html("El usuario ya existe");
            }else{
                validar_user=0;
                $('.txtusuario').html("");
            }
        }
    });
}

function comfirmar_pass(){
    var contrasena = $('#contrasena').val();
    var contrasena2 = $('#contrasena2').val();
    if(contrasena != "" && contrasena == contrasena2) {
        $('.txtpass').html("");
        validar_pass = 0;
    }else{
        $('.txtpass').html("Las contraseñas no coinciden!");
        validar_pass = 1;
    }
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Usuarios/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
