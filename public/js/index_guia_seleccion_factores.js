var base_url = $('#base_url').val();
$(document).ready(function() {
    
});
/*
function vista_tipo(tipo){
    $('.link_data').removeClass('active');
    $('.vista_txt').hide(500);
    $('.btn_txt').show(500);

    if(tipo==1){
        $('.link1').addClass('active');
        $('.vista_txt_1').show(500);
        get_tabla_guiamecardo();
    }else if(tipo==2){
        $('.link2').addClass('active');
        $('.vista_txt_2').show(500);
        get_tabla_guiafortalezas();
    }else if(tipo==3){
        $('.link3').addClass('active');
        $('.vista_txt_3').show(500);
        get_tabla_fortaleza_pregunta_seleccioncliente();
    }else if(tipo==4){  
        $('.link4').addClass('active');
        $('.vista_txt_4').show(500);
        get_tabla_fortaleza_pregunta_seleccionestado_resultados();
    }else if(tipo==5){  
        $('.link5').addClass('active');
        $('.vista_txt_5').show(500);
        get_tabla_fortaleza_pregunta_seleccionresumenestrategias();
    }else{
    }
}
*/
function get_tabla_guiamecardo(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_mercado',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_mercado').html(data);
            var button = $('.encabezado_m');
            var originalOffsetTop = button.offset().top;
            $(window).scroll(function() {
                var scrollTop = $(window).scrollTop();
                console.log(scrollTop + 110);
                if (scrollTop + 79 >= originalOffsetTop) {
                    button.addClass('fixed-button_m');
                } else {
                    button.removeClass('fixed-button_m');
                }
            });
            
            var scrollableDiv = $('.tabla_mer');
            var originalOffsetLeft = scrollableDiv.offset().left;

            scrollableDiv.scroll(function() {
                var scrollLeft = scrollableDiv.scrollLeft();
                console.log(scrollLeft); // Muestra el desplazamiento horizontal en la consola
                $("#encabezado_m").css('margin-left', '-'+scrollLeft+'px');
            });
            /*const tabla = document.getElementById('encabezado_m');
            const contenedor = document.getElementById('body');

            contenedor.addEventListener('scroll', () => {
              const desplazamientoHorizontal = contenedor.scrollLeft;
              tabla.style.left = desplazamientoHorizontal + 'px';
            });*/
            //$('textarea.js-auto-size').textareaAutoSize();
            /*
            setTimeout(function(){
                clonalencabezado(1,'tabla_datos1','clonetr_data');
                //=====================
                    var button = $('#clonetr_data');
                        var originalOffsetTop = button.offset().top;
                        $(window).scroll(function() {
                            var scrollTop = $(window).scrollTop();
                            console.log(scrollTop + 110);
                            if (scrollTop + 79 >= originalOffsetTop) {
                                button.addClass('fixed-button');
                            } else {
                                button.removeClass('fixed-button');
                            }
                        });
                //=====================
            }, 1500);
            */
        }
    });
}
function pregunta_mercado(idfactor,idpregunta){
    var resp=$('#pre_'+idfactor+'_'+idpregunta+' option:selected').val();
    if(idfactor==1){
        if(resp=='1'){
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='2'){
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idfactor==2){
        if(resp=='1'){
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='2'){
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_'+idfactor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }

    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_pregunta_seleccion',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_mercado_input(idfactor,idpregunta){
    var resp=$('#pre_'+idfactor+'_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_pregunta_seleccion',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_mercado_instrucciones(id,tipo){
    var resp=$('#opo_resp'+tipo+'_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_pregunta_seleccion_instrucciones',
        data:{id:id,respuesta:resp,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });
    /*var resp=$('#pre_cfi_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_pregunta_seleccion_instrucciones',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });*/  
}
function get_tabla_guiafortalezas(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_guia_fortalezas',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_fortalezas').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            /*
            setTimeout(function(){ 
                clonalencabezado(1,'tabla_datos2','clonetr_data2');
                //=====================
                    var button = $('#clonetr_data2');
                        var originalOffsetTop = button.offset().top;
                        $(window).scroll(function() {
                            var scrollTop = $(window).scrollTop();
                            console.log(scrollTop + 110);
                            if (scrollTop + 79 >= originalOffsetTop) {
                                button.addClass('fixed-button');
                            } else {
                                button.removeClass('fixed-button');
                            }
                        });
                //=====================
            }, 1000);
            */
        }
    });
}
function pregunta_factor(idfactor,idpregunta){
    var resp=$('#pre_f_'+idfactor+'_'+idpregunta+' option:selected').val();
    if(idfactor==1){
        if(resp=='1'){
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='2'){
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }else if(idfactor==2){
        if(resp=='1'){
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#70ad47');
        }else if(resp=='2'){
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_f_'+idfactor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }

    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_factor_input(idfactor,idpregunta){
    var resp=$('#pre_f_'+idfactor+'_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_factor_instrucciones(id,tipo){
    var resp=$('#d_resp'+tipo+'_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion_instrucciones',
        data:{id:id,respuesta:resp,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function get_tabla_fortaleza_pregunta_seleccioncliente(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_fortaleza_pregunta_seleccion_cliente',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_cliente_gsf').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
        }
    });
}
function pregunta_fortaleza_pregunta_seleccioncliente(id){
    var resp=$('#pre_sfc_'+id+'').val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion_cliente',
        data:{id:id,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function get_tabla_fortaleza_pregunta_seleccionestado_resultados(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_estado_resultados',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_estados_resultados').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
            setTimeout(function(){ 
                // clonalencabezado(1,'tabla_datos4','clonetr_data4');
                // console.log('clonetr_data4');
                //=====================
                    
                    // var button = $('#clonetr_data4');
                    //     var originalOffsetTop = button.offset().top;
                    //     $(window).scroll(function() {
                    //         var scrollTop = $(window).scrollTop();
                    //         console.log(scrollTop + 110);
                    //         if (scrollTop + 79 >= originalOffsetTop) {
                    //             button.addClass('fixed-button');
                    //         } else {
                    //             button.removeClass('fixed-button');
                    //         }
                    //     });
                //=====================
            }, 1000);
        }
    });
}
function pregunta_seleccionestado_resultados(idfactor,idpregunta){
    var resp=$('#pre_fer_'+idfactor+'_'+idpregunta+' option:selected').val();
    if(idfactor==1){
        if(resp=='1'){
            $('#pre_fer_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else if(resp=='2'){
            $('#pre_fer_'+idfactor+'_'+idpregunta).css('background','#f5bd00');
        }else{
            $('#pre_fer_'+idfactor+'_'+idpregunta).css('background','#a7a7a4');
        }
    }
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_seleccion_estado_resultados',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_seleccionestado_resultados_input(idfactor,idpregunta){
    var resp=$('#pre_fer_'+idfactor+'_'+idpregunta+'').val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_seleccion_estado_resultados',
        data:{id:idfactor,idpregunta:idpregunta,respuesta:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}


function get_tabla_fortaleza_pregunta_seleccionresumenestrategias(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_resumen_estrategias',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_estrategias').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

/*function get_tabla_fortaleza_pregunta_seleccionresumenestrategias(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_resumen_estrategias',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_estrategias').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}
*/

function pregunta_fortaleza_resumen_input(idpregunta){
    var resp1=$('#pre_for_p_'+idpregunta).val();
    var resp2=$('#pre_for_g_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion_resumen',
        data:{idpregunta:idpregunta,respuesta1:resp1,respuesta2:resp2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_mercado_resumen_input(idpregunta){
    var resp1=$('#pre_mer_p_'+idpregunta).val();
    var resp2=$('#pre_mer_g_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_pregunta_seleccion_resumen',
        data:{idpregunta:idpregunta,respuesta1:resp1,respuesta2:resp2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function pregunta_estado_resumen_input(idpregunta){
    var resp1=$('#pre_est_p_'+idpregunta).val();
    var resp2=$('#pre_est_g_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_seleccion_estado_resultados_resumen',
        data:{idpregunta:idpregunta,respuesta1:resp1,respuesta2:resp2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}
function clonalencabezado(tr,tableorigen,tabledestino){
    $('#'+tabledestino).html('');
    console.log('clonar');
    var originalTable = document.getElementById(tableorigen);
    var rowToCopy = originalTable.getElementsByTagName('tr')[tr]; // Cambia el índice según la fila que necesites copiar
    console.log(rowToCopy);
    // Clona la fila
    var clonedRow = rowToCopy.cloneNode(true);

    // Copia los tamaños actuales de cada celda (td y th)
    var originalCells = rowToCopy.children;
    var clonedCells = clonedRow.children;

    for (var i = 0; i < originalCells.length; i++) {
        var rect = originalCells[i].getBoundingClientRect();
        clonedCells[i].style.width = rect.width + 'px';
        clonedCells[i].style.height = rect.height + 'px';

        // Copia otros estilos necesarios
        var style = window.getComputedStyle(originalCells[i]);
        clonedCells[i].style.padding = style.padding;
        clonedCells[i].style.border = style.border;
        clonedCells[i].style.textAlign = style.textAlign;
        clonedCells[i].style.verticalAlign = style.verticalAlign;
    }

    // Selecciona la nueva tabla y agrega la fila clonada
    var newTable = document.getElementById(tabledestino);
    console.log(clonedRow);
    newTable.appendChild(clonedRow);
    //$('#'+tabledestino).html(clonedRow);
    //newTable.innerHTML = clonedRow.outerHTML;
}
function clonalencabezado2(tr,tableorigen,tabledestino){
    // Selecciona la tabla original y la fila a copiar
            var originalTable = document.getElementById(tableorigen);
            var rowToCopy = originalTable.getElementsByTagName('tr')[tr]; // Cambia el índice según la fila que necesites copiar

            // Clona la fila
            var clonedRow = rowToCopy.cloneNode(true);
            /*
            // Copia los tamaños actuales de cada celda
            var originalCells = rowToCopy.getElementsByTagName('td');
            var clonedCells = clonedRow.getElementsByTagName('td');

            for (var i = 0; i < originalCells.length; i++) {
                var rect = originalCells[i].getBoundingClientRect();
                clonedCells[i].style.width = rect.width + 'px';
                clonedCells[i].style.height = rect.height + 'px';
                // Copia otros estilos necesarios
                var style = window.getComputedStyle(originalCells[i]);
                clonedCells[i].style.padding = style.padding;
                clonedCells[i].style.border = style.border;
                clonedCells[i].style.textAlign = style.textAlign;
                clonedCells[i].style.verticalAlign = style.verticalAlign;
            }
            */

            // Selecciona la nueva tabla y agrega la fila clonada
            var newTable = document.getElementById(tabledestino);
            console.log(clonedRow);
            newTable.appendChild(clonedRow);
}

function add_oportunidades(){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/add_data_o_m',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            add_tabla_oportunidades_mercado();
        }
    });
}


function add_tabla_oportunidades_mercado(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_oportunidades_tabla',
        type: 'POST',
        success: function(data){ 
            $('#tbody_opo').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}


function eliminar_registro_ope(id){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/delete_record_o',
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tr_opo_'+id).remove();
        }
    });  
}

function add_fortaleza_mejorar(){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/add_data_f',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            add_tabla_fortaleza_mejorar();
        }
    });
}

function add_tabla_fortaleza_mejorar(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_fortaleza_mejorar_tabla',
        type: 'POST',
        success: function(data){ 
            $('#tbody_deb').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function eliminar_registro_deb(id){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/delete_record_f',
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tr_d_'+id).remove();
        }
    });  
}