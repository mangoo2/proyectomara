var base_url = $('#base_url').val();
var idguiaindicadores_estrategias = $('#idguiaindicadores_estrategiasx').val();
$(document).ready(function() {
    /// funcion de guardado inicio	
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();
	        	$('.btn_registro').attr('disabled',true);
	        	var datos = $('#form_registro').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Cronograma_actividades/add_data',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                	var idr=parseFloat(data);
	                    Swal.fire({
					        icon: 'success',
					        title: 'Registro guardado correctamente',
					        showConfirmButton: false,
					        buttonsStyling: false
					    });
					    setTimeout(function(){ 
                            window.location = base_url+'Cronograma_actividades/registro/'+idr;
                        }, 1000);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
	// fin
	if(idguiaindicadores_estrategias!=0){
        get_estrategias();
        setTimeout(function(){ 
            text_select_pan_trabajo();
        }, 1000);
	}
});

function get_estrategias(){
	$.ajax({
	    type:'POST',
	    url: base_url+'Cronograma_actividades/get_estrategias__all',
	    data:{id:$('#idguiaindicadores option:selected').val(),
	        idguiaindicadores_estrategias:idguiaindicadores_estrategias
	    },
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
            $('.text_estrategias').html(data);
	    }
	});  
}

function text_select_pan_trabajo(){
	var estrategia=$('#idestrategias option:selected').val();
	if(estrategia!=0){
        $('.text_btn').html('<button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>');
        $('.text_plan').css('display','block');
        $('.text_btn_plan').html('<button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">\
                <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>\
              </button>');
	}
}


/*function text_select_pan_trabajo(){
	var estrategia=$('#idestrategias option:selected').val();
	if(estrategia!=0){
		$('.text_plan').css('display','block');
        $('.text_btn_plan').html('<button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">\
                <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>\
              </button>');
	}
}
*/