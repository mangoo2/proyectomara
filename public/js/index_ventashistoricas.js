var base_url = $('#base_url').val();
$(document).ready(function() {
    /**/
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
 
            }else{
                event.preventDefault();
                $('.btn_registro').attr('disabled',true);
                var datos = $('#form_registro').serialize();
                $.ajax({
                    type:'POST',
                    url: base_url+'Ventashistoricas/add_data',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        Swal.fire({
                            icon: 'success',
                            title: 'Registro guardado correctamente',
                            showConfirmButton: false,
                            buttonsStyling: false
                        });
                        setTimeout(function(){ 
                            window.location = base_url+'Ventashistoricas';
                        }, 2000);
                    }
                }); 
            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    /**/
    
});


function vista_tipo(tipo){
    $('.link_data').removeClass('active');
    $('.vista_txt').hide(500);
    $('.btn_txt').show(500);
    $('.pestanas_text').html('');
    $('.servicios_text').html('');
    $('.resumen_text').html('');
    $('.proyeccion_text').html('');
    $('.reportegrafica_text').html('');
    if(tipo==1){
        $('.link1').addClass('active');
        $('.vista_txt_1').show(500);
        get_pestanas();
    }else if(tipo==2){
        $('.link2').addClass('active');
        $('.vista_txt_2').show(500);
        get_ventas_ingresos();
    }else if(tipo==3){
        $('.link3').addClass('active');
        $('.vista_txt_3').show(500);
        get_resumen_presupuesto();
    }else if(tipo==4){
        $('.link4').addClass('active');
        $('.vista_txt_4').show(500);
        get_proyeccion_ventas();
    }else if(tipo==5){
        $('.link5').addClass('active');
        $('.vista_txt_5').show(500);
        get_ventas_ingresos2();
    }else{
    }
}

function get_pestanas(){
    $('.pestanas_text').html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_datos_ventashistoricas',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.pestanas_text').html(data);
        }
    });  
}

function get_ventashistoricasdetalles(id){
    $('.text_tabla_ventas_historicas_detalles').html('');
    $('.text_tabla_ventas_historicas_detalles_'+id).html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_ventas_historicas_detalles',
        data: {id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_tabla_ventas_historicas_detalles_'+id).html(data);
            setTimeout(function(){ 
                calcular_promedio(0);
            }, 2000);
        }
    });  
}

function guardar_registro(){
    $('.btn_registro').attr('disabled',true);
    var DATA  = [];
    var TABLA   = $("#tabla_datos_1 thead > .tr_1");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["mes"] = $(this).find("input[id*='mes_x']").val();
        item ["idventashistoricas"] = $('#idventashistoricas_x').val();
        item ["anio_6"] = $(this).find("input[id*='anio6_x']").val();
        item ["cantidad_6"] = $(this).find("input[id*='cantidad_6x']").val();
        item ["anio_5"] = $(this).find("input[id*='anio5_x']").val();
        item ["cantidad_5"] = $(this).find("input[id*='cantidad_5x']").val();
        item ["anio_4"] = $(this).find("input[id*='anio4_x']").val();
        item ["cantidad_4"] = $(this).find("input[id*='cantidad_4x']").val();
        item ["anio_3"] = $(this).find("input[id*='anio3_x']").val();
        item ["cantidad_3"] = $(this).find("input[id*='cantidad_3x']").val();
        item ["anio_2"] = $(this).find("input[id*='anio2_x']").val();
        item ["cantidad_2"] = $(this).find("input[id*='cantidad_2x']").val();
        item ["anio_1"] = $(this).find("input[id*='anio1_x']").val();
        item ["cantidad_1"] = $(this).find("input[id*='cantidad_1x']").val();
        item ["promedio"] = $(this).find("input[id*='promedio_x']").val();
        DATA.push(item);
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Ventashistoricas/registro_datos_ventashistoricasdetalles',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
            Swal.fire({
                icon: 'success',
                title: 'Registro guardado correctamente',
                showConfirmButton: false,
                buttonsStyling: false,
                timer: 5000,
            });
            setTimeout(function(){ 
                var id = $('#idventashistoricas_x').val();
                get_ventashistoricasdetalles(id);
                $('.btn_registro').attr('disabled',false);
            }, 5000);
        }
    }); 
}

function calcular_promedio(num_x){
    var con6=0;
    var con5=0;
    var con4=0;
    var con3=0;
    var con2=0;
    var con1=0;
    /// cantidad por mes
    var can6_1=0;
    var can5_1=0;
    var can4_1=0;
    var can3_1=0;
    var can2_1=0;
    var can1_1=0;

    var can6_2=0;
    var can5_2=0;
    var can4_2=0;
    var can3_2=0;
    var can2_2=0;
    var can1_2=0;

    var can6_3=0;
    var can5_3=0;
    var can4_3=0;
    var can3_3=0;
    var can2_3=0;
    var can1_3=0;

    var can6_4=0;
    var can5_4=0;
    var can4_4=0;
    var can3_4=0;
    var can2_4=0;
    var can1_4=0;

    var can6_5=0;
    var can5_5=0;
    var can4_5=0;
    var can3_5=0;
    var can2_5=0;
    var can1_5=0;

    var can6_6=0;
    var can5_6=0;
    var can4_6=0;
    var can3_6=0;
    var can2_6=0;
    var can1_6=0;

    var can6_7=0;
    var can5_7=0;
    var can4_7=0;
    var can3_7=0;
    var can2_7=0;
    var can1_7=0;

    var can6_8=0;
    var can5_8=0;
    var can4_8=0;
    var can3_8=0;
    var can2_8=0;
    var can1_8=0;

    var can6_9=0;
    var can5_9=0;
    var can4_9=0;
    var can3_9=0;
    var can2_9=0;
    var can1_9=0;

    var can6_10=0;
    var can5_10=0;
    var can4_10=0;
    var can3_10=0;
    var can2_10=0;
    var can1_10=0;

    var can6_11=0;
    var can5_11=0;
    var can4_11=0;
    var can3_11=0;
    var can2_11=0;
    var can1_11=0;

    var can6_12=0;
    var can5_12=0;
    var can4_12=0;
    var can3_12=0;
    var can2_12=0;
    var can1_12=0;
    
    var pv1=0;
    var pv2=0;
    var pv3=0;
    var pv4=0;
    var pv5=0;
    var pv6=0;
    var pv7=0;
    var pv8=0;
    var pv9=0;
    var pv10=0;
    var pv11=0;
    var pv12=0;


    var TABLA   = $("#tabla_datos_1 thead > .tr_1");
    var cont=1;
    TABLA.each(function(){         
        var c6 = $(this).find("input[id*='cantidad_6x']").val();
        con6+=parseFloat(c6);
        if(cont==1){
            can6_1=parseFloat(c6);
        }
        if(cont==2){
            can6_2=parseFloat(c6);
        }
        if(cont==3){
            can6_3=parseFloat(c6);
        }
        if(cont==4){
            can6_4=parseFloat(c6);
        }
        if(cont==5){
            can6_5=parseFloat(c6);
        }
        if(cont==6){
            can6_6=parseFloat(c6);
        }
        if(cont==7){
            can6_7=parseFloat(c6);
        }
        if(cont==8){
            can6_8=parseFloat(c6);
        }
        if(cont==9){
            can6_9=parseFloat(c6);
        }
        if(cont==10){
            can6_10=parseFloat(c6);
        }
        if(cont==11){
            can6_11=parseFloat(c6);
        }
        if(cont==12){
            can6_12=parseFloat(c6);
        }

        var c5 = $(this).find("input[id*='cantidad_5x']").val();
        con5+=parseFloat(c5);
        if(cont==1){
            can5_1=parseFloat(c5);
        }
        if(cont==2){
            can5_2=parseFloat(c5);
        }
        if(cont==3){
            can5_3=parseFloat(c5);
        }
        if(cont==4){
            can5_4=parseFloat(c5);
        }
        if(cont==5){
            can5_5=parseFloat(c5);
        }
        if(cont==6){
            can5_6=parseFloat(c5);
        }
        if(cont==7){
            can5_7=parseFloat(c5);
        }
        if(cont==8){
            can5_8=parseFloat(c5);
        }
        if(cont==9){
            can5_9=parseFloat(c5);
        }
        if(cont==10){
            can5_10=parseFloat(c5);
        }
        if(cont==11){
            can5_11=parseFloat(c5);
        }
        if(cont==12){
            can5_12=parseFloat(c5);
        }

        var c4 = $(this).find("input[id*='cantidad_4x']").val();
        con4+=parseFloat(c4);
        if(cont==1){
            can4_1=parseFloat(c4);
        }
        if(cont==2){
            can4_2=parseFloat(c4);
        }
        if(cont==3){
            can4_3=parseFloat(c4);
        }
        if(cont==4){
            can4_4=parseFloat(c4);
        }
        if(cont==5){
            can4_5=parseFloat(c4);
        }
        if(cont==6){
            can4_6=parseFloat(c4);
        }
        if(cont==7){
            can4_7=parseFloat(c4);
        }
        if(cont==8){
            can4_8=parseFloat(c4);
        }
        if(cont==9){
            can4_9=parseFloat(c4);
        }
        if(cont==10){
            can4_10=parseFloat(c4);
        }
        if(cont==11){
            can4_11=parseFloat(c4);
        }
        if(cont==12){
            can4_12=parseFloat(c4);
        }

        var c3 = $(this).find("input[id*='cantidad_3x']").val();
        con3+=parseFloat(c3);
        if(cont==1){
            can3_1=parseFloat(c3);
        }
        if(cont==2){
            can3_2=parseFloat(c3);
        }
        if(cont==3){
            can3_3=parseFloat(c3);
        }
        if(cont==4){
            can3_4=parseFloat(c3);
        }
        if(cont==5){
            can3_5=parseFloat(c3);
        }
        if(cont==6){
            can3_6=parseFloat(c3);
        }
        if(cont==7){
            can3_7=parseFloat(c3);
        }
        if(cont==8){
            can3_8=parseFloat(c3);
        }
        if(cont==9){
            can3_9=parseFloat(c3);
        }
        if(cont==10){
            can3_10=parseFloat(c3);
        }
        if(cont==11){
            can3_11=parseFloat(c3);
        }
        if(cont==12){
            can3_12=parseFloat(c3);
        }

        var c2 = $(this).find("input[id*='cantidad_2x']").val();
        con2+=parseFloat(c2);
        if(cont==1){
            can2_1=parseFloat(c2);
        }
        if(cont==2){
            can2_2=parseFloat(c2);
        }
        if(cont==3){
            can2_3=parseFloat(c2);
        }
        if(cont==4){
            can2_4=parseFloat(c2);
        }
        if(cont==5){
            can2_5=parseFloat(c2);
        }
        if(cont==6){
            can2_6=parseFloat(c2);
        }
        if(cont==7){
            can2_7=parseFloat(c2);
        }
        if(cont==8){
            can2_8=parseFloat(c2);
        }
        if(cont==9){
            can2_9=parseFloat(c2);
        }
        if(cont==10){
            can2_10=parseFloat(c2);
        }
        if(cont==11){
            can2_11=parseFloat(c2);
        }
        if(cont==12){
            can2_12=parseFloat(c2);
        }

        var c1 = $(this).find("input[id*='cantidad_1x']").val();
        con1+=parseFloat(c1);
        if(cont==1){
            can1_1=parseFloat(c1);
        }
        if(cont==2){
            can1_2=parseFloat(c1);
        }
        if(cont==3){
            can1_3=parseFloat(c1);
        }
        if(cont==4){
            can1_4=parseFloat(c1);
        }
        if(cont==5){
            can1_5=parseFloat(c1);
        }
        if(cont==6){
            can1_6=parseFloat(c1);
        }
        if(cont==7){
            can1_7=parseFloat(c1);
        }
        if(cont==8){
            can1_8=parseFloat(c1);
        }
        if(cont==9){
            can1_9=parseFloat(c1);
        }
        if(cont==10){
            can1_10=parseFloat(c1);
        }
        if(cont==11){
            can1_11=parseFloat(c1);
        }
        if(cont==12){
            can1_12=parseFloat(c1);
        }

        var array1 = [];
        if(c6!=0){
            array1.push(c6);
        }
        if(c5!=0){
            array1.push(c5);
        }
        if(c4!=0){
            array1.push(c4);
        }
        if(c3!=0){
            array1.push(c3);
        }
        if(c2!=0){
            array1.push(c2);
        }
        if(c1!=0){
            array1.push(c1);
        }

        let sum = 0;
        for (let i = 0; i < array1.length; i++) {
          sum += parseFloat(array1[i]);
        }
        var num=array1.length;
        var pv = sum/num;
        //console.log(pv1);
        $(this).find("input[id*='promedio_x']").val(Math.round(pv));
        $(this).find("span[class*='thpm']").html(Math.round(pv));

        if(cont==1){
            pv1=pv;
        }
        if(cont==2){
            pv2=pv;
        }
        if(cont==3){
            pv3=pv;
        }
        if(cont==4){
            pv4=pv;
        }
        if(cont==5){
            pv5=pv;
        }
        if(cont==6){
            pv6=pv;
        }
        if(cont==7){
            pv7=pv;
        }
        if(cont==8){
            pv8=pv;
        }
        if(cont==9){
            pv9=pv;
        }
        if(cont==10){
            pv10=pv;
        }
        if(cont==11){
            pv11=pv;
        }
        if(cont==12){
            pv12=pv;
        }
        
        //$(this).find("th[class*='f_m']").html(Math.round(f_m)+'%');
        cont++;
        //console.log(cont);
    });
    var con6d = dosDecimales(con6);
    $('.t_col6').html(Math.round(con6d));
    var con5d = dosDecimales(con5);
    $('.t_col5').html(Math.round(con5d));
    var con4d = dosDecimales(con4);
    $('.t_col4').html(Math.round(con4d));
    var con3d = dosDecimales(con3);
    $('.t_col3').html(Math.round(con3d));
    var con2d = dosDecimales(con2);
    $('.t_col2').html(Math.round(con2d));
    var con1d = dosDecimales(con1);
    $('.t_col1').html(Math.round(con1d));

    var TABLAx   = $("#tabla_datos_1 thead > .tr_1");
    var can6_12=0;
    var can5_12=0;
    var can4_12=0;
    var can3_12=0;
    var can2_12=0;
    TABLAx.each(function(){         
        var c6 = $(this).find("input[id*='cantidad_6x']").val();
        can6_12=parseFloat(c6);
        var c5 = $(this).find("input[id*='cantidad_5x']").val();
        can5_12=parseFloat(c5);
        var c4 = $(this).find("input[id*='cantidad_4x']").val();
        can4_12=parseFloat(c4);
        var c3 = $(this).find("input[id*='cantidad_3x']").val();
        can3_12=parseFloat(c3);
        var c2 = $(this).find("input[id*='cantidad_2x']").val();
        can2_12=parseFloat(c2);
    });

    var col5_1m=0;
    if(can6_12!=0){
      var col5_1r=can5_1-can6_12;  
      var col5_1d=col5_1r/can6_12;
      col5_1m=col5_1d*100;
    }

    var col4_1m=0;
    if(can5_12!=0){
      var col4_1r=can4_1-can5_12;  
      var col4_1d=col4_1r/can5_12;
      col4_1m=col4_1d*100;
    }

    var col3_1m=0;
    if(can4_12!=0){
      var col3_1r=can3_1-can4_12;  
      var col3_1d=col3_1r/can4_12;
      col3_1m=col3_1d*100;
    }

    var col2_1m=0;
    if(can3_12!=0){
      var col2_1r=can2_1-can3_12;  
      var col2_1d=col2_1r/can3_12;
      col2_1m=col2_1d*100;
    }

    var col1_1m=0;
    if(can2_12!=0){
      var col1_1r=can1_1-can2_12;  
      var col1_1d=col1_1r/can2_12;
      col1_1m=col1_1d*100;
    }
    
    /*console.log(col5_1m);
    console.log(col4_1m);
    console.log(col3_1m);
    console.log(col2_1m);
    console.log(col1_1m);*/

    $('.col5_1m').html(Math.round(col5_1m)+'%');
    $('.col4_1m').html(Math.round(col4_1m)+'%');
    $('.col3_1m').html(Math.round(col3_1m)+'%');
    $('.col2_1m').html(Math.round(col2_1m)+'%');
    $('.col1_1m').html(Math.round(col1_1m)+'%');
    /// promedio por columna
    //febrero
    var col6_2m=0;
    var col5_2m=0;
    var col4_2m=0;
    var col3_2m=0;
    var col2_2m=0;
    var col1_2m=0;
    if(can6_1!=0){
      var col6_2r=can6_2-can6_1;
      var col6_2d=col6_2r/can6_1;
      col6_2m=col6_2d*100;  
    }

    var col5_2m=0;
    if(can5_1!=0){
      var col5_2r=can5_2-can5_1;
      var col5_2d=col5_2r/can5_1;
      col5_2m=col5_2d*100;
    }

    var col4_2m=0;
    if(can4_1!=0){
      var col4_2r=can4_2-can4_1;
      var col4_2d=col4_2r/can4_1;
      col4_2m=col4_2d*100;
    }

    var col3_2m=0;
    if(can3_1!=0){
      var col3_2r=can3_2-can3_1;
      var col3_2d=col3_2r/can3_1;
      col3_2m=col3_2d*100;
    }

    var col2_2m=0;
    if(can2_1!=0){
      var col2_2r=can2_2-can2_1;
      var col2_2d=col2_2r/can2_1;
      col2_2m=col2_2d*100;
    }

    var col1_2m=0;
    if(can1_1!=0){
      var col1_2r=can1_2-can1_1;
      var col1_2d=col1_2r/can1_1;
      col1_2m=col1_2d*100;
    }
    $('.col6_2m').html(Math.round(col6_2m)+'%');
    $('.col5_2m').html(Math.round(col5_2m)+'%');
    $('.col4_2m').html(Math.round(col4_2m)+'%');
    $('.col3_2m').html(Math.round(col3_2m)+'%');
    $('.col2_2m').html(Math.round(col2_2m)+'%');
    $('.col1_2m').html(Math.round(col1_2m)+'%');

    // marzo
    var col6_3m=0;
    if(can6_2!=0){
      var col6_3r=can6_3-can6_2;
      var col6_3d=col6_3r/can6_2;
      col6_3m=col6_3d*100;  
    }

    col5_3m=0;
    if(can5_2!=0){
      var col5_3r=can5_3-can5_2;
      var col5_3d=col5_3r/can5_2;
      col5_3m=col5_3d*100;
    }

    var col4_3m=0;
    if(can4_2!=0){
      var col4_3r=can4_3-can4_2;
      var col4_3d=col4_3r/can4_2;
      col4_3m=col4_3d*100;
    }

    var col3_3m=0;
    if(can3_2!=0){
      var col3_3r=can3_3-can3_2;
      var col3_3d=col3_3r/can3_2;
      col3_3m=col3_3d*100;
    }

    var col2_3m=0;
    if(can2_2!=0){
      var col2_3r=can2_3-can2_2;
      var col2_3d=col2_3r/can2_2;
      col2_3m=col2_3d*100;
    }

    var col1_3m=0;
    if(can1_2!=0){
      var col1_3r=can1_3-can1_2;
      var col1_3d=col1_3r/can1_2;
      col1_3m=col1_3d*100;
    } 

    $('.col6_3m').html(Math.round(col6_3m)+'%');
    $('.col5_3m').html(Math.round(col5_3m)+'%');
    $('.col4_3m').html(Math.round(col4_3m)+'%');
    $('.col3_3m').html(Math.round(col3_3m)+'%');
    $('.col2_3m').html(Math.round(col2_3m)+'%');
    $('.col1_3m').html(Math.round(col1_3m)+'%');

    // abril
    var col6_4m=0;
    if(can6_3!=0){
      var col6_4r=can6_4-can6_3;
      var col6_4d=col6_4r/can6_3;
      col6_4m=col6_4d*100;
    }

    var col5_4m=0;
    if(can5_3!=0){
      var col5_4r=can5_4-can5_3;
      var col5_4d=col5_4r/can5_3;
      col5_4m=col5_4d*100;
    }

    var col4_4m=0;
    if(can4_3!=0){
      var col4_4r=can4_4-can4_3;
      var col4_4d=col4_4r/can4_3;
      col4_4m=col4_4d*100;
    }

    var col3_4m=0;
    if(can3_3!=0){
      var col3_4r=can3_4-can3_3;
      var col3_4d=col3_4r/can3_3;
      col3_4m=col3_4d*100;
    }

    var col2_4m=0;
    if(can2_3!=0){
      var col2_4r=can2_4-can2_3;
      var col2_4d=col2_4r/can2_3;
      col2_4m=col2_4d*100;
    }

    var col1_4m=0;
    if(can1_3!=0){
      var col1_4r=can1_4-can1_3;
      var col1_4d=col1_4r/can1_3;
      col1_4m=col1_4d*100;
    }
    /*console.log(col6_4m+'ddd');
    if (isNaN(x)) {
        $('.col6_4m').html('0%');
    }else{
        $('.col6_4m').html(Math.round(col6_4m)+'%');
    }*/
    $('.col5_4m').html(Math.round(col5_4m)+'%');
    $('.col4_4m').html(Math.round(col4_4m)+'%');
    $('.col3_4m').html(Math.round(col3_4m)+'%');
    $('.col2_4m').html(Math.round(col2_4m)+'%');
    $('.col1_4m').html(Math.round(col1_4m)+'%');

    /// mayo
    var col6_5m=0;
    if(can6_4!=0){
      var col6_5r=can6_5-can6_4;
      var col6_5d=col6_5r/can6_4;
      col6_5m=col6_5d*100;
    }

    var col5_5m=0;
    if(can5_4!=0){
      var col5_5r=can5_5-can5_4;
      var col5_5d=col5_5r/can5_4;
      col5_5m=col5_5d*100;
    }

    var col4_5m=0;
    if(can4_4!=0){
      var col4_5r=can4_5-can4_4;
      var col4_5d=col4_5r/can4_4;
      col4_5m=col4_5d*100;
    }

    var col3_5m=0;
    if(can3_4!=0){
      var col3_5r=can3_5-can3_4;
      var col3_5d=col3_5r/can3_4;
      col3_5m=col3_5d*100;
    }

    var col2_5m=0;
    if(can2_4!=0){
      var col2_5r=can2_5-can2_4;
      var col2_5d=col2_5r/can2_4;
      col2_5m=col2_5d*100;
    }

    var col1_5m=0;
    if(can1_4!=0){
      var col1_5r=can1_5-can1_4;
      var col1_5d=col1_5r/can1_4;
      col1_5m=col1_5d*100;
    }

    $('.col6_5m').html(Math.round(col6_5m)+'%');
    $('.col5_5m').html(Math.round(col5_5m)+'%');
    $('.col4_5m').html(Math.round(col4_5m)+'%');
    $('.col3_5m').html(Math.round(col3_5m)+'%');
    $('.col2_5m').html(Math.round(col2_5m)+'%');
    $('.col1_5m').html(Math.round(col1_5m)+'%');
    
    /// junio
    var col6_6m=0;
    if(can6_5!=0){
      var col6_6r=can6_6-can6_5;
      var col6_6d=col6_6r/can6_5;
      col6_6m=col6_6d*100;
    }

    var col5_6m=0;
    if(can5_5!=0){
      var col5_6r=can5_6-can5_5;
      var col5_6d=col5_6r/can5_5;
      col5_6m=col5_6d*100;
    }

    var col4_6m=0;
    if(can4_5!=0){
      var col4_6r=can4_6-can4_5;
      var col4_6d=col4_6r/can4_5;
      col4_6m=col4_6d*100;
    }

    var col3_6m=0;
    if(can3_5!=0){
      var col3_6r=can3_6-can3_5;
      var col3_6d=col3_6r/can3_5;
      col3_6m=col3_6d*100;
    }

    var col2_6m=0;
    if(can2_5!=0){
      var col2_6r=can2_6-can2_5;
      var col2_6d=col2_6r/can2_5;
      col2_6m=col2_6d*100;
    }

    var col1_6m=0;
    if(can1_5!=0){
      var col1_6r=can1_6-can1_5;
      var col1_6d=col1_6r/can1_5;
      col1_6m=col1_6d*100;
    }

    $('.col6_6m').html(Math.round(col6_6m)+'%');
    $('.col5_6m').html(Math.round(col5_6m)+'%');
    $('.col4_6m').html(Math.round(col4_6m)+'%');
    $('.col3_6m').html(Math.round(col3_6m)+'%');
    $('.col2_6m').html(Math.round(col2_6m)+'%');
    $('.col1_6m').html(Math.round(col1_6m)+'%');

    /// julio
    var col6_7m=0;
    if(can6_6!=0){
      var col6_7r=can6_7-can6_6;
      var col6_7d=col6_7r/can6_6;
      col6_7m=col6_7d*100;
    }

    var col5_7m=0;
    if(can5_6!=0){
      var col5_7r=can5_7-can5_6;
      var col5_7d=col5_7r/can5_6;
      col5_7m=col5_7d*100;
    }

    var col4_7m=0;
    if(can4_6!=0){
      var col4_7r=can4_7-can4_6;
      var col4_7d=col4_7r/can4_6;
      col4_7m=col4_7d*100;
    }

    var col3_7m=0;
    if(can3_6!=0){
      var col3_7r=can3_7-can3_6;
      var col3_7d=col3_7r/can3_6;
      col3_7m=col3_7d*100;
    }

    var col2_7m=0;
    if(can2_6!=0){
      var col2_7r=can2_7-can2_6;
      var col2_7d=col2_7r/can2_6;
      col2_7m=col2_7d*100;
    }

    var col1_7m=0;
    if(can1_6!=0){
      var col1_7r=can1_7-can1_6;
      var col1_7d=col1_7r/can1_6;
      col1_7m=col1_7d*100;
    }

    $('.col6_7m').html(Math.round(col6_7m)+'%');
    $('.col5_7m').html(Math.round(col5_7m)+'%');
    $('.col4_7m').html(Math.round(col4_7m)+'%');
    $('.col3_7m').html(Math.round(col3_7m)+'%');
    $('.col2_7m').html(Math.round(col2_7m)+'%');
    $('.col1_7m').html(Math.round(col1_7m)+'%');

    /// agosto
    var col6_8m=0;
    if(can6_7!=0){
      var col6_8r=can6_8-can6_7;
      var col6_8d=col6_8r/can6_7;
      col6_8m=col6_8d*100;
    }

    var col5_8m=0;
    if(can5_7!=0){
      var col5_8r=can5_8-can5_7;
      var col5_8d=col5_8r/can5_7;
      col5_8m=col5_8d*100;
    }

    var col4_8m=0;
    if(can4_7!=0){
      var col4_8r=can4_8-can4_7;
      var col4_8d=col4_8r/can4_7;
      col4_8m=col4_8d*100;
    }

    var col3_8m=0;
    if(can3_7!=0){
      var col3_8r=can3_8-can3_7;
      var col3_8d=col3_8r/can3_7;
      col3_8m=col3_8d*100;
    }

    var col2_8m=0;
    if(can2_7!=0){
      var col2_8r=can2_8-can2_7;
      var col2_8d=col2_8r/can2_7;
      col2_8m=col2_8d*100;
    }

    var col1_8m=0;
    if(can1_7!=0){
      var col1_8r=can1_8-can1_7;
      var col1_8d=col1_8r/can1_7;
      col1_8m=col1_8d*100;
    }

    $('.col6_8m').html(Math.round(col6_8m)+'%');
    $('.col5_8m').html(Math.round(col5_8m)+'%');
    $('.col4_8m').html(Math.round(col4_8m)+'%');
    $('.col3_8m').html(Math.round(col3_8m)+'%');
    $('.col2_8m').html(Math.round(col2_8m)+'%');
    $('.col1_8m').html(Math.round(col1_8m)+'%');
    
    /// septiembre
    var col6_9m=0;
    if(can6_8!=0){
      var col6_9r=can6_9-can6_8;
      var col6_9d=col6_9r/can6_8;
      col6_9m=col6_9d*100;
    }

    var col5_9m=0;
    if(can5_8!=0){
      var col5_9r=can5_9-can5_8;
      var col5_9d=col5_9r/can5_8;
      col5_9m=col5_9d*100;
    }

    var col4_9m=0;
    if(can4_8!=0){
      var col4_9r=can4_9-can4_8;
      var col4_9d=col4_9r/can4_8;
      col4_9m=col4_9d*100;
    }

    var col3_9m=0;
    if(can3_8!=0){
      var col3_9r=can3_9-can3_8;
      var col3_9d=col3_9r/can3_8;
      col3_9m=col3_9d*100;
    }

    var col2_9m=0;
    if(can2_8!=0){
      var col2_9r=can2_9-can2_8;
      var col2_9d=col2_9r/can2_8;
      col2_9m=col2_9d*100;
    }

    var col1_9m=0;
    if(can1_8!=0){
      var col1_9r=can1_9-can1_8;
      var col1_9d=col1_9r/can1_8;
      col1_9m=col1_9d*100;
    }

    $('.col6_9m').html(Math.round(col6_9m)+'%');
    $('.col5_9m').html(Math.round(col5_9m)+'%');
    $('.col4_9m').html(Math.round(col4_9m)+'%');
    $('.col3_9m').html(Math.round(col3_9m)+'%');
    $('.col2_9m').html(Math.round(col2_9m)+'%');
    $('.col1_9m').html(Math.round(col1_9m)+'%');
    
    /// octubre
    var col6_10m=0;
    if(can6_9!=0){
      var col6_10r=can6_10-can6_9;
      var col6_10d=col6_10r/can6_9;
      col6_10m=col6_10d*100;
    }

    var col5_10m=0;
    if(can5_9!=0){
      var col5_10r=can5_10-can5_9;
      var col5_10d=col5_10r/can5_9;
      col5_10m=col5_10d*100;
    }

    var col4_10m=0;
    if(can4_9!=0){
      var col4_10r=can4_10-can4_9;
      var col4_10d=col4_10r/can4_9;
      col4_10m=col4_10d*100;
    }

    var col3_10m=0;
    if(can3_9!=0){
      var col3_10r=can3_10-can3_9;
      var col3_10d=col3_10r/can3_9;
      col3_10m=col3_10d*100;
    }

    var col2_10m=0;
    if(can2_9!=0){
      var col2_10r=can2_10-can2_9;
      var col2_10d=col2_10r/can2_9;
      col2_10m=col2_10d*100;
    }

    var col1_10m=0;
    if(can1_9!=0){
      var col1_10r=can1_10-can1_9;
      var col1_10d=col1_10r/can1_9;
      col1_10m=col1_10d*100;
    }

    $('.col6_10m').html(Math.round(col6_10m)+'%');
    $('.col5_10m').html(Math.round(col5_10m)+'%');
    $('.col4_10m').html(Math.round(col4_10m)+'%');
    $('.col3_10m').html(Math.round(col3_10m)+'%');
    $('.col2_10m').html(Math.round(col2_10m)+'%');
    $('.col1_10m').html(Math.round(col1_10m)+'%');

    /// Noviembre
    var col6_11m=0;
    if(can6_10!=0){
      var col6_11r=can6_11-can6_10;
      var col6_11d=col6_11r/can6_10;
      col6_11m=col6_11d*100;
    }

    var col5_11m=0;
    if(can5_10!=0){
      var col5_11r=can5_11-can5_10;
      var col5_11d=col5_11r/can5_10;
      col5_11m=col5_11d*100;
    }

    var col4_11m=0;
    if(can4_10!=0){
      var col4_11r=can4_11-can4_10;
      var col4_11d=col4_11r/can4_10;
      col4_11m=col4_11d*100;
    }

    var col3_11m=0;
    if(can3_10!=0){
      var col3_11r=can3_11-can3_10;
      var col3_11d=col3_11r/can3_10;
      col3_11m=col3_11d*100;
    }

    var col2_11m=0;
    if(can2_10!=0){
      var col2_11r=can2_11-can2_10;
      var col2_11d=col2_11r/can2_10;
      col2_11m=col2_11d*100;
    }

    var col1_11m=0;
    if(can1_10!=0){
      var col1_11r=can1_11-can1_10;
      var col1_11d=col1_11r/can1_10;
      col1_11m=col1_11d*100;
    }

    $('.col6_11m').html(Math.round(col6_11m)+'%');
    $('.col5_11m').html(Math.round(col5_11m)+'%');
    $('.col4_11m').html(Math.round(col4_11m)+'%');
    $('.col3_11m').html(Math.round(col3_11m)+'%');
    $('.col2_11m').html(Math.round(col2_11m)+'%');
    $('.col1_11m').html(Math.round(col1_11m)+'%');

    /// diciembre
    var col6_12m=0;
    if(can6_11!=0){
      var col6_12r=can6_12-can6_11;
      var col6_12d=col6_12r/can6_11;
      col6_12m=col6_12d*100;
    }

    var col5_12m=0;
    if(can5_11!=0){
      var col5_12r=can5_12-can5_11;
      var col5_12d=col5_12r/can5_11;
      col5_12m=col5_12d*100;
    }

    var col4_12m=0;
    if(can4_11!=0){
      var col4_12r=can4_12-can4_11;
      var col4_12d=col4_12r/can4_11;
      col4_12m=col4_12d*100;
    }

    var col3_12m=0;
    if(can3_11!=0){
      var col3_12r=can3_12-can3_11;
      var col3_12d=col3_12r/can3_11;
      col3_12m=col3_12d*100;
    }

    var col2_12m=0;
    if(can2_11!=0){
      var col2_12r=can2_12-can2_11;
      var col2_12d=col2_12r/can2_11;
      col2_12m=col2_12d*100;
    }

    var col1_12m=0;
    if(can1_11!=0){
      var col1_12r=can1_12-can1_11;
      var col1_12d=col1_12r/can1_11;
      col1_12m=col1_12d*100;
    }

    $('.col6_12m').html(Math.round(col6_12m)+'%');
    $('.col5_12m').html(Math.round(col5_12m)+'%');
    $('.col4_12m').html(Math.round(col4_12m)+'%');
    $('.col3_12m').html(Math.round(col3_12m)+'%');
    $('.col2_12m').html(Math.round(col2_12m)+'%');
    $('.col1_12m').html(Math.round(col1_12m)+'%');

    var promh6=[col6_2m,col6_3m,col6_4m,col6_5m,col6_6m,col6_7m,col6_8m,col6_9m,col6_10m,col6_11m,col6_12m];
    let sum6 = 0;
    for (let i = 0; i < promh6.length; i++) {
      sum6 += parseFloat(promh6[i]);
    }
    var num6=promh6.length;
    var mch6 = sum6/num6;

    var promh5=[col5_2m,col5_3m,col5_4m,col5_5m,col5_6m,col5_7m,col5_8m,col5_9m,col5_10m,col5_11m,col5_12m];
    let sum5 = 0;
    for (let i = 0; i < promh5.length; i++) {
      sum5 += parseFloat(promh5[i]);
    }
    var num5=promh5.length;
    var mch5 = sum5/num5;

    var promh4=[col4_2m,col4_3m,col4_4m,col4_5m,col4_6m,col4_7m,col4_8m,col4_9m,col4_10m,col4_11m,col4_12m];
    let sum4 = 0;
    for (let i = 0; i < promh4.length; i++) {
      sum4 += parseFloat(promh4[i]);
    }
    var num4=promh4.length;
    var mch4 = sum4/num4;

    var promh3=[col3_2m,col3_3m,col3_4m,col3_5m,col3_6m,col3_7m,col3_8m,col3_9m,col3_10m,col3_11m,col3_12m];
    let sum3 = 0;
    for (let i = 0; i < promh3.length; i++) {
      sum3 += parseFloat(promh3[i]);
    }
    var num3=promh3.length;
    var mch3 = sum3/num3;

    var promh2=[col2_2m,col2_3m,col2_4m,col2_5m,col2_6m,col2_7m,col2_8m,col2_9m,col2_10m,col2_11m,col2_12m];
    let sum2 = 0;
    for (let i = 0; i < promh2.length; i++) {
      sum2 += parseFloat(promh2[i]);
    }
    var num2=promh2.length;
    var mch2 = sum2/num2;

    var promh1=[col1_2m,col1_3m,col1_4m,col1_5m,col1_6m,col1_7m,col1_8m,col1_9m,col1_10m,col1_11m,col1_12m];
    let sum1 = 0;
    for (let i = 0; i < promh1.length; i++) {
      sum1 += parseFloat(promh1[i]);
    }
    var num1=promh1.length;
    var mch1 = sum1/num6;
    
    console.log(mch6);
    console.log(mch5);
    console.log(mch4);
    console.log(mch3);
    console.log(mch2);
    console.log(mch1);

    $('.mch6').html(Math.round(mch6)+'%');
    $('.mch5').html(Math.round(mch5)+'%');
    $('.mch4').html(Math.round(mch4)+'%');
    $('.mch3').html(Math.round(mch3)+'%');
    $('.mch2').html(Math.round(mch2)+'%');
    $('.mch1').html(Math.round(mch1)+'%');

    var ft5_m=0;
    if(con6d!=0){
      var ft5_r=con5d-con6d;
      var ft5_d=ft5_r/con6d;
      ft5_m=ft5_d*100;
    }

    var ft4_m=0;
    if(con5d!=0){
      var ft4_r=con4d-con5d;
      var ft4_d=ft4_r/con5d;
      ft4_m=ft4_d*100;
    }

    var ft3_m=0;
    if(con4d!=0){
      var ft3_r=con3d-con4d;
      var ft3_d=ft3_r/con4d;
      ft3_m=ft3_d*100;
    }

    var ft2_m=0;
    if(con3d!=0){
      var ft2_r=con2d-con3d;
      var ft2_d=ft2_r/con3d;
      ft2_m=ft2_d*100;
    }

    var ft1_m=0;
    if(con2d!=0){
      var ft1_r=con1d-con2d;
      var ft1_d=ft1_r/con2d;
      ft1_m=ft1_d*100;
    }

    $('.ft5_m').html(Math.round(ft5_m)+'%');
    $('.ft4_m').html(Math.round(ft4_m)+'%');
    $('.ft3_m').html(Math.round(ft3_m)+'%');
    $('.ft2_m').html(Math.round(ft2_m)+'%');
    $('.ft1_m').html(Math.round(ft1_m)+'%');

    var f2_m=0;
    if(pv1!=0){
      var f2_r=pv2-pv1;
      var f2_d=f2_r/pv1;
      f2_m=f2_d*100;
    }

    var f3_m=0;
    if(pv2!=0){
      var f3_r=pv3-pv2;
      var f3_d=f3_r/pv2;
      f3_m=f3_d*100;
    }

    var f4_m=0;
    if(pv3!=0){
      var f4_r=pv4-pv3;
      var f4_d=f4_r/pv3;
      f4_m=f4_d*100;
    }

    var f5_m=0;
    if(pv4!=0){
      var f5_r=pv5-pv4;
      var f5_d=f5_r/pv4;
      f5_m=f5_d*100;
    }

    var f6_m=0;
    if(pv5!=0){
      var f6_r=pv6-pv5;
      var f6_d=f6_r/pv5;
      f6_m=f6_d*100;
    }

    var f7_m=0;
    if(pv6!=0){
      var f7_r=pv7-pv6;
      var f7_d=f7_r/pv6;
      f7_m=f7_d*100;
    }

    var f8_m=0;
    if(pv7!=0){
      var f8_r=pv8-pv7;
      var f8_d=f8_r/pv7;
      f8_m=f8_d*100;
    }

    var f9_m=0;
    if(pv8!=0){
      var f9_r=pv9-pv8;
      var f9_d=f9_r/pv8;
      f9_m=f9_d*100;
    }

    var f10_m=0;
    if(pv9!=0){
      var f10_r=pv10-pv9;
      var f10_d=f10_r/pv9;
      f10_m=f10_d*100;
    }

    var f11_m=0;
    if(pv10!=0){
      var f11_r=pv11-pv10;
      var f11_d=f11_r/pv10;
      f11_m=f11_d*100;
    }

    var f12_m=0;
    if(pv11!=0){
      var f12_r=pv12-pv11;
      var f12_d=f12_r/pv11;
      f12_m=f12_d*100;
    }

    $('.f2_m').html(Math.round(f2_m)+'%');
    $('.f3_m').html(Math.round(f3_m)+'%');
    $('.f4_m').html(Math.round(f4_m)+'%');
    $('.f5_m').html(Math.round(f5_m)+'%');
    $('.f6_m').html(Math.round(f6_m)+'%');
    $('.f7_m').html(Math.round(f7_m)+'%');
    $('.f8_m').html(Math.round(f8_m)+'%');
    $('.f9_m').html(Math.round(f9_m)+'%');
    $('.f10_m').html(Math.round(f10_m)+'%');
    $('.f11_m').html(Math.round(f11_m)+'%');
    $('.f12_m').html(Math.round(f12_m)+'%');

    var prvaf1=[f2_m,f3_m,f4_m,f5_m,f6_m,f7_m,f8_m,f9_m,f10_m,f11_m,f12_m];
    let sumf = 0;
    for (let i = 0; i < prvaf1.length; i++) {
      sumf += parseFloat(prvaf1[i]);
    }
    var numf=prvaf1.length;
    var prvf1 = sumf/numf;
    $('.prvf1').html(Math.round(prvf1)+'%');   

    var suma_pv=parseFloat(pv1)+parseFloat(pv2)+parseFloat(pv3)+parseFloat(pv4)+parseFloat(pv5)+parseFloat(pv6)+parseFloat(pv7)+parseFloat(pv8)+parseFloat(pv9)+parseFloat(pv10)+parseFloat(pv11)+parseFloat(pv12);       
    $('.suma_pv').html(Math.round(suma_pv)); 
    if(num_x!=0){
        add_lote(num_x); 
    }
}

function dosDecimales(n) {
  let t=n.toString();
  let regex=/(\d*.\d{0,2})/;
  return t.match(regex)[0];
}

function eliminar_registro(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este registro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Ventashistoricas/delete_record',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      get_pestanas();
                  }, 1000); 
                }
            });  
            
        },
    });
}

function edit_reg(){
    $('.txt_titulo').hide(500);
    $('.txt_titulo_edit').show(500);
}

function edit_registro(id){
    var nom=$('#nombre_'+id).val();
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/update_nombre',
        data:{id:id,nombre:nom},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.tt_p_'+id).html(nom);
            $('.tt_p_'+id).html(nom);
            $('.txt_titulo').show(500);
            $('.txt_titulo_edit').hide(500);
        }
    });  
}

function add_lote(id){
    var DATAd  = [];
    var TABLAd   = $("#tabla_datos_1 thead > .tr_1");
    contx=1;
    TABLAd.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["mes"] = $(this).find("input[id*='mes_x']").val();
        item ["idventashistoricas"] = $('#idventashistoricas_x').val();
        item ["anio_6"] = $(this).find("input[id*='anio6_x']").val();
        item ["cantidad_6"] = $(this).find("input[id*='cantidad_6x']").val();
        item ["anio_5"] = $(this).find("input[id*='anio5_x']").val();
        item ["cantidad_5"] = $(this).find("input[id*='cantidad_5x']").val();
        item ["anio_4"] = $(this).find("input[id*='anio4_x']").val();
        item ["cantidad_4"] = $(this).find("input[id*='cantidad_4x']").val();
        item ["anio_3"] = $(this).find("input[id*='anio3_x']").val();
        item ["cantidad_3"] = $(this).find("input[id*='cantidad_3x']").val();
        item ["anio_2"] = $(this).find("input[id*='anio2_x']").val();
        item ["cantidad_2"] = $(this).find("input[id*='cantidad_2x']").val();
        item ["anio_1"] = $(this).find("input[id*='anio1_x']").val();
        item ["cantidad_1"] = $(this).find("input[id*='cantidad_1x']").val();
        item ["promedio"] = $(this).find("input[id*='promedio_x']").val();
        if(contx==id){
            console.log(contx+'=='+id);
            DATAd.push(item);
        }
        contx++;
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATAd);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Ventashistoricas/inser_registro_ventashistoricasdetalles',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
            var idx=parseFloat(data);
            console.log(idx);
            $('.id_x'+id).val(idx);
        }
    }); 
}


function get_ventas_ingresos(){
    $('.servicios_text').html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_ventas_ingresos',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.servicios_text').html(data);
            setTimeout(function(){ 
                calcular_promedio(0);
            }, 2000);
        }
    });  
}


function get_resumen_presupuesto(){
    $('.resumen_text').html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_resumen_presupuesto',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.resumen_text').html(data);
            setTimeout(function(){ 
                calcular_promedio(0);
            }, 2000);
        }
    });  
}

function get_ventasresumendetalles(id){
    var idr=$('.id_r_'+id).val();
    var idrf=parseFloat(idr);
    if(idrf==0){
        $.ajax({
            type:'POST',
            url: base_url+'Ventashistoricas/add_data_resumen',
            data: {anio:id},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                var idg=parseFloat(data);
                $('.id_r_'+id).val(idg);   
            }
        }); 
    }
    $('.text_tabla_ventas_resumen_detalles').html('');
    $('.text_tabla_ventas_resumen_detalles_'+id).html('<h5 style="text-align: center;">Procesando...</h5>');
    setTimeout(function(){ 
        $.ajax({
            type:'POST',
            url: base_url+'Ventashistoricas/get_ventas_resumen_detalles',
            data: {anio:id},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                $('.text_tabla_ventas_resumen_detalles_'+id).html(data);
                setTimeout(function(){ 
                    calcular_resumen(0);
                }, 1000);
            }
        });  
    }, 1000);
}


function calcular_resumen(num_x){
    var TABLAx   = $("#tabla_datos_resumen thead > .tr_r");
    var ct1=0;
    var ct2=0;
    var ct3=0;
    var ct4=0;
    var ct5=0;
    var ct6=0;
    var ct7=0;
    var ct8=0;
    var ct9=0;
    var ct10=0;
    var ct11=0;
    var ct12=0;
    TABLAx.each(function(){         
        var cr1 = $(this).find("input[id*='cantidad_1r']").val();
        var cr2 = $(this).find("input[id*='cantidad_2r']").val();
        var cr3 = $(this).find("input[id*='cantidad_3r']").val();
        var cr4 = $(this).find("input[id*='cantidad_4r']").val();
        var cr5 = $(this).find("input[id*='cantidad_5r']").val();
        var cr6 = $(this).find("input[id*='cantidad_6r']").val();
        var cr7 = $(this).find("input[id*='cantidad_7r']").val();
        var cr8 = $(this).find("input[id*='cantidad_8r']").val();
        var cr9 = $(this).find("input[id*='cantidad_9r']").val();
        var cr10 = $(this).find("input[id*='cantidad_10r']").val();
        var cr11 = $(this).find("input[id*='cantidad_11r']").val();
        var cr12 = $(this).find("input[id*='cantidad_12r']").val();
        var crx1=parseFloat(cr1);
        var crx2=parseFloat(cr2);
        var crx3=parseFloat(cr3);
        var crx4=parseFloat(cr4);
        var crx5=parseFloat(cr5);
        var crx6=parseFloat(cr6);
        var crx7=parseFloat(cr7);
        var crx8=parseFloat(cr8);
        var crx9=parseFloat(cr9);
        var crx10=parseFloat(cr10);
        var crx11=parseFloat(cr11);
        var crx12=parseFloat(cr12);
        ct1+=parseFloat(cr1);
        ct2+=parseFloat(cr2);
        ct3+=parseFloat(cr3);
        ct4+=parseFloat(cr4);
        ct5+=parseFloat(cr5);
        ct6+=parseFloat(cr6);
        ct7+=parseFloat(cr7);
        ct8+=parseFloat(cr8);
        ct9+=parseFloat(cr9);
        ct10+=parseFloat(cr10);
        ct11+=parseFloat(cr11);
        ct12+=parseFloat(cr12);

        var suma=crx1+crx2+crx3+crx4+crx5+crx6+crx7+crx8+crx9+crx10+crx11+crx12;
        $(this).find("th[class*='totalanio']").html(Math.round(suma));
    });
    $('.suma_1').html(Math.round(ct1));
    $('.suma_2').html(Math.round(ct2));
    $('.suma_3').html(Math.round(ct3));
    $('.suma_4').html(Math.round(ct4));
    $('.suma_5').html(Math.round(ct5));
    $('.suma_6').html(Math.round(ct6));
    $('.suma_7').html(Math.round(ct7));
    $('.suma_8').html(Math.round(ct8));
    $('.suma_9').html(Math.round(ct9));
    $('.suma_10').html(Math.round(ct10));
    $('.suma_11').html(Math.round(ct11));
    $('.suma_12').html(Math.round(ct12));
    

    TABLAx.each(function(){         
        var cr1 = $(this).find("input[id*='cantidad_1r']").val();
        var cr2 = $(this).find("input[id*='cantidad_2r']").val();
        var cr3 = $(this).find("input[id*='cantidad_3r']").val();
        var cr4 = $(this).find("input[id*='cantidad_4r']").val();
        var cr5 = $(this).find("input[id*='cantidad_5r']").val();
        var cr6 = $(this).find("input[id*='cantidad_6r']").val();
        var cr7 = $(this).find("input[id*='cantidad_7r']").val();
        var cr8 = $(this).find("input[id*='cantidad_8r']").val();
        var cr9 = $(this).find("input[id*='cantidad_9r']").val();
        var cr10 = $(this).find("input[id*='cantidad_10r']").val();
        var cr11 = $(this).find("input[id*='cantidad_11r']").val();
        var cr12 = $(this).find("input[id*='cantidad_12r']").val();
        var crx1=parseFloat(cr1);
        var crx2=parseFloat(cr2);
        var crx3=parseFloat(cr3);
        var crx4=parseFloat(cr4);
        var crx5=parseFloat(cr5);
        var crx6=parseFloat(cr6);
        var crx7=parseFloat(cr7);
        var crx8=parseFloat(cr8);
        var crx9=parseFloat(cr9);
        var crx10=parseFloat(cr10);
        var crx11=parseFloat(cr11);
        var crx12=parseFloat(cr12);
        var div=crx1/parseFloat(ct1);
        var mult=div*100;
        $(this).find("th[class*='pc1']").html(Math.round(mult)+'%');
    });
    if(num_x!=0){
        add_resumen(num_x); 
    }
}

function add_resumen(id){
    var DATAd  = [];
    var TABLAd   = $("#tabla_datos_resumen thead > .tr_r");
    contx=1;
    TABLAd.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_r']").val();
        item ["idventashistoricas_resumen"] = $('#id_ventashistoricas_resumenr').val(); 
        item ["idventashistoricas"] = $(this).find("input[id*='idventashistoricas_r']").val();
        item ["cantidad1"] = $(this).find("input[id*='cantidad_1r']").val();
        item ["cantidad2"] = $(this).find("input[id*='cantidad_2r']").val();
        item ["cantidad3"] = $(this).find("input[id*='cantidad_3r']").val();
        item ["cantidad4"] = $(this).find("input[id*='cantidad_4r']").val();
        item ["cantidad5"] = $(this).find("input[id*='cantidad_5r']").val();
        item ["cantidad6"] = $(this).find("input[id*='cantidad_6r']").val();
        item ["cantidad7"] = $(this).find("input[id*='cantidad_7r']").val();
        item ["cantidad8"] = $(this).find("input[id*='cantidad_8r']").val();
        item ["cantidad9"] = $(this).find("input[id*='cantidad_9r']").val();
        item ["cantidad10"] = $(this).find("input[id*='cantidad_10r']").val();
        item ["cantidad11"] = $(this).find("input[id*='cantidad_11r']").val();
        item ["cantidad12"] = $(this).find("input[id*='cantidad_12r']").val();
        if(contx==id){
            console.log(contx+'=='+id);
            DATAd.push(item);
        }
        contx++;
    });
    INFO  = new FormData();
    aInfo = JSON.stringify(DATAd);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'Ventashistoricas/inser_registro_resumen',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
            var idx=parseFloat(data);
            console.log(idx);
            $('.id_rd'+id).val(idx);
        }
    }); 
}

function get_proyeccion_ventas(){
    $('.proyeccion_text').html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_datos_proyeccion',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.proyeccion_text').html(data);
        }
    });  
}

function get_proyeccion_ventas_detalles(id){
    $('.text_tabla_proyeccion_ventas_detalles').html('');
    $('.text_tabla_proyeccion_ventas_detalles_'+id).html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_proyeccion_ventas_detalles',
        data: {id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_tabla_proyeccion_ventas_detalles_'+id).html(data);
            setTimeout(function(){ 
                calcular_proyeccion1();
            }, 2000);
        }
    });  
}

function calcular_proyeccion1(){
    var tg=$('#tg_1').val();
    var inc_p=$('#inc_p_1').val();

    var inc_b=$('#inc_b_1').val();

    var div=0;
    var sum=0;
    if(inc_p!=0){
        var mult=parseFloat(tg)*inc_p;
        var c_div=mult/100;
        sum=parseFloat(tg)+parseFloat(c_div);
        div=parseFloat(sum)/12;
    }else{
        div=parseFloat(tg)/12;
        sum=parseFloat(tg);
    }
    var sum_m=Math.round(sum);
    var tg_txt=sum_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_a_1').html(tg_txt);
    //$('#tg_1').val(sum_m);
    var div_m=Math.round(div);
    var div_th=div_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    var TABLAx   = $("#tabla_p_1 thead > .tr_pv");
    var sumatg_b=0;
    var sumatg_c=0;
    var tipo = $('#sct1 option:selected').val();
    var cont_1=1;
    TABLAx.each(function(){         
        $(this).find("div[class*='op_a1']").html(div_th);
        $(this).find("input[id*='inp_op_a1']").val(div_m);
        var pr_b1=$(this).find("input[id*='pr_b1_1']").val();
        var mult=parseFloat(pr_b1)*parseFloat(inc_b);
        var div_b=mult/100;
        var suma_b=parseFloat(div_b)+parseFloat(pr_b1);
        var suma_b_m=Math.round(suma_b);
        var suma_b_th=suma_b_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_b1']").html(suma_b_th);
        $(this).find("input[id*='inp_op_b1']").val(suma_b_m);
        sumatg_b+=parseFloat(suma_b_m);
        ////////////////////////
        var inc_c=$(this).find("input[id*='inc_c_1']").val();
        var ca_c=$(this).find("input[id*='ca_c1_1']").val();
        var mult_c=parseFloat(ca_c)*parseFloat(inc_c);
        var div_c=mult_c/100;
        var suma_c=parseFloat(ca_c)+parseFloat(div_c);
        var suma_c_m=Math.round(suma_c);
        var suma_c_th=suma_c_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_c1']").html(suma_c_th);
        $(this).find("input[id*='inp_op_c1']").val(suma_c_m);
        sumatg_c+=parseFloat(suma_c_m);

        if(tipo==1){
            $(this).find("div[class*='op_t1']").html(div_th);
            $(this).find("input[id*='inp_op_t1']").val(div_m);
            $('#can_b'+cont_1+'_2').val(div_m);
            $('.op_c'+cont_1+'_2').html(div_th); 
        }else if(tipo==2){
            $(this).find("div[class*='op_t1']").html(suma_b_th);
            $(this).find("input[id*='inp_op_t1']").val(suma_b_m);
            $('#can_b'+cont_1+'_2').val(suma_b_m);
            $('.op_c'+cont_1+'_2').html(suma_b_th); 
        }else if(tipo==3){
            $(this).find("div[class*='op_t1']").html(suma_c_th);
            $(this).find("input[id*='inp_op_t1']").val(suma_c_m);
            $('#can_b'+cont_1+'_2').val(suma_c_m);
            $('.op_c'+cont_1+'_2').html(suma_c_th); 
        }
        cont_1++;
    });
    var tg_b=sumatg_b.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_b_1').html(tg_b);
    $('#inptg_b_1').val(sumatg_b);
    var tg_c_1=sumatg_c.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_c_1').html(tg_c_1);
    
    if(tipo==1){
        $('.op_tg1').html(tg_txt);
    }else if(tipo==2){
        $('.op_tg1').html(tg_b);
    }else if(tipo==3){
        $('.op_tg1').html(tg_c_1);
    }
    setTimeout(function(){ 
        calcular_proyeccion2();
    }, 1000);
}
// 14 831 327
// 706253.65
function get_ventas_ingresos2(){
    $('.reportegrafica_text').html('<h5 style="text-align: center;">Procesando...</h5>');
    $('.reportegrafica_canvas').html('<canvas id="chart_report" class="chartjs" data-height="500"></canvas>');
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/get_ventas_ingresos',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.reportegrafica_text').html(data);
            $('.anio_aux6').prepend('Serie 1<br>');
            $('.anio_aux5').prepend('Serie 2<br>');
            $('.anio_aux4').prepend('Serie 3<br>');
            $('.anio_aux3').prepend('Serie 4<br>');
            $('.anio_aux2').prepend('Serie 5<br>');
            $('.anio_aux1').prepend('Serie 6<br>');
            setTimeout(function(){ 
                calcular_promedio(0);
                graficar();
            }, 2000);
        }
    });  
}
function graficar(){
    var serie1=$(".anio_aux6").data('anio');
    var data_s_1 = [];
    $(".v_anio_"+serie1).each(function() {
        var vstotal = $(this).val();
        data_s_1.push(vstotal);
    });

    var serie2=$(".anio_aux5").data('anio');
    var data_s_2 = [];
    $(".v_anio_"+serie2).each(function() {
        var vstotal = $(this).val();
        data_s_2.push(vstotal);
    });

    var serie3=$(".anio_aux4").data('anio');
    var data_s_3 = [];
    $(".v_anio_"+serie3).each(function() {
        var vstotal = $(this).val();
        data_s_3.push(vstotal);
    });

    var serie4=$(".anio_aux3").data('anio');
    var data_s_4 = [];
    $(".v_anio_"+serie4).each(function() {
        var vstotal = $(this).val();
        data_s_4.push(vstotal);
    });

    var serie5=$(".anio_aux2").data('anio');
    var data_s_5 = [];
    $(".v_anio_"+serie5).each(function() {
        var vstotal = $(this).val();
        data_s_5.push(vstotal);
    });

    var serie6=$(".anio_aux1").data('anio');
    var data_s_6 = [];
    $(".v_anio_"+serie6).each(function() {
        var vstotal = $(this).val();
        data_s_6.push(vstotal);
    });




    const ctx = document.getElementById('chart_report').getContext('2d');
        const myLineChart = new Chart(ctx, {
            type: 'line', // Tipo de gráfico
            data: {
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], // Eje X
                datasets: [{
                    label: 'Serie 1',
                    data: data_s_1, // Datos de la línea
                    fill: false,
                    borderColor: '#ff6384', // Color de la línea
                    backgroundColor:'#ff6384',
                    tension: 0.1 // Suaviza las curvas de la línea
                },
                {
                    label: 'Serie 2',
                    data: data_s_2, // Datos de la línea
                    fill: false,
                    borderColor: '#36a2eb', // Color de la línea
                    backgroundColor:'#36a2eb',
                    tension: 0.1 // Suaviza las curvas de la línea
                },
                {
                    label: 'Serie 3',
                    data: data_s_3, // Datos de la línea
                    fill: false,
                    borderColor: '#ff9f40', // Color de la línea
                    backgroundColor:'#ff9f40',
                    tension: 0.1 // Suaviza las curvas de la línea
                },
                {
                    label: 'Serie 4',
                    data: data_s_4, // Datos de la línea
                    fill: false,
                    borderColor: '#ffcd56', // Color de la línea
                    backgroundColor:'#ffcd56',
                    tension: 0.1 // Suaviza las curvas de la línea
                },
                {
                    label: 'Serie 5',
                    data: data_s_5, // Datos de la línea
                    fill: false,
                    borderColor: '#9966ff', // Color de la línea
                    backgroundColor:'#9966ff',
                    tension: 0.1 // Suaviza las curvas de la línea
                },
                {
                    label: 'Serie 6',
                    data: data_s_6, // Datos de la línea
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)', // Color de la línea
                    backgroundColor:'rgb(75, 192, 192)',
                    tension: 0.1 // Suaviza las curvas de la línea
                }
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true // Comienza el eje Y en 0
                    }
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(tooltipItem) {
                                let label = tooltipItem.dataset.label || '';
                                if (label) {
                                    label += ': ';
                                }
                                // para modificar el contenido en tooltip y formatear el monto
                                label += new Intl.NumberFormat('es-MX',{style: "currency", currency: "MXN"}).format(tooltipItem.parsed.y);
                                return label;
                            }
                        }
                    }
                }
            }
        });
}


function calcular_proyeccion2(){
    var inc_p_2=$('#inc_p_2').val();
    var inc_b=$('#inc_b_2').val();

    var tg_1=$('#tg_1').val();
    var mult_a=parseFloat(tg_1)*parseFloat(inc_p_2);
    var div_a=mult_a/100;
    var suma_a=parseFloat(div_a)+parseFloat(tg_1);
    var suma_a_m=Math.round(suma_a);
    var suma_a_th=suma_a_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_a_2').html(suma_a_th);
    $('#inptg_a_2').val(suma_a_m);
    var op_a2=parseFloat(suma_a_m)/12;
    var op_a2_a=Math.round(op_a2);
    var op_a2_a2=op_a2_a.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

    var sumatg_b=0;
    var sumatg_c=0;
    var cont_2=1;
    var tipo = $('#sct2 option:selected').val();
    var TABLAx   = $("#tabla_p_2 thead > .tr_pv2");
    TABLAx.each(function(){         
        $(this).find("div[class*='op_a2']").html(op_a2_a2);
        $(this).find("input[id*='inp_op_a2']").val(op_a2_a);
        ///b
        var can_b=$(this).find("input[class*='can_b_2']").val();
        var mult_b=parseFloat(can_b)*parseFloat(inc_b);
        var div_b=mult_b/100;
        var suma_b=parseFloat(div_b)+parseFloat(can_b);
        var suma_b_m=Math.round(suma_b);
        var suma_b_th=suma_b_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_b2']").html(suma_b_th);
        $(this).find("input[id*='inp_op_b2']").val(suma_b_m);
        sumatg_b+=parseFloat(suma_b_m);

        ////////////////////////
        var inc_c=$(this).find("input[id*='inc_c_2']").val();
        var ca_c=$(this).find("input[class*='can_b_2']").val();
        var mult_c=parseFloat(ca_c)*parseFloat(inc_c);
        var div_c=mult_c/100;
        var suma_c=parseFloat(ca_c)+parseFloat(div_c);
        var suma_c_m=Math.round(suma_c);
        var suma_c_th=suma_c_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_c2']").html(suma_c_th);
        $(this).find("input[id*='inp_op_c2']").val(suma_c_m);
        sumatg_c+=parseFloat(suma_c_m);
        if(tipo==1){
            $(this).find("div[class*='op_t2']").html(op_a2_a2);
            $(this).find("input[id*='inp_op_t2']").val(op_a2_a);
            $('#can_b'+cont_2+'_3').val(op_a2);
            $('.op_c'+cont_2+'_3').html(op_a2_a); 
        }else if(tipo==2){
            $(this).find("div[class*='op_t2']").html(suma_b_th);
            $(this).find("input[id*='inp_op_t2']").val(suma_b_m);
            $('#can_b'+cont_2+'_3').val(suma_b_m);
            $('.op_c'+cont_2+'_3').html(suma_b_th); 
        }else if(tipo==3){
            $(this).find("div[class*='op_t2']").html(suma_c_th);
            $(this).find("input[id*='inp_op_t2']").val(suma_c_m);
            $('#can_b'+cont_2+'_3').val(suma_c_m);
            $('.op_c'+cont_2+'_3').html(suma_c_th); 
        }
        cont_2++;
    });
    var tg_b=sumatg_b.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_b_2').html(tg_b);
    $('#inptg_b_2').val(sumatg_b);
    var tg_c_2=sumatg_c.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_c_2').html(tg_c_2);
    $('#inptg_c_2').val(sumatg_c);
    if(tipo==1){
        $('.op_tg2').html(suma_a_th);
    }else if(tipo==2){
        $('.op_tg2').html(tg_b);
    }else if(tipo==3){
        $('.op_tg2').html(tg_c_2);
    }
    var inptg_b_1=$('#inptg_b_1').val();
    var menus_b=parseFloat(sumatg_b)-parseFloat(inptg_b_1);
    var div_tg_b=parseFloat(menus_b)/parseFloat(inptg_b_1);
    var div_tg_bm=div_tg_b*100;
    $('.incp_c_2').html(Math.round(div_tg_bm)+'%');
    setTimeout(function(){ 
        calcular_proyeccion3();
    }, 1000);
}


function calcular_proyeccion3(){
    var inc_p_3=$('#inc_p_3').val();
    var inc_b=$('#inc_b_3').val();

    var tg_1=$('#inptg_a_2').val();
    var mult_a=parseFloat(tg_1)*parseFloat(inc_p_3);
    var div_a=mult_a/100;
    var suma_a=parseFloat(div_a)+parseFloat(tg_1);
    var suma_a_m=Math.round(suma_a);
    var suma_a_th=suma_a_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_a_3').html(suma_a_th);
    $('#inptg_a_3').val(suma_a_m);
    var op_a3=parseFloat(suma_a_m)/12;
    var op_a3_a=Math.round(op_a3);
    var op_a3_a3=op_a3_a.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    console.log(op_a3_a3); 
    var sumatg_b=0;
    var sumatg_c=0;
    var tipo = $('#sct3 option:selected').val();
    var TABLAx   = $("#tabla_p_3 thead > .tr_pv3");
    TABLAx.each(function(){         
        $(this).find("div[class*='op_a3']").html(op_a3_a3);
        $(this).find("input[id*='inp_op_a3']").val(op_a3_a);
        ///b
        var can_b=$(this).find("input[class*='can_b_3']").val();
        var mult_b=parseFloat(can_b)*parseFloat(inc_b);
        var div_b=mult_b/100;
        var suma_b=parseFloat(div_b)+parseFloat(can_b);
        var suma_b_m=Math.round(suma_b);
        var suma_b_th=suma_b_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_b3']").html(suma_b_th);
        $(this).find("input[id*='inp_op_b3']").val(suma_b_m);
        sumatg_b+=parseFloat(suma_b_m);

        ////////////////////////
        var inc_c=$(this).find("input[id*='inc_c_3']").val();
        var ca_c=$(this).find("input[class*='can_b_3']").val();
        var mult_c=parseFloat(ca_c)*parseFloat(inc_c);
        var div_c=mult_c/100;
        var suma_c=parseFloat(ca_c)+parseFloat(div_c);
        var suma_c_m=Math.round(suma_c);
        var suma_c_th=suma_c_m.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $(this).find("div[class*='op_c3']").html(suma_c_th);
        $(this).find("input[id*='inp_op_c3']").val(suma_c_m);
        sumatg_c+=parseFloat(suma_c_m);
        if(tipo==1){
            $(this).find("div[class*='op_t3']").html(op_a3_a3);
            $(this).find("input[id*='inp_op_t3']").val(op_a3_a);
        }else if(tipo==2){
            $(this).find("div[class*='op_t3']").html(suma_b_th);
            $(this).find("input[id*='inp_op_t3']").val(suma_b_m);
        }else if(tipo==3){
            $(this).find("div[class*='op_t3']").html(suma_c_th);
            $(this).find("input[id*='inp_op_t3']").val(suma_c_m);
        }
    });
    var tg_b=sumatg_b.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_b_3').html(tg_b);
    
    var tg_c_3=sumatg_c.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    $('.tg_c_3').html(tg_c_3);

    if(tipo==1){
        $('.op_tg3').html(suma_a_th);
    }else if(tipo==2){
        $('.op_tg3').html(tg_b);
    }else if(tipo==3){
        $('.op_tg3').html(tg_c_3);
    }

    var inptg_b_2=$('#inptg_b_2').val();
    var menus_b=parseFloat(sumatg_c)-parseFloat(inptg_b_2);
    var div_tg_b=parseFloat(menus_b)/parseFloat(inptg_b_2);
    var div_tg_bm=div_tg_b*100;
    $('.incp_c_3').html(Math.round(div_tg_bm)+'%');
}

function add_tablas(){
    add_tablas_proyeccion1();
}

function add_tablas_proyeccion1(){
    $('.btn_registro_proyeccion').attr('disabled',true);
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/add_data_presupuesto',
        data: {
            id:$('#id_vhpv').val(),
            idventashistoricas:$('#idventashistoricas_x').val(),
            opcion:$('#sct1 option:selected').val(),
            porcentaje_a:$('#inc_p_1').val(),
            porcentaje_b:$('#inc_b_1').val(),
            anio:$('#anio_t1').val(),
            num_tabla:1
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var idr=parseFloat(data);
            $('#id_vhpv').val(idr);
            /**/
            var DATA  = [];
            var TABLA   = $("#tabla_p_1 thead > .tr_pv");
            TABLA.each(function(){         
                item = {};
                item ["idventashistoricas_proyecion_venta"] = idr;
                item ["idventashistoricas"] = $('#idventashistoricas_x').val();
                item ["id"] = $(this).find("input[id*='id_pv_1']").val();
                item ["mes"] = $(this).find("input[id*='mes_pv']").val();
                item ["opcion_a"] = $(this).find("input[id*='inp_op_a1']").val();
                item ["opcion_b"] = $(this).find("input[id*='inp_op_b1']").val();
                item ["opcion_c"] = $(this).find("input[id*='inp_op_c1']").val();
                item ["opcion_porcentaje_c"] = $(this).find("input[id*='inc_c_1']").val();
                item ["opcion_seleccionada"] = $(this).find("input[id*='inp_op_t1']").val();
                item ["num_tabla"] = 1;
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Ventashistoricas/registro_datos_proyecion_venta_detalles',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                    },
                    500: function(){
                    }
                },
                success: function(data){
                    Swal.fire({
                        icon: 'success',
                        title: 'Guardando por favor espere',
                        showConfirmButton: false,
                        buttonsStyling: false,
                        timer: 5000,
                    });
                    /*Swal.fire({
                        icon: 'success',
                        title: 'Registro guardado correctamente',
                        showConfirmButton: false,
                        buttonsStyling: false,
                        timer: 5000,
                    });
                    setTimeout(function(){ 
                        var id = $('#idventashistoricas_x').val();
                        get_proyeccion_ventas_detalles(id);
                        $('.btn_registro_proyeccion').attr('disabled',false);
                    }, 5000);*/
                    add_tablas_proyeccion2();
                }
            });
            /**/
        }
    }); 
    
}

function add_tablas_proyeccion2(){
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/add_data_presupuesto',
        data: {
            id:$('#id_vhpv2').val(),
            idventashistoricas:$('#idventashistoricas_x').val(),
            opcion:$('#sct2 option:selected').val(),
            porcentaje_a:$('#inc_p_2').val(),
            porcentaje_b:$('#inc_b_2').val(),
            anio:$('#anio_t2').val(),
            num_tabla:2
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var idr=parseFloat(data);
            $('#id_vhpv2').val(idr);
            /**/
            var DATA  = [];
            var TABLA   = $("#tabla_p_2 thead > .tr_pv2");
            TABLA.each(function(){         
                item = {};
                item ["idventashistoricas_proyecion_venta"] = idr;
                item ["idventashistoricas"] = $('#idventashistoricas_x').val();
                item ["id"] = $(this).find("input[id*='id_pv_2']").val();
                item ["mes"] = $(this).find("input[id*='mes_pv2']").val();
                item ["opcion_a"] = $(this).find("input[id*='inp_op_a2']").val();
                item ["opcion_b"] = $(this).find("input[id*='inp_op_b2']").val();
                item ["opcion_c"] = $(this).find("input[id*='inp_op_c2']").val();
                item ["opcion_porcentaje_c"] = $(this).find("input[id*='inc_c_2']").val();
                item ["opcion_seleccionada"] = $(this).find("input[id*='inp_op_t2']").val();
                item ["num_tabla"] = 2;
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Ventashistoricas/registro_datos_proyecion_venta_detalles',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                    },
                    500: function(){
                    }
                },
                success: function(data){
                    add_tablas_proyeccion3();
                }
            });
            /**/
        }
    }); 
    
}

function add_tablas_proyeccion3(){
    $.ajax({
        type:'POST',
        url: base_url+'Ventashistoricas/add_data_presupuesto',
        data: {
            id:$('#id_vhpv3').val(),
            idventashistoricas:$('#idventashistoricas_x').val(),
            opcion:$('#sct3 option:selected').val(),
            porcentaje_a:$('#inc_p_3').val(),
            porcentaje_b:$('#inc_b_3').val(),
            anio:$('#anio_t3').val(),
            num_tabla:3
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            var idr=parseFloat(data);
            $('#id_vhpv2').val(idr);
            /**/
            var DATA  = [];
            var TABLA   = $("#tabla_p_3 thead > .tr_pv3");
            TABLA.each(function(){         
                item = {};
                item ["idventashistoricas_proyecion_venta"] = idr;
                item ["idventashistoricas"] = $('#idventashistoricas_x').val();
                item ["id"] = $(this).find("input[id*='id_pv_3']").val();
                item ["mes"] = $(this).find("input[id*='mes_pv3']").val();
                item ["opcion_a"] = $(this).find("input[id*='inp_op_a3']").val();
                item ["opcion_b"] = $(this).find("input[id*='inp_op_b3']").val();
                item ["opcion_c"] = $(this).find("input[id*='inp_op_c3']").val();
                item ["opcion_porcentaje_c"] = $(this).find("input[id*='inc_c_3']").val();
                item ["opcion_seleccionada"] = $(this).find("input[id*='inp_op_t3']").val();
                item ["num_tabla"] = 3;
                DATA.push(item);
            });
            INFO  = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url : base_url+'Ventashistoricas/registro_datos_proyecion_venta_detalles',
                processData: false, 
                contentType: false,
                async: false,
                statusCode:{
                    404: function(data){
                    },
                    500: function(){
                    }
                },
                success: function(data){
                    Swal.fire({
                        icon: 'success',
                        title: 'Registro guardado correctamente',
                        showConfirmButton: false,
                        buttonsStyling: false,
                        timer: 5000,
                    });
                    setTimeout(function(){ 
                        var id = $('#idventashistoricas_x').val();
                        get_proyeccion_ventas_detalles(id);
                        $('.btn_registro_proyeccion').attr('disabled',false);
                    }, 5000);
                }
            });
            /**/
        }
    }); 
}