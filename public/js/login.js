var base_url = $('#base_url').val();
$(document).ready(function() {
    $("#password").keypress(function(e) {
        if (e.which==13) {
            ingresar();
        }
    });
    $("#usuario").focus();
});

function ingresar(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Login/login",
        data: $('#sign_in').serialize(), 
        beforeSend: function(){

        },
        success: function (result) {
            console.log(result);
            if(result==1){ 
                $('.alerta_c').show(700);
                setTimeout(function(){window.location.href = base_url+"Sistema"},3500);
            }else{
                $('.txtusuario').html('Usuario incorrecto');
                $('.txtpass').html('Contraseña incorrecta');
                $('#usuario').css('border','solid red');
                $('.tusu').css('color','red');
                $('#password').css('border','solid red');
                $('.tpass').css('color','red');
            }
        }
    });
}

