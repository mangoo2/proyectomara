var base_url = $('#base_url').val();
$(document).ready(function() {
    get_macroeconomicos();
});

function get_macroeconomicos(){
    $('.text_macroeconomico').html('<h5 style="text-align: center;">Procesando...</h5>');
    $.ajax({
        type:'POST',
        url: base_url+'DatosMacroeconomicos/get_datos_macroeconomicos',
        data: {anio:$('#anio option:selected').val()},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.text_macroeconomico').html(data);
        }
    });  
}

function guardar_registro(){
	$('.btn_registro').attr('disabled',true);
    var DATA  = [];
    var TABLA   = $("#tabla_datos_1 thead > .tr_1");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["orden"] = $(this).find("input[id*='orden_x']").val();
        item ["tipo"] = $(this).find("input[id*='tipo_x']").val();
        item ["anio"] = $(this).find("input[id*='anio_x']").val();
        item ["proyeccion"] = $(this).find("input[id*='proyeccion_x']").val();
        item ["concepto_anio"] = $(this).find("input[id*='concepto_anio_x']").val();
        item ["concepto_proyeccion"] = $(this).find("input[id*='concepto_proyeccion_x']").val();
        item ["fuente"] = $(this).find("input[id*='fuente_x']").val();
        item ["pagina"] = $(this).find("input[id*='pagina_x']").val();
        DATA.push(item);
    });

    var TABLA   = $("#tabla_datos_2 thead > .tr_2");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["orden"] = $(this).find("input[id*='orden_x']").val();
        item ["tipo"] = $(this).find("input[id*='tipo_x']").val();
        item ["anio"] = $(this).find("input[id*='anio_x']").val();
        item ["proyeccion"] = $(this).find("input[id*='proyeccion_x']").val();
        item ["concepto_anio"] = $(this).find("input[id*='concepto_anio_x']").val();
        item ["concepto_proyeccion"] = $(this).find("input[id*='concepto_proyeccion_x']").val();
        item ["fuente"] = $(this).find("input[id*='fuente_x']").val();
        item ["pagina"] = $(this).find("input[id*='pagina_x']").val();
        DATA.push(item);
    });
    
    var TABLA   = $("#tabla_datos_3 thead > .tr_3");
    TABLA.each(function(){         
        item = {};
        item ["id"] = $(this).find("input[id*='id_x']").val();
        item ["orden"] = $(this).find("input[id*='orden_x']").val();
        item ["tipo"] = $(this).find("input[id*='tipo_x']").val();
        item ["anio"] = $(this).find("input[id*='anio_x']").val();
        item ["proyeccion"] = $(this).find("input[id*='proyeccion_x']").val();
        item ["concepto_anio"] = $(this).find("input[id*='concepto_anio_x']").val();
        item ["concepto_proyeccion"] = $(this).find("input[id*='concepto_proyeccion_x']").val();
        item ["fuente"] = $(this).find("input[id*='fuente_x']").val();
        item ["pagina"] = $(this).find("input[id*='pagina_x']").val();
        DATA.push(item);
    });

    INFO  = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);
    $.ajax({
        data: INFO,
        type: 'POST',
        url : base_url+'DatosMacroeconomicos/registro_datos_macroeconomicos',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data){
            },
            500: function(){
            }
        },
        success: function(data){
        	Swal.fire({
                icon: 'success',
                title: 'Registro guardado correctamente',
                showConfirmButton: false,
                buttonsStyling: false,
                timer: 5000,
            });
        	setTimeout(function(){ 
        		$('.btn_registro').attr('disabled',false);
                get_macroeconomicos();
            }, 5000);
        }
    }); 
}