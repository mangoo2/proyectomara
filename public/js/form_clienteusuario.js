var base_url = $('#base_url').val();
var idreg = $('#idreg').val();
var validar_pass=0;
var validar_user=0;
$(document).ready(function() {
    var avatar1 = new KTImageInput('kt_image_1');
    $('.vista_txt_5').hide(10);
    texto_fecha();
    /// funcion de guardado inicio  
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
        form.addEventListener(
          'submit',
          function (event) {
            if (!form.checkValidity()) {
              event.preventDefault();
              event.stopPropagation();
 
            }else{
                event.preventDefault();
                if(validar_pass==0 && validar_user==0){
                    $('.btn_registro').attr('disabled',true);
                    var datos = $('#form_registro').serialize();
                    $.ajax({
                        type:'POST',
                        url: base_url+'UsuarioCliente/add_data',
                        data: datos,
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                            var idr=parseFloat(data);
                            guardar_registro_usuario(idr);
                            add_file(idr);
                            Swal.fire({
                                icon: 'success',
                                title: 'Registro guardado correctamente',
                                showConfirmButton: false,
                                buttonsStyling: false
                            });
                            setTimeout(function(){ 
                                window.location = base_url+'UsuarioCliente';
                            }, 3000);
                        }
                    }); 
                }

            }

            form.classList.add('was-validated');
          },
          false
        );
    });
    // fin
    fecha_picker();
    if(idreg!=0){
        var vrf=$('#regimen_fiscalx').val();
        $("#regimen_fiscal option[value='"+vrf+"']").attr("selected", true);
        setTimeout(function(){ 
            v_rf();
        }, 2000);
    }
});


function vista_tipo(tipo){
	$('.link_data').removeClass('active');
	$('.vista_txt').hide(500);
    $('.btn_txt').show(500);
	if(tipo==1){
        $('.link1').addClass('active');
        $('.vista_txt_1').show(500);
	}else if(tipo==2){
        $('.link2').addClass('active');
        $('.vista_txt_2').show(500);
	}else if(tipo==3){
        $('.link3').addClass('active');
        $('.vista_txt_3').show(500);
	}else if(tipo==4){ 	
        $('.link4').addClass('active');
        $('.vista_txt_4').show(500);
    }else if(tipo==5){ 	
        $('.link5').addClass('active');
        $('.vista_txt_5').show(500);
	}else{
	}
}

function texto_fecha(){
    var fecha = $('#dia').val(); 
    var d = new Date(fecha);
    d.setDate(d.getDate() + 1);
    $('.fecha_tex').html(textoFecha(d));
}
function textoFecha(fecha){
    //var fecha = $('#dia').val(); 
    var numDiaSem = fecha.getDay(); //getDay() devuelve el dia de la semana.(0-6).
    //Creamos un Array para los nombres de los días    
    var diasSemana = new Array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    var diaLetras = diasSemana[fecha.getDay()];   //El día de la semana en letras. getDay() devuelve el dia de la semana.(0-6).
    //Otro Array para los nombres de los meses    
    var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    var mesLetras = meses[fecha.getMonth()];  //El mes en letras
    var diaMes = (fecha.getDate());   //getDate() devuelve el dia(1-31).
    var anho = fecha.getFullYear();  //getFullYear() devuelve el año(4 dígitos).
    var hora = fecha.getHours();    //getHours() devuelve la hora(0-23).
    var min = fecha.getMinutes();   //getMinutes() devuelve los minutos(0-59).
    if ((min >= 0) && (min < 10)) {    //Algoritmo para añadir un cero cuando el min tiene 1 cifra.
      min = "0" + min;
    }
    //var devolver = diaLetras+ ", " + diaMes + " de " + mesLetras + " de " + anho;
    var devolver = 'Ingresado desde'+ " " + diaMes + " de " + mesLetras + " del " + anho;
    return devolver;
}

function guardar_registro_usuario(id){
    $.ajax({
        type:'POST',
        url: base_url+'UsuarioCliente/add_usuarios',
        data: {
            UsuarioID:$('#UsuarioID').val(),
            usuario:$('#usuario').val(),
            contrasena:$('#contrasena').val(),
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==1){
                setTimeout(function(){ 
                    window.location.href = base_url+"index.php/Login/logout";
                }, 1000);
            }
        }
    });
}

function fecha_picker(){
    const datepickerList = document.querySelectorAll('.dob-picker');
    datepickerList.forEach(function (datepicker) {
      datepicker.flatpickr({
        monthSelectorType: 'static'
      });
    });
}

function v_rf(){
    $('.pararf').prop( "disabled", true );
    var rf=$('#regimen_fiscal option:selected').val();
    $( "#cfdi ."+rf ).prop( "disabled", false )
}

function cambiaCP(id){
    var cp = $('#codigo_postal').val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'UsuarioCliente/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array.length>0) {
                    $("#estado").val(array[0].estado);
                    $("#ciudad").val(array[0].ciudad);
                    cambia_cp_colonia(id);
                }    
                
            }
        });

    }
}

function cambia_cp_colonia(id){
    var cp = $('#codigo_postal').val();  
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'UsuarioCliente/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'UsuarioCliente/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 

                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                            $('#colonia').prepend('<option value="Ninguna">Ninguna</option>');

                        }
                     });
                }
            }
        });
    }else{
        $("#estado").attr("readonly",false);
    }
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'UsuarioCliente/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                $('.txtusuario').html("El usuario ya existe");
            }else{
                validar_user=0;
                $('.txtusuario').html("");
            }
        }
    });
}

function comfirmar_pass(){
    var contrasena = $('#contrasena').val();
    var contrasena2 = $('#contrasena2').val();
    if(contrasena != "" && contrasena == contrasena2) {
        $('.txtpass').html("");
        validar_pass = 0;
    }else{
        $('.txtpass').html("Las contraseñas no coinciden!");
        validar_pass = 1;
    }
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'UsuarioCliente/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
