var base_url = $('#base_url').val();
$(document).ready(function() {
    /// funcion de guardado inicio	
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();
	        	$('.btn_registro').attr('disabled',true);
	        	var datos = $('#form_registro').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Guiaindicadores/add_data',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                    Swal.fire({
					        icon: 'success',
					        title: 'Registro guardado correctamente',
					        showConfirmButton: false,
					        buttonsStyling: false
					    });
					    setTimeout(function(){ 
                            text_factor(0);
                            text_estrategia();
                        }, 2000);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
	// fin
    text_estrategia();
});

function text_factor(id){
	$.ajax({
	    type:'POST',
	    url: base_url+'Guiaindicadores/editar_factor',
	    data:{id:id},
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
            $('.text_factor').html(data);
	    }
	});  
}

function text_estrategia(){
	$.ajax({
	    type:'POST',
	    url: base_url+'Guiaindicadores/get_texto_factor_estrategias',
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
            $('.texto_estrategias').html(data);
	    }
	});  
}

function editar_factor(id){
	text_factor(id);
}

function eliminar_registro_factor(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este registro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Guiaindicadores/delete_record_factor',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Factor eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      text_estrategia();
                  }, 1000); 
                }
            });  
            
        },
    });
}

function guardar_estrategia(id){
	$.ajax({
        type:'POST',
        url: base_url+'Guiaindicadores/add_data_estrategias',
        data: {
            idguiaindicadores:id,
            id:$('#id_registro_'+id).val(),
            estrategia:$('#estrategia_'+id).val(),
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            Swal.fire({
		        icon: 'success',
		        title: 'Registro guardado correctamente',
		        showConfirmButton: false,
		        buttonsStyling: false
		    });
		    setTimeout(function(){ 
                text_editar_factor_estrategias(0,id);
                get_estrategias(id);
            }, 2000);
        }
    }); 
}

function get_estrategias(id){
	$.ajax({
	    type:'POST',
	    url: base_url+'Guiaindicadores/get_estrategias_all',
	    data:{id:id},
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
            $('.text_estrategia_tabla_'+id).html(data);
	    }
	}); 
}


function text_editar_factor_estrategias(id,idf){
	$.ajax({
	    type:'POST',
	    url: base_url+'Guiaindicadores/editar_estrategias',
	    data:{idguiaindicadores:idf,
	    	id:id},
	    statusCode:{
	        404: function(data){
	            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	        },
	        500: function(){
	            Swal.fire("Error!", "500", "error");
	        }
	    },
	    success:function(data){
            $('.text_estrategia_form_'+idf).html(data);
	    }
	});  
}

function text_factor_estrategia(id,idf){
	text_editar_factor_estrategias(id,idf);
}

function eliminar_registro_factor_estrategia(id,idf){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este registro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Guiaindicadores/delete_record_factor_estrategia',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Estrategia eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      get_estrategias(idf);
                  }, 1000); 
                }
            });  
            
        },
    });
}