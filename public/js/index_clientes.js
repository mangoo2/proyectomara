var base_url = $('#base_url').val();
var tabla;
var tipo;
$(document).ready(function() {
    table();
    setTimeout(function(){ 
        tabla.ajax.reload(); 
    }, 1000); 
});

function reload_registro(){
    tabla.destroy();
    table();
}

function update_tabla(){
  tabla.ajax.reload(); 
}

function table(){
  tabla=$("#tabla_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Cliente/getlistado",
            type: "post",
            data:{
              coml1:$('#coml1').val(),
              coml2:$('#coml2').val(),
              coml3:$('#coml3').val(),
              coml4:$('#coml4').val(),
              coml5:$('#coml5').val(),
              coml6:$('#coml6').val(),
              coml7:$('#coml7').val(),
              coml8:$('#coml8').val(),
              coml9:$('#coml9').val(),
            },
            error: function(){
               $("#tabla_datos").css("display","none");
            }
        },
        columns: [
            { data:'id'},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html=row.nombre+' '+row.ap_paterno+' '+row.ap_materno;
                return html;
                }
            },
            { data:'curso'},
            { data:'email'},
            { data:'telefono'},
            { data:'celular'},
            { data:'razon_social'},
            { data:'giro'},
            { data:'Usuario'},
            { data:'reg'},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.suspender==1){
                    html='<button class="btn btn-success btn-sm" type="button">Activo</button>';
                }else{
                    html='<button class="btn btn-sm btn-danger" type="button">Suspendido</button>';
                }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<div class="demo-inline-spacing">\
                          <button type="button" class="btn btn-icon btn-outline-primary" onclick="href_cliente('+row.id+')">\
                            <span class="tf-icons mdi mdi-pencil"></span>\
                          </button>\
                          <button type="button" class="btn btn-icon btn-outline-secondary" onclick="eliminar_registro('+row.id+')">\
                            <span class="tf-icons mdi mdi-delete-empty"></span>\
                          </button>\
                        </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: {
            url: base_url+'public/es-ES.json',
        },
        columnDefs: [
          {
            className: 'control',
            orderable: false,
            targets: 0,
            searchable: false,
            render: function (data, type, full, meta) {
              return '';
            }
          },
          {
            // Label
            targets: -1,
            render: function (data, type, full, meta) {
              var $status_number = full['status'];
              var $status = {
                1: { title: 'Current', class: 'bg-label-primary' },
                2: { title: 'Professional', class: ' bg-label-success' },
                3: { title: 'Rejected', class: ' bg-label-danger' },
                4: { title: 'Resigned', class: ' bg-label-warning' },
                5: { title: 'Applied', class: ' bg-label-info' }
              };
              if (typeof $status[$status_number] === 'undefined') {
                return data;
              }
              return (
                '<span class="badge rounded-pill ' +
                $status[$status_number].class +
                '">' +
                $status[$status_number].title +
                '</span>'
              );
            }
          }
        ],
        // scrollX: true,
        destroy: true,
        dom: '<"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6 d-flex justify-content-center justify-content-md-end"f>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Detalles';// + data['nombre']+ ' ' + data['ap_paterno']+ ' ' + data['ap_materno'];
              }
            }),
            type: 'column',
            renderer: function (api, rowIdx, columns) {
              var data = $.map(columns, function (col, i) {
                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                  ? '<tr data-dt-row="' +
                      col.rowIndex +
                      '" data-dt-column="' +
                      col.columnIndex +
                      '">' +
                      '<td>' +
                      col.title +
                      ':' +
                      '</td> ' +
                      '<td>' +
                      col.data +
                      '</td>' +
                      '</tr>'
                  : '';
              }).join('');

              return data ? $('<table class="table"/><tbody />').append(data) : false;
            }
          }
        }
        // Cambiamos lo principal a Español
    });
}

function href_cliente(id){
  window.location = base_url+'Cliente/registro/'+id;
}

function eliminar_registro(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este cliente?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar cliente',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Cliente/delete_record',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Cliente eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      tabla.ajax.reload(); 
                  }, 1000); 
                }
            });  
            
        },
    });
}
