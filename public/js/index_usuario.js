var base_url = $('#base_url').val();
var tabla;
var tipo;
$(document).ready(function() {
    table();
    setTimeout(function(){ 
        tabla.ajax.reload(); 
    }, 1000); 
});

function reload_registro(){
    tabla.destroy();
    table();
}

function update_tabla(){
  tabla.ajax.reload(); 
}

function table(){
  tabla=$("#tabla_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Usuarios/getlistado",
            type: "post",
            error: function(){
               $("#tabla_datos").css("display","none");
            }
        },
        columns: [
            { data:'personalId'},
            { data:'nombre'},
            { data:'correo'},
            { data:'telefono'},
            { data:'perfil'},
            { data:'Usuario'},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var check='';
                    var txt_u='';
                	if(row.acceso==1){
                    check='';
                    txt_u='Suspender usuario';
                	}else{
                    check='checked';
                    txt_u='Usuario suspendido';
                	}
                    var html='<div class="demo-inline-spacing">\
                          <label class="switch switch-square">\
                          <input type="checkbox" class="switch-input idreg_'+row.UsuarioID+'" '+check+' onclick="suspeder_usuario('+row.UsuarioID+')" />\
                          <span class="switch-toggle-slider">\
                            <span class="switch-on"></span>\
                            <span class="switch-off"></span>\
                          </span>\
                          <span class="switch-label">'+txt_u+'</span>\
                        </label>\
                          <button type="button" class="btn btn-icon btn-outline-primary" onclick="href_usuario('+row.personalId+')">\
                            <span class="tf-icons mdi mdi-pencil"></span>\
                          </button>\
                          <button type="button" class="btn btn-icon btn-outline-secondary" onclick="eliminar_registro('+row.personalId+','+row.UsuarioID+')">\
                            <span class="tf-icons mdi mdi-delete-empty"></span>\
                          </button>\
                        </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        language: {
            url: base_url+'public/es-ES.json',
        },
        columnDefs: [
        {
          className: 'control',
          orderable: false,
          targets: 0,
          searchable: false,
          render: function (data, type, full, meta) {
            return '';
          }
        },
        {
          // Label
          targets: -1,
          render: function (data, type, full, meta) {
            var $status_number = full['status'];
            var $status = {
              1: { title: 'Current', class: 'bg-label-primary' },
              2: { title: 'Professional', class: ' bg-label-success' },
              3: { title: 'Rejected', class: ' bg-label-danger' },
              4: { title: 'Resigned', class: ' bg-label-warning' },
              5: { title: 'Applied', class: ' bg-label-info' }
            };
            if (typeof $status[$status_number] === 'undefined') {
              return data;
            }
            return (
              '<span class="badge rounded-pill ' +
              $status[$status_number].class +
              '">' +
              $status[$status_number].title +
              '</span>'
            );
          }
        }
      ],
      // scrollX: true,
      destroy: true,
      dom: '<"row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6 d-flex justify-content-center justify-content-md-end"f>>t<"row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
      responsive: {
        details: {
          display: $.fn.dataTable.Responsive.display.modal({
            header: function (row) {
              var data = row.data();
              return 'Detalles';// + data['nombre']+ ' ' + data['ap_paterno']+ ' ' + data['ap_materno'];
            }
          }),
          type: 'column',
          renderer: function (api, rowIdx, columns) {
            var data = $.map(columns, function (col, i) {
              return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                ? '<tr data-dt-row="' +
                    col.rowIndex +
                    '" data-dt-column="' +
                    col.columnIndex +
                    '">' +
                    '<td>' +
                    col.title +
                    ':' +
                    '</td> ' +
                    '<td>' +
                    col.data +
                    '</td>' +
                    '</tr>'
                : '';
            }).join('');

            return data ? $('<table class="table"/><tbody />').append(data) : false;
          }
        }
      }
        // Cambiamos lo principal a Español
    });
}

function href_usuario(id){
  window.location = base_url+'Usuarios/registro/'+id;
}

function eliminar_registro(id,idusu){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este usuario?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar usuario',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Usuarios/delete_record',
                data:{id:id,idusu:idusu},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Cliente eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                  });
                  setTimeout(function(){ 
                      tabla.ajax.reload(); 
                  }, 1000); 
                }
            });  
            
        },
    });
}

function suspeder_usuario(id){
	
	if($('.idreg_'+id).is(':checked')){
	    Swal.fire({
	        title: '<strong>¿Estás seguro de suspender este usuario?</strong>',
	        icon: 'error',
	        showCloseButton: true,
	        showCancelButton: true,
	        focusConfirm: false,
	        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Suspender usuario',
	        confirmButtonAriaLabel: 'Thumbs up, great!',
	        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
	        cancelButtonAriaLabel: 'Thumbs down',
	        customClass: {
	          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
	          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
	        },
	        buttonsStyling: false,
	        preConfirm: login => {
                $(".idreg_"+id).prop('checked', false);
	            $.ajax({
	                type:'POST',
	                url: base_url+'Usuarios/update_record',
	                data:{id:id},
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                  Swal.fire({
	                      icon: 'success',
	                      title: 'Usuario suspendido correctamente',
	                      showConfirmButton: false,
	                      timer: 3000,
	                      buttonsStyling: false
	                  });
	                 
	                  setTimeout(function(){ 
	                  	  $(".idreg_"+id).prop('checked', true);
	                      tabla.ajax.reload(); 
	                  }, 1000); 
	                }
	            });  
	            
	        },
	    });
	}else{
        Swal.fire({
	        title: '<strong>¿Estás seguro de activar este usuario?</strong>',
	        icon: 'success',
	        showCloseButton: true,
	        showCancelButton: true,
	        focusConfirm: false,
	        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Activar usuario',
	        confirmButtonAriaLabel: 'Thumbs up, great!',
	        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
	        cancelButtonAriaLabel: 'Thumbs down',
	        customClass: {
	          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
	          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
	        },
	        buttonsStyling: false,
	        preConfirm: login => {
                $(".idreg_"+id).prop('checked', true);
	            $.ajax({
	                type:'POST',
	                url: base_url+'Usuarios/activar_record',
	                data:{id:id},
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                  Swal.fire({
	                      icon: 'success',
	                      title: 'Usuario activado correctamente',
	                      showConfirmButton: false,
	                      timer: 3000,
	                      buttonsStyling: false
	                  });
	                  setTimeout(function(){ 
	                  	  $(".idreg_"+id).prop('checked', false);
	                      tabla.ajax.reload(); 
	                  }, 1000); 
	                }
	            });  
	            
	        },
	    });
	}
}
