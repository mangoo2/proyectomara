var base_url = $('#base_url').val();
var idfoda = $('#idfoda').val();
var tipo_m=$('#tipo_m').val();
var fechaActual = new Date();
fechaActual.setHours(0, 0, 0, 0);
var ultimoDiaDelAno = new Date(fechaActual.getFullYear(), 11, 31);
$(document).ready(function() {

    vista_tipo(tipo_m);

    $('textarea.js-auto-size').textareaAutoSize();
    /**/
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();

	        	$('.btn_registro').attr('disabled',true);
	        	var datos = $('#form_registro').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Analisisfoda/add_data',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                    add_tabla_fortaleza();
					    $('#concepto').val('');
					    setTimeout(function(){ 
	                        $('.btn_registro').attr('disabled',false);
	                        //get_fortaleza();
	                    }, 500);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
    /**/
    const bsValidationFormso = document.querySelectorAll('#form_registro_o');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationFormso).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();

	        	$('.btn_registro_o').attr('disabled',true);
	        	var datos = $('#form_registro_o').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Analisisfoda/add_data_o',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                    add_tabla_oportunidades();
					    $('#concepto_o').val('');
					    setTimeout(function(){ 
	                        $('.btn_registro_o').attr('disabled',false);
	                        //get_oportunidad();
	                    }, 500);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
    /**/
    const bsValidationForms_c = document.querySelectorAll('#form_registro_d');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms_c).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();

	        	$('.btn_registro_d').attr('disabled',true);
	        	var datos = $('#form_registro_d').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Analisisfoda/add_data_d',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                    add_tabla_debilidad();
					    $('#concepto_d').val('');
					    setTimeout(function(){ 
	                        $('.btn_registro_d').attr('disabled',false);
	                        //get_debilidad();
	                    }, 500);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
    /**/
    const bsValidationFormsa = document.querySelectorAll('#form_registro_a');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationFormsa).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
	        }else{
	        	event.preventDefault();

	        	$('.btn_registro_a').attr('disabled',true);
	        	var datos = $('#form_registro_a').serialize();
	            $.ajax({
	                type:'POST',
	                url: base_url+'Analisisfoda/add_data_a',
	                data: datos,
	                statusCode:{
	                    404: function(data){
	                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
	                    },
	                    500: function(){
	                        Swal.fire("Error!", "500", "error");
	                    }
	                },
	                success:function(data){
	                    add_tabla_amenaza();
					    $('#concepto_a').val('');
					    setTimeout(function(){ 
	                        $('.btn_registro_a').attr('disabled',false);
	                        //get_amenaza();
	                    }, 500);
	                }
	            }); 
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
	});
    /**/
    if(tipo_m==1){
        get_fortaleza();
        get_oportunidad();
        get_debilidad();
        get_amenaza();
    }else if(tipo_m==2){
        get_tabla_fortaleza_pregunta_seleccionresumenestrategias();
        get_tabla_fortaleza_pregunta_prioriza();
    }else if(tipo_m==3){
        get_tablacronograma();
        get_tablacronogramadebilidad_fortaleza();
    }
});

function vista_tipo(tipo){
    $('.link_data').removeClass('active');
    $('.vista_txt').hide(500);
    $('.btn_txt').show(500);
    $('.img_li').removeClass('img_color');
    $('.link_c').removeClass('link_color');
    $('.text_foda').css('display','none');
    if(tipo==1){
        $('.link1').addClass('active');
        $('.link1_c').addClass('link_color');
        $('.linkimg1').addClass('img_color');
        $('.text_foda1').css('display','block');
    }else if(tipo==2){
        $('.link2').addClass('active');
        $('.link2_c').addClass('link_color');
        $('.linkimg2').addClass('img_color');
        $('.text_foda2').css('display','block');
    }else if(tipo==3){    
        $('.link3').addClass('active');
        $('.link3_c').addClass('link_color');
        $('.linkimg3').addClass('img_color');
        $('.text_foda3').css('display','block');
    }
}


function add_tabla_fortaleza(){

    var concepto=$('#concepto').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea style="width: 1030px;" rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_fortaleza').append(html);
    get_fortaleza()
}


function get_fortaleza(){
	$.ajax({
        url: base_url+'Analisisfoda/get_fortalezas',
        type: 'POST',
        success: function(data){ 
            $('.txt_fortaleza').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function add_tabla_oportunidades(){

    var concepto_o=$('#concepto_o').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea style="width: 1030px;" rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_o+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_oportunidades').append(html);
    get_oportunidad();
}

function get_oportunidad(){
	$.ajax({
        url: base_url+'Analisisfoda/get_oportunidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_oportunidades').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function add_tabla_debilidad(){

    var concepto_d=$('#concepto_d').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea style="width: 1030px;" rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_d+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_debilidad').append(html);
     get_debilidad()
}

function get_debilidad(){
	$.ajax({
        url: base_url+'Analisisfoda/get_debilidades',
        type: 'POST',
        success: function(data){ 
            $('.txt_debilidades').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function add_tabla_amenaza(){

    var concepto_a=$('#concepto_a').val();
    var html='<div class="row g-3">\
                <div class="col-md-12">\
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">\
                    <textarea style="width: 1030px;" rows="1" type="text" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'+concepto_a+'</textarea>\
                    <div class="">\
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light">\
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
                        </button>\
                    </div>\
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary">\
                        <span class="tf-icons mdi mdi-delete-empty"></span>\
                    </button>\
                  </div>\
                </div>\
            </div>';
    $('.row_amenazas').append(html);
    get_amenaza();
}

function get_amenaza(){
	$.ajax({
        url: base_url+'Analisisfoda/get_amenazas',
        type: 'POST',
        success: function(data){ 
            $('.txt_amenazas').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function btn_editar_f(id){
	$('.btn_f_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_f('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
	$('#concepto_f_'+id).attr('disabled',false);

}

function btn_editar_o(id){
	$('.btn_o_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_o('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
	$('#concepto_o_'+id).attr('disabled',false);

}

function btn_editar_d(id){
	$('.btn_d_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_d('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
	$('#concepto_d_'+id).attr('disabled',false);

}

function btn_editar_a(id){
	$('.btn_a_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-success btn-fab demo waves-effect waves-light" onclick="btn_guardar_a('+id+')">\
            <span class="tf-icons mdi mdi-circle-edit-outline mdi-24px"></span>\
        </button>');
	$('#concepto_a_'+id).attr('disabled',false);

}

function btn_guardar_f(id){
	$.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_f',
        data: {
            concepto:$('#concepto_f_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
        	Swal.fire({
		        icon: 'success',
		        title: 'Registro editado correctamente',
		        showConfirmButton: false,
		        buttonsStyling: false
		    });
            $('.btn_f_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_f_'+id+'" onclick="btn_editar_f('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
	        $('#concepto_f_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_o(id){
	$.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_o',
        data: {
            concepto:$('#concepto_o_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
        	Swal.fire({
		        icon: 'success',
		        title: 'Registro editado correctamente',
		        showConfirmButton: false,
		        buttonsStyling: false
		    });
            $('.btn_o_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_o_'+id+'" onclick="btn_editar_o('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
	        $('#concepto_o_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_d(id){
	$.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_d',
        data: {
            concepto:$('#concepto_d_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
        	Swal.fire({
		        icon: 'success',
		        title: 'Registro editado correctamente',
		        showConfirmButton: false,
		        buttonsStyling: false
		    });
            $('.btn_d_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_d_'+id+'" onclick="btn_editar_d('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
	        $('#concepto_d_'+id).attr('disabled',true);
        }
    });
}

function btn_guardar_a(id){
	$.ajax({
        type: 'POST',
        url: base_url+'Analisisfoda/edit_data_a',
        data: {
            concepto:$('#concepto_a_'+id).val(),
            id:id
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
        	Swal.fire({
		        icon: 'success',
		        title: 'Registro editado correctamente',
		        showConfirmButton: false,
		        buttonsStyling: false
		    });
            $('.btn_a_'+id).html('<button type="button" class="btn rounded-pill btn-icon btn-outline-primary btn-fab demo waves-effect waves-light icon_a_'+id+'" onclick="btn_editar_a('+id+')">\
                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>\
            </button>');
	        $('#concepto_a_'+id).attr('disabled',true);
        }
    });
}


function eliminar_registro_fo(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_f',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_fortaleza();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_op(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_o',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_oportunidad();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_de(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_d',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_debilidad();
                    }, 2000);
                }
            });  
            
        },
    });
}

function eliminar_registro_am(id){
  Swal.fire({
        title: '<strong>¿Estás seguro de eliminar este refistro?</strong>',
        icon: 'error',
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i> Eliminar registro',
        confirmButtonAriaLabel: 'Thumbs up, great!',
        cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
        cancelButtonAriaLabel: 'Thumbs down',
        customClass: {
          confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
          cancelButton: 'btn btn-outline-secondary btn-icon waves-effect'
        },
        buttonsStyling: false,
        preConfirm: login => {
          $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/delete_record_a',
                data:{id:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    Swal.fire({
                      icon: 'success',
                      title: 'Registro eliminado correctamente',
                      showConfirmButton: false,
                      timer: 3000,
                      buttonsStyling: false
                    });
                    setTimeout(function(){ 
                        get_amenaza();
                    }, 2000);
                }
            });  
            
        },
    });
}

function siguiente_ocultar(num){
    $('.vista_x').css('display','none');
    $('.txt_fortaleza').html('');
    $('.txt_oportunidades').html('');
    $('.txt_debilidades').html('');
    $('.txt_amenazas').html('');
    if(num==1){
        $('.vista_1').css('display','block');
    }else if(num==2){
        $('.vista_2').css('display','block');
    }else if(num==3){
        $('.vista_3').css('display','block');
        get_fortaleza();
    }else if(num==4){
        $('.vista_4').css('display','block');   
    }else if(num==5){
        $('.vista_5').css('display','block'); 
        get_oportunidad();
    }else if(num==6){
        $('.vista_6').css('display','block'); 
    }else if(num==7){
        $('.vista_7').css('display','block');  
        get_debilidad();
    }else if(num==8){
        $('.vista_8').css('display','block');  
    }else if(num==9){
        $('.vista_9').css('display','block');  
        get_amenaza();
    }else if(num==10){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/update_foda',
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });  
        $('.vista_10').css('display','block');  
        get_fortaleza();
        get_oportunidad();
        get_debilidad();
        get_amenaza();
    }
}

function get_tabla_fortaleza_pregunta_seleccionresumenestrategias(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_resumen_estrategias',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_estrategias').html(data);
            //$('textarea.js-auto-size').textareaAutoSize();
        }
    });
}


function get_tabla_fortaleza_pregunta_prioriza(){
    $.ajax({
        url: base_url+'Guia_seleccion_factores_criticos_exito/get_tabla_resumen_estrategias_prioriza',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_prioriza').html(data);
        }
    });
}

function pregunta_fortaleza_resumen_input(idpregunta){
    var resp1=$('#pre_for_p_'+idpregunta).val();
    var resp2=$('#pre_for_g_'+idpregunta).val();
    $.ajax({
        type:'POST',
        url: base_url+'Guia_seleccion_factores_criticos_exito/edit_fortaleza_pregunta_seleccion_resumen',
        data:{idpregunta:idpregunta,respuesta1:resp1,respuesta2:resp2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){

        }
    });  
}

function est_costo_imple(id,tipo){
    var resp=$('#est_costo_imple'+id+'_'+tipo+' option:selected').val();
    if(resp=='1'){
        $('#est_costo_imple'+id+'_'+tipo).css('background','#70ad47');
        $('#est_costo_imple'+id+'_'+tipo).css('color','white');
    }else if(resp=='2'){
        $('#est_costo_imple'+id+'_'+tipo).css('background','#f5bd00');
        $('#est_costo_imple'+id+'_'+tipo).css('color','black');
    }else if(resp=='3'){
        $('#est_costo_imple'+id+'_'+tipo).css('background','#e31b1b');
        $('#est_costo_imple'+id+'_'+tipo).css('color','black');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_pregunta_resumen',
        data:{idpregunta:id,respuesta:resp,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            /**/
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/calculo_resumen1',
                data:{idpregunta:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    $('.txt_prioridad_1_'+id).html(data);
                }
            });
            /**/
        }
    });
}

function est_riesgo(id,tipo){
    var resp=$('#est_riesgo'+id+'_'+tipo+' option:selected').val();
    if(resp=='1'){
        $('#est_riesgo'+id+'_'+tipo).css('background','#70ad47');
        $('#est_riesgo'+id+'_'+tipo).css('color','white');
    }else if(resp=='2'){
        $('#est_riesgo'+id+'_'+tipo).css('background','#f5bd00');
        $('#est_riesgo'+id+'_'+tipo).css('color','black');
    }else if(resp=='3'){
        $('#est_riesgo'+id+'_'+tipo).css('background','#e31b1b');
        $('#est_riesgo'+id+'_'+tipo).css('color','black');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_pregunta_riesgo',
        data:{idpregunta:id,respuesta:resp,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            /**/
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/calculo_resumen2',
                data:{idpregunta:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    $('.txt_prioridad_2_'+id).html(data);
                }
            });
            /**/
        }
    });
}

function est_fortaleza(id,tipo){
    var resp=$('#est_fortaleza'+id+'_'+tipo+' option:selected').val();
    if(resp=='1'){
        $('#est_fortaleza'+id+'_'+tipo).css('background','#70ad47');
        $('#est_fortaleza'+id+'_'+tipo).css('color','white');
    }else if(resp=='2'){
        $('#est_fortaleza'+id+'_'+tipo).css('background','#f5bd00');
        $('#est_fortaleza'+id+'_'+tipo).css('color','black');
    }else if(resp=='3'){
        $('#est_fortaleza'+id+'_'+tipo).css('background','#e31b1b');
        $('#est_fortaleza'+id+'_'+tipo).css('color','black');
    }
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_pregunta_fortaleza_resultado',
        data:{idpregunta:id,respuesta:resp,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            /**/
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/calculo_resumen3',
                data:{idpregunta:id},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                    $('.txt_prioridad_3_'+id).html(data);
                }
            });
            /**/
        }
    });

}

function siguiente_resumen_ocultar(id){
    $('.text_foda').css('display','none');
    $('.text_foda2_'+id).css('display','block');
    setTimeout(function(){ 
        if(id==1){
            vista_tipo(2);  
        }else if(id==2){
            get_tabla_fortaleza_pregunta_prioriza_2_m();
            get_tabla_fortaleza_pregunta_prioriza_2_i();
            get_tabla_fortaleza_pregunta_prioriza_2_e();
        }
    }, 1000); 
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);       
}



function get_tabla_fortaleza_pregunta_prioriza_2_m(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_resumen_estrategias_prioriza_2_m',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_prioriza_2_m').html(data);
        }
    });
}

function get_tabla_fortaleza_pregunta_prioriza_2_i(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_resumen_estrategias_prioriza_2_i',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_prioriza_2_i').html(data);
        }
    });
}

function get_tabla_fortaleza_pregunta_prioriza_2_e(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_resumen_estrategias_prioriza_2_e',
        type: 'POST',
        success: function(data){ 
            $('.tabla_guia_resumen_prioriza_2_e').html(data);
        }
    });
}

function b_edit_resp_m_a(id){
    var resp=$('#id_m_a_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_m_a',
        data:{id:id,anio:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function b_edit_resp_m_s(id){
    var resp=$('#id_m_s_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_m_s',
        data:{id:id,semestre:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}


function b_edit_resp_i_a(id){
    var resp=$('#id_i_a_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_i_a',
        data:{id:id,anio:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function b_edit_resp_i_s(id){
    var resp=$('#id_i_s_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_i_s',
        data:{id:id,semestre:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}


function b_edit_resp_s_a(id){
    var resp=$('#id_s_a_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_s_a',
        data:{id:id,anio:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function b_edit_resp_s_s(id){
    var resp=$('#id_s_s_'+id+' option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_resp_s_s',
        data:{id:id,semestre:resp},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}


function get_tablacronograma(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_cronograma',
        type: 'POST',
        success: function(data){ 
            $('.tabla_cronograma').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function b_edit_resp(id,tipo,num){
    var pasa=true;
    var pre=$('#id_pre_'+id+'_'+tipo+'_'+num).val();
    //alert(pre);
    /*if(num==4 || num==5){
        var  fechaSeleccionada = new Date(pre+ 'T00:00:00');
        //alert(fechaSeleccionada);
        if (fechaSeleccionada >= fechaActual && fechaSeleccionada <= ultimoDiaDelAno) {
            pasa=true;
        } else {
            pasa=false;
            Swal.fire("Error!", "Fecha fuera de rango", "error");
        }
    }*/
    if(pasa){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/b_edit_respuesta_cro',
            data:{id:id,pre:pre,tipo:tipo,num:num},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                /*if(num==4 || num==5){
                    if(data==0){
                        setTimeout(function(){ 
                            $('#id_pre_'+id+'_'+tipo+'_'+num).val('');
                        }, 1000);
                    }
                }*/
            }
        });
    }
    
    if(num==4 || num==5){
        fecha_validar(id,tipo,num);  

    }
    
}


function b_add_resp_cro(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_add_respuesta_cro',
        data:{id:id,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            get_tablacronograma();
        } 
    });
}

function b_edit_resp_cro(id,tipo,num){
    var pasa=true;
    var pre=$('#id_pre_cro_'+id+'_'+tipo+'_'+num).val();
    /*if(num==4 || num==5){
        var  fechaSeleccionada = new Date(pre+ 'T00:00:00');
        if (fechaSeleccionada >= fechaActual && fechaSeleccionada <= ultimoDiaDelAno) {
            pasa=true;
        } else {
            pasa=false;
            Swal.fire("Error!", "Fecha fuera de rango", "error");
        }
    }*/
    if(pasa){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/b_edit_respuesta_cro_sub',
            data:{id:id,pre:pre,tipo:tipo,num:num},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
                if(num==4 || num==5){
                    fecha_validar_cro(id,tipo,num);  
                }
            }  
        });
    }
}

function eliminar_registro_cro(id,tipo){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_delete_respuesta_cro_sub',
        data:{id:id,tipo:tipo},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.pre_cro_'+id+'_'+tipo).remove();
        }
    });
}

function fecha_validar(id,tipo,num){
    var f1=$('#id_pre_'+id+'_'+tipo+'_4').val();
    var f2=$('#id_pre_'+id+'_'+tipo+'_5').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/fecha_validar',
        data:{f1:f1,f2:f2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('#id_pre_'+id+'_'+tipo+'_6').val(data);
            $('.id_pre_'+id+'_'+tipo+'_6').html(data);
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/b_edit_respuesta_cro',
                data:{id:id,pre:data,tipo:tipo,num:6},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                }
            });
        }
    });
}

function fecha_validar_cro(id,tipo,num){
    var f1=$('#id_pre_cro_'+id+'_'+tipo+'_4').val();
    var f2=$('#id_pre_cro_'+id+'_'+tipo+'_5').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/fecha_validar',
        data:{f1:f1,f2:f2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('#id_pre_cro_'+id+'_'+tipo+'_6').val(data);
            $('.id_pre_cro_'+id+'_'+tipo+'_6').html(data);
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/b_edit_respuesta_cro_sub',
                data:{id:id,pre:data,tipo:tipo,num:6},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                }
            });
        }
    });

}

function btn_add_debilidad_fortaleza(){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_add_cronograma_debilidad_fortaleza',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            get_tablacronogramadebilidad_fortaleza();
        } 
    });
    
}

function get_tablacronogramadebilidad_fortaleza(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_cronograma_debilidad_fortaleza',
        type: 'POST',
        success: function(data){ 
            $('.tabla_cronograma_d_f').html(data);
            $('textarea.js-auto-size').textareaAutoSize();
        }
    });
}

function b_edit_resp_df(id,num){
    var pasa=true;
    var pre=$('#id_pre_df_'+id+'_'+num).val();
    /*if(num==4 || num==5){
        var  fechaSeleccionada = new Date(pre+ 'T00:00:00');
        if (fechaSeleccionada >= fechaActual && fechaSeleccionada <= ultimoDiaDelAno) {
            pasa=true;
        } else {
            pasa=false;
            Swal.fire("Error!", "Fecha fuera de rango", "error");
        }
    }*/
    if(pasa){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/b_edit_respuesta_cro_debilidad_fortaleza',
            data:{id:id,pre:pre,num:num},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });
        if(num==4 || num==5){
            fecha_validar_df(id,num);  
        }
    }
    
}

function b_edit_resp_df_select(id,num){
    var pasa=true;
    var pre=$('#id_pre_df_'+id+'_'+num+' option:selected').val();
    /*if(num==4 || num==5){
        var  fechaSeleccionada = new Date(pre+ 'T00:00:00');
        if (fechaSeleccionada >= fechaActual && fechaSeleccionada <= ultimoDiaDelAno) {
            pasa=true;
        } else {
            pasa=false;
            Swal.fire("Error!", "Fecha fuera de rango", "error");
        }
    }*/
    if(pasa){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/b_edit_respuesta_cro_debilidad_fortaleza',
            data:{id:id,pre:pre,num:num},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });
        if(num==4 || num==5){
            fecha_validar_df(id,num);  
        }
    }
    
}

function fecha_validar_df(id,num){
    var f1=$('#id_pre_df_'+id+'_4').val();
    var f2=$('#id_pre_df_'+id+'_5').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/fecha_validar_df',
        data:{f1:f1,f2:f2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('#id_pre_df_'+id+'_6').val(data);
            $('.id_pre_df_'+id+'_6').html(data);
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/b_edit_respuesta_cro_debilidad_fortaleza',
                data:{id:id,pre:data,num:6},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                }
            });
        }
    });
}

function eliminar_resp_cro_df(id){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_delete_cro_debilidad_fortaleza',
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.row_cro_df_'+id).remove();
            validar_total_debilidad_fortaleza()
        }
    });
}

function validar_total_debilidad_fortaleza(){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/total_debilidad_fortaleza',
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            if(data==0){
                $('.menu_debilidad').remove();
            }
        }
    });
}


function b_add_resp_cro_df_d(id){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_add_respuesta_cro_df_d',
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            get_tablacronogramadebilidad_fortaleza();
        } 
    });
}

function b_edit_resp_df_d(id,num){
    var pasa=true;
    var pre=$('#id_pre_df_d_'+id+'_'+num).val();
    /*if(num==4 || num==5){
        var  fechaSeleccionada = new Date(pre+ 'T00:00:00');
        if (fechaSeleccionada >= fechaActual && fechaSeleccionada <= ultimoDiaDelAno) {
            pasa=true;
        } else {
            pasa=false;
            Swal.fire("Error!", "Fecha fuera de rango", "error");
        }
    }*/
    if(pasa){
        $.ajax({
            type:'POST',
            url: base_url+'Analisisfoda/b_edit_respuesta_cro_debilidad_fortaleza_detalles',
            data:{id:id,pre:pre,num:num},
            statusCode:{
                404: function(data){
                    Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                },
                500: function(){
                    Swal.fire("Error!", "500", "error");
                }
            },
            success:function(data){
            }
        });
    }
    if(num==4 || num==5){
        fecha_validar_df_d(id,num);  
    }
    
}

function fecha_validar_df_d(id,num){
    var f1=$('#id_pre_df_d_'+id+'_4').val();
    var f2=$('#id_pre_df_d_'+id+'_5').val();
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/fecha_validar_df',
        data:{f1:f1,f2:f2},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('#id_pre_df_d_'+id+'_6').val(data);
            $('.id_pre_df_d_'+id+'_6').html(data);
            $.ajax({
                type:'POST',
                url: base_url+'Analisisfoda/b_edit_respuesta_cro_debilidad_fortaleza_detalles',
                data:{id:id,pre:data,num:6},
                statusCode:{
                    404: function(data){
                        Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                    },
                    500: function(){
                        Swal.fire("Error!", "500", "error");
                    }
                },
                success:function(data){
                }
            });
        }
    });
}

function eliminar_resp_cro_df_d(id){
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/b_delete_cro_debilidad_fortaleza_d',
        data:{id:id},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
            $('.row_cro_df_d'+id).remove();
        }
    });
}

function btn_tabla_cronograma_actividad(){
    /*var id=$('#idactividad option:selected').val();
    var anio=$('#anio option:selected').val();*/
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_cronograma_actividad',
        //data:{id:id,anio:anio},
        type: 'POST',
        success: function(data){ 
            $('.tabla_cronograma_actividad').html(data);
        }
    });
}

function resp_actividad(id,sem){
    var mes=0;//$('#idactividad option:selected').val();
    var resp=$('#idact_'+sem+'_'+id+' option:selected').val();
    if(resp==1){
        $('#idact_'+sem+'_'+id).css('background','#70ad47');
    }else if(resp==2){
        $('#idact_'+sem+'_'+id).css('background','#e31a1a');
    }else{
        $('#idact_'+sem+'_'+id).css('background','#a7a7a4');
    }
    
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_respuesta_actividad',
        data:{id:id,mes:mes,respuesta:resp,sem:sem},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function resp_actividad_i(id,sem){
    var mes=$('#idactividad option:selected').val();
    var resp=$('#idact_i_'+sem+'_'+id+' option:selected').val();
    if(resp==1){
        $('#idact_i_'+sem+'_'+id).css('background','#70ad47');
    }else if(resp==2){
        $('#idact_i_'+sem+'_'+id).css('background','#e31a1a');
    }else{
        $('#idact_i_'+sem+'_'+id).css('background','#a7a7a4');
    }
    
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_respuesta_actividad_i',
        data:{id:id,mes:mes,respuesta:resp,sem:sem},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function resp_actividad_s(id,sem){
    var mes=$('#idactividad option:selected').val();
    var resp=$('#idact_s_'+sem+'_'+id+' option:selected').val();
    if(resp==1){
        $('#idact_s_'+sem+'_'+id).css('background','#70ad47');
    }else if(resp==2){
        $('#idact_s_'+sem+'_'+id).css('background','#e31a1a');
    }else{
        $('#idact_s_'+sem+'_'+id).css('background','#a7a7a4');
    }
    
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_respuesta_actividad_s',
        data:{id:id,mes:mes,respuesta:resp,sem:sem},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function resp_actividad_n(id,sem){
    var mes=$('#idactividad option:selected').val();
    var resp=$('#idact_n_'+sem+'_'+id+' option:selected').val();
    if(resp==1){
        $('#idact_n_'+sem+'_'+id).css('background','#70ad47');
    }else if(resp==2){
        $('#idact_n_'+sem+'_'+id).css('background','#e31a1a');
    }else{
        $('#idact_n_'+sem+'_'+id).css('background','#a7a7a4');
    }
    
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_respuesta_actividad_n',
        data:{id:id,mes:mes,respuesta:resp,sem:sem},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function resp_actividad_nd(id,sem){
    var mes=$('#idactividad option:selected').val();
    var resp=$('#idact_nd_'+sem+'_'+id+' option:selected').val();
    if(resp==1){
        $('#idact_nd_'+sem+'_'+id).css('background','#70ad47');
    }else if(resp==2){
        $('#idact_nd_'+sem+'_'+id).css('background','#e31a1a');
    }else{
        $('#idact_nd_'+sem+'_'+id).css('background','#a7a7a4');
    }
    
    $.ajax({
        type:'POST',
        url: base_url+'Analisisfoda/edit_respuesta_actividad_nd',
        data:{id:id,mes:mes,respuesta:resp,sem:sem},
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function siguiente_cronograma_ocultar(id){
    $('.text_foda').css('display','none');
    setTimeout(function(){ 
        if(id==1){
            $('.text_foda3').css('display','block');
            get_tablacronograma();
            get_tablacronogramadebilidad_fortaleza();
        }else if(id==2){
            $('.text_foda3_'+id).css('display','block');
            btn_tabla_cronograma_actividad();
        }else if(id==3){
            $('.text_foda3_'+id).css('display','block');
            //get_tablacronograma_resumen_btn();
            get_tablacronograma_resumen();

        }
    }, 1000);   
    setTimeout(function(){ 
        scrollTo(0,0);
    }, 1000);    
}

/*function get_tablacronograma_resumen_btn(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_cronograma_resumen_anio',
        type: 'POST',
        success: function(data){ 
            $('.btn_anios_resumen').html(data);
        }
    });
}*/

function get_tablacronograma_resumen(){
    $.ajax({
        url: base_url+'Analisisfoda/get_tabla_cronograma_resumen',
        type: 'POST',
        success: function(data){ 
            $('.tabla_cronograma_resumen').html(data);
        }
    });
}

function btn_exporta_cronograma(){
    location.href= base_url+'Analisisfoda/reporte_excel_actividades';
}

function btn_exporta_cumplimiento(){
    location.href= base_url+'Analisisfoda/reporte_excel_cumplimiento';
}


