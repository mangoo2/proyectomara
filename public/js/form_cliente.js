var base_url = $('#base_url').val();
var idreg = $('#idreg').val();
var validar_pass=0;
var validar_user=0;
$(document).ready(function() {
  
    /// funcion de guardado inicio	
    const bsValidationForms = document.querySelectorAll('#form_registro');
    // Loop over them and prevent submission
    Array.prototype.slice.call(bsValidationForms).forEach(function (form) {
	    form.addEventListener(
	      'submit',
	      function (event) {
	        if (!form.checkValidity()) {
	          event.preventDefault();
	          event.stopPropagation();
 
	        }else{
	        	event.preventDefault();
            var tabla_p = $(".menu_txt_p .ul_p > .li_p");
            var tabla_t = tabla_p.length;
            if(tabla_t==0){

              Swal.fire({
                  title: 'Debe seleccionar por lo menos un menú para asignarle al cliente',
                  icon: 'error',
                  showCloseButton: true,
                  showCancelButton: false,
                  focusConfirm: false,
                  confirmButtonText: 'Aceptar',
                  confirmButtonAriaLabel: 'Thumbs up, great!',
                  cancelButtonAriaLabel: 'Thumbs down',
                  customClass: {
                    confirmButton: 'btn btn-primary me-3 waves-effect waves-light',
                  },
                  buttonsStyling: false,
                  preConfirm: login => {                      
                  },
              }); 
            }else{
              if(validar_pass==0 && validar_user==0){
                if($('#correo_enviar').is(':checked')){
                  var email = $('#email').val();
                  if(email!=''){
                    $('.btn_registro').attr('disabled',true);
                    var datos = $('#form_registro').serialize();
                    $.ajax({
                        type:'POST',
                        url: base_url+'Cliente/add_data',
                        data: datos,
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                          var idr=parseFloat(data);
                          guardar_registro_usuario(idr);
                          guardar_registro_permisos(idr);
                          
                          setTimeout(function(){ 
                            if($('#correo_enviar').is(':checked')){
                              enviarcorreo(idr);
                            }
                          }, 1000);
                          Swal.fire({
                            icon: 'success',
                            title: 'Registro guardado correctamente',
                            showConfirmButton: false,
                            buttonsStyling: false
                          }); 
                          setTimeout(function(){ 
                            window.location = base_url+'Cliente';
                          }, 5000);
                        }
                    }); 
                  }else{
                    $('#modal_agregar_correo').modal('show');
                  }
                }else{
                    $('.btn_registro').attr('disabled',true);
                    var datos = $('#form_registro').serialize();
                    $.ajax({
                        type:'POST',
                        url: base_url+'Cliente/add_data',
                        data: datos,
                        statusCode:{
                            404: function(data){
                                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                            },
                            500: function(){
                                Swal.fire("Error!", "500", "error");
                            }
                        },
                        success:function(data){
                          var idr=parseFloat(data);
                          guardar_registro_usuario(idr);
                          guardar_registro_permisos(idr);

                          Swal.fire({
                            icon: 'success',
                            title: 'Registro guardado correctamente',
                            showConfirmButton: false,
                            buttonsStyling: false
                          }); 
                          setTimeout(function(){ 
                            window.location = base_url+'Cliente';
                          }, 5000);
                        }
                    }); 
                }
	             	    
	            }
            }
	        }

	        form.classList.add('was-validated');
	      },
	      false
	    );
    if(idreg!=0){  
      get_menu();
    } 
	});
	// fin
	fecha_picker();
	if(idreg!=0){
		var vrf=$('#regimen_fiscalx').val();
		$("#regimen_fiscal option[value='"+vrf+"']").attr("selected", true);
		setTimeout(function(){ 
            v_rf();
        }, 2000);
	}
});

function guardar_registro_usuario(id){
    var suspender=0;
    if($('#suspender').is(':checked')){
        suspender=0; 
    }else{
        suspender=1; 
    }
	$.ajax({
        type:'POST',
        url: base_url+'Cliente/add_usuarios',
        data: {
        	idcliente:id,
        	UsuarioID:$('#UsuarioID').val(),
        	usuario:$('#usuario').val(),
            contrasena:$('#contrasena').val(),
            suspender:suspender,
        },
        statusCode:{
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success:function(data){
        }
    });
}

function fecha_picker(){
	const datepickerList = document.querySelectorAll('.dob-picker');
	datepickerList.forEach(function (datepicker) {
      datepicker.flatpickr({
        monthSelectorType: 'static'
      });
    });
}

function v_rf(){
    $('.pararf').prop( "disabled", true );
    var rf=$('#regimen_fiscal option:selected').val();
    $( "#cfdi ."+rf ).prop( "disabled", false )
}

function cambiaCP(id){
    var cp = $('#codigo_postal').val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Cliente/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array.length>0) {
	                $("#estado").val(array[0].estado);
	                $("#ciudad").val(array[0].ciudad);
	                cambia_cp_colonia(id);
	            }    
                
            }
        });

    }
}

function cambia_cp_colonia(id){
    var cp = $('#codigo_postal').val();  
    $('#colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Cliente/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Cliente/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 

                            data.forEach(function(element){
                                $('#colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                            $('#colonia').prepend('<option value="Ninguna">Ninguna</option>');

                        }
                     });
                }
            }
        });
    }else{
        $("#estado").attr("readonly",false);
    }
}

function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Cliente/validar',
        data: {
            Usuario:$('#usuario').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                $('.txtusuario').html("El usuario ya existe");
            }else{
                validar_user=0;
                $('.txtusuario').html("");
            }
        }
    });
}

function comfirmar_pass(){
	var contrasena = $('#contrasena').val();
	var contrasena2 = $('#contrasena2').val();
	if(contrasena != "" && contrasena == contrasena2) {
        $('.txtpass').html("");
        validar_pass = 0;
    }else{
        $('.txtpass').html("Las contraseñas no coinciden!");
        validar_pass = 1;
    }
}


function get_menu(){
    $.ajax({
        type: 'POST',
        url: base_url+'Cliente/get_submenu',
        data: {
            idmenu:$('#idmenu option:selected').val(),idreg:idreg
        },
        async: false,
        statusCode: {
            404: function(data){
                Swal.fire("Error!", "No Se encuentra el archivo!", "error");
            },
            500: function(){
                Swal.fire("Error!", "500", "error");
            }
        },
        success: function(data) {
            $('.menu_txt').html(data);
            setTimeout(function(){ 
                (function () {
                  const cardEl = document.getElementById('sortable-cards'),
                    pendingTasks = document.getElementById('pending-tasks'),
                    completedTasks = document.getElementById('completed-tasks'),
                    cloneSource1 = document.getElementById('clone-source-1'),
                    cloneSource2 = document.getElementById('clone-source-2'),
                    handleList1 = document.getElementById('handle-list-1'),
                    handleList2 = document.getElementById('handle-list-2'),
                    imageList1 = document.getElementById('image-list-1'),
                    imageList2 = document.getElementById('image-list-2');

                  // Cards
                  // --------------------------------------------------------------------
                  if (cardEl) {
                    Sortable.create(cardEl);
                  }

                  // Images
                  // --------------------------------------------------------------------
                  if (imageList1) {
                    Sortable.create(imageList1, {
                      animation: 150,
                      group: 'imgList'
                    });
                  }
                  if (imageList2) {
                    Sortable.create(imageList2, {
                      animation: 150,
                      group: 'imgList'
                    });
                  }

                  // Cloning
                  // --------------------------------------------------------------------
                  if (cloneSource1) {
                    Sortable.create(cloneSource1, {
                      animation: 150,
                      group: {
                        name: 'cloneList',
                        pull: 'clone',
                        revertClone: true
                      }
                    });
                  }
                  if (cloneSource2) {
                    Sortable.create(cloneSource2, {
                      animation: 150,
                      group: {
                        name: 'cloneList',
                        pull: 'clone',
                        revertClone: true
                      }
                    });
                  }

                  // Multiple
                  // --------------------------------------------------------------------
                  if (pendingTasks) {
                    Sortable.create(pendingTasks, {
                      animation: 150,
                      group: 'taskList'
                    });
                  }
                  if (completedTasks) {
                    Sortable.create(completedTasks, {
                      animation: 150,
                      group: 'taskList'
                    });
                  }

                  // Handles
                  // --------------------------------------------------------------------
                  if (handleList1) {
                    Sortable.create(handleList1, {
                      animation: 150,
                      group: 'handleList',
                      handle: '.drag-handle'
                    });
                  }
                  if (handleList2) {
                    Sortable.create(handleList2, {
                      animation: 150,
                      group: 'handleList',
                      handle: '.drag-handle'
                    });
                  }
                })();
            }, 1000);

        }
    });
}

function guardar_registro_permisos(id){
    var DATA  = [];
    var TABLA   = $(".menu_txt_p .ul_p > .li_p");
    TABLA.each(function(){         
        item = {};
        item ["idcliente"] = id;
        item ["idsubmenu"] = $(this).find("input[id*='idsubmenux']").val();
        DATA.push(item);
    });
    console.log(TABLA.length);
    if(TABLA.length>0) {
      INFO  = new FormData();
      aInfo = JSON.stringify(DATA);
      INFO.append('data', aInfo);
      INFO.append('idcliente', id);
      $.ajax({
          data: INFO,
          type: 'POST',
          url : base_url+'Cliente/registro_datos_permisos',
          processData: false, 
          contentType: false,
          async: false,
          statusCode:{
              404: function(data){
              },
              500: function(){
              }
          },
          success: function(data){
          }
      }); 
    }
}

function enviarcorreo(id) {
  $.ajax({
      type: 'POST',
      url: base_url+'Cliente/envio_correo',
      data: {
          id:id
      },
      async: false,
      statusCode: {
          404: function(data){
              Swal.fire("Error!", "No Se encuentra el archivo!", "error");
          },
          500: function(){
              Swal.fire("Error!", "500", "error");
          }
      },
      success: function(data) {
      }
  });
}

function agregar_correo(){
  correo_validar = $('#correo_validar').val();
  if(correo_validar!=''){
    var validEmail =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;

    // Using test we can check if the text match the pattern
    if( validEmail.test(correo_validar)){
      $('#email').val(correo_validar);
      $('#modal_agregar_correo').modal('hide');
      $('.btn_registro').attr('disabled',true);
      var datos = $('#form_registro').serialize();
      $.ajax({
          type:'POST',
          url: base_url+'Cliente/add_data',
          data: datos,
          statusCode:{
              404: function(data){
                  Swal.fire("Error!", "No Se encuentra el archivo!", "error");
              },
              500: function(){
                  Swal.fire("Error!", "500", "error");
              }
          },
          success:function(data){
            var idr=parseFloat(data);
            guardar_registro_usuario(idr);
            guardar_registro_permisos(idr);
            
            setTimeout(function(){ 
              if($('#correo_enviar').is(':checked')){
                enviarcorreo(idr);
              }
            }, 1000);
            Swal.fire({
              icon: 'success',
              title: 'Registro guardado correctamente',
              showConfirmButton: false,
              buttonsStyling: false
            }); 
            setTimeout(function(){ 
              window.location = base_url+'Cliente';
            }, 5000);
          }
      }); 
    }else{
      Swal.fire({
          title: '<strong>Ingrese una dirección de correo electrónico válida</strong>',
          icon: 'error',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText: '<i class="mdi mdi-thumb-up-outline me-2"></i>Aceptar',
          confirmButtonAriaLabel: 'Thumbs up, great!',
          cancelButtonText: '<i class="mdi mdi-thumb-down-outline"></i>',
          cancelButtonAriaLabel: 'Thumbs down',
          customClass: {
            confirmButton: 'btn btn-primary me-3 waves-effect waves-light'
          },
          buttonsStyling: false,
          preConfirm: login => {
          },
      });
    }
    //$('#email').val(correo_validar);
    //$('#modal_agregar_correo').modal('hide');
  }else{
    Swal.fire("!Atención¡", "Falta agregar correo", "error");
  }
}