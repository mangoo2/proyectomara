<style type="text/css">
  .th_c{
    background: #d1b697 !important;
    color: white !important;
  }
  .td_input{
    padding-left: 1px !important;
    padding-right: 1px !important;
  }
  .table_info th {
    padding-top: 1px;
    padding-bottom: 1px;
  }
</style>
<div class="container-xxl flex-grow-1 container-p-y">  
  <div class="row my-4">
    <div class="col">
      
        <div class="row mt-4">
          <div class="col-sm-2">
            <h6 >Datos Macroeconómicos</h6>
          </div>
          <div class="col-sm-4">
            <button
              type="button"
              class="btn btn-sm btn-secondary waves-effect waves-light"
              data-bs-toggle="modal"
              data-bs-target="#onboardingHorizontalSlideModal">
              <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span> Instrucciones de captura
            </button>
          </div>
          <div class="col-sm-4" align="right">
            <label>Seleccionar año</label>
          </div>  
          <div class="col-sm-2">
            <select class="form-select" id="anio" onchange="get_macroeconomicos()">
              <?php 
                for($i=2024;$i<=date("Y");$i++)
                { 
                  $check='';  
                  if(date("Y")==$i){
                    $check='selected';
                  } 
                  echo "<option value='".$i."' ".$check.">".$i."</option>";
                }
             ?>
            </select>
          </div>  
        </div>
        <br>
        <div class="card mb-4">
          <div class="card-body">
            <div class="text_macroeconomico"></div>
          </div>  
        </div>
        <div class="row mt-4">
          <div class="col-sm-9">
            <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" onclick="guardar_registro()">Guardar</button>
          </div>
        </div>

    </div>
  </div>
</div>

<div
                        class="modal-onboarding modal fade animate__animated"
                        id="onboardingHorizontalSlideModal"
                        tabindex="-1"
                        aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                          <div class="modal-content text-center">
                            <div class="modal-header border-0">
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"></button>
                            </div>
                            <div
                              id="modalHorizontalCarouselControls"
                              class="carousel slide pb-4 mb-2"
                              data-bs-interval="false">
                              <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <div class="onboarding-horizontal">
                                    <div class="onboarding-media">
                                      <img
                                        src="../../assets/img/illustrations/boy-with-rocket-light.png"
                                        alt="boy-with-rocket-light"
                                        width="273"
                                        class="img-fluid"
                                        data-app-light-img="illustrations/boy-with-rocket-light.png"
                                        data-app-dark-img="illustrations/boy-with-rocket-dark.png" />
                                    </div>
                                    <div class="onboarding-content">
                                      <h4 class="onboarding-title text-body">Datos macroeconómicos</h4>
                                      <div class="onboarding-info" style="text-align: justify;">
                                        Instrucciones de captura:  Consultar datos de fuentes confiables, de preferencia gubernamentales y capturar todas las celdas en color blanco
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>



                      