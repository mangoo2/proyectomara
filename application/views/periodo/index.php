<div class="container-xxl flex-grow-1 container-p-y">
  <h4 class="py-3 mb-4">Cargar periodos</h4>
  <!-- Product List Table -->
  <div class="card mb-4">
    <div class="card-body">
      <div class="text_periodo">
        <form class="form" method="post" role="form" id="form_registro" novalidate>
          <div class="row">
            <div class="col-md-3">
              <select class="form-select" id="anio" name="anio" required>
                <option selected value="">Seleccionar año</option>
                <?php
                    $ano = date("Y");
                    for ($i=2024;$i<=$ano;$i++){
                         echo '<option value="'.$i.'">'.$i.'</option>';
                    }
                ?>
              </select>
            </div>
            <div class="col-md-3">
              <select class="form-select" id="mes" name="mes" required>
                <option selected value="">Seleccionar mes</option>
                <option value="1">ENERO</option>
                <option value="2">FEBRERO</option>
                <option value="3">MARZO</option>
                <option value="4">ABRIL</option>
                <option value="5">MAYO</option>
                <option value="6">JUNIO</option>
                <option value="7">JULIO</option>
                <option value="8">AGOSTO</option>
                <option value="9">SEPTIEMBRE</option>
                <option value="10">OCTUBRE</option>
                <option value="11">NOVIEMBRE</option>
                <option value="12">DICIEMBRE</option>
              </select>
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del curso/programa" required />
            </div>
            <div class="col-md-3">
              <button type="submit" class="btn btn-outline-primary waves-effect btn_registro">
                <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span>Agregar periodo
              </button>
            </div>  
          </div>  
        </form>
      </div>  
      <br>
      <div class="card-datatable table-responsive">
        <table class="dt-responsive table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre del curso/programa</th>
              <th>Año</th>
              <th>Mes</th>

            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Nombre del curso/programa</th>
              <th>Año</th>
              <th>Mes</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>  
</div>