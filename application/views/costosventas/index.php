<style type="text/css">
  #table_datos_filter{
    display: none;
  }
  .tab-content{
    background: #f6f5f5 !important;
  }
  .table_oit_0 th,.table_oit_0 td{
    padding-left: 8px;
    padding-right: 8px;
    font-size: 10px;
  }
  .td_c_input{
    padding: 1px !important;
  }
  .td_c_input input,.td_c_input input:focus{
    padding: 7px 4px;
    font-size: 12px;
    min-width: 87px;
  }
  .table3 input,.table3 input:focus{
    width: 55px !important;
  }
  .info_table_2_1{
    overflow: auto;
    padding: 0px;
    margin-top: 15px;
  }
  .promc{
    min-width: 54px;
  }
  .tdred td{
    text-align: center;
    color: red;
  }
</style>

<div class="container-xxl_quitar flex-grow-1 container-p-y">
  <h4 class="py-3 mb-4">Costos Ventas <?php echo $result_vh->num_rows(); ?></h4>
  <!-- Product List Table -->
  <div class="col-xl-12">
    <div class="nav-align-left mb-4">
      <ul class="nav nav-pills me-3" role="tablist">
        <?php
            $html_nav=''; 
            foreach ($result_vh->result() as $item){
              $html_nav.='<li class="nav-item" role="presentation">';
                $html_nav.='<button type="button" class="nav-link waves-effect waves-light" role="tab" data-bs-toggle="tab" data-bs-target="#navs-'.$item->id.'" aria-controls="navs-pills-left-home" aria-selected="true">'.$item->nombre.'</button>';
              $html_nav.='</li>';
            } 
            echo $html_nav;
            ?>
        
      </ul>
      <div class="tab-content">

        <?php
            $html_p=''; 
            foreach ($result_vh->result() as $item){ 
              $html_p.='<div class="tab-pane fade" id="navs-'.$item->id.'" role="tabpanel">';
                          //=================================================================
                            $vacio="''";
                            $html_p.='<div class="accordion  mt-3" >';
                                      $html_p.='<div class="accordion mt-3" id="accordionExample">
                                                  <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingOne'.$item->id.'">
                                                      <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordionOne'.$item->id.'" aria-expanded="false" aria-controls="accordionOne'.$item->id.'" onclick="obtener_info_tables(0,'.$item->id.','.$vacio.',0)">
                                                        Línea de Servicio / Producto:  '.$item->nombre.'
                                                      </button>
                                                    </h2>

                                                    <div id="accordionOne'.$item->id.'" class="accordion-collapse collapse" data-bs-parent="#accordionExample" style="">
                                                      <div class="accordion-body info_table info_table_0_'.$item->id.'"></div>
                                                    </div>
                                                  </div>
                                                  <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingTwo'.$item->id.'">
                                                      <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordionTwo'.$item->id.'" aria-expanded="false" aria-controls="accordionTwo'.$item->id.'" onclick="obtener_info_tables(2,'.$item->id.','.$anio_actual.',2)">
                                                        '.$anio_actual.'
                                                      </button>
                                                    </h2>
                                                    <div id="accordionTwo'.$item->id.'" class="accordion-collapse collapse" aria-labelledby="headingTwo'.$item->id.'" data-bs-parent="#accordionExample" style="">
                                                      <div class="accordion-body">
                                                        <div class="row">
                                                          <div class="col-md-12" style="text-align: end;">
                                                            <button class="btn btn-success" onclick="agregardescrip('.$item->id.')">Agregar Descripción</button>
                                                          </div>
                                                          <div class="col-md-12 info_table info_table_2_'.$item->id.'"></div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingThree'.$item->id.'">
                                                      <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordionThree'.$item->id.'" aria-expanded="false" aria-controls="accordionThree'.$item->id.'" onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_1.',3)">
                                                        '.$anio_next_1.'
                                                      </button>
                                                    </h2>
                                                    <div id="accordionThree'.$item->id.'" class="accordion-collapse collapse" aria-labelledby="headingThree'.$item->id.'" data-bs-parent="#accordionExample" style="">
                                                      <div class="accordion-body info_table info_table_3_'.$item->id.'"></div>
                                                    </div>
                                                  </div>

                                                  <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingFor'.$item->id.'">
                                                      <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordionFor'.$item->id.'" aria-expanded="false" aria-controls="accordionFor'.$item->id.'" onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_2.',4)">
                                                        '.$anio_next_2.'
                                                      </button>
                                                    </h2>
                                                    <div id="accordionFor'.$item->id.'" class="accordion-collapse collapse" aria-labelledby="headingFor'.$item->id.'" data-bs-parent="#accordionExample" style="">
                                                      <div class="accordion-body info_table info_table_4_'.$item->id.'"></div>
                                                    </div>
                                                  </div>

                                                  <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingFive'.$item->id.'">
                                                      <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#accordionFive'.$item->id.'" aria-expanded="false" aria-controls="accordionFive'.$item->id.'" onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_3.',5)">
                                                        '.$anio_next_3.'
                                                      </button>
                                                    </h2>
                                                    <div id="accordionFive'.$item->id.'" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample" style="">
                                                      <div class="accordion-body info_table info_table_5_'.$item->id.'"></div>
                                                    </div>
                                                  </div>
                                                </div>';
                                                /*
                              $acor_item1='acor_item1';
                              $vacio="''";
                              $html_p.='<div class="accordion-item">
                                          <h2 class="accordion-header" id="headingPopoutOne">
                                            <button type="button" class="accordion-button collapsed" 
                                              data-bs-toggle="collapse" data-bs-target="#'.$acor_item1.'" aria-expanded="false" 
                                              

                                              onclick="obtener_info_tables(0,'.$item->id.','.$vacio.',0)">
                                              Línea de Servicio / Producto:  '.$item->nombre.'
                                            </button>
                                          </h2>

                                          <div id="'.$acor_item1.'" class="accordion-collapse collapse"  style="">
                                            <div class="accordion-body info_table info_table_0_'.$item->id.'">
                                              
                                            </div>
                                          </div>
                                        </div>';
                              $acor_item1='acor_item2';
                              $html_p.='<div class="accordion-item">
                                          <h2 class="accordion-header" id="tabs_2" onclick="obtener_info_tables(2,'.$item->id.','.$anio_actual.',2)">
                                            <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#'.$acor_item1.'" aria-expanded="false"  >
                                              '.$anio_actual.'
                                            </button>
                                          </h2>

                                          <div id="'.$acor_item1.'" class="accordion-collapse collapse"   style="">
                                            <div class="accordion-body ">
                                              <div class="row">
                                                <div class="col-md-12" style="text-align: end;">
                                                  <button class="btn btn-success" onclick="agregardescrip('.$item->id.')">Agregar Descripción</button>
                                                </div>
                                                <div class="col-md-12 info_table info_table_2_'.$item->id.'"></div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>';
                              $acor_item1='acor_item3';

                              $html_p.='<div class="accordion-item">
                                          <h2 class="accordion-header" id="headingPopoutOne">
                                            <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#'.$acor_item1.'" aria-expanded="false"  onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_1.',3)">
                                              '.$anio_next_1.'
                                            </button>
                                          </h2>

                                          <div id="'.$acor_item1.'" class="accordion-collapse collapse"   style="">
                                            <div class="accordion-body info_table info_table_3_'.$item->id.'">
                                              
                                            </div>
                                          </div>
                                        </div>';
                              $acor_item1='acor_item4';
                              $html_p.='<div class="accordion-item">
                                          <h2 class="accordion-header" id="headingPopoutOne">
                                            <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#'.$acor_item1.'" aria-expanded="false"  onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_2.',4)">
                                              '.$anio_next_2.'
                                            </button>
                                          </h2>

                                          <div id="'.$acor_item1.'" class="accordion-collapse collapse"   style="">
                                            <div class="accordion-body info_table info_table_4_'.$item->id.'">
                                              
                                            </div>
                                          </div>
                                        </div>';

                              $acor_item1='acor_item5';
                              $html_p.='<div class="accordion-item">
                                          <h2 class="accordion-header" id="headingPopoutOne">
                                            <button type="button" class="accordion-button collapsed" data-bs-toggle="collapse" data-bs-target="#'.$acor_item1.'" aria-expanded="false"  onclick="obtener_info_tables(3,'.$item->id.','.$anio_next_3.',5)">
                                              '.$anio_next_3.'
                                            </button>
                                          </h2>

                                          <div id="'.$acor_item1.'" class="accordion-collapse collapse"   style="">
                                            <div class="accordion-body info_table info_table_5_'.$item->id.'">
                                              
                                            </div>
                                          </div>
                                        </div>';
                              */
                              
                            $html_p.='</div>';
                          //=================================================================
                  //$html_p.=$item->nombre;
                $html_p.=''; 
              $html_p.='</div>';
            } 
            echo $html_p;
        ?>
        
      </div>
    </div>
  </div> 
</div>
<div class="modal fade" id="descripModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1">Agregar Descripción</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
        <div class="modal-body">
          <form id="form_descrit" class="form">
            <input type="hidden" name="idpro" id="idpro">
            <div class="row">
              <div class="col-md-9">
                <label>Descripción</label>
                <input type="text" name="namepro" id="namepro" class="form-control" required>
              </div>
              <div class="col-md-3">
                <a class="btn btn-success" id="adddescript" style="margin-top: 20px;">Agregar</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered" style=" margin-top: 10px;">
                  <thead>
                    <tr>
                      <th>Descripción</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody class="table_tb_descrip"></tbody>
                </table>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cerrar</button>
          <!--<button type="submit" class="btn btn-primary btn_registro">Agregar</button>-->
        </div>
        
    </div>
  </div>
</div>