<?php
if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
}else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $idcliente=$this->session->userdata('idcliente');
    $foto=$this->session->userdata('foto');
    $tipo=$this->session->userdata('tipo');
    $this->fechainicio = date('Y-m-d');
}
?>
<style type="text/css">
  .img_logo1{
    width: 35%;
  }
  .img_logo2{
    width: 27% !important;
  }
  .text_ayuda{
    display: block;
  }
  .text_ayudax{
    display: none;
  }
  .menu-vertical .menu-item .menu-link > div:not(.badge) {
    overflow: hidden;
    text-overflow: revert;
    white-space: normal;
  }
  .text_color{
     color: white !important; 
  }
  .text_borde{
    /*border: 1px solid white !important;*/
    border-radius: 12px !important;
  }
  .fondo_color{
     background: #bd9250 !important; 
  }
  .volt{
    transform: scaleY(-1);
  }
  .text_color_p{
    color: #bd9250 !important;
    border-radius: 0rem !important;
    border-bottom-style: solid;
  }

  .form-control:focus, .form-select:focus {
    border-color: #bd925000 !important;
  }
</style>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          <div class="app-brand demo">
            <a href="<?php echo base_url();?>Inicio" class="app-brand-link">
              <span class="app-brand-logo demo">
                <span style="color: var(--bs-primary)">
                  <img class="img_logo img_logo1" style="width: 35%;" src="<?php echo base_url() ?>images/LM.svg">
                </span>
              </span>
            </a>

            <a class="layout-menu-toggle menu-link text-large ms-auto" onclick="logo_mp()">
              <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M11.4854 4.88844C11.0081 4.41121 10.2344 4.41121 9.75715 4.88844L4.51028 10.1353C4.03297 10.6126 4.03297 11.3865 4.51028 11.8638L9.75715 17.1107C10.2344 17.5879 11.0081 17.5879 11.4854 17.1107C11.9626 16.6334 11.9626 15.8597 11.4854 15.3824L7.96672 11.8638C7.48942 11.3865 7.48942 10.6126 7.96672 10.1353L11.4854 6.61667C11.9626 6.13943 11.9626 5.36568 11.4854 4.88844Z"
                  fill="currentColor"
                  fill-opacity="0.6" />
                <path
                  d="M15.8683 4.88844L10.6214 10.1353C10.1441 10.6126 10.1441 11.3865 10.6214 11.8638L15.8683 17.1107C16.3455 17.5879 17.1192 17.5879 17.5965 17.1107C18.0737 16.6334 18.0737 15.8597 17.5965 15.3824L14.0778 11.8638C13.6005 11.3865 13.6005 10.6126 14.0778 10.1353L17.5965 6.61667C18.0737 6.13943 18.0737 5.36568 17.5965 4.88844C17.1192 4.41121 16.3455 4.41121 15.8683 4.88844Z"
                  fill="currentColor"
                  fill-opacity="0.38" />
              </svg>
            </a>
          </div>

          <div class="menu-inner-shadow"></div>

          <ul class="menu-inner py-1">
            <!-- Dashboards -->
            <?php 
                $btn_active_sub_my=0;
                if(isset($btn_active_sub_m)){
                  $btn_active_sub_my=$btn_active_sub_m;
                }else{ 
                  $btn_active_sub_my=0;
                }
                //var_dump();die;
                $menu=$this->Login_model->getMenus($perfilid,$tipo,$idcliente);
                    $menu_aux=0;
                    foreach ($menu as $items) { 
                    	$MenuId=intval($items->MenuId);
                        
                        $activar_borde='';
                        $activar_color='';
                        $volt='';
                        if($MenuId==$btn_active){
                           $activar='active open';
                           $activar_color='text_color';
                           $activar_borde='text_borde';
                           $activar_fondo='fondo_color';
                        }else{
                           $activar=''; 
                           $activar_color='';
                           $activar_borde='';
                           $activar_fondo='';
                        }
                        if($MenuId==7){
                          $volt='volt';
                        }
                      ?>
                       <li class="menu-item <?php echo $activar ?> ">
			              <a href="javascript:void(0);" class="menu-link menu-toggle">
			                <i class="menu-icon tf-icons mdi <?php echo $activar_fondo ?> <?php echo $volt ?> <?php echo $activar_color ?> <?php echo $items->Icon ?> <?php echo $activar_color ?> <?php echo $activar_borde ?>"></i>
			                <div class="menu_c" data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
			                <div class="badge bg-primary rounded-pill ms-auto"></div>
			              </a>
			              <ul class="menu-sub">
			              	<?php $menusub=$this->Login_model->submenus($perfilid,$items->MenuId,0,$tipo,$idcliente);
		                    foreach ($menusub as $items){ 
		                        $MenuId_sub=intval($items->MenusubId); 
		                        if($MenuId_sub==$btn_active_sub){
		                            $activar_sub='active';
		                        }else{
		                            $activar_sub=''; 
		                        }
                            if($items->tipo==0){
                            ?> 
                            <li class="menu-item <?php echo $activar_sub ?>">
                              <a href="<?php echo base_url()?><?php echo $items->Pagina ?>" class="menu-link">
                                <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                              </a>
                            </li>
                          
				                    <?php  
                            }else{
                              $MenuId_subm=intval($items->MenusubId); 
                              //var_dump($MenuId_subm.' - '.$btn_active_sub_my);die;
                              if($MenuId_subm==$btn_active_sub_my){
                                $activar_subm='open';
                                $activar_color_p='text_color_p';
                              }else{
                                  $activar_subm='';
                                  $activar_color_p=''; 
                              }
                             ?>
                              <li class="menu-item sdsd <?php echo $activar_subm ?>">
                              <a href="javascript:void(0);" class="menu-link menu-toggle <?php echo $activar_color_p ?>">
                                <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                              </a>
                              <ul class="menu-sub">
                                <?php 
                                //var_dump($perfilid.'/'.$items->MenuId.'/'.$items->MenusubId.'/'.$tipo.'/'.$idcliente);
                                $menusub=$this->Login_model->submenus($perfilid,$items->MenuId,$items->MenusubId,$tipo,$idcliente);
                                //var_dump($menusub);
                                foreach ($menusub as $items){ 
                                  if($items->MenusubId==21){
                                    if(isset($_GET['tipo'])){
                                      $tipo=$_GET['tipo'];
                                    }else{
                                      $tipo=0;
                                    } 
                                    if($tipo==1){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?tipo='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==25){ 
                                    if(isset($_GET['tipo'])){
                                      $tipo=$_GET['tipo'];
                                    }else{
                                      $tipo=0;
                                    } 
                                    if($tipo==2){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?tipo='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==26){ 
                                    if(isset($_GET['tipo'])){
                                      $tipo=$_GET['tipo'];
                                    }else{
                                      $tipo=0;
                                    } 
                                    if($tipo==3){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?tipo='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==27){ 
                                    if(isset($_GET['tipo'])){
                                      $tipo=$_GET['tipo'];
                                    }else{
                                      $tipo=0;
                                    } 
                                    if($tipo==4){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?tipo='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==19){ 
                                    //var_dump($items->MenusubId);
                                    if(isset($_GET['con'])){
                                      $con=$_GET['con'];
                                    }else{
                                      $con=0;
                                    } 
                                    if($con==1){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?con='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==28){ 
                                    if(isset($_GET['con'])){
                                      $con=$_GET['con'];
                                    }else{
                                      $con=0;
                                    } 
                                    if($con==2){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?con='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }else if($items->MenusubId==29){ 
                                    if(isset($_GET['con'])){
                                      $con=$_GET['con'];
                                    }else{
                                      $con=0;
                                    } 
                                    if($con==3){
                                        $activar_sub='active';
                                        
                                    }else{
                                        $activar_sub=''; 
                                    }
                                    ?>
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina.'?con='.$items->orden ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php 

                                  }else{ ?>  
                                    <li class="menu-item <?php echo $activar_sub ?>">
                                      <a href="<?php echo base_url()?><?php echo $items->Pagina ?>" class="menu-link">
                                        <div data-i18n="<?php echo $items->Nombre ?>"><?php echo $items->Nombre ?></div>
                                      </a>
                                    </li>
                                  <?php }
              
                                 } ?>  
                              </ul>
                            </li>
                          <?php  }                 
				            }
                            ?>
			              </ul>
			            </li>  
			    <?php }
			?>              
           
          </ul>
          <div class="info_text text_ayuda text_ayudax">
            <div style="margin-left:29px; margin-right: 29px">
              <div><b style="font-size: 18px;"><span style="border: 1px solid #FBFFF4; padding: 2px; border-radius: 15px; padding-left: 7px;"> ? </span> &nbsp;¿Necesitas ayuda? </b></div>
            
            
              <div>Por favor, checa nuestras<br>FAQ's</div>
              <div style="background-color: #FBFFF4; color: black; text-align: center; border-radius: 4px;">FAQ's</div>
            </div>
            <br>
          </div>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
          <!-- Navbar -->

          <nav
            class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
            id="layout-navbar">
            <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
              <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                <i class="mdi mdi-menu mdi-24px"></i>
              </a>
            </div>

            <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
              <!-- Search -->
              <div class="navbar-nav align-items-center" style="width: 100% !important;">
                <div class="nav-item navbar-search-wrapper mb-0">
                  <!-- <a class="nav-item nav-link search-toggler fw-normal px-0" href="javascript:void(0);">
                    <i class="mdi mdi-magnify mdi-24px scaleX-n1-rtl"></i>
                    <span class="d-none d-md-inline-block text-muted">Search (Ctrl+/)</span>
                  </a> -->
                  <div class="logo_center">
                  <img src="<?php echo base_url() ?>images/MVP Logo Mara.png" style="width: 176px;">
                  </div>
                </div>
              </div>
              <!-- /Search -->

              <ul class="navbar-nav flex-row align-items-center ms-auto">
                <!-- Language -->
                <li class="nav-item dropdown-language dropdown me-1 me-xl-0">
                  <!-- <a
                    class="nav-link btn btn-text-secondary rounded-pill btn-icon dropdown-toggle hide-arrow"
                    href="javascript:void(0);"
                    data-bs-toggle="dropdown">
                    <i class="mdi mdi-translate mdi-24px"></i>
                  </a> -->
                  <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-language="en" data-text-direction="ltr">
                        <span class="align-middle">English</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-language="fr" data-text-direction="ltr">
                        <span class="align-middle">French</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-language="ar" data-text-direction="rtl">
                        <span class="align-middle">Arabic</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="javascript:void(0);" data-language="de" data-text-direction="ltr">
                        <span class="align-middle">German</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <!--/ Language -->

                <!-- Quick links  -->
                <!-- <li class="nav-item dropdown-shortcuts navbar-dropdown dropdown me-1 me-xl-0"> -->
                  <!-- <a
                    class="nav-link btn btn-text-secondary rounded-pill btn-icon dropdown-toggle hide-arrow"
                    href="javascript:void(0);"
                    data-bs-toggle="dropdown"
                    data-bs-auto-close="outside"
                    aria-expanded="false">
                    <i class="mdi mdi-cog mdi-24px"></i>
                  </a> -->
                  <!-- <div class="dropdown-menu dropdown-menu-end py-0">
                    <div class="dropdown-menu-header border-bottom">
                      <div class="dropdown-header d-flex align-items-center py-3">
                        <h5 class="text-body mb-0 me-auto">Accesos directos</h5>
                        <a
                          href="javascript:void(0)"
                          class="dropdown-shortcuts-add text-muted"
                          data-bs-toggle="tooltip"
                          data-bs-placement="top"
                          title="Add shortcuts"
                          ><i class="mdi mdi-view-grid-plus-outline mdi-24px"></i
                        ></a>
                      </div>
                    </div>
                    <div class="dropdown-shortcuts-list scrollable-container">
                      <div class="row row-bordered overflow-visible g-0">
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-calendar fs-4"></i>
                          </span>
                          <a href="app-calendar.html" class="stretched-link">Mi agenda</a>
                        </div>
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-file-document-outline fs-4"></i>
                          </span>
                          <a href="app-invoice-list.html" class="stretched-link">Biblioteca</a>
                        </div>
                      </div>
                      <div class="row row-bordered overflow-visible g-0">
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-account-outline fs-4"></i>
                          </span>
                          <a href="app-user-list.html" class="stretched-link">Mis usuarios</a>
                        </div>
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-shield-check-outline fs-4"></i>
                          </span>
                          <a href="app-access-roles.html" class="stretched-link">Ventas</a>
                        </div>
                      </div>
                      <div class="row row-bordered overflow-visible g-0">
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-chart-pie-outline fs-4"></i>
                          </span>
                          <a href="index.html" class="stretched-link">Estado de resultados</a>
                        </div>
                        <div class="dropdown-shortcuts-item col">
                          <span class="dropdown-shortcuts-icon bg-label-secondary rounded-circle mb-2">
                            <i class="mdi mdi-cog-outline fs-4"></i>
                          </span>
                          <a href="pages-account-settings-account.html" class="stretched-link">Descargas</a>
                        </div>
                      </div>
                    </div>
                  </div> -->
                <!-- </li> -->
                <!-- Quick links -->

                <!-- Notification -->
                <!-- <li class="nav-item dropdown-notifications navbar-dropdown dropdown me-2 me-xl-1">
                  <a
                    class="nav-link btn btn-text-secondary rounded-pill btn-icon dropdown-toggle hide-arrow"
                    href="javascript:void(0);"
                    data-bs-toggle="dropdown"
                    data-bs-auto-close="outside"
                    aria-expanded="false">
                    <i class="mdi mdi-bell-outline mdi-24px"></i>
                    <span
                      class="position-absolute top-0 start-50 translate-middle-y badge badge-dot bg-danger mt-2 border"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end py-0">
                    <li class="dropdown-menu-header border-bottom">
                      <div class="dropdown-header d-flex align-items-center py-3">
                        <h6 class="mb-0 me-auto">Notification</h6>
                        <span class="badge rounded-pill bg-label-primary">8 New</span>
                      </div>
                    </li>
                    <li class="dropdown-notifications-list scrollable-container">
                      <ul class="list-group list-group-flush">
                        <li class="list-group-item list-group-item-action dropdown-notifications-item">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <img src="<?php // echo base_url();?>assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Congratulation Lettie 🎉</h6>
                              <small class="text-truncate text-body">Won the monthly best seller gold badge</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">1h ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <span class="avatar-initial rounded-circle bg-label-danger">CF</span>
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Charles Franklin</h6>
                              <small class="text-truncate text-body">Accepted your connection</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">12hr ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item marked-as-read">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <img src="<?php echo base_url();?>assets/img/avatars/2.png" alt class="w-px-40 h-auto rounded-circle" />
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">New Message ✉️</h6>
                              <small class="text-truncate text-body">You have new message from Natalie</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">1h ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <span class="avatar-initial rounded-circle bg-label-success"
                                  ><i class="mdi mdi-cart-outline"></i
                                ></span>
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Whoo! You have new order 🛒</h6>
                              <small class="text-truncate text-body">ACME Inc. made new order $1,154</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">1 day ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item marked-as-read">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <img src="<?php echo base_url();?>assets/img/avatars/9.png" alt class="w-px-40 h-auto rounded-circle" />
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Application has been approved 🚀</h6>
                              <small class="text-truncate text-body"
                                >Your ABC project application has been approved.</small
                              >
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">2 days ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item marked-as-read">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <span class="avatar-initial rounded-circle bg-label-success"
                                  ><i class="mdi mdi-chart-pie-outline"></i
                                ></span>
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Monthly report is generated</h6>
                              <small class="text-truncate text-body">July monthly financial report is generated </small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">3 days ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item marked-as-read">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <img src="<?php echo base_url();?>assets/img/avatars/5.png" alt class="w-px-40 h-auto rounded-circle" />
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">Send connection request</h6>
                              <small class="text-truncate text-body">Peter sent you connection request</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">4 days ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <img src="<?php echo base_url();?>assets/img/avatars/6.png" alt class="w-px-40 h-auto rounded-circle" />
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1 text-truncate">New message from Jane</h6>
                              <small class="text-truncate text-body">Your have new message from Jane</small>
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">5 days ago</small>
                            </div>
                          </div>
                        </li>
                        <li class="list-group-item list-group-item-action dropdown-notifications-item marked-as-read">
                          <div class="d-flex gap-2">
                            <div class="flex-shrink-0">
                              <div class="avatar me-1">
                                <span class="avatar-initial rounded-circle bg-label-warning"
                                  ><i class="mdi mdi-alert-circle-outline"></i
                                ></span>
                              </div>
                            </div>
                            <div class="d-flex flex-column flex-grow-1 overflow-hidden w-px-200">
                              <h6 class="mb-1">CPU is running high</h6>
                              <small class="text-truncate text-body"
                                >CPU Utilization Percent is currently at 88.63%,</small
                              >
                            </div>
                            <div class="flex-shrink-0 dropdown-notifications-actions">
                              <small class="text-muted">5 days ago</small>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>
                    <li class="dropdown-menu-footer border-top p-2">
                      <a href="javascript:void(0);" class="btn btn-primary d-flex justify-content-center">
                        View all notifications
                      </a>
                    </li>
                  </ul>
                </li> -->
                <!--/ Notification -->

                <!-- User -->
                <li class="nav-item navbar-dropdown dropdown-user dropdown">
                  <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                    <div class="avatar avatar-online">
                      <?php 
                        $tipox=$this->session->userdata('tipo');
                          if($tipox==1){ 
                            $file=$this->session->userdata('file');
                            //var_dump($file);die;
                            if($file==''){ ?>
                              <img src="<?php echo base_url();?>assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                            <?php }else{ ?>
                              <img src="<?php echo base_url() ?>uploads/personal/<?php echo $file ?>" alt class="w-px-40 h-auto rounded-circle" />
                            <?php }
                        ?>
                      <?php }else{ 
                            $cl=$this->General_model->getselectwhererow('clientes',array('id'=>$idcliente));
                            $foto=$cl->foto;
                            if($foto==''){ ?>
                              <img src="<?php echo base_url();?>assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                            <?php }else{ ?>
                              <img src="<?php echo base_url() ?>uploads/cliente/<?php echo $foto ?>" alt class="w-px-40 h-auto rounded-circle" />
                            <?php }
                       } ?>
                    </div>
                  </a>
                  <ul class="dropdown-menu dropdown-menu-end">
                    <li>
                      <a class="dropdown-item" href="pages-account-settings-account.html">
                        <div class="d-flex">
                          <div class="flex-shrink-0 me-3">
                            <div class="avatar avatar-online">
                              <?php 
                                $tipox=$this->session->userdata('tipo');
                                  if($tipox==1){ 
                                    $file=$this->session->userdata('file');
                                    //var_dump($file);die;
                                    if($file==''){ ?>
                                      <img src="<?php echo base_url();?>assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                                    <?php }else{ ?>
                                      <img src="<?php echo base_url() ?>uploads/personal/<?php echo $file ?>" alt class="w-px-40 h-auto rounded-circle" />
                                    <?php }
                                ?>
                              <?php }else{ 
                                    $cl=$this->General_model->getselectwhererow('clientes',array('id'=>$idcliente));
                                    $foto=$cl->foto;
                                    if($foto==''){ ?>
                                      <img src="<?php echo base_url();?>assets/img/avatars/1.png" alt class="w-px-40 h-auto rounded-circle" />
                                    <?php }else{ ?>
                                      <img src="<?php echo base_url() ?>uploads/cliente/<?php echo $foto ?>" alt class="w-px-40 h-auto rounded-circle" />
                                    <?php }
                                ?>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="flex-grow-1">
                            <span class="fw-medium d-block"><?php echo $this->session->userdata('usuario'); ?></span>
                            <small class="text-muted"><?php echo $this->session->userdata('perfil'); ?></small>
                          </div>
                        </div>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <?php 
                    $tipox=$this->session->userdata('tipo');
                    if($tipox==0){ ?>
                      <li>
                        <a class="dropdown-item" href="<?php echo base_url();?>UsuarioCliente">
                          <i class="mdi mdi-account-outline me-2"></i>
                          <span class="align-middle">Mi perfil</span>
                        </a>
                      </li>
                    <?php } ?>
                    
                    <!-- <li>
                      <a class="dropdown-item" href="pages-account-settings-account.html">
                        <i class="mdi mdi-cog-outline me-2"></i>
                        <span class="align-middle">Settings</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="pages-account-settings-billing.html">
                        <span class="d-flex align-items-center align-middle">
                          <i class="flex-shrink-0 mdi mdi-credit-card-outline me-2"></i>
                          <span class="flex-grow-1 align-middle ms-1">Billing</span>
                          <span class="flex-shrink-0 badge badge-center rounded-pill bg-danger w-px-20 h-px-20">4</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li>
                    <li>
                      <a class="dropdown-item" href="pages-faq.html">
                        <i class="mdi mdi-help-circle-outline me-2"></i>
                        <span class="align-middle">FAQ</span>
                      </a>
                    </li>
                    <li>
                      <a class="dropdown-item" href="pages-pricing.html">
                        <i class="mdi mdi-currency-usd me-2"></i>
                        <span class="align-middle">Pricing</span>
                      </a>
                    </li>
                    <li>
                      <div class="dropdown-divider"></div>
                    </li> -->
                    <li>
                      <a class="dropdown-item" onclick="salir_sesion()">
                        <i class="mdi mdi-logout me-2"></i>
                        <span class="align-middle">Cerrar sesión</span>
                      </a>
                    </li>
                  </ul>
                </li>
                <!--/ User -->
              </ul>
            </div>

            <!-- Search Small Screens -->
            <div class="navbar-search-wrapper search-input-wrapper d-none">
              <input
                type="text"
                class="form-control search-input container-xxl border-0"
                placeholder="Search..."
                aria-label="Search..." />
              <i class="mdi mdi-close search-toggler cursor-pointer"></i>
            </div>
          </nav>

          <!-- / Navbar -->

          <!-- Content wrapper -->
          <div class="content-wrapper">