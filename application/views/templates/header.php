<!doctype html>

<!-- <html
  lang="en"
  class="light-style layout-navbar-fixed layout-menu-fixed layout-compact"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="<?php // echo base_url();?>assets/"
  data-template="vertical-menu-template-no-customizer"> -->

<html lang="en" 
  class="dark-style layout-navbar-fixed layout-compact layout-menu-fixed" 
  dir="ltr" 
  data-theme="theme-default" 
  data-assets-path="<?php echo base_url();?>assets/" 
  data-template="vertical-menu-template-dark" 
  data-style="dark">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>images/lm.png" type="image/x-icon">
    <title>Mara Pérez</title>

    <meta name="description" content="" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&ampdisplay=swap"
      rel="stylesheet" />

    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fonts/materialdesignicons.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fonts/flag-icons.css" />

    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/node-waves/node-waves.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/rtl/core.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/rtl/theme-default.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/typeahead-js/typeahead.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/apex-charts/apex-charts.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/swiper/swiper.css" />

    <!-- Page CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/pages/cards-statistics.css" />

    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/sweetalert2/sweetalert2.css" />
    <!-- Helpers -->

    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/datatables-bs5/datatables.bootstrap5.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/datatables-responsive-bs5/responsive.bootstrap5.css" />
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/flatpickr/flatpickr.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/@form-validation/form-validation.css" />
    <script src="<?php echo base_url();?>assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="<?php echo base_url();?>assets/js/config.js"></script>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
  </head>
  <style type="text/css">
      .btn-flotante {
        font-size: 16px; /* Cambiar el tamaño de la tipografia */
        text-transform: uppercase; /* Texto en mayusculas */
        font-weight: bold; /* Fuente en negrita o bold */
        color: #ffffff; /* Color del texto */
        border-radius: 5px; /* Borde del boton */
        letter-spacing: 2px; /* Espacio entre letras */
        background-color: #bd9250; /* Color de fondo */
        padding: 10px 15px; /* Relleno del boton */
        transition: all 300ms ease 0ms;
        box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
        z-index: 99;
      }
      .btn-flotante:hover {
        background-color: #bd9250; /* Color de fondo al pasar el cursor */
        box-shadow: 0px 15px 20px rgba(0, 0, 0, 0.3);
        transform: translateY(-7px);
      }


      @media only screen and (max-width: 600px) {
        .btn-flotante {
          font-size: 14px;
          padding: 12px 20px;
          bottom: 20px;
          right: 20px;
        }
      } 

      .div-linea {
        width: 100%; 
        height: 3px; 
        background: linear-gradient(to right, transparent, #ccc, transparent);
        margin: auto; 
      }
      @font-face {
        font-family: 'Roboto';
        src: url("<?php  echo base_url(); ?>public/Kenao.otf");
      }

      @media (min-width: 1400px) {
        .container-xxl, .container-xl, .container-lg, .container-md, .container-sm, .container {
          max-width: none !important;
        }
      }
      @media (min-width: 1200px) {
        .navbar-expand-xl .navbar-nav {
            flex-direction: column;
        }
      }

      @media (min-width: 1200px) {
          h4, .h4 {
              font-size: 19px;
          }
      }
    /*.menu_c{
      color: #bd9250 !important;
    }*/
    /*.accordion-button{
      color: #bd9250 !important;
    }
    .menu-vertical .menu-item .menu-link > div:not(.badge) {
      overflow: hidden;
      text-overflow: initial !important;
      white-space: pre-line !important;
    }
    select{
      text-align: center;
    }
    .table td{
      border-bottom: 1px solid #d9d9d9;
      border-collapse: unset;
    }
    table {
      border-collapse: unset !important;
    }
    .table td.th_c{
      border-bottom: 0px;
    }
    .table textarea{
      font-size: 12px;
    }
    
    }*/
    /*body {
      background: #272726;
    }
    .layout-navbar-fixed .layout-page:not(.window-scrolled) .layout-navbar.navbar-detached {
        background: #272726;
    }
    .bg-menu-theme {
      background-color: #484947 !important;
      color: #d7d8ed;
    }
    .card {
      --bs-card-spacer-y: 1.25rem;
      --bs-card-spacer-x: 1.25rem;
      --bs-card-title-spacer-y: 0.875rem;
      --bs-card-title-color: #11167d;
      --bs-card-subtitle-color: #9698af;
      --bs-card-border-width: 0;
      --bs-card-border-color: #464963;
      --bs-card-border-radius: 0.625rem;
      --bs-card-box-shadow: 0 0.25rem 0.875rem 0 rgba(16, 17, 33, 0.26);
      --bs-card-inner-border-radius: 0.625rem;
      --bs-card-cap-padding-y: 1.25rem;
      --bs-card-cap-padding-x: 1.25rem;
      --bs-card-cap-bg: transparent;
      --bs-card-cap-color: #d7d8ed;
      --bs-card-height: ;
      --bs-card-color: ;
      --bs-card-bg: #30334e;
      --bs-card-img-overlay-padding: 1.25rem;
      --bs-card-group-margin: 1.5rem;
      position: relative;
      display: flex;
      flex-direction: column;
      min-width: 0;
      height: var(--bs-card-height);
      color: var(--bs-body-color);
      word-wrap: break-word;
      background-color: #272726;
      background-clip: border-box;
      border: var(--bs-card-border-width) solid var(--bs-card-border-color);
      border-radius: var(--bs-card-border-radius);
  }
  .bg-menu-theme .menu-link, .bg-menu-theme .menu-horizontal-prev, .bg-menu-theme .menu-horizontal-next {
    color: white;
}*/
  </style> 
  <body id="body">
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
      <div class="layout-container">
        <!-- Menu -->

        