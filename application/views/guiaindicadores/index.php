<div class="container-xxl flex-grow-1 container-p-y">  
  <div class="row my-4">
    <div class="col">
      <h6>Guía de indicadores</h6>
      <form class="form" method="post" role="form" id="form_registro" novalidate>
        <div class="card mb-4">
          <div class="card-body">
            <div class="text_factor">
              <div class="row">
                <div class="col-md-4">
                  <input type="hidden" name="id" id="idfactor" value="0">
                  <div class="form-floating form-floating-outline">
                    <input type="text" class="form-control" id="factor" name="factor" placeholder="" required  />
                    <label for="factor">Factor</label>
                  </div>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>
                </div>  
              </div>  
            </div>
          </div>
        </div>  
      </form>  
      <div class="texto_estrategias"></div>
        
    </div>
  </div>
</div>