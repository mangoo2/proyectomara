<style type="text/css">
  .form-control:focus, .form-select:focus {
    border-color: #bd9250 !important;
  }
</style>
<link href="<?php echo base_url();?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
<div class="container-xxl flex-grow-1 container-p-y">  
  <div class="row my-4">
    <div class="col">
      <h6><?php echo $titulo ?></h6>
      <form class="form" method="post" role="form" id="form_registro" novalidate>
        <input type="hidden" id="idreg" name="personalId" value="<?php echo $personalId ?>">
        <input type="hidden" id="UsuarioID" name="UsuarioID" value="<?php echo $UsuarioID ?>">
        <div class="accordion" id="collapsibleSection">
          <!-- Delivery Address -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="headingDeliveryAddress">
              <button
                class="accordion-button"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseDeliveryAddress"
                aria-expanded="true"
                aria-controls="collapseDeliveryAddress">
                Datos generales
              </button>
            </h2>
            <div
              id="collapseDeliveryAddress"
              class="accordion-collapse collapse show"
              aria-labelledby="headingDeliveryAddress"
              data-bs-parent="#collapsibleSection">
              <div class="accordion-body">
                <div class="row g-3">
                  <div class="col-md-12">
                    <div class="form-floating form-floating-outline mb-4">
                      <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>" placeholder="" required />
                      <label for="nombre">Nombre completo</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge">
                      <span class="input-group-text"><i class="mdi mdi-email-outline"></i></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="correo"
                          name="correo"
                          value="<?php echo $correo ?>"
                          class="form-control"
                          placeholder=""
                          aria-label=""
                          aria-describedby="correo" />
                        <label for="correo">Email</label>
                      </div>
                      <span id="Email" class="input-group-text">@example.com</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="celular" class="input-group-text"
                        ><i class="mdi mdi-cellphone"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="telefono"
                          name="telefono"
                          value="<?php echo $telefono ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="celular" />
                        <label for="telefono">Celular</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <select class="form-select" id="perfilId" name="perfilId">
                        <option selected>Selecciona una opción</option>
                        <?php foreach ($perfil->result() as $x){ ?>
                            <option value="<?php echo $x->perfilId ?>" <?php if($x->perfilId==$perfilId) echo 'selected' ?>><?php echo $x->nombre ?></option>
                        <?php } ?>
                      </select>
                      <label for="regimen_fiscal">Perfil</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                      <h4>Foto</h4>
                      <div class="image-input image-input-outline" id="kt_image_1">
                          <?php if($file==''){ ?>
                              <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>images/avatar.png)"></div>
                          <?php }else { ?>
                              <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>uploads/personal/<?php echo $file ?>) !important;"></div>
                          <?php }?>    
                          <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                              <i class="tf-icons mdi mdi-pencil"></i>
                              <input type="file" id="foto_avatar" accept=".png, .jpg, .jpeg" />
                          </label>
                          <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                              <i class="tf-icons mdi mdi-delete-empty"></i>
                          </span>
                      </div>
                  </div>  
                </div>
              </div>
            </div>
          </div>
          <!-- Payment Method -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="headingPaymentMethod">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapsePaymentMethod"
                aria-expanded="false"
                aria-controls="collapsePaymentMethod">
                Datos de inicio de sesión
              </button>
            </h2>
            <div
              id="collapsePaymentMethod"
              class="accordion-collapse collapse"
              aria-labelledby="headingPaymentMethod"
              data-bs-parent="#collapsibleSection">
              <!-- -->
              <div class="accordion-body">
                  <div class="row g-3">
                    <div class="col-md-6">
                      <div class="row">
                        <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-username"
                          >Usuario</label
                        >
                        <div class="col-sm-9">
                          <input
                            autocomplete="nope"
                            type="text"
                            id="usuario"
                            name="usuario"
                            value="<?php echo $usuario ?>"
                            class="form-control"
                            placeholder="Usuario" 
                            oninput="verificar_usuario()"
                            />
                            <small class="txtusuario" style="color: red;"></small>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                      <div class="row form-password-toggle">
                        <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-password"
                          >Contraseña</label
                        >
                        <div class="col-sm-9">
                          <div class="input-group input-group-merge">
                            <input
                              autocomplete="new-password"
                              type="password"
                              id="contrasena"
                              name="contrasena"
                              value="<?php echo $contrasena ?>"
                              class="form-control"
                              placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                              aria-describedby="formtabs-password2" />
                            <span class="input-group-text cursor-pointer" id="formtabs-password2"
                              ><i class="mdi mdi-eye-off-outline"></i
                            ></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="row form-password-toggle">
                        <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-confirm-password"
                          >Confirmar</label
                        >
                        <div class="col-sm-9">
                          <div class="input-group input-group-merge">
                            <input
                              autocomplete="new-password"
                              type="password"
                              id="contrasena2"
                              name="contrasena2"
                              value="<?php echo $contrasena2 ?>"
                              class="form-control"
                              placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                              aria-describedby="formtabs-confirm-password2" oninput="comfirmar_pass()" />
                            <span class="input-group-text cursor-pointer" id="formtabs-confirm-password2"
                              ><i class="mdi mdi-eye-off-outline"></i
                            ></span>
                          </div><small class="txtpass" style="color: red;"></small>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>  
              <!-- -->
            </div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-sm-9">
            <button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>
            <a href="<?php echo base_url() ?>Usuarios" class="btn btn-outline-secondary">Regresar</a> 
          </div>
        </div>
      </form>  
      
    </div>
  </div>
</div>