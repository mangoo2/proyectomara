<div class="container-xxl flex-grow-1 container-p-y">
  <h4 class="py-3 mb-4">Usuarios de sistema</h4>
  <!-- Product List Table -->
  <div class="card mb-4">
    <div class="card-body">
      <div class="row">
        <div class="col-md-3">
          <a href="<?php echo base_url()?>Usuarios/registro"  class="btn btn-outline-primary waves-effect">
            <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span>Agregar usuario
          </a>
        </div>  
      </div>  
      <br>
      <div class="card-datatable table-responsive">
        <table class="dt-responsive table table-bordered" id="tabla_datos">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre completo</th>
              <th>Email</th>
              <th>Celular</th>
              <th>Perfil</th>
              <th>Usuario</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            <!-- <tr>
              <td>1</td>
              <td>demo</td>
              <td>demo</td>
              <td>demo</td>
              <td>demo</td>
              <td>demo</td>
              <td><div class="demo-inline-spacing">
                <label class="switch switch-square">
                          <input type="checkbox" class="switch-input" />
                          <span class="switch-toggle-slider">
                            <span class="switch-on"></span>
                            <span class="switch-off"></span>
                          </span>
                          <span class="switch-label">Suspender usuario</span>
                        </label>
                          <button type="button" class="btn btn-icon btn-outline-primary">
                            <span class="tf-icons mdi mdi-pencil"></span>
                          </button>
                          <button type="button" class="btn btn-icon btn-outline-secondary">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                          </button>
                        </div></td>
            </tr> -->
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Nombre completo</th>
              <th>Email</th>
              <th>Celular</th>
              <th>Perfil</th>
              <th>Usuario</th>
              <th>Acciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>  
</div>