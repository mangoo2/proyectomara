<input type="hidden" id="idguiaindicadores_estrategiasx" value="<?php echo $idguiaindicadores_estrategias ?>">
<div class="container-xxl flex-grow-1 container-p-y">  
    <div class="row">
      <div class="col-md-12">
        <h6>​​Cronograma de Aterrizaje de la Planeación Estratégica</h6>
      </div>
    </div>
    <div class="card mb-4">
      <div class="card-body">
        <div class="text_macroeconomico">
          <form class="form" method="post" role="form" id="form_registro" novalidate>
            <input type="hidden" id="idreg" name="id" value="<?php echo $id ?>">
            <div class="row">
              <div class="col-md-4">
                <div class="form-floating form-floating-outline mb-4">
                  <select class="form-select" id="idguiaindicadores" name="idguiaindicadores" onchange="get_estrategias()">
                    <option selected>Selecciona una opción</option>
                    <?php foreach ($get_guiaindicadores->result() as $x) { ?> 
                        <option value="<?php echo $x->id;?>" <?php if($x->id==$idguiaindicadores) echo 'selected' ?>><?php echo $x->factor;?></option> 
                    <?php } ?> 
                  </select>
                  <label for="cfdi">Debilidad a cambiar / Fortaleza a mejorar</label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="text_estrategias"></div>
              </div> 
              <div class="col-md-1">
                <div class="text_btn"></div>
              </div>  
            </div>
          </form>
          <!--  -->
          <div class="row">
            <div class="col-md-4">
              <div class="text_plan" style="display: none;">
                <div class="form-floating form-floating-outline mb-4">
                  <input type="text" class="form-control" id="nombre"  />
                  <label for="cfdi">Plan de trabajo General</label>
                </div>
              </div>
            </div>
            <div class="col-md-1">
              <div class="text_btn_plan"></div>
              
            </div>  
          </div>

          <!--  -->  
          <div class="text_plan" style="display: none;">
            <div class="row">
              <div class="col-md-12">
                <h4>Plan de trabajo General: Demo 1 <button type="button" class="btn btn-icon btn-outline-secondary">
                              <span class="tf-icons mdi mdi-delete-empty"></span>
                            </button></h4>
              </div>
              <div class="col-md-3">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="text" class="form-control" id="nombre"  />
                    <label for="cfdi">Plan de trabajo específico</label>
                  </div>
              </div>
              <div class="col-md-3">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="text" class="form-control" id="nombre"  />
                    <label for="cfdi">Responsable</label>
                  </div>
              </div>  
              <div class="col-md-2">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="date" class="form-control" id="nombre"  />
                    <label for="cfdi">Fecha de Inicio</label>
                  </div>
              </div>
              <div class="col-md-2">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="date" class="form-control" id="nombre"  />
                    <label for="cfdi">Fecha de fin</label>
                  </div>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                  <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                </button>
              </div>  
            </div>  
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive text-nowrap">
                  <table class="table table-bordered" id="tabla_datos">
                    <thead>
                      <tr>
                        <th>Plan de trabajo específico</th>
                        <th>Responsable</th>
                        <th>Fecha de Inicio</th>
                        <th>Fecha de fin</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>a) Diversificación de proveedores</td>
                        <td>Arturo Gerardo</td>
                        <td>11/7/2024</td>
                        <td>8/11/2024</td>
                        <td>
                          <button type="button" class="btn btn-icon btn-outline-secondary">
                              <span class="tf-icons mdi mdi-delete-empty"></span>
                            </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>    
            <!--  -->
            <br>
            <div class="row">
              <div class="col-md-12">
                <h4>Plan de trabajo General: Demo 2 <button type="button" class="btn btn-icon btn-outline-secondary">
                              <span class="tf-icons mdi mdi-delete-empty"></span>
                            </button></h4>
              </div>
              <div class="col-md-3">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="text" class="form-control" id="nombre"  />
                    <label for="cfdi">Plan de trabajo específico</label>
                  </div>
              </div>
              <div class="col-md-3">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="text" class="form-control" id="nombre"  />
                    <label for="cfdi">Responsable</label>
                  </div>
              </div>  
              <div class="col-md-2">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="date" class="form-control" id="nombre"  />
                    <label for="cfdi">Fecha de Inicio</label>
                  </div>
              </div>
              <div class="col-md-2">
                <div class="form-floating form-floating-outline mb-4">
                    <input type="date" class="form-control" id="nombre"  />
                    <label for="cfdi">Fecha de fin</label>
                  </div>
              </div>
              <div class="col-md-1">
                <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                  <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                </button>
              </div>  
            </div>  
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive text-nowrap">
                  <table class="table table-bordered" id="tabla_datos">
                    <thead>
                      <tr>
                        <th>Plan de trabajo específico</th>
                        <th>Responsable</th>
                        <th>Fecha de Inicio</th>
                        <th>Fecha de fin</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>a) Diversificación de proveedores</td>
                        <td>Arturo Gerardo</td>
                        <td>11/7/2024</td>
                        <td>8/11/2024</td>
                        <td>
                          <button type="button" class="btn btn-icon btn-outline-secondary">
                              <span class="tf-icons mdi mdi-delete-empty"></span>
                            </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>    
          </div>  
          <!--  -->
        </div>
      </div>
    </div> 
    <div class="row mt-4">
      <div class="col-sm-9"><?php /*if($id!=0){ ?>
        <button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button><?php } */?>
        <a href="#" class="btn btn-outline-secondary">Regresar</a> 
      </div>
    </div>       
</div>    