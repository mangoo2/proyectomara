<script src="<?php echo base_url();?>assets/vendor/libs/fullcalendar/locale-all.min.js"></script><!--para español el fullcalendar-->
<script src="<?php echo base_url();?>assets/vendor/libs/fullcalendar/flatpickr_es.js"></script><!--para español de flatpickr-->
<script src="<?php echo base_url();?>assets/vendor/libs/fullcalendar/fullcalendar.js"></script>
<script src="<?php echo base_url();?>assets/js/app-calendar-events.js"></script>
<script src="<?php echo base_url();?>assets/js/app-calendar.js"></script>
<script src="<?php echo base_url();?>public/js/index_cronograma_actividades.js?v=<?php echo date('YmdGis') ?>"></script>

