<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/libs/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/libs/flatpickr/flatpickr.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/css/pages/app-calendar.css" />
<style type="text/css">
  .img_li{ width: 70px;  }
  .contenedor{
    text-align-last: center;
  }
</style>

<div class="container-xxl flex-grow-1 container-p-y">  
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-sm-row mb-4">
          <li class="nav-item">
            <a class="nav-link link_data link1" onclick="vista_tipo(1)">
              <div class="contenedor">
              <img src="<?php echo base_url().'public/img/6.svg';?>" class="img_li"><br>
              Cronograma de Aterrizaje
              </div>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link link_data link2" onclick="vista_tipo(2)">
              <div class="contenedor">
              <img src="<?php echo base_url().'public/img/7.svg';?>" class="img_li"><br>
              Agenda de actividades
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="vista_txt vista_txt_1" style="display: none">
      <div class="card mb-4">
        <div class="card-body">
          <div class="text_macroeconomico">
            <div class="row">
              <div class="col-md-5">
                <a href="<?php echo base_url()?>Cronograma_actividades/registro" class="btn btn-outline-primary waves-effect">
                  <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span>Agregar Cronograma de Aterrizaje
                </a>
              </div>  
            </div>  
            <br>
            <div class="row">
              <div class="col-sm-12">
                 <div class="card-datatable text-nowrap">
                    <table class="table table-bordered" id="tabla_datos">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Debilidad a cambiar / Fortaleza a mejorar</th>
                          <th>Factor a mejorar</th>
                          <th>Plan de trabajo General</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Captación de Valor</td>
                          <td>Disminuir costos de entrega</td>
                          <td><button type="button" class="btn btn-icon btn-outline-primary" onclick="detalles_aterrizaje(1)">
                              <span class="tf-icons mdi mdi-eye"></span>
                            </button>
                          </td>
                          <td>
                            <div class="demo-inline-spacing">
                              <button type="button" class="btn btn-icon btn-outline-primary" onclick="href_cliente()">
                                <span class="tf-icons mdi mdi-pencil"></span>
                              </button>
                              <button type="button" class="btn btn-icon btn-outline-secondary" onclick="eliminar_registro()">
                                <span class="tf-icons mdi mdi-delete-empty"></span>
                              </button>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>#</th>
                          <th>Debilidad a cambiar / Fortaleza a mejorar</th>
                          <th>Factor a mejorar</th>
                          <th>Plan de trabajo General</th>
                          <th>Acciones</th>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
              </div>
            </div>
          </div>
        </div>  
      </div>
    </div>  

    <div class="vista_txt vista_txt_2">
      <div class="card mb-4">
        <div class="card-body">
              <!--  -->
              <div class="row g-0">
                <div class="col-md-3">
                  <!-- Calendar Sidebar -->
                  <div class="col app-calendar-sidebar border-end" id="app-calendar-sidebar">
                    <div class="p-3 pb-2 my-sm-0 mb-3">
                      <div class="d-grid">
                        <div class="d-grid">
                        <a href="<?php echo base_url() ?>Cronograma_actividades/registro"  class="btn btn-primary btn-toggle-sidebar">
                          <i class="mdi mdi-plus me-1"></i>
                          <span class="align-middle">Nueva actividad</span>
                        </a>
                      </div>
                      </div>
                    </div>
                    <div class="p-4">
                      <!-- inline calendar (flatpicker) -->
                      <div class="inline-calendar"></div>

                      <hr class="container-m-nx my-4" />

                      <!-- Filter -->
                      <div class="mb-4">
                        <small class="text-small text-muted text-uppercase align-middle">Filter</small>
                      </div>

                      <div class="form-check form-check-secondary mb-3">
                        <input
                          class="form-check-input select-all"
                          type="checkbox"
                          id="selectAll"
                          data-value="all"
                          checked />
                        <label class="form-check-label" for="selectAll">View All</label>
                      </div>

                      <div class="app-calendar-events-filter">
                        <div class="form-check form-check-danger mb-3">
                          <input
                            class="form-check-input input-filter"
                            type="checkbox"
                            id="select-personal"
                            data-value="personal"
                            checked />
                          <label class="form-check-label" for="select-personal">Personal</label>
                        </div>
                        <div class="form-check mb-3">
                          <input
                            class="form-check-input input-filter"
                            type="checkbox"
                            id="select-business"
                            data-value="business"
                            checked />
                          <label class="form-check-label" for="select-business">Business</label>
                        </div>
                        <div class="form-check form-check-warning mb-3">
                          <input
                            class="form-check-input input-filter"
                            type="checkbox"
                            id="select-family"
                            data-value="family"
                            checked />
                          <label class="form-check-label" for="select-family">Family</label>
                        </div>
                        <div class="form-check form-check-success mb-3">
                          <input
                            class="form-check-input input-filter"
                            type="checkbox"
                            id="select-holiday"
                            data-value="holiday"
                            checked />
                          <label class="form-check-label" for="select-holiday">Holiday</label>
                        </div>
                        <div class="form-check form-check-info">
                          <input
                            class="form-check-input input-filter"
                            type="checkbox"
                            id="select-etc"
                            data-value="etc"
                            checked />
                          <label class="form-check-label" for="select-etc">ETC</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /Calendar Sidebar -->
                </div>
                <div class="col-md-9">
                  <!-- Calendar & Modal -->
                  <div class="col app-calendar-content">
                    <div class="card shadow-none border-0">
                      <div class="card-body pb-0">
                        <!-- FullCalendar -->
                        <div id="calendar"></div>
                      </div>
                    </div>
                    <div class="app-overlay"></div>
                    <!-- FullCalendar Offcanvas -->
                    <div
                      class="offcanvas offcanvas-end event-sidebar"
                      tabindex="-1"
                      id="addEventSidebar"
                      aria-labelledby="addEventSidebarLabel">

                    </div>
                  </div>

                  <!-- /Calendar & Modal -->
                </div>
              <!--  -->
              </div>
        </div>  
      </div>
    </div>  
</div>  

<div class="modal fade" id="aterrizajeModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1">Detalles de Cronograma de Aterrizaje</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
        <div class="modal-body">
          <div class="table-responsive text-nowrap">
            <table class="table table-bordered" id="tabla_datos">
              <thead>
                <tr>
                  <th>Plan de trabajo General</th>
                  <th>Plan de trabajo específico</th>
                  <th>Responsable</th>
                  <th>Fecha de Inicio</th>
                  <th>Tiempo estimado para concluir el objetivo especifico</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>a) Diversificación de proveedores</td>
                  <td>Buscar proveedores de madera</td>
                  <td>Arturo Gerardo</td>
                  <td>11/7/2024</td>
                  <td>8/11/2024</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <th>Plan de trabajo General</th>
                  <th>Plan de trabajo específico</th>
                  <th>Responsable</th>
                  <th>Fecha de Inicio</th>
                  <th>Tiempo estimado para concluir el objetivo especifico</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cerrar</button>
          <!--<button type="submit" class="btn btn-primary btn_registro">Agregar</button>-->
        </div>
        
    </div>
  </div>
</div>