<style type="text/css">
  #table_datos_filter{
    display: none;
  }

</style>
<div class="container-xxl flex-grow-1 container-p-y">
  <h4 class="py-3 mb-4">Clientes</h4>
  <!-- Product List Table -->
  <div class="card mb-4">
    <div class="card-body">
      <div class="row">
        <div class="col-md-3">
          <a href="<?php echo base_url()?>Cliente/registro" class="btn btn-outline-primary waves-effect">
            <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span>Agregar cliente
          </a>
        </div>  
      </div>  
      <br>
      <div class="row g-3" style="display: none">
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml1" oninput="reload_registro()" />
            <label for="coml1">Nombre de contacto</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <select class="form-select" id="coml2" onchange="reload_registro()">
              <option selected value="0">Todos</option>
              <?php foreach ($get_periodo->result() as $x) { ?> 
                  <option value="<?php echo $x->id;?>"><?php echo $x->nombre;?></option> 
              <?php } ?> 
            </select>
            <label for="coml2">Curso</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml3" oninput="reload_registro()" />
            <label for="coml3">Email</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml4" oninput="reload_registro()" />
            <label for="coml4">Teléfono</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml5" oninput="reload_registro()" />
            <label for="coml5">Celular</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml6" oninput="reload_registro()" />
            <label for="coml6">Razón social</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml7" oninput="reload_registro()" />
            <label for="coml7">Giro de empresa</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="coml8" oninput="reload_registro()" />
            <label for="coml8">Usuario</label>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-floating form-floating-outline mb-4">
            <select class="form-select" id="coml9" onchange="reload_registro()">
              <option value="2">Todos</option>
              <option value="1">Activo</option>
              <option value="0">Suspendido</option>
            </select>
            <label for="coml9">Estatus</label>
          </div>
        </div>
      </div>  
      <div class="card-datatable text-nowrap">
        <table class="table table-bordered" id="tabla_datos">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre de contacto</th>
              <th>curso</th>
              <th>Email</th>
              <th>Teléfono</th>
              <th>Celular</th>
              <th>Razón social</th>
              <th>Giro del a empresa</th>
              <th>Usuario</th>
              <th>Último inicio de sesión</th>
              <th>Status</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th>#</th>
              <th>Nombre de contacto</th>
              <th>curso</th>
              <th>Email</th>
              <th>Teléfono</th>
              <th>Celular</th>
              <th>Razón social</th>
              <th>Giro del a empresa</th>
              <th>Usuario</th>
              <th>Último inicio de sesión</th>
              <th>Status</th>
              <th>Acciones</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>  
</div>