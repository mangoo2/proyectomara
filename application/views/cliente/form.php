<style type="text/css">
  input:-webkit-autofill {
    -webkit-text-fill-color: white; /* Color del texto */    
  }
  .form-control:focus, .form-select:focus {
    border-color: #bd9250 !important;
  }
</style>
<input type="hidden" id="regimen_fiscalx" value="<?php echo $regimen_fiscal ?>">
<input type="hidden" id="UsuarioID" value="<?php echo $UsuarioID ?>">
<div class="container-xxl flex-grow-1 container-p-y">  
  <div class="row my-4">
    <div class="col">
      <h6>Alta de nuevo cliente</h6>
      <form class="form" method="post" role="form" id="form_registro" novalidate>
        <input type="hidden" id="idreg" name="id" value="<?php echo $id ?>">
        <div class="accordion" id="collapsibleSection">
          <!-- Delivery Address -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="headingDeliveryAddress">
              <button
                class="accordion-button"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseDeliveryAddress"
                aria-expanded="true"
                aria-controls="collapseDeliveryAddress">
                Datos generales
              </button>
            </h2>
            <div
              id="collapseDeliveryAddress"
              class="accordion-collapse collapse show"
              aria-labelledby="headingDeliveryAddress"
              data-bs-parent="#collapsibleSection">
              <div class="accordion-body">
                <div class="row g-3">
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>" placeholder="" required  />
                      <label for="nombre">Nombre de contacto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <input type="text" class="form-control" id="ap_paterno" name="ap_paterno" value="<?php echo $ap_paterno ?>" placeholder="" />
                      <label for="ap_paterno">Apellido paterno</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <input type="text" class="form-control" id="ap_materno" name="ap_materno" value="<?php echo $ap_materno ?>" placeholder="" />
                      <label for="ap_materno">Apellido materno</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge">
                      <span class="input-group-text"><i class="mdi mdi-email-outline"></i></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="email"
                          id="email"
                          name="email"
                          class="form-control"
                          placeholder=""
                          aria-label=""
                          aria-describedby="Email" value="<?php echo $email ?>" />
                        <label for="email">Email</label>
                      </div>
                      <span id="Email" class="input-group-text">@example.com</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-phone"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="number"
                          id="telefono"
                          name="telefono"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="telefono" value="<?php echo $telefono ?>" />
                        <label for="telefono">Teléfono</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="celular" class="input-group-text"
                        ><i class="mdi mdi-cellphone"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="number"
                          id="celular"
                          name="celular"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="celular" value="<?php echo $celular ?>" />
                        <label for="celular">Celular</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <input
                          type="text"
                          id="fecha_operacion"
                          name="fecha_operacion"
                          value="<?php echo $fecha_operacion ?>"
                          class="form-control dob-picker"
                          placeholder="YYYY-MM-DD" />
                      <label for="fecha_operacion">Fecha de inicio de operaciones en sistema</label>
                    </div>
                  </div>
                  <div class="col-md-4">
           
                    <div class="text-light small fw-medium mb-3">Suspender cliente</div>
                    <label class="switch switch-square">
                      <input type="checkbox" class="switch-input" id="suspender" name="suspender" <?php if($suspender==0) echo 'checked'; ?> />
                      <span class="switch-toggle-slider">
                        <span class="switch-on"></span>
                        <span class="switch-off"></span>
                      </span>
                      <span class="switch-label"></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Delivery Options -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="headingDeliveryOptions">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseDeliveryOptions"
                aria-expanded="false"
                aria-controls="collapseDeliveryOptions">
                Datos de empresa
              </button>
            </h2>
            <div
              id="collapseDeliveryOptions"
              class="accordion-collapse collapse"
              aria-labelledby="headingDeliveryOptions"
              data-bs-parent="#collapsibleSection">
              <div class="accordion-body">
                <div class="row">
                  <div class="col-md mb-md-0 mb-2">
                    <div class="form-check custom-option custom-option-basic">
                      <label class="form-check-label custom-option-content" for="tipo_persona1">
                        <input
                          name="tipo_persona"
                          class="form-check-input"
                          type="radio"
                          value="1"
                          id="tipo_persona1"
                          <?php if($tipo_persona==1) echo 'checked'; ?> />
                        <span class="custom-option-header">
                          <span class="h6 mb-0">Persona Física</span>
                        </span>
                      </label>
                    </div>
                  </div>
                  <div class="col-md mb-md-0 mb-2">
                    <div class="form-check custom-option custom-option-basic">
                      <label class="form-check-label custom-option-content" for="tipo_persona2">
                        <input
                          name="tipo_persona"
                          class="form-check-input"
                          type="radio"
                          value="2"
                          id="tipo_persona2" <?php if($tipo_persona==2) echo 'checked'; ?> />
                        <span class="custom-option-header">
                          <span class="h6 mb-0">Persona Moral</span>
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group input-group-merge mb-4">
                      <span id="razon_social" class="input-group-text"
                        ><i class="mdi mdi-account"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="razon_social"
                          name="razon_social"
                          value="<?php echo $razon_social ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="telefono" />
                        <label for="razon_social">Razón social</label>
                      </div>
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <input type="text" class="form-control" id="rfc" name="rfc" value="<?php echo $rfc ?>" placeholder="" />
                      <label for="rfc">RFC</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <select class="form-select" id="regimen_fiscal" name="regimen_fiscal" onchange="v_rf()">
                        <option selected>Selecciona una opción</option>
                        <option value="601" >601 General de Ley Personas Morales</option>
                        <option value="603" >603 Personas Morales con Fines no Lucrativos</option>
                        <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                        <option value="606" >606 Arrendamiento</option>
                        <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>
                        <option value="608" >608 Demás ingresos</option>
                        <option value="609" >609 Consolidación</option>
                        <option value="610" >610 Residentes en el Extranjero sin Establecimiento Pe</option>
                        <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>
                        <option value="612" >612 Personas Físicas con Actividades Empresariales</option>
                        <option value="614" >614 Ingresos por intereses</option>
                        <option value="615" >615 Régimen de los ingresos por obtención de premios</option>
                        <option value="616" >616 Sin obligaciones fiscales</option>
                        <option value="620" >620 Sociedades Cooperativas de Producción que optaingresos</option>
                        <option value="621" >621 Incorporación Fiscal</option>
                        <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                        <option value="623" >623 Opcional para Grupos de Sociedades</option>
                        <option value="624" >624 Coordinados</option>
                        <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                        <option value="626" >626 Régimen Simplificado de Confianza</option>
                        <option value="628" >628 Hidrocarburos</option>
                        <option value="629" >629 De los Regímenes Fiscales Preferentes Multinacionales</option>
                        <option value="630" >630 Enajenación de acciones en bolsa de valores</option>      
                      </select>
                      <label for="regimen_fiscal">Régimen Fiscal</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <select class="form-select" id="cfdi" name="cfdi">
                        <option selected>Selecciona una opción</option>
                        <?php foreach ($uso_cfdi->result() as $item) { ?> 
                            <option value="<?php echo $item->uso_cfdi;?>" class="pararf <?php echo str_replace(',',' ',$item->pararf) ?>" <?php if($item->uso_cfdi==$cfdi) echo 'selected' ?> disabled ><?php echo $item->uso_cfdi_text;?></option> 
                        <?php } ?> 
                      </select>
                      <label for="cfdi">Uso de CFDI</label>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-counter"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="number"
                          id="codigo_postal"
                          name="codigo_postal"
                          value="<?php echo $codigo_postal ?>"
                          class="form-control"
                          placeholder=""
                          aria-label=""
                          aria-describedby="codigo_postal" oninput="cambiaCP()" />
                        <label for="codigo_postal">Código postal</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-map-search"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="calle_numero"
                          name="calle_numero"
                          value="<?php echo $calle_numero ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="calle_numero" />
                        <label for="calle_numero">Calle y número</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-map-search"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="municipio"
                          name="municipio"
                          value="<?php echo $municipio ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="municipio" />
                        <label for="municipio">Municipio</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-map-search"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <select class="form-select" id="colonia" name="colonia">
                          <option selected disabled="">Selecciona una opción</option>
                          <?php if($colonia!=''){
                            echo '<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                          }
                          ?>
                        </select>
                        <label for="basic-icon-default-phone">Colonia</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-map-search"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="ciudad"
                          name="ciudad"
                          value="<?php echo $ciudad ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="ciudad" />
                        <label for="ciudad">Ciudad</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group input-group-merge mb-4">
                      <span id="telefono" class="input-group-text"
                        ><i class="mdi mdi-map-search"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="estado"
                          name="estado"
                          value="<?php echo $estado ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="estado" />
                        <label for="estado">Estado</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group input-group-merge mb-4">
                      <span id="razon_social" class="input-group-text"
                        ><i class="mdi mdi-account"></i
                      ></span>
                      <div class="form-floating form-floating-outline">
                        <input
                          type="text"
                          id="giro"
                          name="giro"
                          value="<?php echo $giro ?>"
                          class="form-control phone-mask"
                          placeholder=""
                          aria-label=""
                          aria-describedby="giro" />
                        <label for="giro">Giro de la empresa</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Payment Method -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="headingPaymentMethod">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapsePaymentMethod"
                aria-expanded="false"
                aria-controls="collapsePaymentMethod">
                Datos de inicio de sesión
              </button>
            </h2>
            <div
              id="collapsePaymentMethod"
              class="accordion-collapse collapse"
              aria-labelledby="headingPaymentMethod"
              data-bs-parent="#collapsibleSection">
              <!-- -->
              <div class="accordion-body">
                <div class="row g-3">
                  <div class="col-md-6">
                    <div class="row">
                      <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-username"
                        >Usuario</label
                      >
                      <div class="col-sm-9">
                        <input
                          type="text"
                          id="usuario"
                          name="usuario"
                          value="<?php echo $usuario ?>"
                          class="form-control"
                          placeholder="" autocomplete="nope" oninput="verificar_usuario()" required />
                          <small class="txtusuario" style="color: red;"></small>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="text-light small fw-medium mb-3">¿Desea enviar por correo electrónico los accesos al cliente?</div>
                    <label class="switch switch-square">
                      <input type="checkbox" class="switch-input" id="correo_enviar" name="correo_enviar" />
                      <span class="switch-toggle-slider">
                        <span class="switch-on"></span>
                        <span class="switch-off"></span>
                      </span>
                      <span class="switch-label"></span>
                    </label>
                  </div>
                  <div class="col-md-6">
                    <div class="row form-password-toggle">
                      <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-password"
                        >Contraseña</label
                      >
                      <div class="col-sm-9">
                        <div class="input-group input-group-merge">
                          <input
                            type="password"
                            id="contrasena"
                            name="contrasena"
                            value="<?php echo $contrasena ?>"
                            class="form-control"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="formtabs-password2" required />
                          
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row form-password-toggle">
                      <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-confirm-password"
                        >Confirmar</label
                      >
                      <div class="col-sm-9">
                        <div class="input-group input-group-merge">
                          <input
                            type="password"
                            id="contrasena2"
                            name="contrasena2"
                            value="<?php echo $contrasena2 ?>"
                            class="form-control"
                            placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                            aria-describedby="formtabs-confirm-password2" required oninput="comfirmar_pass()" />
                          

                        </div><small class="txtpass" style="color: red;"></small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
              <!-- -->
            </div>
          </div>
          <!-- Payment Method -->
          <div class="card accordion-item">
            <h2 class="accordion-header" id="form_enlazar">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#formu_enlazar"
                aria-expanded="false"
                aria-controls="formu_enlazar">
                Enlazar Curso/Programa
              </button>
            </h2>
            <div
              id="formu_enlazar"
              class="accordion-collapse collapse"
              aria-labelledby="form_enlazar"
              data-bs-parent="#collapsibleSection">
              <!-- -->
              <div class="accordion-body">

                <?php /*
                <div class="row g-3">
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <select class="form-select" id="idperiodo" name="idperiodo">
                        <option selected value="0">Selecciona una opción</option>
                        <?php foreach ($get_periodo->result() as $x) { ?> 
                            <option value="<?php echo $x->id;?>" <?php if($x->id==$idperiodo) echo 'selected' ?>><?php echo $x->nombre;?></option> 
                        <?php } ?> 
                      </select>
                      <label for="idperiodo">Seleccionar Curso/Programa</label>
                    </div>
                  </div>
                </div>
                */ ?>

                <div class="row g-3">
                  <div class="col-md-4">
                    <div class="form-floating form-floating-outline mb-4">
                      <select class="form-select" id="idmenu" onchange="get_menu()">
                        <option selected value="0">Selecciona una opción</option>
                        <?php foreach ($get_menu as $m) { ?> 
                            <option value="<?php echo $m->idmenu;?>"><?php echo $m->Nombre;?></option> 
                        <?php } ?> 
                      </select>
                      <label for="idperiodo">Seleccionar Menú</label>
                    </div>
                  </div>
                  
                </div>
                <div class="menu_txt"> 
                </div>  
              </div>  
              <!-- -->
            </div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-sm-9">
            <button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>
            <a href="<?php echo base_url() ?>Cliente" class="btn btn-outline-secondary">Regresar</a> 
          </div>
        </div>
      </form>
      
    </div>
  </div>
</div>

<div class="modal fade" id="modal_agregar_correo" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1">!Atención¡</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <h4>Es necesario ingresar un correo electrónico para poder enviar los accesos por correo</h4>
            </div>
          </div>
          <div class="row g-3">
            <div class="col-md-12">
              <div class="form-floating form-floating-outline mb-4">
                <input type="email" class="form-control" id="correo_validar" value="" placeholder=""  />
                <label for="nombre">Agregar correo</label>
              </div>
            </div>
          </div>   
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cerrar</button>
          <button class="btn btn-primary" onclick="agregar_correo()">Agregar</button>
        </div>
        
    </div>
  </div>
</div>
