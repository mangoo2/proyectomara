
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/pages/page-profile.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/typeahead-js/typeahead.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/flatpickr/flatpickr.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/select2/select2.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/quill/editor.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/@form-validation/form-validation.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/pages/app-calendar.css" />

<style type="text/css">
  .image-input {
    position: relative;
    display: inline-block;
    border-radius: 0.42rem;
    background-repeat: no-repeat;
    background-size: cover;
  }

  .image-input.image-input-outline .image-input-wrapper {
    border: 3px solid #ffffff;
    -webkit-box-shadow: 0 0.5rem 1.5rem 0.5rem rgba(0, 0, 0, 0.075);
    box-shadow: 0 0.5rem 1.5rem 0.5rem rgba(0, 0, 0, 0.075);
  }
  .image-input.image-input-outline .image-input-wrapper {
      border: 3px solid #ffffff;
      -webkit-box-shadow: 0 0.5rem 1.5rem 0.5rem rgba(0, 0, 0, 0.075);
      box-shadow: 0 0.5rem 1.5rem 0.5rem rgba(0, 0, 0, 0.075);
  }
  .image-input .image-input-wrapper {
      width: 120px;
      height: 120px;
      border-radius: 0.42rem;
      background-repeat: no-repeat;
      background-size: cover;
  }
  
  .btn-white {
    color: #181C32;
    background-color: #ffffff;
    border-color: #ffffff;
    -webkit-box-shadow: none;
    box-shadow: none;
}

  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn.btn-icon {
      display: -webkit-inline-box;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      padding: 0;
      height: calc(1.5em + 1.3rem + 2px);
      width: calc(1.5em + 1.3rem + 2px);
  }
  .btn.btn-white {
      color: #3F4254;
      background-color: #ffffff;
      border-color: #ffffff;
  }
  .image-input [data-action=change] {
      cursor: pointer;
      position: absolute;
      right: -10px;
      top: -10px;
  }
  .image-input [data-action=change] {
      cursor: pointer;
      position: absolute;
      right: -10px;
      top: -10px;
  }

  .btn.btn-white {
      color: #3F4254;
      background-color: #ffffff;
      border-color: #ffffff;
  }
  .btn.btn-icon {
      display: -webkit-inline-box;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      padding: 0;
      height: calc(1.5em + 1.3rem + 2px);
      width: calc(1.5em + 1.3rem + 2px);
  }
  .btn:not(.btn-text) {
      cursor: pointer;
  }
  .btn-icon.btn-xs, .btn-group-xs > .btn-icon.btn {
      width: calc(1.074rem + calc(1px* 2));
      height: calc(1.074rem + calc(1px* 2));
      font-size: 0.75rem;
  }
  label.btn {
      margin-bottom: 0;
  }
  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-white.btn-shadow {
      -webkit-box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
      box-shadow: 0px 9px 16px 0px rgba(24, 28, 50, 0.25) !important;
  }
  .btn.btn-icon.btn-circle {
      border-radius: 50%;
  }
  .btn.btn-icon.btn-xs {
      height: 24px;
      width: 24px;
  }
  .btn:not(:disabled):not(.disabled) {
      cursor: pointer;
  }
  .btn.btn-icon {
      display: -webkit-inline-box;
      display: -ms-inline-flexbox;
      display: inline-flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      padding: 0;
      height: calc(1.5em + 1.3rem + 2px);
      width: calc(1.5em + 1.3rem + 2px);
  }
  .btn.btn-white {
      color: #3F4254;
      background-color: #ffffff;
      border-color: #ffffff;
  }
  .image-input [data-action=cancel] {
      display: none;
  }
  .image-input [data-action=cancel], .image-input [data-action=remove] {
      position: absolute;
      right: -10px;
      bottom: -5px;
  }
  .btn.btn-white {
      color: #3F4254;
      background-color: #ffffff;
      
   }

  .btn.btn-white i {
    color: #3F4254;
  }

  .btn.btn-icon i {
      padding: 0;
      margin: 0;
  }
  .btn.btn-xs i {
      font-size: 1rem;
      padding-right: 0.3rem;
  }
  .btn i {
      font-size: 1.3rem;
      padding-right: 0.35rem;
      vertical-align: middle;
      line-height: 1;
      display: -webkit-inline-box;
      display: -ms-inline-flexbox;
      display: inline-flex;
  }

</style>
<input type="hidden" id="dia" value="<?php echo $cl->fecha_operacion; ?>">

<input type="hidden" id="regimen_fiscalx" value="<?php echo $regimen_fiscal ?>">
<input type="hidden" id="UsuarioID" value="<?php echo $UsuarioID ?>">

<div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="py-3 mb-4"><span class="text-muted fw-light">Perfil de usuario /</span><?php echo $this->session->userdata('usuario'); ?></h4>

              <!-- Header -->
              <div class="row">
                <div class="col-12">
                  <div class="card mb-4">
                    <div class="user-profile-header-banner">
                      <img src="<?php echo base_url();?>assets/img/pages/profile-banner.png" alt="Banner image" class="rounded-top" />
                    </div>
                    <div class="user-profile-header d-flex flex-column flex-sm-row text-sm-start text-center mb-4">
                      <div class="flex-shrink-0 mt-n2 mx-sm-0 mx-auto">
                        
                          <?php if($foto==''){ ?>
                            <img
                              src="<?php echo base_url();?>assets/img/avatars/1.png"
                              alt="user image"
                              class="d-block h-auto ms-0 ms-sm-4 rounded user-profile-img" />
                          <?php }else { ?>
                            <img
                              src="<?php echo base_url() ?>uploads/cliente/<?php echo $foto ?>"
                              alt="user image"
                              class="d-block h-auto ms-0 ms-sm-4 rounded user-profile-img" />
                          <?php }?> 
                      </div>
                      <div class="flex-grow-1 mt-3 mt-sm-5">
                        <div
                          class="d-flex align-items-md-end align-items-sm-start align-items-center justify-content-md-between justify-content-start mx-4 flex-md-row flex-column gap-4">
                          <div class="user-profile-info">
                            <h4><?php if($cl->razon_social==''){ echo $cl->nombre.' '.$cl->ap_paterno.' '.$cl->ap_materno; }else{ echo $cl->razon_social; } ?></h4>
                            <ul
                              class="list-inline mb-0 d-flex align-items-center flex-wrap justify-content-sm-start justify-content-center gap-2">
                              <li class="list-inline-item">
                                <i class="mdi mdi-calendar-blank-outline me-1 mdi-20px"></i
                                ><span class="fw-medium fecha_tex"></span>
                              </li>
                            </ul>
                          </div>
                          <a href="javascript:void(0)" class="btn btn-success">
                            <i class="mdi mdi-account-check-outline me-1"></i>Conectado
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!--/ Header -->

              <!-- Navbar pills -->
                <div class="row">
                  <div class="col-md-12">
                    <ul class="nav nav-pills flex-column flex-sm-row mb-4">
                      <li class="nav-item">
                        <a class="nav-link link_data link1" onclick="vista_tipo(1)" 
                          ><i class="mdi mdi-account-outline me-1 mdi-20px"></i>Datos generales</a
                        >
                      </li>
                      <li class="nav-item ">
                        <a class="nav-link link_data link2" onclick="vista_tipo(2)"
                          ><i class="mdi mdi-account-multiple-outline me-1 mdi-20px"></i>Datos de empresa</a
                        >
                      </li>
                      <li class="nav-item">
                        <a class="nav-link link_data link3" onclick="vista_tipo(3)"
                          ><i class="mdi mdi-view-grid-outline me-1 mdi-20px"></i>Datos de inicio de sesión</a
                        >
                      </li>
                      <!-- <li class="nav-item">
                        <a class="nav-link link_data link4" onclick="vista_tipo(4)"
                          ><i class="mdi mdi-link me-1 mdi-20px"></i>Mis usuarios</a
                        >
                      </li>
                      <li class="nav-item">
                        <a class="nav-link link_data link5" onclick="vista_tipo(5)"
                          ><i class="mdi mdi-link me-1 mdi-20px"></i>Mi Agenda</a
                        >
                      </li> -->
                    </ul>
                  </div>
                </div>
                <form class="form" method="post" role="form" id="form_registro" novalidate>
                  <!--/ Navbar pills -->
                  <div class="vista_txt vista_txt_1" style="display: none">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="accordion" >
                          <!-- Delivery Address -->
                          <div class="card accordion-item">
                            <br>
                            <div>
                              <div class="accordion-body">
                                <!--  -->
                                  <div class="row g-3">
                                    <div class="col-md-4">
                                      <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>" placeholder="" required  />
                                        <label for="nombre">Nombre de contacto</label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control" id="ap_paterno" name="ap_paterno" value="<?php echo $ap_paterno ?>" placeholder="" />
                                        <label for="ap_paterno">Apellido paterno</label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-floating form-floating-outline mb-4">
                                        <input type="text" class="form-control" id="ap_materno" name="ap_materno" value="<?php echo $ap_materno ?>" placeholder="" />
                                        <label for="ap_materno">Apellido materno</label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="input-group input-group-merge">
                                        <span class="input-group-text"><i class="mdi mdi-email-outline"></i></span>
                                        <div class="form-floating form-floating-outline">
                                          <input
                                            type="email"
                                            id="email"
                                            name="email"
                                            class="form-control"
                                            placeholder=""
                                            aria-label=""
                                            aria-describedby="Email" value="<?php echo $email ?>" />
                                          <label for="email">Email</label>
                                        </div>
                                        <span id="Email" class="input-group-text">@example.com</span>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="input-group input-group-merge mb-4">
                                        <span id="telefono" class="input-group-text"
                                          ><i class="mdi mdi-phone"></i
                                        ></span>
                                        <div class="form-floating form-floating-outline">
                                          <input
                                            type="number"
                                            id="telefono"
                                            name="telefono"
                                            class="form-control phone-mask"
                                            placeholder=""
                                            aria-label=""
                                            aria-describedby="telefono" value="<?php echo $telefono ?>" />
                                          <label for="telefono">Teléfono</label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="input-group input-group-merge mb-4">
                                        <span id="celular" class="input-group-text"
                                          ><i class="mdi mdi-cellphone"></i
                                        ></span>
                                        <div class="form-floating form-floating-outline">
                                          <input
                                            type="number"
                                            id="celular"
                                            name="celular"
                                            class="form-control phone-mask"
                                            placeholder=""
                                            aria-label=""
                                            aria-describedby="celular" value="<?php echo $celular ?>" />
                                          <label for="celular">Celular</label>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-floating form-floating-outline mb-4">
                                        <input
                                            type="text"
                                            id="fecha_operacion"
                                            name="fecha_operacion"
                                            value="<?php echo $fecha_operacion ?>"
                                            class="form-control dob-picker"
                                            placeholder="YYYY-MM-DD" />
                                        <label for="fecha_operacion">Fecha de inicio de operaciones en sistema</label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <h4>Foto</h4>
                                      <div class="image-input image-input-outline" id="kt_image_1">
                                          <?php if($foto==''){ ?>
                                              <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>images/avatar.png)"></div>
                                          <?php }else { ?>
                                              <div class="image-input-wrapper" style="background-image: url(<?php echo base_url() ?>uploads/cliente/<?php echo $foto ?>) !important;"></div>
                                          <?php }?>    
                                          <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                              <i class="tf-icons mdi mdi-pencil" style="position: absolute; background: white;"></i>
                                              <input type="file" id="foto_avatar" accept=".png, .jpg, .jpeg" />
                                          </label>
                                          <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                              <i class="tf-icons mdi mdi-delete-empty"></i>
                                          </span>
                                      </div>
                                    </div>  
                                  </div>  
                                <!--  -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="vista_txt vista_txt_2" style="display: none">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="accordion" >
                          <!-- Delivery Address -->
                          <div class="card accordion-item">
                            <br>
                            <!--  -->
                            <div class="accordion-body">
                              <div class="row">
                                <div class="col-md mb-md-0 mb-2">
                                  <div class="form-check custom-option custom-option-basic">
                                    <label class="form-check-label custom-option-content" for="tipo_persona1">
                                      <input
                                        name="tipo_persona"
                                        class="form-check-input"
                                        type="radio"
                                        value="1"
                                        id="tipo_persona1"
                                        <?php if($tipo_persona==1) echo 'checked'; ?> />
                                      <span class="custom-option-header">
                                        <span class="h6 mb-0">Persona Física</span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                                <div class="col-md mb-md-0 mb-2">
                                  <div class="form-check custom-option custom-option-basic">
                                    <label class="form-check-label custom-option-content" for="tipo_persona2">
                                      <input
                                        name="tipo_persona"
                                        class="form-check-input"
                                        type="radio"
                                        value="2"
                                        id="tipo_persona2" <?php if($tipo_persona==2) echo 'checked'; ?> />
                                      <span class="custom-option-header">
                                        <span class="h6 mb-0">Persona Moral</span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                              </div>
                              <br>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="razon_social" class="input-group-text"
                                      ><i class="mdi mdi-account"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="razon_social"
                                        name="razon_social"
                                        value="<?php echo $razon_social ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="telefono" />
                                      <label for="razon_social">Razón social</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <br>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="rfc" name="rfc" value="<?php echo $rfc ?>" placeholder="" />
                                    <label for="rfc">RFC</label>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <select class="form-select" id="regimen_fiscal" name="regimen_fiscal" onchange="v_rf()">
                                      <option selected>Selecciona una opción</option>
                                      <option value="601" >601 General de Ley Personas Morales</option>
                                      <option value="603" >603 Personas Morales con Fines no Lucrativos</option>
                                      <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                                      <option value="606" >606 Arrendamiento</option>
                                      <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>
                                      <option value="608" >608 Demás ingresos</option>
                                      <option value="609" >609 Consolidación</option>
                                      <option value="610" >610 Residentes en el Extranjero sin Establecimiento Pe</option>
                                      <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>
                                      <option value="612" >612 Personas Físicas con Actividades Empresariales</option>
                                      <option value="614" >614 Ingresos por intereses</option>
                                      <option value="615" >615 Régimen de los ingresos por obtención de premios</option>
                                      <option value="616" >616 Sin obligaciones fiscales</option>
                                      <option value="620" >620 Sociedades Cooperativas de Producción que optaingresos</option>
                                      <option value="621" >621 Incorporación Fiscal</option>
                                      <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                                      <option value="623" >623 Opcional para Grupos de Sociedades</option>
                                      <option value="624" >624 Coordinados</option>
                                      <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                                      <option value="626" >626 Régimen Simplificado de Confianza</option>
                                      <option value="628" >628 Hidrocarburos</option>
                                      <option value="629" >629 De los Regímenes Fiscales Preferentes Multinacionales</option>
                                      <option value="630" >630 Enajenación de acciones en bolsa de valores</option>      
                                    </select>
                                    <label for="regimen_fiscal">Régimen Fiscal</label>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <select class="form-select" id="cfdi" name="cfdi">
                                      <option selected>Selecciona una opción</option>
                                      <?php foreach ($uso_cfdi->result() as $item) { ?> 
                                          <option value="<?php echo $item->uso_cfdi;?>" class="pararf <?php echo str_replace(',',' ',$item->pararf) ?>" <?php if($item->uso_cfdi==$cfdi) echo 'selected' ?> disabled ><?php echo $item->uso_cfdi_text;?></option> 
                                      <?php } ?> 
                                    </select>
                                    <label for="cfdi">Uso de CFDI</label>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-counter"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="number"
                                        id="codigo_postal"
                                        name="codigo_postal"
                                        value="<?php echo $codigo_postal ?>"
                                        class="form-control"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="codigo_postal" oninput="cambiaCP()" />
                                      <label for="codigo_postal">Código postal</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-map-search"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="calle_numero"
                                        name="calle_numero"
                                        value="<?php echo $calle_numero ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="calle_numero" />
                                      <label for="calle_numero">Calle y número</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-map-search"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="municipio"
                                        name="municipio"
                                        value="<?php echo $municipio ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="municipio" />
                                      <label for="municipio">Municipio</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-map-search"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <select class="form-select" id="colonia" name="colonia">
                                        <option selected disabled="">Selecciona una opción</option>
                                        <?php if($colonia!=''){
                                          echo '<option value="'.$colonia.'" selected>'.$colonia.'</option>';
                                        }
                                        ?>
                                      </select>
                                      <label for="basic-icon-default-phone">Colonia</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-map-search"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="ciudad"
                                        name="ciudad"
                                        value="<?php echo $ciudad ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="ciudad" />
                                      <label for="ciudad">Ciudad</label>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="telefono" class="input-group-text"
                                      ><i class="mdi mdi-map-search"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="estado"
                                        name="estado"
                                        value="<?php echo $estado ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="estado" />
                                      <label for="estado">Estado</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="input-group input-group-merge mb-4">
                                    <span id="razon_social" class="input-group-text"
                                      ><i class="mdi mdi-account"></i
                                    ></span>
                                    <div class="form-floating form-floating-outline">
                                      <input
                                        type="text"
                                        id="giro"
                                        name="giro"
                                        value="<?php echo $giro ?>"
                                        class="form-control phone-mask"
                                        placeholder=""
                                        aria-label=""
                                        aria-describedby="giro" />
                                      <label for="giro">Giro de la empresa</label>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!--  -->
                          </div>
                        </div>
                      </div>      
                    </div>
                  </div>
                  <div class="vista_txt vista_txt_3" style="display: none">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="accordion" >
                          <!-- Delivery Address -->
                          <div class="card accordion-item">
                          <!--  -->
                            <br>
                            <!--  -->
                            <div class="accordion-body">
                              <div class="row g-3">
                                <div class="col-md-6">
                                  <div class="row">
                                    <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-username"
                                      >Usuario</label
                                    >
                                    <div class="col-sm-9">
                                      <input
                                        type="text"
                                        id="usuario"
                                        name="usuario"
                                        value="<?php echo $usuario ?>"
                                        class="form-control"
                                        placeholder="" autocomplete="nope" oninput="verificar_usuario()" required />
                                        <small class="txtusuario" style="color: red;"></small>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                 
                                </div>
                                <div class="col-md-6">
                                  <div class="row form-password-toggle">
                                    <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-password"
                                      >Contraseña</label
                                    >
                                    <div class="col-sm-9">
                                      <div class="input-group input-group-merge">
                                        <input
                                          type="password"
                                          id="contrasena"
                                          name="contrasena"
                                          value="<?php echo $contrasena ?>"
                                          class="form-control"
                                          placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                          aria-describedby="formtabs-password2" required />
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="row form-password-toggle">
                                    <label class="col-sm-3 col-form-label text-sm-end" for="formtabs-confirm-password"
                                      >Confirmar</label
                                    >
                                    <div class="col-sm-9">
                                      <div class="input-group input-group-merge">
                                        <input
                                          type="password"
                                          id="contrasena2"
                                          name="contrasena2"
                                          value="<?php echo $contrasena2 ?>"
                                          class="form-control"
                                          placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                          aria-describedby="formtabs-confirm-password2" required oninput="comfirmar_pass()" />
                                        

                                      </div><small class="txtpass" style="color: red;"></small>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>  
                            <!--  -->
                          </div>
                        </div>
                      </div>
                    </div>    
                  </div>
                  <div class="vista_txt vista_txt_4" style="display: none">
                    <!--  -->
                    <div class="row g-4">
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/1.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Mark Gilbert</h5>
                            <span>UI Designer</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-secondary rounded-pill">Figma</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-warning rounded-pill">Sketch</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">18</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">834</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">129</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-success d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-check-outline me-1"></i>Conectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/8.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Eugenia Parsons</h5>
                            <span>Developer</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-danger rounded-pill">Angular</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-info rounded-pill">React</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">112</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">23.1k</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">1.28k</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-success d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-check-outline me-1"></i>Conectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/3.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Francis Byrd</h5>
                            <span>Developer</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-info rounded-pill">React</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-primary rounded-pill">HTML</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">32</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">1.25k</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">890</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-success d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-check-outline me-1"></i>Conectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/18.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Leon Lucas</h5>
                            <span>UI/UX Designer</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-secondary rounded-pill">Figma</span></a
                              >
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-warning rounded-pill">Sketch</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-primary rounded-pill">Photoshop</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">86</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">12.4k</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">890</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-dangerq+
                               d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-plus-outline me-1"></i>Desconectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/5.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Jayden Rogers</h5>
                            <span>Full Stack Developer</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-info rounded-pill">React</span></a
                              >
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-danger rounded-pill">Angular</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-primary rounded-pill">HTML</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">244</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">23.8k</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">2.14k</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-danger d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-check-outline me-1"></i>Desconectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="card">
                          <div class="card-body text-center">
                            <div class="dropdown btn-pinned">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connection</a></li>
                                <li><a class="dropdown-item" >Block connection</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item text-danger" >Delete</a></li>
                              </ul>
                            </div>
                            <div class="mx-auto mb-4">
                              <img src="<?php echo base_url();?>assets/img/avatars/10.png" alt="Avatar Image" class="rounded-circle w-px-100" />
                            </div>
                            <h5 class="mb-1 card-title">Jeanette Powell</h5>
                            <span>SEO</span>
                            <div class="d-flex align-items-center justify-content-center my-4 gap-2">
                              <a href="javascript:;" class="me-1"
                                ><span class="badge bg-label-success rounded-pill">Writing</span></a
                              >
                              <a href="javascript:;"><span class="badge bg-label-secondary rounded-pill">Analysis</span></a>
                            </div>

                            <div class="d-flex align-items-center justify-content-around mb-4">
                              <div>
                                <h4 class="mb-1">32</h4>
                                <span>Projects</span>
                              </div>
                              <div>
                                <h4 class="mb-1">1.28k</h4>
                                <span>Tasks</span>
                              </div>
                              <div>
                                <h4 class="mb-1">1.27k</h4>
                                <span>Connections</span>
                              </div>
                            </div>
                            <div class="d-flex align-items-center justify-content-center">
                              <a href="javascript:;" class="btn btn-danger d-flex align-items-center me-3"
                                ><i class="mdi mdi-account-plus-outline me-1"></i>Desconectado</a
                              >
                              <a href="javascript:;" class="btn btn-outline-secondary btn-icon"
                                ><i class="mdi mdi-email-outline lh-sm"></i
                              ></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--  -->
                  </div>
                  <div class="vista_txt vista_txt_5" style="display: none">
                    <!--  -->
                    <div class="card app-calendar-wrapper">
                      <div class="row g-0">
                        <!-- Calendar Sidebar -->
                        <div class="col app-calendar-sidebar border-end" id="app-calendar-sidebar">
                          <div class="p-3 pb-2 my-sm-0 mb-3">
                            <div class="d-grid">
                              <button
                                class="btn btn-primary btn-toggle-sidebar"
                                data-bs-toggle="offcanvas"
                                data-bs-target="#addEventSidebar"
                                aria-controls="addEventSidebar">
                                <i class="mdi mdi-plus me-1"></i>
                                <span class="align-middle">Add Event</span>
                              </button>
                            </div>
                          </div>
                          <div class="p-4">
                            <!-- inline calendar (flatpicker) -->
                            <div class="inline-calendar"></div>

                            <hr class="container-m-nx my-4" />

                            <!-- Filter -->
                            <div class="mb-4">
                              <small class="text-small text-muted text-uppercase align-middle">Filter</small>
                            </div>

                            <div class="form-check form-check-secondary mb-3">
                              <input
                                class="form-check-input select-all"
                                type="checkbox"
                                id="selectAll"
                                data-value="all"
                                checked />
                              <label class="form-check-label" for="selectAll">View All</label>
                            </div>

                            <div class="app-calendar-events-filter">
                              <div class="form-check form-check-danger mb-3">
                                <input
                                  class="form-check-input input-filter"
                                  type="checkbox"
                                  id="select-personal"
                                  data-value="personal"
                                  checked />
                                <label class="form-check-label" for="select-personal">Personal</label>
                              </div>
                              <div class="form-check mb-3">
                                <input
                                  class="form-check-input input-filter"
                                  type="checkbox"
                                  id="select-business"
                                  data-value="business"
                                  checked />
                                <label class="form-check-label" for="select-business">Business</label>
                              </div>
                              <div class="form-check form-check-warning mb-3">
                                <input
                                  class="form-check-input input-filter"
                                  type="checkbox"
                                  id="select-family"
                                  data-value="family"
                                  checked />
                                <label class="form-check-label" for="select-family">Family</label>
                              </div>
                              <div class="form-check form-check-success mb-3">
                                <input
                                  class="form-check-input input-filter"
                                  type="checkbox"
                                  id="select-holiday"
                                  data-value="holiday"
                                  checked />
                                <label class="form-check-label" for="select-holiday">Holiday</label>
                              </div>
                              <div class="form-check form-check-info">
                                <input
                                  class="form-check-input input-filter"
                                  type="checkbox"
                                  id="select-etc"
                                  data-value="etc"
                                  checked />
                                <label class="form-check-label" for="select-etc">ETC</label>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /Calendar Sidebar -->

                        <!-- Calendar & Modal -->
                        <div class="col app-calendar-content">
                          <div class="card shadow-none border-0">
                            <div class="card-body pb-0">
                              <!-- FullCalendar -->
                              <div id="calendar"></div>
                            </div>
                          </div>
                          <div class="app-overlay"></div>
                          <!-- FullCalendar Offcanvas -->
                          <div
                            class="offcanvas offcanvas-end event-sidebar"
                            tabindex="-1"
                            id="addEventSidebar"
                            aria-labelledby="addEventSidebarLabel">
                            <div class="offcanvas-header border-bottom">
                              <h5 class="offcanvas-title" id="addEventSidebarLabel">Add Event</h5>
                              <button
                                type="button"
                                class="btn-close text-reset"
                                data-bs-dismiss="offcanvas"
                                aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <div class="form-floating form-floating-outline mb-4">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="eventTitle"
                                    
                                    placeholder="Event Title" />
                                  <label for="eventTitle">Title</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <select class="select2 select-event-label form-select" id="eventLabel">
                                    <option data-label="primary" value="Business" selected>Business</option>
                                    <option data-label="danger" value="Personal">Personal</option>
                                    <option data-label="warning" value="Family">Family</option>
                                    <option data-label="success" value="Holiday">Holiday</option>
                                    <option data-label="info" value="ETC">ETC</option>
                                  </select>
                                  <label for="eventLabel">Label</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="eventStartDate"
                                    
                                    placeholder="Start Date" />
                                  <label for="eventStartDate">Start Date</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="eventEndDate"
                                    
                                    placeholder="End Date" />
                                  <label for="eventEndDate">End Date</label>
                                </div>
                                <div class="mb-3">
                                  <label class="switch">
                                    <input type="checkbox" class="switch-input allDay-switch" />
                                    <span class="switch-toggle-slider">
                                      <span class="switch-on"></span>
                                      <span class="switch-off"></span>
                                    </span>
                                    <span class="switch-label">All Day</span>
                                  </label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <input
                                    type="url"
                                    class="form-control"
                                    id="eventURL"
                                    
                                    placeholder="https://www.google.com" />
                                  <label for="eventURL">Event URL</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4 select2-primary">
                                  <select
                                    class="select2 select-event-guests form-select"
                                    id="eventGuests"
                                    
                                    multiple>
                                    <option data-avatar="1.png" value="Jane Foster">Jane Foster</option>
                                    <option data-avatar="3.png" value="Donna Frank">Donna Frank</option>
                                    <option data-avatar="5.png" value="Gabrielle Robertson">Gabrielle Robertson</option>
                                    <option data-avatar="7.png" value="Lori Spears">Lori Spears</option>
                                    <option data-avatar="9.png" value="Sandy Vega">Sandy Vega</option>
                                    <option data-avatar="11.png" value="Cheryl May">Cheryl May</option>
                                  </select>
                                  <label for="eventGuests">Add Guests</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <input
                                    type="text"
                                    class="form-control"
                                    id="eventLocation"
                                    
                                    placeholder="Enter Location" />
                                  <label for="eventLocation">Location</label>
                                </div>
                                <div class="form-floating form-floating-outline mb-4">
                                  <textarea class="form-control"  id="eventDescription"></textarea>
                                  <label for="eventDescription">Description</label>
                                </div>
                                <div class="mb-3 d-flex justify-content-sm-between justify-content-start my-4 gap-2">
                                  <div class="d-flex">
                                    <button type="submit" class="btn btn-primary btn-add-event me-sm-2 me-1">Add</button>
                                    <button
                                      type="reset"
                                      class="btn btn-outline-secondary btn-cancel me-sm-0 me-1"
                                      data-bs-dismiss="offcanvas">
                                      Cancel
                                    </button>
                                  </div>
                                  <button class="btn btn-outline-danger btn-delete-event d-none">Delete</button>
                                </div>
                            </div>
                          </div>
                        </div>
                        <!-- /Calendar & Modal -->
                      </div>
                    </div>
                    <!--  -->
                  </div>
                  <br>
                  <div class="btn_txt"  style="display: none">
                    <button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>
                  </div>
                  <br><br>  
               </form>   
              
              <!-- User Profile Content -->
              <?php /*
              <div class="row">
                <div class="col-xl-4 col-lg-5 col-md-5">
                  <!-- About User -->
                  <div class="card mb-4">
                    <div class="card-body">
                      <small class="card-text text-uppercase">About</small>
                      <ul class="list-unstyled my-3 py-1">
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-account-outline mdi-24px"></i
                          ><span class="fw-medium mx-2">Full Name:</span> <span>John Doe</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-check mdi-24px"></i><span class="fw-medium mx-2">Status:</span>
                          <span>Active</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-star-outline mdi-24px"></i><span class="fw-medium mx-2">Role:</span>
                          <span>Developer</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-flag-outline mdi-24px"></i><span class="fw-medium mx-2">Country:</span>
                          <span>USA</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-translate mdi-24px"></i><span class="fw-medium mx-2">Languages:</span>
                          <span>English</span>
                        </li>
                      </ul>
                      <small class="card-text text-uppercase">Contacts</small>
                      <ul class="list-unstyled my-3 py-1">
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-phone-outline mdi-24px"></i><span class="fw-medium mx-2">Contact:</span>
                          <span>(123) 456-7890</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-message-outline mdi-24px"></i><span class="fw-medium mx-2">Skype:</span>
                          <span>john.doe</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-email-outline mdi-24px"></i><span class="fw-medium mx-2">Email:</span>
                          <span>john.doe@example.com</span>
                        </li>
                      </ul>
                      <small class="card-text text-uppercase">Teams</small>
                      <ul class="list-unstyled mb-0 mt-3 pt-1">
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-github mdi-24px text-secondary me-2"></i>
                          <div class="d-flex flex-wrap">
                            <span class="fw-medium me-2">Backend Developer</span><span>(126 Members)</span>
                          </div>
                        </li>
                        <li class="d-flex align-items-center">
                          <i class="mdi mdi-react mdi-24px text-info me-2"></i>
                          <div class="d-flex flex-wrap">
                            <span class="fw-medium me-2">React Developer</span><span>(98 Members)</span>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!--/ About User -->
                  <!-- Profile Overview -->
                  <div class="card mb-4">
                    <div class="card-body">
                      <small class="card-text text-uppercase">Overview</small>
                      <ul class="list-unstyled mb-0 mt-3 pt-1">
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-check mdi-24px"></i><span class="fw-medium mx-2">Task Compiled:</span>
                          <span>13.5k</span>
                        </li>
                        <li class="d-flex align-items-center mb-3">
                          <i class="mdi mdi-account-outline mdi-24px"></i
                          ><span class="fw-medium mx-2">Projects Compiled:</span> <span>146</span>
                        </li>
                        <li class="d-flex align-items-center">
                          <i class="mdi mdi-view-grid-outline mdi-24px"></i
                          ><span class="fw-medium mx-2">Connections:</span> <span>897</span>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!--/ Profile Overview -->
                </div>
                <div class="col-xl-8 col-lg-7 col-md-7">
                  <!-- Activity Timeline -->
                  <div class="card card-action mb-4">
                    <div class="card-header align-items-center">
                      <h5 class="card-action-title mb-0">
                        <i class="mdi mdi-format-list-bulleted mdi-24px me-2"></i>Activity Timeline
                      </h5>
                      <div class="card-action-element">
                        <div class="dropdown">
                          <button
                            type="button"
                            class="btn dropdown-toggle hide-arrow p-0"
                            data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                          </button>
                          <ul class="dropdown-menu dropdown-menu-end">
                            <li><a class="dropdown-item" >Share timeline</a></li>
                            <li><a class="dropdown-item" >Suggest edits</a></li>
                            <li>
                              <hr class="dropdown-divider" />
                            </li>
                            <li><a class="dropdown-item" >Report bug</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="card-body pt-3 pb-0">
                      <ul class="timeline mb-0">
                        <li class="timeline-item timeline-item-transparent">
                          <span class="timeline-point timeline-point-danger"></span>
                          <div class="timeline-event">
                            <div class="timeline-header mb-1">
                              <h6 class="mb-0">Client Meeting</h6>
                              <small class="text-muted">Today</small>
                            </div>
                            <p class="mb-2">Project meeting with john @10:15am</p>
                            <div class="d-flex flex-wrap">
                              <div class="avatar me-3">
                                <img src="<?php echo base_url();?>assets/img/avatars/3.png" alt="Avatar" class="rounded-circle" />
                              </div>
                              <div>
                                <h6 class="mb-0">Lester McCarthy (Client)</h6>
                                <span>CEO of Infibeam</span>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item timeline-item-transparent">
                          <span class="timeline-point timeline-point-primary"></span>
                          <div class="timeline-event">
                            <div class="timeline-header mb-1">
                              <h6 class="mb-0">Create a new project for client</h6>
                              <small class="text-muted">2 Day Ago</small>
                            </div>
                            <p class="mb-0">Add files to new design folder</p>
                          </div>
                        </li>
                        <li class="timeline-item timeline-item-transparent">
                          <span class="timeline-point timeline-point-warning"></span>
                          <div class="timeline-event">
                            <div class="timeline-header mb-1">
                              <h6 class="mb-0">Shared 2 New Project Files</h6>
                              <small class="text-muted">6 Day Ago</small>
                            </div>
                            <p class="mb-2">
                              Sent by Mollie Dixon
                              <img
                                src="<?php echo base_url();?>assets/img/avatars/4.png"
                                class="rounded-circle me-3"
                                alt="avatar"
                                height="24"
                                width="24" />
                            </p>
                            <div class="d-flex flex-wrap gap-2">
                              <a href="javascript:void(0)" class="me-3">
                                <img
                                  src="<?php echo base_url();?>assets/img/icons/misc/doc.png"
                                  alt="Document image"
                                  width="15"
                                  class="me-2" />
                                <span class="fw-medium text-body">App Guidelines</span>
                              </a>
                              <a href="javascript:void(0)">
                                <img
                                  src="<?php echo base_url();?>assets/img/icons/misc/xls.png"
                                  alt="Excel image"
                                  width="15"
                                  class="me-2" />
                                <span class="fw-medium text-body">Testing Results</span>
                              </a>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item timeline-item-transparent border-transparent">
                          <span class="timeline-point timeline-point-info"></span>
                          <div class="timeline-event">
                            <div class="timeline-header mb-1">
                              <h6 class="mb-0">Project status updated</h6>
                              <small class="text-muted">10 Day Ago</small>
                            </div>
                            <p class="mb-0">Woocommerce iOS App Completed</p>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <!--/ Activity Timeline -->
                  <div class="row">
                    <!-- Connections -->
                    <div class="col-lg-12 col-xl-6">
                      <div class="card card-action mb-4">
                        <div class="card-header align-items-center">
                          <h5 class="card-action-title mb-0">Connections</h5>
                          <div class="card-action-element">
                            <div class="dropdown">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share connections</a></li>
                                <li><a class="dropdown-item" >Suggest edits</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item" >Report bug</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="card-body">
                          <ul class="list-unstyled mb-0">
                            <li class="mb-3">
                              <div class="d-flex align-items-start">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img src="<?php echo base_url();?>assets/img/avatars/2.png" alt="Avatar" class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Cecilia Payne</h6>
                                    <small>45 Connections</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <button class="btn btn-outline-primary btn-icon">
                                    <i class="mdi mdi-account-outline mdi-24px"></i>
                                  </button>
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-start">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img src="<?php echo base_url();?>assets/img/avatars/3.png" alt="Avatar" class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Curtis Fletcher</h6>
                                    <small>1.32k Connections</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <button class="btn btn-primary btn-icon">
                                    <i class="mdi mdi-account-outline mdi-24px"></i>
                                  </button>
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-start">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img src="<?php echo base_url();?>assets/img/avatars/8.png" alt="Avatar" class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Alice Stone</h6>
                                    <small>125 Connections</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <button class="btn btn-primary btn-icon">
                                    <i class="mdi mdi-account-outline mdi-24px"></i>
                                  </button>
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-start">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img src="<?php echo base_url();?>assets/img/avatars/7.png" alt="Avatar" class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Darrell Barnes</h6>
                                    <small>456 Connections</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <button class="btn btn-outline-primary btn-icon">
                                    <i class="mdi mdi-account-outline mdi-24px"></i>
                                  </button>
                                </div>
                              </div>
                            </li>

                            <li class="mb-3">
                              <div class="d-flex align-items-start">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img src="<?php echo base_url();?>assets/img/avatars/12.png" alt="Avatar" class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Eugenia Moore</h6>
                                    <small>1.2k Connections</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <button class="btn btn-outline-primary btn-icon">
                                    <i class="mdi mdi-account-outline mdi-24px"></i>
                                  </button>
                                </div>
                              </div>
                            </li>
                            <li class="text-center">
                              <a href="javascript:;">View all connections</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--/ Connections -->
                    <!-- Teams -->
                    <div class="col-lg-12 col-xl-6">
                      <div class="card card-action mb-4">
                        <div class="card-header align-items-center">
                          <h5 class="card-action-title mb-0">Teams</h5>
                          <div class="card-action-element">
                            <div class="dropdown">
                              <button
                                type="button"
                                class="btn dropdown-toggle hide-arrow p-0"
                                data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="mdi mdi-dots-vertical mdi-24px text-muted"></i>
                              </button>
                              <ul class="dropdown-menu dropdown-menu-end">
                                <li><a class="dropdown-item" >Share teams</a></li>
                                <li><a class="dropdown-item" >Suggest edits</a></li>
                                <li>
                                  <hr class="dropdown-divider" />
                                </li>
                                <li><a class="dropdown-item" >Report bug</a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="card-body">
                          <ul class="list-unstyled mb-0">
                            <li class="mb-3">
                              <div class="d-flex align-items-center">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img
                                      src="<?php echo base_url();?>assets/img/icons/brands/react-label.png"
                                      alt="Avatar"
                                      class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">React Developers</h6>
                                    <small>72 Members</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <a href="javascript:;"
                                    ><span class="badge bg-label-danger rounded-pill">Developer</span></a
                                  >
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-center">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img
                                      src="<?php echo base_url();?>assets/img/icons/brands/support-label.png"
                                      alt="Avatar"
                                      class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Support Team</h6>
                                    <small>122 Members</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <a href="javascript:;"
                                    ><span class="badge bg-label-primary rounded-pill">Support</span></a
                                  >
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-center">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img
                                      src="<?php echo base_url();?>assets/img/icons/brands/figma-label.png"
                                      alt="Avatar"
                                      class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">UI Designers</h6>
                                    <small>7 Members</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <a href="javascript:;"
                                    ><span class="badge bg-label-info rounded-pill">Designer</span></a
                                  >
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-center">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img
                                      src="<?php echo base_url();?>assets/img/icons/brands/vue-label.png"
                                      alt="Avatar"
                                      class="rounded-circle" />
                                  </div>
                                  <div class="me-2">
                                    <h6 class="mb-0">Vue.js Developers</h6>
                                    <small>289 Members</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <a href="javascript:;"
                                    ><span class="badge bg-label-danger rounded-pill">Developer</span></a
                                  >
                                </div>
                              </div>
                            </li>
                            <li class="mb-3">
                              <div class="d-flex align-items-center">
                                <div class="d-flex align-items-start">
                                  <div class="avatar me-3">
                                    <img
                                      src="<?php echo base_url();?>assets/img/icons/brands/twitter-label.png"
                                      alt="Avatar"
                                      class="rounded-circle" />
                                  </div>
                                  <div class="me-w">
                                    <h6 class="mb-0">Digital Marketing</h6>
                                    <small>24 Members</small>
                                  </div>
                                </div>
                                <div class="ms-auto">
                                  <a href="javascript:;"
                                    ><span class="badge bg-label-secondary rounded-pill">Marketing</span></a
                                  >
                                </div>
                              </div>
                            </li>
                            <li class="text-center">
                              <a href="javascript:;">View all teams</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--/ Teams -->
                  </div>

                  <!-- Projects table -->
                  <div class="card mb-4">
                    <div class="card-datatable table-responsive">
                      <table class="datatable-project table">
                        <thead class="table-light">
                          <tr>
                            <th></th>
                            <th></th>
                            <th>Project</th>
                            <th class="text-nowrap">Total Task</th>
                            <th>Progress</th>
                            <th>Hours</th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                  <!--/ Projects table -->
                </div>
              </div>
              */
              ?>
              <!--/ User Profile Content -->
            </div>