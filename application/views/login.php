<!doctype html>

<html
  lang="en"
  class="light-style layout-wide customizer-hide"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="<?php echo base_url();?>assets/"
  data-template="vertical-menu-template-no-customizer">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <link rel="shortcut icon" href="<?php echo base_url(); ?>images/LM.svg" type="image/x-icon">
    <title>Mara Pérez</title>

    <meta name="description" content="" />
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&ampdisplay=swap"
      rel="stylesheet" />

    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fonts/materialdesignicons.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/fonts/flag-icons.css" />

    <!-- Menu waves for no-customizer fix -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/node-waves/node-waves.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/rtl/core.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/rtl/theme-default.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/typeahead-js/typeahead.css" />
    <!-- Vendor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/libs/@form-validation/form-validation.css" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/css/pages/page-auth.css" />

    <!-- Helpers -->
    <script src="<?php echo base_url();?>assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="<?php echo base_url();?>assets/js/config.js"></script>
    <input type="hidden" id="base_url" value="<?php echo base_url() ?>">
  </head>
  <style type="text/css">
    .form-control{
      border: 1px solid #bd9250;
      color: #ffffff !important;
    }
    .form-control:focus {
      color: #ffffff;
      background-color: #484947;
    }

    .input-group-text {
      display: flex;
      align-items: center;
      padding: 0.5rem 0.875rem;
      font-size: 0.9375rem;
      font-weight: 400;
      line-height: 1.47;
      color: #bd9250;
      text-align: center;
      white-space: nowrap;
      background-color: #484947;
      border: 1px solid #bd9250;
      border-radius: var(--bs-border-radius);
    }
    input:-webkit-autofill {
      -webkit-text-fill-color: white; /* Color del texto */
      
    }
  </style>
  <body>
    <!-- Content -->

    <div class="position-relative">
      <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner py-4">
          <!-- Login -->
          <div class="card p-2">
            <!-- Logo -->
            <div class="app-brand justify-content-center mt-5" align="center">
              <a href="index.html" class="gap-2">
                <img style="width: 100%;" src="<?php echo base_url() ?>images/MVP Logo Mara.png">
              </a>
            </div>
            <!-- /Logo -->

            <div class="card-body mt-2">
              <h4 class="mb-2">¡Bienvenido de nuevo! 👋</h4>
              <p class="mb-4">Inicia Sesión</p>

              <form id="sign_in" class="mb-3">
                <div>
                  <label for="usuario" class="form-label tusu">Usuario</label>
                  <input type="text" class="form-control" id="usuario" name="usuario">
                  <small class="txtusuario" style="color: red;"></small>
                </div>      
                <br>
                <div>
                  <label for="usuario" class="form-label password">Contraseña</label>
                  <div class="input-group input-group-merge">
                    <div class="form-floating form-floating-outline">
                      <input
                        type="password"
                        id="password"
                        class="form-control"
                        name="password"
                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                        aria-describedby="password" />
                    </div>
                    <span class="input-group-text cursor-pointer"><i class="mdi mdi-eye-off-outline"></i></span>
                    <br>
                    
                  </div><small class="txtpass" style="color: red;"></small>
                </div>    
                <!-- <div class="mb-3 d-flex justify-content-between">
                  <a href="auth-forgot-password-basic.html" class="float-end mb-1">
                    <span>¿Olvidaste tu contraseña?</span>
                  </a>
                </div> -->
              </form>
              <div class="mb-3">
                  <button class="btn btn-primary d-grid w-100"  onclick="ingresar()">Inicia Sesión</button>
              </div>
              <div class="alerta_c" style="display: none;">
                <div class="alert alert-success alert-dismissible" role="alert">
                  <h4 class="alert-heading d-flex align-items-center">
                    <i class="mdi mdi-check-circle-outline mdi-24px me-2"></i>Inicio de sesión correcto :)
                  </h4>
                  <hr />
                  <p class="mb-0">
                    Cargando base de datos e interfaces, en breve será re dirigido a su sesión.
                  </p>
                </div>
              <div>
            </div>
          </div>
          <!-- /Login -->
          <img
            alt="mask"
            src="<?php echo base_url();?>assets/img/illustrations/auth-basic-login-mask-light.png"
            class="authentication-image d-none d-lg-block"
            data-app-light-img="illustrations/auth-basic-login-mask-light.png"
            data-app-dark-img="illustrations/auth-basic-login-mask-dark.png" />
        </div>
      </div>
    </div>

    <!-- / Content -->

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="<?php echo base_url();?>assets/vendor/libs/jquery/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/popper/popper.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/node-waves/node-waves.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/hammer/hammer.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/i18n/i18n.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/typeahead-js/typeahead.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/js/menu.js"></script>

    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="<?php echo base_url();?>assets/vendor/libs/@form-validation/popular.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/@form-validation/bootstrap5.js"></script>
    <script src="<?php echo base_url();?>assets/vendor/libs/@form-validation/auto-focus.js"></script>

    <!-- Main JS -->
    <script src="<?php echo base_url();?>assets/js/main.js"></script>

    <!-- Page JS -->
    <script src="<?php echo base_url();?>assets/js/pages-auth.js"></script>
    <script src="<?php echo base_url();?>public/js/login.js?v=<?php echo date('YmdGis') ?>"></script>
  </body>
</html>
