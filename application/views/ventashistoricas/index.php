<style type="text/css">
  .th_c{
    background: #d1b697 !important;
    color: white !important;
    text-align-last: center;
  }
  .input_vh{    
    border-radius: 6px;
    border: thin;
  }

  .th_input{
    padding-left: 1px !important;
    padding-right: 1px !important;
  }
  .table_info th {
    padding-top: 1px;
    padding-bottom: 1px;
  }
  .form-control{
    width: 135px;
  }
  .th_purple{
    background: #e4dfec !important;
    color: black !important;
    text-align-last: center;
  }
  .th_grey{
    background: #bfbfbf !important;
    color: black !important;
    text-align-last: center;
  }

  .th_grey{
    background: #bfbfbf !important;
    color: black !important;
    text-align-last: center;
  }

  .th_orange{
    background: #fde9d9 !important;
    color: black !important;
    text-align-last: center;
  }

  .th_grey_c{
    background: #d9d9d9 !important;
    color: black !important;
    text-align-last: center;
  }
  .th_yellow{
    background: #ffff00 !important;
    color: black !important;
    text-align-last: center;
  }
  .th_border{
    border-style: hidden;
  }
  .th_height{
    height: 38px;
  }
</style>
<div class="container-xxl flex-grow-1 container-p-y">  
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-sm-row mb-4">
          <li class="nav-item">
            <a class="nav-link link_data link1" onclick="vista_tipo(1)" 
              ><i class="mdi mdi-cart-percent me-1 mdi-20px"></i>I. ANÁLISIS DE VENTAS / INGRESOS</a
            >
          </li>
          <li class="nav-item ">
            <a class="nav-link link_data link2" onclick="vista_tipo(2)"
              ><i class="mdi mdi-cart-arrow-right me-1 mdi-20px"></i>VENTAS / INGRESOS TOTALES</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link3" onclick="vista_tipo(3)"
              ><i class="mdi mdi-view-grid-outline me-1 mdi-20px"></i>I-B. RESUMEN DE PRESUPUESTOS</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link4" onclick="vista_tipo(4)"
              ><i class="mdi mdi-text-box-plus-outline me-1 mdi-20px"></i>I-A. PROYECCIÓN DE VENTAS / INGRESOS</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link5" onclick="vista_tipo(5)"
              ><i class="mdi mdi-chart-areaspline me-1 mdi-20px"></i>Grafica de ventas</a
            >
          </li>
        </ul>
      </div>
    </div>
    <div class="vista_txt vista_txt_1" style="display: none">
        <div class="row my-4">
            <!--  -->
            <div class="col-xl-4">
              <button style="width: 100%;" 
                type="button"
                class="btn btn-primary"
                data-bs-toggle="modal"
                data-bs-target="#basicModal">
                Agregar linea de servicio / producto
              </button>
            </div>
            <div class="col-xl-3">
              <button
                type="button"
                class="btn btn-sm btn-secondary waves-effect waves-light"
                data-bs-toggle="modal"
                data-bs-target="#onboardingHorizontalSlideModal">
                <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span> Instrucciones de captura
              </button>
            </div>
        </div>
        <div class="row my-4">  
            <div class="col-xl-12">
              <div class="pestanas_text"></div>
            </div>
        </div>
    </div>    
    <div class="vista_txt vista_txt_2" style="display: none">
        <div class="row my-4">  
            <div class="col-xl-12">
              <div class="servicios_text"></div>
            </div>
        </div>
    </div>    
    <div class="vista_txt vista_txt_3" style="display: none">    
        <div class="row my-4">  
            <div class="col-xl-12">
              <div class="resumen_text"></div>
            </div>
        </div>
    </div>    
    <div class="vista_txt vista_txt_4" style="display: none">    
        <div class="row my-4">  
            <div class="col-xl-12">
              <div class="proyeccion_text"></div>
            </div>
        </div>
    </div> 
    <div class="vista_txt vista_txt_5" style="display: none">    
        <div class="row my-4">  
            <div class="col-xl-12">
              
              <div class="reportegrafica_canvas"></div>
              <div class="reportegrafica_text"></div>
            </div>
        </div>
    </div>    
</div>        

<div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel1">Agregar linea de servicio / producto</h4>
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <form class="form" method="post" role="form" id="form_registro" novalidate>
        <div class="modal-body">
          <div class="row">
            <div class="col mb-12 mt-2">
              <div class="form-floating form-floating-outline">
                <input type="text" id="nombre" name="nombre" class="form-control" placeholder="" required style="width: 100%;" />
                <label for="nombre">Linea Servicio / Producto:</label>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
            Cerrar
          </button>
          <button type="submit" class="btn btn-primary btn_registro">Agregar</button>
        </div>
      </form>  
    </div>
  </div>
</div>



<div
  class="modal-onboarding modal fade animate__animated"
  id="onboardingHorizontalSlideModal"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modalHorizontalCarouselControls"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="onboarding-horizontal">
              <div class="onboarding-media">
                <img
                  src="../../assets/img/illustrations/boy-with-rocket-light.png"
                  alt="boy-with-rocket-light"
                  width="273"
                  class="img-fluid"
                  data-app-light-img="illustrations/boy-with-rocket-light.png"
                  data-app-dark-img="illustrations/boy-with-rocket-dark.png" />
              </div>
              <div class="onboarding-content">
                <h4 class="onboarding-title text-body">Datos macroeconómicos</h4>
                <div class="onboarding-info" style="text-align: justify;">
                  Instrucciones de captura:  Consultar datos de fuentes confiables, de preferencia gubernamentales y capturar todas las celdas en color blanco
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

