<style type="text/css">
    textarea{
        overflow: hidden;
        min-height: 30px;
    }

  .th_c {
    background: #1e4e79 !important;
    color: white !important;
    text-align-last: center;
    width: 108px;
  }

  .th_cx {
    background: #d6dce4 !important;
    color: black !important;
  }

  .th_cxy{
    background: #8eaadb !important;
    color: white !important;
  }

  .th_cxz{
    background: #70ad47 !important;
    color: white !important;
  }

  .td_input{
    padding-left: 1px !important;
    padding-right: 1px !important;
  }

  .table_info td {
    padding-top: 1px;
    padding-bottom: 1px;
  }
  
  .th_ce {
    background: #8eaadb !important;
    color: black !important;
  }
   
  .th_re {
    background: #b4c6e7 !important;
    color: black !important;
  }

  .th_mer {
    background: #9cc2e5 !important;
    color: black !important;
  }

  .th_mer2 {
    background: #d6dce4 !important;
    color: black !important;
  }

  .th_ent {
    background: #deeaf6 !important;
    color: black !important;
  }

  .th_ren {
    background: #adb9ca !important;
    color: white !important;
  }

  .th_ges {
    background: #333f4f !important;
    color: white !important;
  }

  .th_ges2 {
    background: #adb9ca !important;
    color: white !important;
  }

  .th_pil {
    background: #1f3864 !important;
    color: white !important;
  }

  .th_pil2 {
    background: #d9e2f3 !important;
    color: white !important;
  }   
   
  table {
    caption-side: bottom;
    border-collapse: collapse;
  }
   
  thead, tbody, tfoot, tr, td, th {
    border-color: inherit;
    border-style: hidden;
    border-width: 0;
  }
  #clonetr_data{
    top: 255px;
  }
  #clonetr_data2{
    top: 255px;
  }
  #clonetr_data4{
    top: 255px;
  }
  #clonetr_data,
  #clonetr_data2,
  #clonetr_data4{
    width: auto !important;
    
    position: absolute;
    background: #f7f7f9;
  }
  #clonetr_data.fixed-button{
    top: 63px;
    position: fixed;
  }
  #clonetr_data2.fixed-button{
    top: 63px;
    position: fixed;
  }
  #clonetr_data4.fixed-button{
    top: 63px;
    position: fixed;
  }
  #clonetr_data4 h3{
    margin-bottom: 0px;
  }
</style>
<div class="container-xxl flex-grow-1 container-p-y">  
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-sm-row mb-4">
          <li class="nav-item">
            <a class="nav-link link_data link1" onclick="vista_tipo(1)" 
              >El Mercado</a
            >
          </li>
          <li class="nav-item ">
            <a class="nav-link link_data link2" onclick="vista_tipo(2)"
              >Fortalezas</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link3" onclick="vista_tipo(3)"
              >El cliente</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link4" onclick="vista_tipo(4)"
              >Estado Resultados</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link5" onclick="vista_tipo(5)"
              >Resumen de estrategias</a
            >
          </li>
        </ul>
      </div>
    </div>
    <div class="vista_txt vista_txt_1" style="display: none">
      
      <div class="row">  
        <div class="col-md-12">
          <h4>Guía de selección de factores críticos de éxito</h4>
          <div class="tabla_guia_mercado"></div>
          
        </div>
      </div>  
    </div>    
    <div class="vista_txt vista_txt_2" style="display: none">
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Guía de selección de factores críticos de éxito</h4>
          <div class="tabla_guia_fortaleza"></div>
        </div>
      </div>  
    </div>    
    <div class="vista_txt vista_txt_3" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Guía de selección de factores críticos de éxito</h4>
          <div class="tabla_guia_cliente_gsf"></div>
        </div>
      </div>  
    </div>    
    <div class="vista_txt vista_txt_4" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Guía de selección de factores críticos de éxito</h4>
          <div class="tabla_guia_estados_resultados"></div>
        </div>
      </div>   
    </div>    
    <div class="vista_txt vista_txt_5" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Resumen de Indicadores Estratégicos</h4>
          <div class="tabla_guia_resumen_estrategias"></div>
        </div>
      </div>   
    </div>    
</div>        
