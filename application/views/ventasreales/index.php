<style type="text/css">
	.tablepagos{
		width: 300px;
	}
	.tablepagos td{
		padding-top: 1px;
		padding-bottom: 1px;
	}
	.td_input{
		padding-left: 1px !important;
		padding-right: 1px !important;
	}
	.table_meses_pagos{
		width: 350px;
	}
	td.green{
		background: #00800080;
		color: green;
    	font-weight: bold;
	}	
	td.red{
		background: #ff00006e;
		color: red;
    	font-weight: bold;
	}
	.table_meses_pagos td{
		padding-top: 7px;
		padding-bottom: 7px;
	}
	.table-light{
		--bs-table-bg: #d1b697;
	}
</style>
<input type="hidden" id="idcliente" value="<?php echo $idcliente;?>">
<input type="hidden" id="anio" value="<?php echo $anio;?>">
<div class="row">
	<div class="col-md-12" style="text-align: center;">
		<h4>Año <?php echo $anio;?></h4>
	</div>
</div>
<div class="pestanas_text">
	<div class="nav-align-left mb-4">
        <ul class="nav nav-pills me-3" role="tablist">
        	<?php 
        		$html_m='';
        		foreach ($meses as $itemm) {
        			$html_m.='<li class="nav-item" role="presentation">';
			            $html_m.='<button type="button" class="nav-link mes_num_'.$itemm['mes_num'].'" data-mesname="'.$itemm['mes_name'].'" role="tab" data-bs-toggle="tab" data-bs-target="#navs_'.$itemm['mes_num'].'" aria-controls="navs_'.$itemm['mes_num'].'" aria-selected="false" onclick="get_infomes('.$itemm['mes_num'].')" tabindex="-1">';
			                $html_m.='<span class="tt_p_'.$itemm['mes_num'].'">'.$itemm['mes_name'].'</span>';
			            $html_m.='</button>';
		            $html_m.='</li>';
        		}
        		echo $html_m;

        	?>
        	
        </ul>
      	<div class="tab-content">
      		<?php 
        		$html_mi='';
        		foreach ($meses as $itemm) {
        			$html_mi.='<div class="tab-pane fade" id="navs_'.$itemm['mes_num'].'" role="tabpanel">';
		            	$html_mi.='<h4 class="txt_titulo">Ventas de '.$itemm['mes_name'].'</h4>';
		            	$html_mi.='<div class="row">';
		            		$html_mi.='<div class="col-md-6 info_mes_'.$itemm['mes_num'].'"></div>';
		            		$html_mi.='<div class="col-md-6 info_mes_general"></div>';
		            	$html_mi.='</div>';
		          	$html_mi.='</div>';
        		}
        		echo $html_mi;

        	?>
        </div>
    </div>
</div>