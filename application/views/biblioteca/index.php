<div class="container-xxl flex-grow-1 container-p-y">
  <h4 class="py-3 mb-4">Biblioteca / Recursos</h4>
              <div class="row mt-4">
                <!-- Navigation -->
                <div class="col-lg-3 col-md-4 col-12 mb-md-0 mb-3">
                  <div class="d-flex justify-content-between flex-column mb-2 mb-md-0">
                    <ul class="nav nav-align-left nav-pills flex-column">
                      <li class="nav-item">
                        <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#payment">
                          <i class="mdi mdi-file-pdf-box me-1"></i>
                          <span class="align-middle">Archivo PDF</span>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#delivery">
                          <i class="mdi mdi-file-word me-1"></i>
                          <span class="align-middle">Archivo Word</span>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#cancellation">
                          <i class="mdi mdi-file-powerpoint me-1"></i>
                          <span class="align-middle">Archivo PowerPoint</span>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#orders">
                          <i class="mdi mdi-file-excel me-1"></i>
                          <span class="align-middle">Archivo Excel</span>
                        </button>
                      </li>
                      <li class="nav-item">
                        <button class="nav-link" data-bs-toggle="tab" data-bs-target="#product">
                          <i class="mdi mdi-image me-1"></i>
                          <span class="align-middle">Archivo Imágenes</span>
                        </button>
                      </li>
                    </ul>
                    <div class="d-none d-md-block">
                      <div class="mt-5 text-center">
                        <img
                          src="<?php echo base_url() ?>assets/img/illustrations/faq-illustration.png"
                          class="img-fluid w-px-120"
                          alt="FAQ Image" />
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /Navigation -->

                <!-- FAQ's -->
                <div class="col-lg-9 col-md-8 col-12">
                  <div class="tab-content p-0">
                    <div class="tab-pane fade show active" id="payment" role="tabpanel">
                      <div class="d-flex mb-3 gap-3">
                        <div class="avatar">
                          <div class="avatar-initial bg-label-primary rounded">
                            <i class="mdi mdi-file-pdf-box mdi-24px"></i>
                          </div>
                        </div>
                        <div>
                          <h5 class="mb-0">
                            <span class="align-middle">Archivo PDF</span>
                          </h5>
                        </div>
                      </div>
                      <div id="accordionPayment" class="accordion">
                        <div class="accordion-item active">
                          <div class="accordion-collapse collapse show">
                            <div class="accordion-body">
                              <div class="card-body">
                                <div class="mb-3">
                                  <label for="formFile" class="form-label"></label>
                                  <input class="form-control" type="file" id="formFile" />
                                </div>
                                <div class="mb-3">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" />
                                    <label for="nombre">Nombre del archivo</label>
                                  </div>
                                </div>
                                <div class="row g-3">
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>    
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="delivery" role="tabpanel">
                      <div class="d-flex mb-3 gap-3">
                        <div class="avatar">
                          <div class="avatar-initial bg-label-primary rounded">
                            <i class="mdi mdi-file-word mdi-24px"></i>
                          </div>
                        </div>
                        <div>
                          <h5 class="mb-0">
                            <span class="align-middle">Archivo Word</span>
                          </h5>
                        </div>
                      </div>
                      <div id="accordionPayment" class="accordion">
                        <div class="accordion-item active">
                          <div class="accordion-collapse collapse show">
                            <div class="accordion-body">
                              <div class="card-body">
                                <div class="mb-3">
                                  <label for="formFile" class="form-label"></label>
                                  <input class="form-control" type="file" id="formFile" />
                                </div>
                                <div class="mb-3">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" />
                                    <label for="nombre">Nombre del archivo</label>
                                  </div>
                                </div>
                                <div class="row g-3">
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>    
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="cancellation" role="tabpanel">
                      <div class="d-flex mb-3 gap-3">
                        <div class="avatar">
                          <div class="avatar-initial bg-label-primary rounded">
                            <i class="mdi mdi-file-powerpoint mdi-24px"></i>
                          </div>
                        </div>
                        <div>
                          <h5 class="mb-0">
                            <span class="align-middle">Archivo PowerPoint</span>
                          </h5>
                        </div>
                      </div>
                      <div id="accordionPayment" class="accordion">
                        <div class="accordion-item active">
                          <div class="accordion-collapse collapse show">
                            <div class="accordion-body">
                              <div class="card-body">
                                <div class="mb-3">
                                  <label for="formFile" class="form-label"></label>
                                  <input class="form-control" type="file" id="formFile" />
                                </div>
                                <div class="mb-3">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" />
                                    <label for="nombre">Nombre del archivo</label>
                                  </div>
                                </div>
                                <div class="row g-3">
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>    
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="orders" role="tabpanel">
                      <div class="d-flex mb-3 gap-3">
                        <div class="avatar">
                          <div class="avatar-initial bg-label-primary rounded">
                            <i class="mdi mdi-file-excel mdi-24px"></i>
                          </div>
                        </div>
                        <div>
                          <h5 class="mb-0">
                            <span class="align-middle">Archivo Excel</span>
                          </h5>
                        </div>
                      </div>
                      <div id="accordionPayment" class="accordion">
                        <div class="accordion-item active">
                          <div class="accordion-collapse collapse show">
                            <div class="accordion-body">
                              <div class="card-body">
                                <div class="mb-3">
                                  <label for="formFile" class="form-label"></label>
                                  <input class="form-control" type="file" id="formFile" />
                                </div>
                                <div class="mb-3">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" />
                                    <label for="nombre">Nombre del archivo</label>
                                  </div>
                                </div>
                                <div class="row g-3">
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>    
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane fade" id="product" role="tabpanel">
                      <div class="d-flex mb-3 gap-3">
                        <div class="avatar">
                          <div class="avatar-initial bg-label-primary rounded">
                            <i class="mdi mdi-image mdi-24px"></i>
                          </div>
                        </div>
                        <div>
                          <h5 class="mb-0">
                            <span class="align-middle">Archivo Imágenes</span>
                          </h5>
                        </div>
                      </div>
                      <div id="accordionPayment" class="accordion">
                        <div class="accordion-item active">
                          <div class="accordion-collapse collapse show">
                            <div class="accordion-body">
                              <div class="card-body">
                                <div class="mb-3">
                                  <label for="formFile" class="form-label"></label>
                                  <input class="form-control" type="file" id="formFile" />
                                </div>
                                <div class="mb-3">
                                  <div class="form-floating form-floating-outline mb-4">
                                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" />
                                    <label for="nombre">Nombre del archivo</label>
                                  </div>
                                </div>
                                <div class="row g-3">
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-sm-6 col-lg-3">
                                    <div class="card card-border-shadow-primary h-100">
                                      <div class="card-body">
                                        <div class="d-flex align-items-center mb-2 pb-1">
                                          <div class="avatar me-2">
                                            <span class="avatar-initial rounded bg-label-primary"
                                              ><i class="mdi mdi-file-pdf-box mdi-20px"></i
                                            ></span>
                                          </div>
                                        </div>
                                        <p class="mb-0 text-heading">Demo 1</p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>    
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /FAQ's -->
              </div>
              <!-- /Contact -->
            </div>