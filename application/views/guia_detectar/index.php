<style type="text/css">
  .dfff{    color: black;  }

  textarea{ overflow: hidden; min-height: 30px; }
  .btn_f{    color: #5271ff;    border-color: #5271ff;    background: transparent;     border: 3px solid;  }  
  .btn_f:focus {    color: #666cff;    background-color: #dadcff;    border-color: #b3b6ff;  }
  .btn_f:hover {    color: #666cff !important;    background-color: #e8e9ff !important;    border-color: #b3b6ff !important;  }
  .btn_o{    color: #fe0b9b;    border-color: #fe0b9b;    background: transparent;     border: 3px solid;  }  
  .btn_o:focus {    color: #fe0b9b !important;    background-color: #f8cce8 !important;    border-color: #f98cce !important;  }
  .btn_o:hover {    color: #fe0b9b !important;    background-color: #f8cce8 !important;    border-color: #f98cce !important;  }
  .btn_a{    color: #a205ea;    border-color: #a205ea;    background: transparent;     border: 3px solid;  }  
  .btn_a:focus {    color: #a205ea !important;    background-color: #e3bef5 !important;    border-color: #d18af3 !important;  }
  .btn_a:hover {    color: #a205ea !important;    background-color: #e3bef5 !important;    border-color: #d18af3 !important;  }
  .btn_d{    color: #f46c14;    border-color: #f46c14;    background: transparent;     border: 3px solid;  }  
  .btn_d:focus {    color: #f46c14 !important;    background-color: #f6ddd0 !important;    border-color: #f6b992 !important;  }
  .btn_d:hover {    color: #f46c14 !important;    background-color: #f6ddd0 !important;    border-color: #f6b992 !important;  }
  .th_c {    background: #bd9250 !important;    color: #FBFFF4 !important; text-align: center;  padding: 7px; border-radius: 21px; font-size: 18px; padding-left: 12px; padding-right: 12px;}
  .td_c {    background: #d1b697 !important;    color: #FBFFF4 !important; text-align-last: center; }
  .btn_cafe{    color: #d1b697 !important;    cursor: pointer;  }
  .btn_gris{    color: #767676 !important;    cursor: pointer;  }
  .td_input{    padding-left: 1px !important;    /*padding-right: 1px !important;*/  }
  /*.table_info td {    padding-top: 1px;    padding-bottom: 1px;  }*/
  table {    caption-side: bottom;    border-collapse: collapse;  }
  thead, tbody, tfoot, tr, td, th {    border-color: inherit;    border-style: hidden;    border-width: 0;  }

  .th_cx {    background: #484947 !important; color: #FBFFF4 !important;  border-radius: 30px;  }
  .th_cxy{    background: #8eaadb !important;    color: #342e37 !important;  }
  .th_cxz{    background: #70ad47 !important;    color: #FBFFF4 !important;  border-radius: 30px; }
  .th_ce {    background: #8eaadb !important;    color: black !important;  }
  .th_re {    background: #b4c6e7 !important;    color: black !important;  }
  .th_mer {    background: #9cc2e5 !important;    color: black !important;  }
  .th_mer2 {    background: #d6dce4 !important;    color: black !important;  }
  .th_ent {    background: #deeaf6 !important;    color: black !important;  }
  .th_ren {    background: #adb9ca !important;    color: #FBFFF4 !important;  }
  .th_ges {    background: #333f4f !important;    color: #FBFFF4 !important;  }
  .th_ges2 {    background: #adb9ca !important;    color: #FBFFF4 !important;  }
  .th_pil {    background: #1f3864 !important;    color: #FBFFF4 !important;  }
  .th_pil2 {    background: #d9e2f3 !important;    color: #FBFFF4 !important;  }   


  #clonetr_data1{    top: 450px;  }
  #clonetr_data2{    top: 450px;  }
  #clonetr_data4{    top: 320px;  }
  #clonetr_data1,
  #clonetr_data2,
  #clonetr_data4{    width: auto !important;        position: absolute;    background: #ffffff;  }
  #clonetr_data1.fixed-button{    top: 63px;    position: fixed;  }
  #clonetr_data2.fixed-button{    top: 63px;    position: fixed;  }
  #clonetr_data4.fixed-button{    top: 63px;    position: fixed;  }
  #clonetr_data4 h3{margin-bottom: 0px;}

  .container-xxl{
    margin-left: 0px;
    margin-right: 0px;
  }
  @media (min-width: 1400px) {
    .container-xxl, .container-xl, .container-lg, .container-md, .container-sm, .container {
        max-width: 1641px;
    }
  }
  .img_li{ width: 70px;  }
  .contenedor{ min-width: 50px;  text-align: center;}

  .form-control{
    color: #342E37;
    background-color: #a7a7a4;
  }

  textarea{
    border-radius: 25px !important;
    border: 1px solid #d8d8dd00 !important;
  }

  .link_color{
     color: #bd9250 !important; 
  }
  .img_color{
    background: #bd9250; border-radius: 50px; 
  }
  .form-select {
    background-color: #a7a7a4;
  }

  select {
    border: 1px solid #d8d8dd00 !important;
  }
  .nav-tabs .nav-link, .nav-pills .nav-link {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    text-transform: unset;
  }
   
  .radioimgr{
    background-color: #ff4d49; border-color: #ff4d49; box-shadow: inset 0 0 0 2px #F4F5F8;
  }
  .radioimgv{
    background-color: #72e128; border-color: #72e128; box-shadow: inset 0 0 0 2px #F4F5F8;
  }

  .form-check-label {
    color: #ffffff !important;
    cursor: pointer;
  }

  textarea{
    text-align-last: center; text-align: center;
  }

  select{
    text-align-last: center;
  }

  .table th {
    text-transform: none !important;
  }

  .encabezado.fixed-button{
    position: fixed;
    top: 63px;
    width: 80%;
    background: #342e37;
  }

  .encabezado_m.fixed-button_m{
    position: fixed;
    top: 63px;
    width: 100%;
    background: #342e37;
  }


</style>
<?php 
  $tipo = $_GET['tipo'];
?>
<input type="hidden" id="tipo_m" value="<?php echo $tipo ?>">
<div class="container-xxl flex-grow-1 container-p-y" style="padding-top: 0px !important;">  
    <div class="">
      <div class="row g-6 mb-4">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <ul class="nav nav-pills flex-column flex-sm-row">
                    <li class="nav-item">
                      <a class="nav-link link_data" style="margin-right: 1.125rem;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=1">
                        <div class="contenedor">
                            <img src="<?php echo base_url().'images/graficos/icon-park-outline_shopping-bag.png';?>" class="img_li linkimg1"><br>
                            <div class="link_c link1_c" style="color: #FBFFF4;">Análisis del Mercado</div>
                        </div>
                      </a>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link link_data" style="margin-right: 1.125rem;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=2">
                        <div class="contenedor">
                        <img src="<?php echo base_url().'images/graficos/leader 1.png';?>" class="img_li linkimg2"><br>
                          <div class="link_c link2_c" style="color: #FBFFF4;">Análisis Interno</div>
                        </div>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link link_data" style="margin-right: 1.125rem;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=3">
                        <div class="contenedor">
                        <img src="<?php echo base_url().'images/graficos/handshake (1) 1.png';?>" class="img_li linkimg3"><br>
                          <div class="link_c link3_c" style="color: #FBFFF4;">Análisis del cliente</div>
                        </div>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link link_data" style="margin-right: 1.125rem;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=4">
                        <div class="contenedor" style="text-align: -webkit-center;">
                          <div class="img_li linkimg4">
                            <img src="<?php echo base_url().'images/graficos/statement 1.png';?>" style="width: 100%">
                          </div>  
                          <div class="link_c link4_c" style="color: #FBFFF4;">Análisis del estado Resultados</div>
                        </div>
                      </a>
                    </li>
                    <!-- <li class="nav-item">
                      <a class="nav-link link_data" href="<?php // echo base_url() ?>Guia_detectar_oportunidades?tipo=5">
                        <div class="contenedor">
                        <img src="<?php // echo base_url().'public/img/RESUMEN.svg';?>" class="img_li"><br>
                          <div class="link_c link5_c">Resumen de estrategias</div>
                        </div>
                      </a>
                    </li> -->
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="text_mercado text_mercado1">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <img src="<?php echo base_url() ?>images/graficos/icon-park-outline_shopping-bag.png" style="width: 172px;">
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Analiza a tus 5 principales competidores</h2>
                    </div>
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h2>Analiza tus oportunidades y amenazas</h2>
                      <p>Aprovecha cada oportunidad y anticipate a las amenazas. Identifica los aspectos del mercado que<br> 
                      pueden impulsar tu crecimiento y los posibles riesgos que podrían afectarte. Desarrolla estrategias<br> 
                      para maximizar las oportunidades y mitigar los riesgos, fortaleciendo así tu posición competitiva.</p>
                      
                      <p>Enlista las características principales que ofrecen tus 5 principales competidores:</p>
                
                      <p>En <span style="color: green">verde</span> son tus oportunidades:</p>
                      
                      <p>Tus competidores que tienen más mercado que tú y que representan oportunidades para aprender y crecer.<br>Observa sus estrategias y detecta áreas donde puedas mejorar o innovar.</p>
                      <p>En <span style="color: red">rojo</span> son tus amenazas:</p>
                      <p>Tus competidores que han crecido de forma importante y que pueden representar un riesgo para tu negocio.<br>Identifica sus fortalezas para anticiparte y prepararte frente a estas amenazas.</p>
                    </div> 
                    <div class="col-md-3">
                      <div>
                        <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                        </div> -->
                        <img style="width: 86%;" src="<?php echo base_url() ?>images/graficos/Illustration 1.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="row my-4">  
                      <div class="col-md-12">
                        <div class="tabla_guia"></div>
                        
                      </div>
                    </div>
                  <!--  -->
                </div>  
              </div> 
              <!--  -->
            </div>
          </div>
          <div class="text_mercado text_mercado2" style="display: none"> 
            <div class="card">
               <div class="card-body" style="padding-bottom: 0px;">
                  <div class="card academy-content shadow-none border">
                    <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                      <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                        <img src="<?php echo base_url() ?>images/graficos/mdi_medal-outline.png" style="width: 172px;">
                      </div>  
                      <div class="col-md-9" style="align-content: center;">
                        <h2>Selección de factores críticos de éxito externos</h2>
                        <h4>Ya que haz analizado a tu competencia, en este apartado selecciona cuáles de las fortalezas que tienes versus la competencia vas a mantener y qué debilidades vas a gestionar.</h4>
                      </div> 
                      <div class="col-md-1" style="text-align: right;"> 
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                      </div>
                    </div> 
                  </div>
                </div>
                <!--  -->
                <div class="card-body" >
                  <div class="card academy-content shadow-none border">
                    <div class="row g-3" style="margin: 12px;">
                      <div class="col-md-8">
                        <h4>Descubre los elementos clave para tu crecimiento y crea estrategias efectivas para maximizar su impacto.</h4>
                        <p>Selecciona las fortalezas que vas a mantener y las debilidades que deseas trabajar.<br> Evalúa tus fortalezas actuales para asegurar que sigan siendo una ventaja competitiva. Identifica las <br>debilidades que más impactan tu rendimiento y crea un plan para mejorarlas. Con este enfoque, <br>podrás centrarte en lo que realmente importa para alcanzar tus objetivos estratégicos.</p>
                      </div> 
                      <div class="col-md-4">
                        <div>
                          <img style="width: 90%;" src="<?php echo base_url() ?>images/graficos/Illustration 2.png">
                        </div>
                      </div>  
                    </div> 
                    <!--  -->
                    <div class="row ">  
                        <div class="col-md-12">
                          <div class="tabla_guia_mercado"></div>
                          
                        </div>
                      </div>
                    <!--  -->
                  </div>  
                </div> 
            </div>    
            <!--  --> 
          </div>  
          <!--  -->
          <div class="text_mercado text_mercado3" style="display: none"> 
            <div class="card">
               <div class="card-body" style="padding-bottom: 0px;">
                  <div class="card academy-content shadow-none border">
                    <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                      <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                        <img src="<?php echo base_url() ?>images/graficos/Flower.png" style="width: 172px;">
                      </div>  
                      <div class="col-md-9" style="align-content: center;">
                        <h2>Conclusiones del mercado</h2>
                      </div> 
                      <div class="col-md-1" style="text-align: right;"> 
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                      </div>
                    </div> 
                  </div>
                </div>
                <!--  -->
                <div class="card-body" >
                  <div class="card academy-content shadow-none border">
                    <div class="row g-3" style="margin: 12px;">
                      <div class="col-md-9">
                        <h2>A partir del análisis del mercado, enlista las oportunidades y amenazas que identificaste</h2>
                       <!--  <h4>Aprovecha las ventajas externas para fortalecer tu negocio</h4> -->
                        <p>Oportunidades: Son situaciones o circunstancias que pueden generar un beneficio para la empresa. Pueden surgir de cambios en el mercado, avances tecnológicos, tendencias de consumo, nuevas regulaciones gubernamentales, etc. <br>
                        Amenazas: Son factores externos que podrían comprometer el éxito de la empresa. Pueden ser los competidores emergentes o innovadores, los cambios regulatorios, y las fluctuaciones económicas.
                        </p>
                      </div> 
                      <div class="col-md-3">
                        <div>
                          <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                            
                          </div> -->
                          <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 3.png">
                        </div>
                      </div>  
                    </div> 
                    <!--  -->
                    <div class="row">  
                      <div class="col-md-12">
                        <div class="tabla_guia_mercadod"></div>
                        
                      </div>
                    </div>
                    
                    <!-- <div class="row">  
                      <div class="col-md-12">
                        <img style="width: 100%;" src="<?php // echo base_url() ?>images/linea.svg">
                        
                      </div>
                    </div> -->
                    <div class="div-linea"></div>
                    <div class="row g-3" style="margin: 12px;">
                      <div class="col-md-12">
                        <h2>Define tus principales oportunidades</h2>
                        <h5>Para tu análisis FODA</h5>
                      </div> 
                      <div class="col-md-9">
                        <!--  -->
                        <h6>Destaca todas las oportunidades vs la competencia</h6>
                        <form class="form" method="post" role="form" id="form_registro_o" novalidate>
                          <div class="row g-3">
                            <div class="col-md-10">
                              <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                <textarea rows="1" type="text" name="concepto" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_o">
                                  <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                </button>
                              </div>
                            </div>
                          </div> 
                        </form>
                        <div class="txt_oportunidades">
                        </div>
                        <!--  -->
                      </div>  

                      <div class="col-md-3" style="align-content: center;">
                        <div>
                          <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                            
                          </div> -->
                          <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 4.png">
                        </div>
                      </div>  
                    </div> 
                    <div class="div-linea"></div>
                    <div class="row g-3" style="margin: 12px;">
                      <div class="col-md-12">
                        <h2>Define tus principales amenazas</h2>
                        <h5>Para tu análisis FODA</h5>
                      </div> 
                      <div class="col-md-9">
                        <!--  -->
                        <h6>Encuentra factores de riesgo. Detecta los aspectos externos que pueden afectar a tu negocio y planifica cómo mitigarlos.</h6>
                        <form class="form" method="post" role="form" id="form_registro_a" novalidate>
                          <div class="row g-3">
                            <div class="col-md-10">
                              <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                <textarea rows="1" type="text" name="concepto" id="concepto_a" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                              </div>
                            </div>
                            <div class="col-md-2">
                              <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_a">
                                  <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                </button>
                              </div>
                            </div>
                          </div> 
                        </form>
                        <div class="txt_amenazas">

                          

                        </div>
                        <!--  -->
                      </div>  
                      <div class="col-md-3" style="align-content: center;">
                        <div>
                          <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                            
                          </div> -->
                          <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 5.png">
                        </div>
                      </div>  
                    </div> 

                    <div style="padding-left: 20px; padding-right: 20px;">
                      <div class="row">
                          <div class="col-md-1"></div>
                          <div class="col-md-3" style="text-align:center">
                              <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(1)"><span style="font-size: 20px;">Tus competidores</span></a>
                          </div>
                          <div class="col-md-3" style="text-align:center"> 
                              <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(2)"><span style="font-size: 20px;">Tu posición en el mercado</span></a>
                          </div>
                          <div class="col-md-3" style="text-align:center">
                               &nbsp; &nbsp;<a style="cursor:pointer" onclick="siguiente_competidores_ocultar(3)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Tu entorno externo</span></a><!--&nbsp;&nbsp; <span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                          </div>
                          <div class="col-md-2" style="text-align: right;">
                            <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=2">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                          </div>
                      </div>    
                    </div> 
                    <br>   
                  <!--  -->
                  </div>  
                </div> 
            </div>    
            <!--  --> 
          </div>  
          <!--  -->
          <div class="text_analisisinterno text_analisisinterno1" style="display: none">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <img src="<?php echo base_url() ?>images/graficos/Star_duotone_line.png" style="width: 172px;">
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Analiza tus fortalezas y debilidades internas</h2>
                    </div> 
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-8">
                      <p>Fortalezas: son las ventajas competitivas que hacen que una empresa destaque</p>
                      <p>Debilidades: son los aspectos que pueden ser un inconveniente en el funcionamiento y/o éxito de la empresa</p>
                      <!-- <p>Usa los colores para diferenciar entre los riesgos y las ventajas:</p>
                      <p>En <span style="color: green">verde</span> son tus fortalezas:</p>
                      <p>Destaca las áreas donde puedes crecer y mejorar aprovechando tus puntos fuertes</p>
                      <p>En <span style="color: red">rojo</span> son tus oportunidades:</p>
                      <p>Reconoce los riesgos que pueden desafiar tu negocio y planifica cómo superarlos.</p> -->
                    </div> 
                    <div class="col-md-4" >
                      <div>
                        <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                          
                        </div> -->
                        <img style="width: 90%;" src="<?php echo base_url() ?>images/graficos/Illustration 6.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="row my-4">  
                      <div class="col-md-12">
                        <div class="tabla_guia_fortaleza"></div>
                        
                      </div>
                    </div>
                  <!--  -->
                </div>  
              </div> 
            </div>  
          </div> 
          <!--  -->
          <div class="text_analisisinterno text_analisisinterno2" style="display: none">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <img src="<?php echo base_url() ?>images/graficos/Choose.png" style="width: 172px;">
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Selección de criterios de éxito internos</h2>
                      <h4>Ya que haz realizado un análisis interno, selecciona las fortalezas que vas a mantener/potenciar y/o las debilidades a gestionar</h4>
                    </div> 
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-8">
                      <h2>Selecciona los factores internos</h2>
                      <h5>Descubre e identifica los elementos clave para tu crecimiento y desarrolla estrategias efectivas.</h5>
                      <p>Selecciona las fortalezas que deseas mantener y las debilidades que necesitas mejorar. Analiza tus<br> fortalezas actuales para asegurarte de que continúen siendo una ventaja competitiva. Identifica las <br>debilidades que más afectan tu rendimiento y elabora un plan para superarlas. Con este enfoque,<br> podrás enfocarte en lo esencial y alcanzar tus objetivos estratégicos.</p>
                    </div> 
                    <div class="col-md-4">
                      <div>
                        <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                          
                        </div> -->
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 7.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="row my-4">  
                      <div class="col-md-12">
                        <div class="tabla_guia_fortalezas"></div>
                        
                      </div>
                    </div>
                  <!--  -->
                </div>  
              </div> 
            </div>  
          </div>  
          <!--  -->
          <div class="text_analisisinterno text_analisisinterno3" style="display: none">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <img src="<?php echo base_url() ?>images/graficos/Favorite_duotone_line.png" style="width: 172px;">
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Conclusiones del análisis interno</h2>
                      <!-- <h4>Analiza los factores que te hacen fuerte desde adentro y te diferencian.</h4> -->
                    </div> 
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h2>A partir del análisis interno, enlista tus fortalezas y debilidades que identificaste</h2>
                      <!-- <h5>Identifica tus puntos fuertes internos y trabaja en tus áreas de mejora. Analiza qué es lo que debes mantener.</h5> -->
                      <p>Fortalezas: son las ventajas competitivas que hacen que una empresa destaque</p>
                      <p>Debilidades: son los aspectos que pueden ser un inconveniente en el funcionamiento y/o éxito de la empresa</p>
                    </div> 
                    <div class="col-md-3">
                      <div>
                        <img style="width: 100%; " src="<?php echo base_url() ?>images/graficos/Illustration 8.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="div-linea"></div>
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-12">
                      <h2>Define tus principales fortalezas</h2>
                      <h5>Para tu análisis FODA</h5>
                    </div> 
                    <div class="col-md-9">
                      <!--  -->
                      <h6>Destaca lo que haces bien, identifica y maximiza las áreas internas donde tu negocio ya tiene éxito para aprovechar al máximo las oportunidades del mercado.</h6>
                      <form class="form" method="post" role="form" id="form_registro" novalidate>
                        <div class="row g-3">
                          <div class="col-md-10">
                            <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                              <textarea rows="1" type="text" name="concepto" id="concepto" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                              <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                                <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                              </button>
                            </div>
                          </div>
                        </div> 
                      </form>
                      <div class="txt_fortaleza">

                        

                      </div>
                      <!--  -->
                    </div>  

                    <div class="col-md-3" style="align-content: center;">
                      <div>
                        <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                          
                        </div> -->
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 9.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="div-linea"></div>
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-12">
                      <h2>Define tus principales debilidades</h2>
                      <h5>Para tu análisis FODA</h5>
                    </div> 
                    <div class="col-md-9">
                      <!--  -->
                      <h6>Encuentra áreas de mejora. Detecta los aspectos internos que pueden estar frenando tu progreso y planifica cómo fortalecerlos para convertirlos en oportunidades.</h6>
                      <form class="form" method="post" role="form" id="form_registro_d" novalidate>
                        <div class="row g-3">
                          <div class="col-md-10">
                            <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                              <textarea rows="1" type="text" name="concepto" id="concepto_d" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                              <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_d">
                                <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                              </button>
                            </div>
                          </div>
                        </div> 
                      </form>
                      <div class="txt_debilidades">

                        

                      </div>
                      <!--  -->
                    </div>  

                    <div class="col-md-3" style="align-content: center;">
                      <div>
                        <!-- <diV style="background: #bd9250; border-radius: 150px; padding: 31%; width: 63%; position: relative; top: 233px; left: 27%;">
                          
                        </div> -->
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 11.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  --><br><br>
                  <div style="padding-left: 20px; padding-right: 20px;">
                    <div class="row">
                      <div class="col-md-1" style="text-align: right;">
                        <!--<span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                      </diV>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(1)"><span style="font-size: 20px;">Apalancamiento interno</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(2)"><span style="font-size: 20px;">Factores internos</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(3)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Tu entorno interno</span></a>
                      </div>
                      <div class="col-md-2" style="text-align: right;">
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=3">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                      </div>
                    </div>    
                  </div>
                  <br>  
                  <!--  -->
                </div>  
              </div> 
            </div>
          </div>    
          <!--  -->
          <div class="text_analisis_cliente text_analisis_cliente1" style="display: none">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <br>
                      <img src="<?php echo base_url() ?>images/graficos/Handshake.png" style="width: 172px;">
                      <br><br>
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Análisis del cliente</h2>
                      <h4>Descubre los insights clave para entender mejor a tu cliente potencial.</h4>
                    </div> 
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <!-- <h2>Comprensión del Cliente</h2>
                      <h5>Identifica tus oportunidades</h5>
                      <p>Usa los colores para diferenciar entre los riesgos y las ventajas:</p>
                      <p>En <span style="color: green">verde</span> son tus oportunidades:</p>
                      <p>Destaca las áreas donde puedes crecer y mejorar aprovechando tus puntos fuertes.</p>
                      <p>En <span style="color: red">rojo</span> rojo son tus amenazas:</p>
                      <p>Reconoce los riesgos que pueden desafiar tu negocio y planifica cómo superarlos.</p> -->
                    </div> 
                    <div class="col-md-3" >
                      <div>
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 12.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="row">  
                    <div class="col-md-12">
                      <div class="tabla_guia_cliente"></div>
                      
                    </div>
                  </div>  
                  <!--  -->
                  <div class="div-linea"></div>
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h2>Análisis de tu cliente</h2>
                      <h5>Obtén insights clave sobre tu cliente potencial</h5><br>
                      <p>Si respondiste "No" a alguna de las preguntas anteriores, esta sección te ayudará a identificar oportunidades<br> para mejorar tu enfoque. Utiliza esta base para generar insights valiosos sobre tu cliente potencial y ajustar tu<br> estrategia para satisfacer mejor sus necesidades.</p>
                    </div> 
                    <div class="col-md-3" style="align-content: center;">
                      <div>
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 13.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <br>
                  <div class="row my-4">  
                    <div class="col-md-12">
                      <div class="tabla_guia_cliente_instrucciones"></div>
                      
                    </div>
                  </div>
                  <div style="padding-left: 20px; padding-right: 20px;">
                    <div class="row">
                        <div class="col-md-1">
                        </div>  
                        <div class="col-md-5" style="text-align:center">
                            <a style="cursor:pointer" onclick="siguiente_analisis_cliente_ocultar(1)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Análisis de estrategias para tu cliente</span></a>
                        </div>
                        <!-- <div class="col-md-3" style="text-align:center">
                            <a style="cursor:pointer" onclick="siguiente_analisis_cliente_ocultar(2)"><span style="font-size: 20px;">Estrategias para tu cliente</span></a>
                        </div> -->
                        <div class="col-md-1">
                            <!--<span style="background: #ffbd59; padding: 13px; padding-top: 0px; padding-bottom: 6px; border-radius: 12px;"></span>-->
                        </diV><!-- onclick="siguiente_analisis_cliente_ocultar(2)" -->
                        <div class="col-md-2" style="text-align: right;">
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=4">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                        </div>
                    </div>    
                  </div> 
                  <br>   
                  <!--  -->
                </div>  
              </div> 
            </div>  
          </div> 
          <!--  -->
          <div class="text_analisis_cliente text_analisis_cliente2" style="display: none">
            <div class="card">
              <div class="card-body">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <br>
                      <img src="<?php echo base_url() ?>images/graficos/Group.png" style="width: 172px;">
                      <br><br>
                    </div>  
                    <div class="col-md-10" style="align-content: center;">
                      <h2>Estrategias para tu cliente</h2>
                      <h4>Analiza y desarrolla estrategias personalizadas para satisfacer las necesidades y expectativas de tus clientes.</h4>
                    </div> 
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h2>Conclusiones de tu cliente</h2>
                      <h5>Identifica los insights clave de tu cliente y conviértelos en estrategias para tu éxito comercial</h5>
                      
                      <p>Estos son los insights clave que necesitas conocer sobre tu cliente potencial. Utiliza esta información como base para desarrollar tu comunicación publicitaria y conectar de manera efectiva con tu audiencia.</p>
                    </div> 
                    <div class="col-md-3" >
                      <div>
                        <img style="width: 80%;" src="<?php echo base_url() ?>images/graficos/Illustration 12.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->
                  <div class="row">  
                    <div class="col-md-12">
                      <!-- <h4>Guía de selección de factores críticos de éxito</h4> -->
                      <div class="tabla_guia_cliente_gsf"></div>
                    </div>
                  </div>  
                  <!--  -->
                  <div style="padding-left: 20px; padding-right: 20px;">
                    <div class="row">
                        <div class="col-md-1">
                        </div>  
                        <div class="col-md-3" style="text-align:center">
                            <a style="cursor:pointer" onclick="siguiente_analisis_cliente_ocultar(1)"><span style="font-size: 20px;">Análisis del cliente</span></a>
                        </div>
                        <div class="col-md-3" style="text-align:center">
                            <a style="cursor:pointer" onclick="siguiente_analisis_cliente_ocultar(2)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Estrategias para tu cliente</span></a>
                        </div>
                        <div class="col-md-1">
                            <!--<span style="background: #ffbd59; padding: 13px; padding-top: 0px; padding-bottom: 6px; border-radius: 12px;"></span>-->
                        </diV>
                        <div class="col-md-2" style="text-align: right;">
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Guia_detectar_oportunidades?tipo=4">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                        </div>
                    </div>    
                  </div> 
                  <br>   
                  <!--  -->
                </div>
              </div>
            </div>      
          </div>  
          <!--  -->
          <?php /*
          <div class="text_analisis_estado text_analisis_estado1" style="display: none">
            <div class="card">
              <div class="card-body">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <br>
                      <img src="<?php echo base_url() ?>images/graficos/statement 1.png" style="width: 172px;">
                      <br><br>
                    </div>  
                    <div class="col-md-10" style="align-content: center;">
                      <h2>Registra tu estado de resultados</h2>
                      <h4>Descubre la información financiera de tu estado de resultados y genera estrategias para tu éxito en las finanzas</h4>
                    </div> 
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h2>Registra tu Estado de Resultados</h2>
                      <h5>Explora tus datos financieros y desarrolla estrategias exitosas.</h5>
                      <br><br>
                      <p>Registra en esta sección la información de tu Estado de Resultados para que puedas desarrollar estrategias efectivas en el área financiera de tu empresa.</p>
                    </div> 
                    <div class="col-md-3" style="align-content: center;">
                      <div>
                        <img style="width: 100%; filter: invert(1.4);" src="<?php echo base_url() ?>images/graficos/bonbon-line-man-is-surprised-and-wonders-what-is-in-the-gift-2.png">
                      </div>
                    </div>  
                  </div> 
                  <!--  -->

                  <div style="padding-left: 20px; padding-right: 20px;">
                    <div class="row">
                        <div class="col-md-1" style="text-align: right;">
                            <span style="background: #ffbd59; padding: 13px; padding-top: 0px; padding-bottom: 6px; border-radius: 12px;"></span>
                        </diV>
                        <div class="col-md-3">
                            <span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Registro de estado de resultados</span>
                        </div>
                        <div class="col-md-3">
                            <span style="font-size: 20px;">Análisis del estado de resultados</span>
                        </div>
                        <div class="col-md-2" style="text-align: right;">
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_analisis_estado_ocultar(2)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                        </div>
                    </div>    
                  </div> 
                  <br>   
                  <!--  -->
                </div>  
              </div> 
              <!--  -->
            </div>  
          </div>  
          */ ?> 
          <!--  -->
          <div class="text_analisis_estado text_analisis_estado1" style="display: none">
            <div class="card">
              <div class="card-body" style="padding-bottom: 0px;">
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                    <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                      <br>
                      <img src="<?php echo base_url() ?>images/graficos/statement 1.png" style="width: 172px;">
                      <br><br>
                    </div>  
                    <div class="col-md-9" style="align-content: center;">
                      <h2>Análisis de tu situación financiera</h2>
                      
                    </div> 
                    <div class="col-md-1" style="text-align: right;"> 
                      <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                    </div>
                  </div> 
                </div>
              </div>
              <!--  -->
              <div class="card-body" >
                <div class="card academy-content shadow-none border">
                  <div class="row g-3" style="margin: 12px;">
                    <div class="col-md-9">
                      <h4>Analiza, responde las siguientes preguntas y selecciona las debilidades que identifiques en tu situación financiera</h4>
                    </div> 
                    <div class="col-md-3">
                      <div>
                        <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 14.png">
                      </div>
                    </div>  
                  </div>
                  <!--  -->
                  <div class="row">  
                    <div class="col-md-12">
                      <div class="tabla_guia_estados_resultados"></div>
                    </div>
                  </div>   
                  <!--  -->
                  <div style="padding-left: 20px; padding-right: 20px;">
                    <div class="row">
                        <div class="col-md-3" style="text-align: right;">
                        </diV>
                        <div class="col-md-5">
                          <!--<span style="background: #ffbd59; padding: 13px; padding-top: 0px; padding-bottom: 6px; border-radius: 12px;"></span>&nbsp;&nbsp;&nbsp;&nbsp;-->
                          <span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Análisis del estado de resultados</span>
                        </div>
                        <div class="col-md-2" style="text-align: right;">
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Analisisfoda?con=1">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                        </div>
                    </div>    
                  </div> 
                  <br>
                  <!--  -->
                </div>
              </div>
            </div>  
          </div>      
          <!--  -->
        </div>
      </div>
    </div>

    <?php /* 
    <div class="vista_txt vista_txt_1" style="display: none">
      <div class="accordion" id="accordionExample">
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed generar_1" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne_el_m" aria-expanded="false" aria-controls="collapseOne" onclick="general(1,1)">
              Guía para detectar oportunidades
            </button>
          </h2>
          <div id="collapseOne_el_m" class="accordion-collapse collapse" data-bs-parent="#accordionExample" style="">
            <div class="accordion-body">
              <!----------------------------------------------------------------------------->
                  <!-- <div class="row my-5">
                    <div class="col-md-12 col-lg-6">
                      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(254 11 155) !important; border: 2px solid #fe0b9b;">
                        <div style="top: -55px !important; position: absolute; left: -62px;">
                            <img src="<?php // echo base_url(); ?>public/img/op4/op2.svg">
                        </div>
                        <br><br><br>
                        <div class="d-flex align-items-end row">

                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <div class="txt_oportunidades">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style="text-align: right; top: -56px !important; position: relative; left: 12px;">
                        <button type="button" class="btn btn-sm me-3 waves-effect btn_o" data-bs-toggle="modal"
                                    data-bs-target="#modal_oportunidades" style="font-size: 27px;">O</button>
                      </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(162 5 234) !important; border: 2px solid #a205ea;">
                        <div style="top: -38px !important; position: absolute; text-align: right; right: -62px;">
                            <img src="<?php // echo base_url(); ?>public/img/op4/op4.svg">
                        </div>
                        
                        <br><br><br>
                        <div class="d-flex align-items-end row">
                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <div class="txt_amenazas">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style="top: -56px !important; position: relative; left: 4px;">
                        <button type="button" class="btn btn-sm me-3 waves-effect btn_a" data-bs-toggle="modal"
                                    data-bs-target="#modal_amenazas" style="font-size: 27px;">A</button>
                      </div>
                    </div>

                  </div> -->
                  <!-- <div class="row my-4">  
                      <div class="col-xl-12">
                        <div class="pestanas_text"></div>
                      </div>
                  </div> -->
                  <!-- <div class="row">
                    <div class="col-md-12" style="text-transform: uppercase;font-size: 16px;color: #bd9250;font-weight: bolder;">
                      Análisis de la competencia
                    </div>
                  </div>
                  <div class="row my-4">  
                    <div class="col-md-4">
                      <div class="form-floating form-floating-outline mb-4">
                        <input type="text" class="form-control" id="nombrec" placeholder="" />
                        <label for="nombrec">Agregar competidor</label>
                      </div>
                    </div>
                    <div class="col-md-1">
                      <div class="btn_edit">
                        <button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="guardar_competidor()">
                          <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="row my-4">  
                    <div class="col-md-12">
                      <div class="tabla_guia"></div>
                      
                    </div>
                  </div>  --> 
              <!----------------------------------------------------------------------------->
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed generar_2" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo_el_m" aria-expanded="false" aria-controls="collapseTwo" onclick="general(1,2)">
              Guía de selección de factores críticos de éxito 
            </button>
          </h2>
          <div id="collapseTwo_el_m" class="accordion-collapse collapse" data-bs-parent="#accordionExample">
            <div class="accordion-body">
              <!----------------------------------------------------------------------------->
                <div class="row my-4">  
                  <div class="col-md-12">
                     <h4 style="color: white">Guía de selección de factores críticos de éxito</h4> 
                    <div class="tabla_guia_mercado"></div>
                    
                  </div>
                </div> 
              <!----------------------------------------------------------------------------->
            </div>
          </div>
        </div>
      </div>
      <!-------------------------------------------------->
      
      <!-------------------------------------------------->
    </div> 
    */
    ?>   
    
    <!-- <div class="vista_txt vista_txt_2" style="display: none">
        <div class="accordion" id="accordionExample2">
              <div class="accordion-item">
                <h2 class="accordion-header">
                  <button class="accordion-button collapsed generar_3" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne_la_f" aria-expanded="false" aria-controls="collapseOne" onclick="general(2,3)">
                    Guía para detectar oportunidades
                  </button>
                </h2>
                <div id="collapseOne_la_f" class="accordion-collapse collapse" data-bs-parent="#accordionExample2" style="">
                  <div class="accordion-body"> -->
                    <!----------------------------------------------------------------------------->
                        <!-- <div class="row my-5">
                          <div class="col-md-12 col-lg-6">
                            <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(82 113 255) !important; border: 2px solid #5271ff;">
                              <div style="top: -55px !important; position: absolute; left: -62px;">
                                  <img src="<?php // echo base_url(); ?>public/img/op4/op.svg">
                              </div>
                              <br><br><br>
                              <div class="d-flex align-items-end row">

                                <div class="col-md-12 order-1 order-md-2">
                                  <div class="card-body p-4">
                                    <div class="txt_fortaleza">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div style="text-align: right; top: -56px !important; position: relative; left: 12px;">
                              <button type="button" class="btn btn-sm me-3 waves-effect btn_f" data-bs-toggle="modal"
                                          data-bs-target="#modal_fortaleza" style="font-size: 27px;">F</button>
                            </div>
                          </div>
                          <div class="col-md-12 col-lg-6">
                            <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(244 108 20) !important; border: 2px solid #f46c14;">
                              <div style="top: -38px !important; position: absolute; text-align: right; right: -62px;">
                                  <img src="<?php // echo base_url(); ?>public/img/op4/op3.svg">
                              </div>
                              
                              <br><br><br>
                              <div class="d-flex align-items-end row">
                                <div class="col-md-12 order-1 order-md-2">
                                  <div class="card-body p-4">
                                    <div class="txt_debilidades">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div style="top: -56px !important; position: relative; left: 4px;">
                              <button type="button" class="btn btn-sm me-3 waves-effect btn_d" data-bs-toggle="modal"
                                          data-bs-target="#modal_debilidades" style="font-size: 27px;">D</button>
                            </div>
                          </div>

                        </div>
                        <div class="row my-4">  
                          <div class="col-md-12">
                            <div class="tabla_guia_fortaleza"></div>
                            
                          </div>
                        </div> -->
                    <!----------------------------------------------------------------------------->
                  <!-- </div>
                </div>
              </div>
              <div class="accordion-item">
                <h2 class="accordion-header">
                  <button class="accordion-button collapsed generar_4" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo_la_f" aria-expanded="false" aria-controls="collapseTwo" onclick="general(2,4)">
                    Guía de selección de factores críticos de éxito 
                  </button>
                </h2>
                <div id="collapseTwo_la_f" class="accordion-collapse collapse" data-bs-parent="#accordionExample2">
                  <div class="accordion-body"> -->
                    <!----------------------------------------------------------------------------->
                       <!--  <div class="row my-4">  
                          <div class="col-md-12">
                            <h4 style="color: white">Guía de selección de factores críticos de éxito</h4>
                            <div class="tabla_guia_fortalezas"></div>
                          </div>
                        </div> -->
                    <!----------------------------------------------------------------------------->
                  <!-- </div>
                </div>
              </div>
        </div>
    </div>     -->


    <div class="vista_txt vista_txt_3" style="display: none">
      <div class="accordion" id="accordionExample3">
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed generar_5" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne_cli" aria-expanded="false" aria-controls="collapseOne" onclick="general(3,5)">
              Guía para detectar oportunidades
            </button>
          </h2>
          <div id="collapseOne_cli" class="accordion-collapse collapse" data-bs-parent="#accordionExample3" style="">
            <div class="accordion-body">
              <!----------------------------------------------------------------------------->
                <div class="row ">  
                  <div class="col-md-12">
                    <div class="tabla_guia_cliente"></div>
                    
                  </div>
                </div>  
                <br>
                <div class="row my-4">  
                  <div class="col-md-12">
                    <div class="tabla_guia_cliente_instrucciones"></div>
                    
                  </div>
                </div>
              <!----------------------------------------------------------------------------->
            </div>
          </div>
        </div>
        <div class="accordion-item">
          <h2 class="accordion-header">
            <button class="accordion-button collapsed generar_6" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo_cli" aria-expanded="false" aria-controls="collapseTwo" onclick="general(3,6)">
              Guía de selección de factores críticos de éxito 
            </button>
          </h2>
          <div id="collapseTwo_cli" class="accordion-collapse collapse" data-bs-parent="#accordionExample3">
            <div class="accordion-body">
              <!----------------------------------------------------------------------------->
                <div class="vista_txt vista_txt_3" style="display: none">    
                  <div class="row my-4">  
                    <div class="col-md-12">
                      <!-- <h4>Guía de selección de factores críticos de éxito</h4> -->
                      <div class="tabla_guia_cliente_gsf"></div>
                    </div>
                  </div>  
                </div> 
              <!----------------------------------------------------------------------------->
            </div>
          </div>
        </div>
      </div>
        
    </div>   
    <div class="vista_txt vista_txt_4" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Guía de selección de factores críticos de éxito</h4>
          <div class="tabla_guia_estados_resultados"></div>
        </div>
      </div>   
    </div>    
    <div class="vista_txt vista_txt_5" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <h4>Resumen de Indicadores Estratégicos</h4>
          <div class="tabla_guia_resumen_estrategias"></div>
        </div>
      </div>   
    </div>  
</div>        


<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_oportunidades"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4>Oportunidades</h4>
              <p style="text-align: justify;">Son los factores externos que la empresa puede aprovechar para alcanzar sus objetivos. Estas pueden ser tendencias de mercado, cambios regulatorios, cambios demográficos, asociaciones estratégicas, entre otros.
              <br><br>
              Enumera las oportunidades que observes en el mercado, que te pueden beneficiar en un futuro</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>


<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_amenazas"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4>Amenazas</h4>
              <p style="text-align: justify;">Son los factores externos que podrían causar problemas o daños a la empresa, que pueden poner en peligro la posición o el desempeño de la entidad.<br> 
              Pueden ser la competencia, cambios en la legislación, riesgos económicos, entre otros.
              <br><br>
              Enumera las amenazas que observas en tu mercado</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

