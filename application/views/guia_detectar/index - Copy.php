<style type="text/css">
  .dfff{
    color: black;
  }
  textarea{
        overflow: hidden;
        min-height: 30px;
    }

  .btn_f{
    color: #5271ff;
    border-color: #5271ff;
    background: transparent; 
    border: 3px solid;
  }  

  .btn_f:focus {
    color: #666cff;
    background-color: #dadcff;
    border-color: #b3b6ff;
  }

  .btn_f:hover {
    color: #666cff !important;
    background-color: #e8e9ff !important;
    border-color: #b3b6ff !important;
  }


  .btn_o{
    color: #fe0b9b;
    border-color: #fe0b9b;
    background: transparent; 
    border: 3px solid;
  }  

  .btn_o:focus {
    color: #fe0b9b !important;
    background-color: #f8cce8 !important;
    border-color: #f98cce !important;
  }

  .btn_o:hover {
    color: #fe0b9b !important;
    background-color: #f8cce8 !important;
    border-color: #f98cce !important;
  }

  .btn_a{
    color: #a205ea;
    border-color: #a205ea;
    background: transparent; 
    border: 3px solid;
  }  

  .btn_a:focus {
    color: #a205ea !important;
    background-color: #e3bef5 !important;
    border-color: #d18af3 !important;
  }

  .btn_a:hover {
    color: #a205ea !important;
    background-color: #e3bef5 !important;
    border-color: #d18af3 !important;
  }

  .btn_d{
    color: #f46c14;
    border-color: #f46c14;
    background: transparent; 
    border: 3px solid;
  }  

  .btn_d:focus {
    color: #f46c14 !important;
    background-color: #f6ddd0 !important;
    border-color: #f6b992 !important;
  }

  .btn_d:hover {
    color: #f46c14 !important;
    background-color: #f6ddd0 !important;
    border-color: #f6b992 !important;
  }

  .th_c {
    background: #1e4e79 !important;
    color: white !important;
    text-align-last: center;
    width: 108px;
  }

  .td_c {
    background: #d1b697 !important;
    color: white !important;
    text-align-last: center;
    width: 108px;
  }
  .btn_cafe{
    color: #d1b697 !important;
    cursor: pointer;
  }
  .btn_gris{
    color: #767676 !important;
    cursor: pointer;
  }

  .td_input{
    padding-left: 1px !important;
    padding-right: 1px !important;
  }
  /*.table_info td {
    padding-top: 1px;
    padding-bottom: 1px;
  }*/

  table {
    caption-side: bottom;
    border-collapse: collapse;
  }
   
  thead, tbody, tfoot, tr, td, th {
    border-color: inherit;
    border-style: hidden;
    border-width: 0;
  }
</style>
<div class="container-xxl flex-grow-1 container-p-y">  
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-pills flex-column flex-sm-row mb-4">
          <li class="nav-item">
            <a class="nav-link link_data link1" onclick="vista_tipo(1)" 
              >El Mercado</a
            >
          </li>
          <li class="nav-item ">
            <a class="nav-link link_data link2" onclick="vista_tipo(2)"
              >Las Fortalezas</a
            >
          </li>
          <li class="nav-item">
            <a class="nav-link link_data link3" onclick="vista_tipo(3)"
              >El cliente</a
            >
          </li>
        </ul>
      </div>
    </div>
    <div class="vista_txt vista_txt_1" style="display: none">
      <div class="row my-4">
        <div class="col-md-12 col-lg-6">
          <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(254 11 155) !important; border: 2px solid #fe0b9b;">
            <div style="top: -55px !important; position: absolute; left: -62px;">
                <img src="<?php echo base_url(); ?>public/img/op4/op2.svg">
            </div>
            <br><br><br>
            <div class="d-flex align-items-end row">

              <div class="col-md-12 order-1 order-md-2">
                <div class="card-body p-4">
                  <div class="txt_oportunidades">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="text-align: right; top: -56px !important; position: relative; left: 12px;">
            <button type="button" class="btn btn-sm me-3 waves-effect btn_o" data-bs-toggle="modal"
                        data-bs-target="#modal_oportunidades" style="font-size: 27px;">O</button>
          </div>
        </div>
        <div class="col-md-12 col-lg-6">
          <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(162 5 234) !important; border: 2px solid #a205ea;">
            <div style="top: -38px !important; position: absolute; text-align: right; right: -62px;">
                <img src="<?php echo base_url(); ?>public/img/op4/op4.svg">
            </div>
            
            <br><br><br>
            <div class="d-flex align-items-end row">
              <div class="col-md-12 order-1 order-md-2">
                <div class="card-body p-4">
                  <div class="txt_amenazas">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="top: -56px !important; position: relative; left: 4px;">
            <button type="button" class="btn btn-sm me-3 waves-effect btn_a" data-bs-toggle="modal"
                        data-bs-target="#modal_amenazas" style="font-size: 27px;">A</button>
          </div>
        </div>

      </div>
      <!-- <div class="row my-4">  
          <div class="col-xl-12">
            <div class="pestanas_text"></div>
          </div>
      </div> -->
      <div class="row">
        <div class="col-md-12" style="text-transform: uppercase;font-size: 0.75rem;color: #7a7c8c;">
          Análisis de la competencia
        </div>
      </div>
      <div class="row my-4">  
        <div class="col-md-4">
          <div class="form-floating form-floating-outline mb-4">
            <input type="text" class="form-control" id="nombrec" placeholder="" />
            <label for="nombrec">Agregar competidor</label>
          </div>
        </div>
        <div class="col-md-1">
          <div class="btn_edit">
            <button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_c" onclick="guardar_competidor()">
              <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
            </button>
          </div>
        </div>
      </div>
      <div class="row my-4">  
        <div class="col-md-12">
          <div class="tabla_guia"></div>
          
        </div>
      </div>  
    </div>    
    <div class="vista_txt vista_txt_2" style="display: none">
      <div class="row my-4">
        <div class="col-md-12 col-lg-6">
          <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(82 113 255) !important; border: 2px solid #5271ff;">
            <div style="top: -55px !important; position: absolute; left: -62px;">
                <img src="<?php echo base_url(); ?>public/img/op4/op.svg">
            </div>
            <br><br><br>
            <div class="d-flex align-items-end row">

              <div class="col-md-12 order-1 order-md-2">
                <div class="card-body p-4">
                  <div class="txt_fortaleza">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="text-align: right; top: -56px !important; position: relative; left: 12px;">
            <button type="button" class="btn btn-sm me-3 waves-effect btn_f" data-bs-toggle="modal"
                        data-bs-target="#modal_fortaleza" style="font-size: 27px;">F</button>
          </div>
        </div>
        <div class="col-md-12 col-lg-6">
          <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(244 108 20) !important; border: 2px solid #f46c14;">
            <div style="top: -38px !important; position: absolute; text-align: right; right: -62px;">
                <img src="<?php echo base_url(); ?>public/img/op4/op3.svg">
            </div>
            
            <br><br><br>
            <div class="d-flex align-items-end row">
              <div class="col-md-12 order-1 order-md-2">
                <div class="card-body p-4">
                  <div class="txt_debilidades">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div style="top: -56px !important; position: relative; left: 4px;">
            <button type="button" class="btn btn-sm me-3 waves-effect btn_d" data-bs-toggle="modal"
                        data-bs-target="#modal_debilidades" style="font-size: 27px;">D</button>
          </div>
        </div>

      </div>
      <div class="row my-4">  
        <div class="col-md-12">
          <div class="tabla_guia_fortaleza"></div>
          
        </div>
      </div>  
    </div>    
    <div class="vista_txt vista_txt_3" style="display: none">    
      <div class="row my-4">  
        <div class="col-md-12">
          <div class="tabla_guia_cliente"></div>
          
        </div>
      </div>  
      <br>
      <div class="row my-4">  
        <div class="col-md-12">
          <div class="tabla_guia_cliente_instrucciones"></div>
          
        </div>
      </div>  
    </div>    
</div>        


<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_oportunidades"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4>Oportunidades</h4>
              <p style="text-align: justify;">Son los factores externos que la empresa puede aprovechar para alcanzar sus objetivos. Estas pueden ser tendencias de mercado, cambios regulatorios, cambios demográficos, asociaciones estratégicas, entre otros.
              <br><br>
              Enumera las oportunidades que observes en el mercado, que te pueden beneficiar en un futuro</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>


<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_amenazas"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4>Amenazas</h4>
              <p style="text-align: justify;">Son los factores externos que podrían causar problemas o daños a la empresa, que pueden poner en peligro la posición o el desempeño de la entidad.<br> 
              Pueden ser la competencia, cambios en la legislación, riesgos económicos, entre otros.
              <br><br>
              Enumera las amenazas que observas en tu mercado</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

