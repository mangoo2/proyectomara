<?php
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_cumplimiento".$fecha.".xls");
?>
<?php /*echo utf8_decode('DIRECCION')*/ ?>


<table  border="1">
    <thead>
        <tr>
            <th>Prioridad</th>
            <th>Fecha</th>
            <th>Actividad a Realizarse</th>
            <th>Semana 1</th>
            <th>Semana 2</th>
            <th>Semana 3</th>
            <th>Semana 4</th>
            <th>Semana 5</th>
        </tr> 
    </thead>
    <tbody>
        <?php
        $html='';
        
          $result1=$this->Model_guia_seleccion->get_pri_mercadoresultados_actividad($idcliente);
          foreach ($result1 as $cro){
              $ba1='';
              $ba2='';
              $ba3='';
              $ba4='';
              $ba5='';
              $prioridad_txt='';
              if($cro->resultado==1){
                  $prioridad_txt='Alta';
              }else if($cro->resultado==2){
                  $prioridad_txt='Media';
              }else if($cro->resultado==3){
                  $prioridad_txt='Baja';
              }
               
              if($cro->semana1==1){
                  $ba1='background: #70ad47;';
              }else if($cro->semana1==2){
                  $ba1='background: #e31a1a;';
              }else{
                  $ba1='background: #a7a7a4;';
              }
              if($cro->semana2==1){
                  $ba2='background: #70ad47;';
              }else if($cro->semana2==2){
                  $ba2='background: #e31a1a;';
              }else{
                  $ba2='background: #a7a7a4;';
              }
              if($cro->semana3==1){
                  $ba3='background: #70ad47;';
              }else if($cro->semana3==2){
                  $ba3='background: #e31a1a;';
              }else{
                  $ba3='background: #a7a7a4;';
              }
              if($cro->semana4==1){
                  $ba4='background: #70ad47;';
              }else if($cro->semana4==2){
                  $ba4='background: #e31a1a;';
              }else{
                  $ba4='background: #a7a7a4;';
              }
              if($cro->semana5==1){
                  $ba5='background: #70ad47;';
              }else if($cro->semana5==2){
                  $ba5='background: #e31a1a;';
              }else{
                  $ba5='background: #a7a7a4;';
              }
              $html.='<tr>
                <td>'.utf8_decode($prioridad_txt).'</td>
                <td>'.utf8_decode(date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5))).'</td>
                <td>'.utf8_decode($cro->preg2).'</td>
                <td style="'.$ba1.'">'; if($cro->semana1==1){$html.='Si';} if($cro->semana1==2){$html.='No';} $html.='</td>
                <td style="'.$ba2.'">'; if($cro->semana2==1){$html.='Si';} if($cro->semana2==2){$html.='No';} $html.='</td>
                <td style="'.$ba3.'">'; if($cro->semana3==1){$html.='Si';} if($cro->semana3==2){$html.='No';} $html.='</td>
                <td style="'.$ba4.'">'; if($cro->semana4==1){$html.='Si';} if($cro->semana4==2){$html.='No';} $html.='</td>
                <td style="'.$ba5.'">'; if($cro->semana5==1){$html.='Si';} if($cro->semana5==2){$html.='No';} $html.='</td>
              </tr>';
          }

          $result2=$this->Model_guia_seleccion->get_pri_internoesultados_actividad($idcliente);
          foreach ($result2 as $cro){
              $ba1='';
              $ba2='';
              $ba3='';
              $ba4='';
              $ba5='';
              $prioridad_txt='';
              if($cro->resultado==1){
                  $prioridad_txt='Alta';
              }else if($cro->resultado==2){
                  $prioridad_txt='Media';
              }else if($cro->resultado==3){
                  $prioridad_txt='Baja';
              }

              if($cro->semana1==1){
                  $ba1='background: #70ad47;';
              }else if($cro->semana1==2){
                  $ba1='background: #e31a1a;';
              }else{
                  $ba1='background: #a7a7a4;';
              }
              if($cro->semana2==1){
                  $ba2='background: #70ad47;';
              }else if($cro->semana2==2){
                  $ba2='background: #e31a1a;';
              }else{
                  $ba2='background: #a7a7a4;';
              }
              if($cro->semana3==1){
                  $ba3='background: #70ad47;';
              }else if($cro->semana3==2){
                  $ba3='background: #e31a1a;';
              }else{
                  $ba3='background: #a7a7a4;';
              }
              if($cro->semana4==1){
                  $ba4='background: #70ad47;';
              }else if($cro->semana4==2){
                  $ba4='background: #e31a1a;';
              }else{
                  $ba4='background: #a7a7a4;';
              }
              if($cro->semana5==1){
                  $ba5='background: #70ad47;';
              }else if($cro->semana5==2){
                  $ba5='background: #e31a1a;';
              }else{
                  $ba5='background: #a7a7a4;';
              }
              $html.='<tr>
                <td>'.utf8_decode($prioridad_txt).'</td>
                <td>'.utf8_decode(date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5))).'</td>
                <td>'.utf8_decode($cro->preg2).'</td>
                <td style="'.$ba1.'">'; if($cro->semana1==1){$html.='Si';} if($cro->semana1==2){$html.='No';} $html.='</td>
                <td style="'.$ba2.'">'; if($cro->semana2==1){$html.='Si';} if($cro->semana2==2){$html.='No';} $html.='</td>
                <td style="'.$ba3.'">'; if($cro->semana3==1){$html.='Si';} if($cro->semana3==2){$html.='No';} $html.='</td>
                <td style="'.$ba4.'">'; if($cro->semana4==1){$html.='Si';} if($cro->semana4==2){$html.='No';} $html.='</td>
                <td style="'.$ba5.'">'; if($cro->semana5==1){$html.='Si';} if($cro->semana5==2){$html.='No';} $html.='</td>
              </tr>';
          }

          $result3=$this->Model_guia_seleccion->get_pri_estadoesultados_actividad($idcliente);
          foreach ($result3 as $cro){
              $ba1='';
              $ba2='';
              $ba3='';
              $ba4='';
              $ba5='';

              $prioridad_txt='';
              if($cro->resultado==1){
                  $prioridad_txt='Alta';
              }else if($cro->resultado==2){
                  $prioridad_txt='Media';
              }else if($cro->resultado==3){
                  $prioridad_txt='Baja';
              }

              if($cro->semana1==1){
                  $ba1='background: #70ad47;';
              }else if($cro->semana1==2){
                  $ba1='background: #e31a1a;';
              }else{
                  $ba1='background: #a7a7a4;';
              }
              if($cro->semana2==1){
                  $ba2='background: #70ad47;';
              }else if($cro->semana2==2){
                  $ba2='background: #e31a1a;';
              }else{
                  $ba2='background: #a7a7a4;';
              }
              if($cro->semana3==1){
                  $ba3='background: #70ad47;';
              }else if($cro->semana3==2){
                  $ba3='background: #e31a1a;';
              }else{
                  $ba3='background: #a7a7a4;';
              }
              if($cro->semana4==1){
                  $ba4='background: #70ad47;';
              }else if($cro->semana4==2){
                  $ba4='background: #e31a1a;';
              }else{
                  $ba4='background: #a7a7a4;';
              }
              if($cro->semana5==1){
                  $ba5='background: #70ad47;';
              }else if($cro->semana5==2){
                  $ba5='background: #e31a1a;';
              }else{
                  $ba5='background: #a7a7a4;';
              }
              $html.='<tr>
                <td>'.utf8_decode($prioridad_txt).'</td>
                <td>'.utf8_decode(date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5))).'</td>
                <td>'.utf8_decode($cro->preg2).'</td>
                <td style="'.$ba1.'">'; if($cro->semana1==1){$html.='Si';} if($cro->semana1==2){$html.='No';} $html.='</td>
                <td style="'.$ba2.'">'; if($cro->semana2==1){$html.='Si';} if($cro->semana2==2){$html.='No';} $html.='</td>
                <td style="'.$ba3.'">'; if($cro->semana3==1){$html.='Si';} if($cro->semana3==2){$html.='No';} $html.='</td>
                <td style="'.$ba4.'">'; if($cro->semana4==1){$html.='Si';} if($cro->semana4==2){$html.='No';} $html.='</td>
                <td style="'.$ba5.'">'; if($cro->semana5==1){$html.='Si';} if($cro->semana5==2){$html.='No';} $html.='</td>
              </tr>';
          }
          
          $result4=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_actividad($idcliente);
          foreach ($result4 as $cro){
              $ba1='';
              $ba2='';
              $ba3='';
              $ba4='';
              $ba5='';

              $prioridad_txt='';
              if($cro->prioridad==1){
                  $prioridad_txt='Alta';
              }else if($cro->prioridad==2){
                  $prioridad_txt='Media';
              }else if($cro->prioridad==3){
                  $prioridad_txt='Baja';
              }

              if($cro->semana1==1){
                  $ba1='background: #70ad47;';
              }else if($cro->semana1==2){
                  $ba1='background: #e31a1a;';
              }else{
                  $ba1='background: #a7a7a4;';
              }
              if($cro->semana2==1){
                  $ba2='background: #70ad47;';
              }else if($cro->semana2==2){
                  $ba2='background: #e31a1a;';
              }else{
                  $ba2='background: #a7a7a4;';
              }
              if($cro->semana3==1){
                  $ba3='background: #70ad47;';
              }else if($cro->semana3==2){
                  $ba3='background: #e31a1a;';
              }else{
                  $ba3='background: #a7a7a4;';
              }
              if($cro->semana4==1){
                  $ba4='background: #70ad47;';
              }else if($cro->semana4==2){
                  $ba4='background: #e31a1a;';
              }else{
                  $ba4='background: #a7a7a4;';
              }
              if($cro->semana5==1){
                  $ba5='background: #70ad47;';
              }else if($cro->semana5==2){
                  $ba5='background: #e31a1a;';
              }else{
                  $ba5='background: #a7a7a4;';
              }
              $html.='<tr>
                <td>'.utf8_decode($prioridad_txt).'</td>
                <td>'.utf8_decode(date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5))).'</td>
                <td>'.utf8_decode($cro->preg2).'</td>
                <td style="'.$ba1.'">'; if($cro->semana1==1){$html.='Si';} if($cro->semana1==2){$html.='No';} $html.='</td>
                <td style="'.$ba2.'">'; if($cro->semana2==1){$html.='Si';} if($cro->semana2==2){$html.='No';} $html.='</td>
                <td style="'.$ba3.'">'; if($cro->semana3==1){$html.='Si';} if($cro->semana3==2){$html.='No';} $html.='</td>
                <td style="'.$ba4.'">'; if($cro->semana4==1){$html.='Si';} if($cro->semana4==2){$html.='No';} $html.='</td>
                <td style="'.$ba5.'">'; if($cro->semana5==1){$html.='Si';} if($cro->semana5==2){$html.='No';} $html.='</td>
              </tr>';
          }

          $result5=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_actividad($idcliente);
          foreach ($result5 as $cro){
              $ba1='';
              $ba2='';
              $ba3='';
              $ba4='';
              $ba5='';

              $prioridad_txt='';
              if($cro->prioridad==1){
                  $prioridad_txt='Alta';
              }else if($cro->prioridad==2){
                  $prioridad_txt='Media';
              }else if($cro->prioridad==3){
                  $prioridad_txt='Baja';
              }
              
              if($cro->semana1==1){
                  $ba1='background: #70ad47;';
              }else if($cro->semana1==2){
                  $ba1='background: #e31a1a;';
              }else{
                  $ba1='background: #a7a7a4;';
              }
              if($cro->semana2==1){
                  $ba2='background: #70ad47;';
              }else if($cro->semana2==2){
                  $ba2='background: #e31a1a;';
              }else{
                  $ba2='background: #a7a7a4;';
              }
              if($cro->semana3==1){
                  $ba3='background: #70ad47;';
              }else if($cro->semana3==2){
                  $ba3='background: #e31a1a;';
              }else{
                  $ba3='background: #a7a7a4;';
              }
              if($cro->semana4==1){
                  $ba4='background: #70ad47;';
              }else if($cro->semana4==2){
                  $ba4='background: #e31a1a;';
              }else{
                  $ba4='background: #a7a7a4;';
              }
              if($cro->semana5==1){
                  $ba5='background: #70ad47;';
              }else if($cro->semana5==2){
                  $ba5='background: #e31a1a;';
              }else{
                  $ba5='background: #a7a7a4;';
              }
              $html.='<tr>
                <td>'.utf8_decode($prioridad_txt).'</td>
                <td>'.utf8_decode(date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5))).'</td>
                <td>'.utf8_decode($cro->preg2).'</td>
                <td style="'.$ba1.'">'; if($cro->semana1==1){$html.='Si';} if($cro->semana1==2){$html.='No';} $html.='</td>
                <td style="'.$ba2.'">'; if($cro->semana2==1){$html.='Si';} if($cro->semana2==2){$html.='No';} $html.='</td>
                <td style="'.$ba3.'">'; if($cro->semana3==1){$html.='Si';} if($cro->semana3==2){$html.='No';} $html.='</td>
                <td style="'.$ba4.'">'; if($cro->semana4==1){$html.='Si';} if($cro->semana4==2){$html.='No';} $html.='</td>
                <td style="'.$ba5.'">'; if($cro->semana5==1){$html.='Si';} if($cro->semana5==2){$html.='No';} $html.='</td>
              </tr>';
          }

        echo $html;
        ?>
    </tbody>
</table>
       
