<style type="text/css">
  textarea{
        overflow: hidden;
        min-height: 30px;
        text-align: center;
    }
  select{
    text-align-last: center;
  }
  .btn_f{
    color: white;
    border-color: #363039;
    background: #363039; 
    border: 3px solid #363039;
    border-radius: 66px;
    border-bottom-right-radius: 0px;
    font-size: 90px;
    padding: 10px;
    padding-top: 0px;
    padding-left: 36px;
    padding-right: 24px;
  }  

  .btn_f:focus {
    color: white;
    border-color: #363039 !important;
    background: #363039 !important;
  }

  .btn_f:hover {
    color: white;
    border-color: #363039 !important;
    background: #363039 !important;
  }


  .btn_o{
    color: #363039;
    border-color: white;
    background: white; 
    border: 3px solid white;
    border-radius: 66px;
    border-bottom-left-radius: 0px;
    font-size: 90px;
    padding: 10px;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 0px;
  }  

  .btn_o:focus {
    color: #363039;
    border-color: white !important;
    background: white !important;
  }

  .btn_o:hover {
    color: #363039;
    border-color: white !important;
    background: white !important;
  }

  .btn_d{
    color: #363039;
    border-color: white;
    background: white; 
    border: 3px solid white;
    border-radius: 66px;
    border-top-right-radius: 0px;
    font-size: 90px;
    padding: 10px;
    padding-left: 36px;
    padding-right: 24px;
    padding-top: 0px;
  }  

  .btn_d:focus {
    color: #363039;
    border-color: white !important;
    background: white !important;
  }

  .btn_d:hover {
    color: #363039;
    border-color: white !important;
    background: white !important;
  }

  .btn_a{
    color: white;
    border-color: #363039;
    background: #363039; 
    border: 3px solid #363039;
    border-radius: 66px;
    border-top-left-radius: 0px;
    font-size: 90px;
    padding: 10px;
    padding-left: 24px;
    padding-right: 24px;
    padding-top: 0px;
  }  

  .btn_a:focus {
    color: white;
    border-color: #363039 !important;
    background: #363039 !important;
  }

  .btn_a:hover {
    color: white;
    border-color: #363039 !important;
    background: #363039 !important;
  }

  .img_li{ width: 70px;  }
  .contenedor{ min-width: 50px;  text-align: center;}

  .img_color{
    background: #bd9250; border-radius: 50px;
  }
  .link_color{
     color: #bd9250 !important; 
  }
  .th_c{
    background: #bd9250 !important; color: white !important;border-radius: 25px; font-size: 22px; max-width: fit-content; padding-left: 12px; padding-right: 12px;
  }

  .th_g{
    border-radius: 25px; font-size: 18px; max-width: fit-content; padding-left: 12px; padding-right: 12px; text-align: -webkit-center; color: #000000; background-color: #a7a7a4;
  }

  .th_v{
    background: #70ad47 !important; color: white !important;border-radius: 25px; font-size: 20px; max-width: fit-content; padding-left: 12px; padding-right: 12px;
  }

  textarea {
    border-radius: 25px !important; border: 1px solid #d8d8dd00 !important;
  }

  .form-control {
    display: block;
    width: 100%;
    padding: 0.5rem 0.875rem;
    font-size: 0.9375rem;
    font-weight: 400;
    line-height: 1.47;
    color: #636578;
    appearance: none;
    background-color: #a7a7a4;
    background-clip: padding-box;
    border: 1px solid #d8d8dd;
    border-radius: var(--bs-border-radius);
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  }
  
  select {
    border: 1px solid #d8d8dd00 !important;
  }

  .resum_c1 {
    background: #70ad47 !important;
    color: white !important;
    border-radius: 25px;
    font-size: 15px;
    max-width: fit-content;
    padding: 8px;
  }

  .resum_c2 {
    background: #f5bd00 !important;
    color: black !important;
    border-radius: 25px;
    font-size: 15px;
    max-width: fit-content;
    padding: 8px;
  }

  .resum_c3 {
    background: #e31b1b !important;
    color: black !important;
    border-radius: 25px;
    font-size: 15px;
    max-width: fit-content;
    padding: 8px;
  }
  .padding1{
    padding-left: 12px; padding-right: 12px;
  }
  .padding2{
    padding-left: 12px; padding-right: 12px; font-size: 17px; color:black; text-align: -webkit-center !important;
  }
  .bac_red{
    background: #e31a1a !important;
  }
  .bac_amarillo{
    background: #f5bd00 !important;
  }
  .bac_verde{
    background: #70ad47 !important;
  }
  input{
    border-radius: 29px !important;
    border: 1px solid #d8d8dd00 !important;
  }

  .text_act{
    border-radius: 25px; font-size: 26px; width:100%; padding-left: 12px; padding-right: 12px; text-align: -webkit-center; color: white; background-color: #bd9250;
  }

  .div_g{
    background: #91928f;
    border-radius: 35px;
    padding-left: 16px;
    padding-right: 16px;
    text-transform: capitalize;
    color: #342E37;
    font-size: 16px;
    padding: 0.5rem 0.875rem;
  }

</style>
<?php 
  $con = $_GET['con'];
?>
<input type="hidden" id="tipo_m" value="<?php echo $con ?>">
<input type="hidden" id="idfoda" value="<?php echo $get_cli->estatusfoda; ?>">
<div class="container-xxl flex-grow-1 container-p-y" style="padding-top: 0px !important;">
  <!--  -->
  <div class="vista_x vista_1" style="display:none" ?>>
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Planeación Empresarial: Análisis Foda</h1>
                  <h4>Analiza tu entorno empresarial</h4>
                </div> 
              </div> 
            </div>
          </div>
          <!--  -->
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Fortalezas, Oportunidades, Debelidades y Amenazas</h2>
                </div> 
                <div class="col-md-7">
                  <p>El análisis FODA te ayuda a identificar y entender los factores internos y externos<br> que impactan tu negocio. Aprovecha esta herramienta para tomar decisiones<br> estratégicas y mejorar tu posición en el mercado.</p>
                  <p>¿Cómo funciona el FODA?</p>
                  <ul>
                    <li>Fortalezas:<br>Reconoce los elementos que te dan una ventaja competitiva.</li>
                    <li>Oportunidades:<br>Identifica las oportunidades externas que puedes aprovechar.</li>
                    <li>Debilidades:<br>Detecta áreas internas que necesitan mejoras.</li>
                    <li>Amenazas:<br>Evalúa los factores externos que podrían desafiar tu éxito.</li>
                  </ul>
                  <p>Comienza tu análisis<br>Explora cada sección del FODA para obtener una visión completa de tu negocio y<br> planificar estratégicamente. Comienza definiendo tus fortalezas</p>   
                </div> 
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>  
                <div class="col-md-12" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(2)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div> 
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <div class="vista_x vista_2" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Fortalezas</h1>
                  <h4>Descubre tu poder interno</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Descubre lo que te hace único desde adentro</h2>
                </div> 
                <div class="col-md-7">
                  <p>¿Qué te hace destacar?<br>Identifica las características y recursos internos de tu empresa que te dan una ventaja<br> competitiva. Aquí es donde encuentras tus superpoderes empresariales.</p>
                  <ul>
                    <li><b>Recursos clave</b><br>¿Qué activos únicos posee tu empresa que no tienen tus competidores?</li>
                    <li><b>Habilidades y talentos</b><br>¿Cuáles son las habilidades y competencias distintivas de tu equipo?</li>
                    <li><b>Reconocimientos y logros</b><br>¿Qué éxitos y premios han demostrado tu excelencia en el mercado?</li>
                  </ul>
                  <p>Ejemplo:<br>"Nuestro equipo de ventas cuenta con la única capacitación en neuroventas<br> desarrollada por nuestra propia empresa."</p>   
                </div> 
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>Tips para identificar tus fortalezas</h2>
                  <p>1.Reflexiona sobre tus éxitos recientes.<br>2.Consulta con tu equipo para obtener diversas perspectivas.<br>3.Revisa tus recursos y capacidades clave.</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>¿Manos a la obra!</h2>
                  <p>Completa las siguientes preguntas para detallar tus fortalezas:</p>
                  <p>¿Cuáles son tus principales ventajas competitivas?<br>2. ¿Qué recursos únicos tiene tu empresa?<br>¿Qué habilidades y conocimientos destacan en tu equipo?</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>  
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(1)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(3)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
  </div>
  <!--  -->
  <?php /*
  <div class="vista_x vista_3" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Fortalezas</h1>
                  <h4>Descubre tu poder interno</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Describe cada una de tus fortalezas</h2>
                </div> 
                <div class="col-md-12">
                  <!--  -->
                  <form class="form" method="post" role="form" id="form_registro" novalidate>
                    <div class="row g-3">
                      <div class="col-md-10">
                        <div class="form-floating form-floating-outline mb-4">
                          <textarea type="text" name="concepto" id="concepto" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                          <label for="nombre">Concepto</label>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                          <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                        </button>
                      </div>  
                    </div> 
                  </form>
                  <div class="txt_fortaleza">
                  </div>
                  <!--  -->
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(3)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(4)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
  </div>
  */ ?>
  <!--  -->
  <div class="vista_x vista_4" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Oportunidades</h1>
                  <h4>Aprovecha cada ventaja externa</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;"> 
                <div class="col-md-12">
                  <h2>Descubre las ventajas externas</h2>
                </div> 
                <div class="col-md-7">
                  <p>Identifica las oportunidades externas que pueden impulsar tu crecimiento.<br>Descubre cómo puedes innovar y expandirte en el mercado.</p>
                  <ul>
                    <li><b>Tendencias del mercado</b><br>¿Qué tendencias emergentes puedes aprovechar?</li>
                    <li><b>Nuevos segmentos de clientes</b><br>¿Hay nuevos públicos a los que podrías llegar con tus productos o servicios?</li>
                    <li><b>Avances tecnológicos</b><br>¿Qué tecnologias recientes podrias integrar para mejorar tu oferta?</li>
                  </ul>
                  <p>Ejemplo:<br>"El auge del comercio electrónico nos brinda la oportunidad de expandir nuestras<br> ventas a nivel internacional.</p>   
                </div> 
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>Tips para identificar tus oportunidades</h2>
                  <p>1.Investiga las tendencias del sector.<br>2.Analiza los cambios en las necesidades del cliente.<br>3.Observa a tus competidores y el entorno tecnológico.</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>¡ldentifica tus oportunidades!</h2>
                  <p>Completa las siguientes preguntas para detallar tus fortalezas:</p>
                  <p>1.¿Qué tendencias del mercado puedes aprovechar?<br>2.¿Qué nuevos segmentos de clientes puedes atender?<br>3.¿Qué tecnologías recientes puedes implementar?</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>  
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(3)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(5)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
  <!--  -->
  </div>
  <!--  -->
  <?php /*
  <div class="vista_x vista_5" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Oportunidades</h1>
                  <h4>Aprovecha cada ventaja externa</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Describe cada una de tus oportunidades</h2>
                </div> 
                <div class="col-md-12">
                  <!--  -->
                  <form class="form" method="post" role="form" id="form_registro_o" novalidate>
                    <div class="row g-3">
                      <div class="col-md-10">
                        <div class="form-floating form-floating-outline mb-4">
                          <textarea type="text" name="concepto" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                          <label for="nombre">Concepto</label>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_o">
                          <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                        </button>
                      </div>  
                    </div> 
                  </form>
                  <div class="txt_oportunidades">

                    

                  </div>
                  <!--  -->
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(4)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(6)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>

    
    <!--  -->
  </div>  
  */ ?>
  <!-- debilidades -->
  <div class="vista_x vista_6" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Debilidades</h1>
                  <h4>Identifica tus áreas internas frágiles</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;"> 
                <div class="col-md-12">
                  <h2>Identifica tus áreas internas de mejora</h2>
                </div> 
                <div class="col-md-7">
                  <p>¿Qué necesita mejorar?<br>Detecta las debilidades internas que podrían estar frenando tu crecimiento. Reconocerlas es el primer paso para superarlas y fortalecer tu negocio.</p>
                  <ul>
                    <li>Recursos limitados<br>¿Qué recursos te faltan para alcanzar tus objetivos?</li>
                    <li>Falta de habilidades específicas<br>¿En qué áreas tu equipo necesita capacitación o refuerzo?</li>
                    <li>Procesos ineficientes<br>¿Qué procesos podrían optimizarse para aumentar la eficiencia?</li>
                  </ul>
                  <p>Ejemplo:<br>"Nuestra dependencia de un solo proveedor limita nuestra capacidad de producción."</p>   
                </div> 
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>Tips para identificar tus debilidades</h2>
                  <p>1.Haz una autoevaluación honesta.<br>2.Pide feedback a tu equipo y clientes.<br>3.Compara tu desempeño con el de tus competidores.</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>¡Mejora tu negocio!</h2>
                  <p>Completa las siguientes preguntas para detallar tus debilidades:</p>
                  <p>1.¿Qué recursos limitan tu crecimiento?<br>2.¿En qué áreas necesitas mejorar?<br>3.¿Qué procesos podrían ser más eficientes?</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>  
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(5)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(7)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
    <!--  -->
  </div>
  <!--  -->
  <?php /*
  <div class="vista_x vista_7" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Debilidades</h1>
                  <h4>Identifica tus áreas internas frágiles</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Describe cada una de tus debilidades</h2>
                </div> 
                <div class="col-md-12">
                  <!--  -->
                  <form class="form" method="post" role="form" id="form_registro_d" novalidate>
                    <div class="row g-3">
                      <div class="col-md-10">
                        <div class="form-floating form-floating-outline mb-4">
                          <textarea type="text" name="concepto" id="concepto_d" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                          <label for="nombre">Concepto</label>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_d">
                          <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                        </button>
                      </div>  
                    </div> 
                  </form>
                  <div class="txt_debilidades">

                    

                  </div>
                  <!--  -->
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(6)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(8)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
    <!--  -->
  </div>  
  */ ?> 
   <!-- Amenazas -->
  <div class="vista_x vista_8" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Amenazas</h1>
                  <h4>Anticipa y supera los desafíos externos</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;"> 
                <div class="col-md-12">
                  <h2>Anticipate a los riesgos externos</h2>
                </div> 
                <div class="col-md-7">
                  <p>¿Qué riesgos enfrentas?<br>Identifica los factores externos que podrían poner en peligro tu negocio. Conocer estos riesgos te permite planificar y mitigarlos de manera efectiva.</p>
                  <ul>
                    <li>Competencia creciente<br>¿Qué nuevos competidores podrían afectar tu participación en el mercado?</li>
                    <li>Cambios en la regulación<br>¿Hay nuevas leyes o normativas que podrían impactar tus operaciones?</li>
                    <li>Fluctuaciones económicas<br>¿Cómo podrían las condiciones económicas afectar tu negocio?</li>
                  </ul>
                  <p>Ejemplo:<br>"Una nueva regulación ambiental podría aumentar nuestros costos de producción."</p>   
                </div> 
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>Tips para anricipar tus amenazas</h2>
                  <p>1.Mantente informado sobre las tendencias del sector.<br>2.Realiza análisis periódicos del entorno económico y regulatorio.<br>3.Escucha a tus clientes y proveedores para detectar posibles cambios.</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>   
              </div> 
              <br><br><br><br>
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-7">
                  <h2>¡Mejora tu negocio!</h2>
                  <p>Completa las siguientes preguntas para detallar tus debilidades:</p>
                  <p>1.¿Qué recursos limitan tu crecimiento?<br>2.¿En qué áreas necesitas mejorar?<br>3.¿Qué procesos podrían ser más eficientes?</p>
                </div>  
                <div class="col-md-5" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/Girl and charts.png" style="width: 326px;">
                </div>  
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(7)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(9)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
    <!--  -->
  </div>
  <!--  -->
  <?php /*
  <div class="vista_x vista_9" style="display: none">
    <div class="row g-6 mb-4">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="card academy-content shadow-none border">
              <div class="row g-3">
                <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                  <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                </div>  
                <div class="col-md-10" style="align-content: center;">
                  <h1>Amenazas</h1>
                  <h4>Anticipa y supera los desafíos externos</h4>
                </div> 
              </div> 
            </div>
          </div>
          <div class="card-body" >
            <div class="card academy-content shadow-none border">
              <div class="row g-3" style="margin: 12px;">
                <div class="col-md-12">
                  <h2>Describe cada una de tus amenazas</h2>
                </div> 
                <div class="col-md-12">
                  <!--  -->
                  <form class="form" method="post" role="form" id="form_registro_a" novalidate>
                    <div class="row g-3">
                      <div class="col-md-10">
                        <div class="form-floating form-floating-outline mb-4">
                          <textarea type="text" name="concepto" id="concepto_a" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                          <label for="nombre">Concepto</label>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_a">
                          <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                        </button>
                      </div>  
                    </div> 
                  </form>
                  <div class="txt_amenazas">

                    

                  </div>
                  <!--  -->
                </div>
                <div class="col-md-6">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(8)"><span class="tf-icons mdi mdi-chevron-left"></span> Anterior</button>
                </div>  
                <div class="col-md-6" style="text-align: right;">
                  <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" style="text-transform: math-auto;" onclick="siguiente_ocultar(10)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                </div>  
              </div> 
            </div>  
          </div>    
        </div>
      </div>
    </div>
    <!--  -->
  </div>  
  */ ?>
  <!-- <?php //if($get_cli->estatusfoda==0) echo 'style="display:none"' ?> -->
  <div class="vista_x vista_10" >
    <div class="row g-6">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <ul class="nav nav-pills flex-column flex-sm-row">
                    <li class="nav-item">
                      <a class="nav-link link_data" href="<?php echo base_url() ?>Analisisfoda?con=1">
                        <div class="contenedor" style="text-align: -webkit-center;">
                          <div class="img_li linkimg1">
                            <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 100%;">
                          </div>  
                    
                          <div class="link_c link1_c">Resumen FODA</div>
                        </div>
                      </a>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link link_data" href="<?php echo base_url() ?>Analisisfoda?con=2">
                        <div class="contenedor">
                        <img src="<?php echo base_url().'images/graficos/chess 3.png';?>" class="img_li linkimg2"><br>
                          <div class="link_c link2_c">Resumen De Estrategias</div>
                        </div>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link link_data" href="<?php echo base_url() ?>Analisisfoda?con=3">
                        <div class="contenedor">
                        <img src="<?php echo base_url().'images/graficos/Date_range_duotone.png';?>" class="img_li linkimg3"><br>
                          <div class="link_c link3_c">Cronograma</div>
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div> 
          </div>
        </div>
      </div>    
      <div class="col-lg-12">
        <br>
        <div class="text_foda text_foda1" style="display: none;">
          <div class="card">
            <div class="card-body">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Resumen FODA (Fortalezas – Oportunidades – Debilidades – Amenazas)</h1>
                  </div>
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div> 
                </div> 
              </div>
            </div>
            <!--  -->
            <div class="card-body" style="padding-top: 0px !important;">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-9">
                    <p>Estas conclusiones te ayudarán a mantener un enfoque claro en tus fortalezas, oportunidades, debilidades y 
                    amenazas.<br>Utiliza este resumen para guiar tus decisiones y asegurar el éxito de tu negocio.</p>
                  </div> 
                  <div class="col-md-3" style="align-content: center;">
                    <div>
                      <!-- <div style="border-radius: 150px; position: absolute; top: 214px;">
                        <span style=" background: #bd9250; padding-block-end: 114px; padding-inline-end: 137px; border-radius: 72px;">
                          
                        </span>
                      </div> -->
                      <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 15.png">
                    </div>
                  </div>  
                </div> 
                <!--  -->
                <div class="row my-4">  
                    <div class="col-md-12">
                      <div class="tabla_guia"></div>
                      
                    </div>
                  </div>
                <!--  -->
                <div class="g-3" style="margin: 18px;">
                  <div class="row gy-4">
                    <!-- Gamification Card -->
                    <div class="col-md-12 col-lg-6" style="padding-right: 0px;">
                      <div class="card h-100" style="border: 2px solid white; background: white;">
                        <div class="d-flex align-items-end row">
                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <h1 style="color: #363039; font-family: 'Roboto', sans-serif;">Fortalezas</h1>
                              <form class="form" method="post" role="form" id="form_registro" novalidate>
                                <div class="row g-3">
                                  <div class="col-md-10">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <textarea type="text" name="concepto" id="concepto" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                                    </div>
                                  </div>
                                  <div class="col-md-2">
                                    <div style="display: flex;">
                                      <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                      </button>
                                    </div>
                                  </div>
                                </div> 
                              </form>
                              <div class="txt_fortaleza">

                                

                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                      <div style="text-align: right; top: -124px !important;  position: relative;  left: 16px;">
                        <button type="button" class="btn btn-sm me-3 waves-effect btn_f" data-bs-toggle="modal"
                                    data-bs-target="#modal_fortaleza" style="font-family: 'Roboto', sans-serif;">F</button>
                      </div>
                    </div>
                    <!--/ Gamification Card -->

                    <!-- Statistics Total Order -->
                    <div class="col-md-12 col-lg-6" style="padding-left: 0px;">
                      <div class="card h-100" style="background: #363039;">
                        <div class="d-flex align-items-end row">

                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <h1 style="color: white; text-align: right; font-family: 'Roboto', sans-serif;">Oportunidades</h1>
                              <form class="form" method="post" role="form" id="form_registro_o" novalidate>
                                <div class="row g-3">
                                  <div class="col-md-10">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <textarea style="width: 697px" type="text" name="concepto" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                                    </div>
                                  </div> 
                                  <div class="col-md-2">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_o">
                                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                      </button>
                                    </div>
                                  </div> 
                                </div> 
                              </form>
                              <div class="txt_oportunidades">

                                

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style="top: -124px !important; position: relative; left: 0px;">
                        <button type="button" class="btn btn-sm me-3 waves-effect btn_o" data-bs-toggle="modal" 
                                    data-bs-target="#modal_oportunidades" style="font-family: 'Roboto', sans-serif;">O</button>
                      </div>
                    </div>

                  </div>
                  <div class="row gy-4">
                    <!-- Gamification Card -->
                    <div class="col-md-12 col-lg-6" style="padding-right: 0px;">
                      
                      <div class="card h-100" style="background: #363039;">
                        
                        <div class="row">
                          <div class="col-md-9">
                            <br>
                            <h1 style="color: white; padding-left: 24px; font-family: 'Roboto', sans-serif;">Debilidades</h1>
                          </div>
                          <div class="col-md-3">  
                            <div style="top: 0px !important; position: absolute; text-align: right; right: -16px;">
                              <button type="button" class="btn btn-sm me-3 waves-effect btn_d" data-bs-toggle="modal"
                                          data-bs-target="#modal_debilidades" style="font-family: 'Roboto', sans-serif;" >D</button>
                            </div>
                          </div>  
                        </div>
                        <br><br>
                        <div class="d-flex align-items-end row">
                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <form class="form" method="post" role="form" id="form_registro_d" novalidate>
                                <div class="row g-3">
                                  <div class="col-md-10">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <textarea style="width: 697px" type="text" name="concepto" id="concepto_d" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                                    </div>
                                    
                                  </div>  
                                  <div class="col-md-2">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_d">
                                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                      </button>
                                    </div>
                                  </div>  
                                </div> 
                              </form>
                              <div class="txt_debilidades">

                                

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/ Gamification Card -->

                    <!-- Statistics Total Order -->
                    <div class="col-md-12 col-lg-6" style="padding-left: 0px;">
                      <div class="card h-100" style="border: 2px solid white; background: white;">
                        <div class="row">
                          <div class="col-md-3">  
                            <div style="top: -2px !important; position: absolute; left: -2px;">
                              <button type="button" class="btn btn-sm me-3 waves-effect btn_a" data-bs-toggle="modal"
                                          data-bs-target="#modal_amenazas" style="font-family: 'Roboto', sans-serif;">A</button>
                            </div>
                          </div>  
                          <div class="col-md-9" style="text-align: right;">
                            <br>
                            <h1 style="color: #363039; padding-right: 24px; font-family: 'Roboto', sans-serif;">Amenazas</h1>
                          </div>
                        </div>
                        <br><br>
                        <div class="d-flex align-items-end row">
                          <div class="col-md-12 order-1 order-md-2">
                            <div class="card-body p-4">
                              <form class="form" method="post" role="form" id="form_registro_a" novalidate>
                                <div class="row g-3">
                                  <div class="col-md-10">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <textarea style="width: 697px" type="text" name="concepto" id="concepto_a" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                                    </div>
                                  </div> 
                                  <div class="col-md-2">
                                    <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                                      <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_a">
                                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                      </button>
                                    </div>
                                  </div> 
                                </div> 
                              </form>
                              <div class="txt_amenazas">

                                

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>  
                <!--  -->
                <!-- <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-12">
                    <h2>¿Quieres volver a realizar tu test FODA?</h2>
                  </div> 
                  <div class="col-md-12">
                    <p>Tranquilo, si algo no te ha quedado claro, puedes reiniciar tu test y comenzar<br> de nuevo. Toma en cuenta que los datos que integraste en tu análisis FODA actual<br> se perderá ¿Estás seguro que quieres comenzar de nuevo?</p>   
                  </div> 
                  <div class="col-md-12" style="text-align: right;">
                    <button type="button" class="btn me-sm-3 me-1 btn_registro"  style="color: white; text-transform: math-auto;" onclick="siguiente_ocultar(1)">Tomar test FODA de nuevo <span class="tf-icons mdi mdi-chevron-right"></span></button>
                  </div> 
                </div>  -->
                <br>
                <div style="padding-left: 20px; padding-right: 20px;">
                  <div class="row">
                      <div class="col-md-1"></div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php echo base_url() ?>Analisisfoda?con=1" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Estrategias FODA</span></a>
                      </div>
                      <!-- <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php // echo base_url() ?>Analisisfoda?con=2" ><span style="font-size: 20px;">Resumen de estrategias</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php // echo base_url() ?>Analisisfoda?con=3" ><span style=" font-size: 20px;">Cronograma</span></a>
                      </div> -->
                      <div class="col-md-2" style="text-align: right;">
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Analisisfoda?con=2" >Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                      </div>
                  </div>    
                </div>  
                <br>
                <!--  -->
              </div>
            </div>  
            <!--  -->
          </div>
        </div> 
        <!--  -->
        <div class="text_foda text_foda2_1 text_foda2" style="display: none;">
          <div class="card">
            <div class="card-body" style="padding-bottom: 0px;">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Resumen de estrategias y priorización de debilidades</h1>
                    <h4>Esta sección te presenta un resumen de las fortalezas y debilidades que elegiste</h4>
                  </div> 
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div> 
                </div> 
              </div>
            </div>
            <!--  -->
            <div class="card-body" >
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-8">
                    <!-- <h2>Trabaja tus debilidades</h2>
                    <h5>Consolida tus próximos pasos estratégicos. Revisa las estrategias que has<br> desarrollado a partir de tu análisis FODA. Estas conclusiones te ayudarán a<br> mantener un enfoque claro en tus fortalezas, oportunidades, debilidades y<br> amenazas. Utiliza este resumen para guiar tus decisiones y asegurar el éxito de tu<br> negocio.</h5> -->
                  </div> 
                  <div class="col-md-4">
                    <div>
                      <!-- <div style="border-radius: 150px; position: absolute; top: 214px;">
                        <span style=" background: #bd9250; padding-block-end: 114px; padding-inline-end: 137px; border-radius: 72px;">
                          
                        </span>
                      </div> -->
                      <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 16.png">
                    </div>
                  </div>  
                </div> 
                <!--  -->
                <div class="row" style="margin: 12px;">  
                    <div class="col-md-12">
                      <h2>Resumen de fortalezas</h2>
                    </div>
                    <div class="col-md-12">
                      <div class="tabla_guia_resumen_estrategias">
                        
                      </div>
                      <br>
                    </div>
                    <div class="col-md-12">
                      <h2>Priorización y calibración de debilidades</h2>
                      <h5>Esta sección te presenta un resumen de las debilidades que seleccionaste. A continuación evalúa las debilidades y con base en un algoritmo, la herramienta te proporcionará una guía para priorizarlas, de conformidad con su costo de implementación, su probabilidad de fracaso, la complejidad para su implementación y su beneficio.</h5>
                    </div>
                    <div class="col-md-12">
                      <div class="tabla_guia_resumen_prioriza">
                        
                      </div>
                      
                      <br>
                    </div>

                    <div class="col-md-12">
                      <div style="padding-left: 20px; padding-right: 20px;">
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-4">
                              <a style="cursor:pointer" onclick="siguiente_resumen_ocultar(1)" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Resumen de estrategias</span></a>
                          </div>
                          <div class="col-md-4">
                              <a style="cursor:pointer" onclick="siguiente_resumen_ocultar(2)" ><span style="font-size: 20px;">Priorización de estrategias</span></a>
                          </div>
                          <div class="col-md-2" style="text-align: right;">
                            <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_resumen_ocultar(2)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                          </div>
                        </div>  
                      </div>    
                    </div>  
                </div>  
                <!--  -->
              </div>  
            </div> 
            <!--  -->
          </div>  
        </div>  
        
        <div class="text_foda text_foda2_2" style="display: none;">
          <div class="card">
            <div class="card-body" style="padding-bottom: 0px;">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/pen-tool.svg" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Define fechas clave para gestionar tus debilidades</h1>
                  </div>
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div>  
                </div> 
              </div>
            </div>
            <!--  -->
            <div class="card-body" >
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-6">
                    <h5>Asigna una fecha para trabajar en cada debilidad según su prioridad: alta (primer año), media (segundo año) o baja (tercer año). Dentro de cada año, elige el trimestre (primer, segundo, tercero o cuarto) en el que te enfocarás en cada aspecto. Recuerda que algunas debilidades de baja prioridad pueden ser abordadas antes si requieren menos recursos o son fáciles de implementar.</h5>
                    <h5>Enfócate en mejorar estas áreas críticas para fortalecer tu posición en el mercado y aprovechar mejor tus oportunidades.</h5>
                  </div> 
                  <div class="col-md-2">
                  </div>
                  <div class="col-md-4">
                    <div>
                      <!-- <div style="border-radius: 150px; position: absolute; top: 214px;">
                        <span style=" background: #bd9250; padding-block-end: 114px; padding-inline-end: 137px; border-radius: 72px;">
                          
                        </span>
                      </div> -->
                      <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 16.png">
                    </div>
                  </div>  
                </div> 
                <!--  -->
                <div class="row" style="margin: 12px;">  
                    <div class="col-md-12">
                      <h2>Prioriza y programa tu plan de acción</h2>
                      <h5>Establece fechas y prioridades para abordar cada debilidad según su impacto en tu negocio. Elige en qué año y trimestre trabajarás en cada una, considerando tanto la prioridad como la facilidad de implementación. Recuerda que incluso las debilidades de baja prioridad pueden ser abordadas antes si son rápidas de solucionar.</h5>
                    </div>
                    <div class="col-md-12">
                      <div class="tabla_guia_resumen_prioriza_2_m">
                        
                      </div>
                      <div class="tabla_guia_resumen_prioriza_2_i">
                        
                      </div>
                      <div class="tabla_guia_resumen_prioriza_2_e">
                        
                      </div>
                      <div class="div-linea"></div>
                      <br>
                    </div>
                    <div class="col-md-12">
                      <div style="padding-left: 20px; padding-right: 20px;">
                        <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-4">
                              <a style="cursor:pointer" onclick="siguiente_resumen_ocultar(1)" ><span style="font-size: 20px;">Resumen de estrategias</span></a>
                          </div>
                          <div class="col-md-4">
                              <a style="cursor:pointer" onclick="siguiente_resumen_ocultar(2)" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Priorización de estrategias</span></a>
                          </div>
                          <div class="col-md-2" style="text-align: right;">
                            <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="<?php echo base_url() ?>Analisisfoda?con=3">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                          </div>
                        </div>  
                      </div>    
                    </div>  
                </div>  
                <!--  -->
              </div>  
            </div> 
            <!--  -->
          </div>  
        </div>  


        <!--  -->
        <div class="text_foda text_foda3" style="display: none;">
          <div class="card">
            <div class="card-body">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/graficos/Date_range_duotone.png" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Cronograma</h1>
                    <h4>Visualiza y gestiona el tiempo de tus proyectos para alcanzar tus objetivos a tiempo.</h4>
                  </div> 
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div> 
                </div> 
              </div>
            </div>
            <!--  -->
            <div class="card-body" style="padding-top: 0px !important;">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-8">
                    <h2>Organiza tus tareas y proyectos</h2>
                    <p style="text-align: justify; font-size: 18px;">Crea un cronograma para planificar y organizar todas las tareas y estrategias de tu empresa. Asigna fechas de inicio y fin, marca plazos y establece responsables para cada actividad. Utiliza el cronograma para visualizar el progreso, ajustar tiempos y asegurarte de que todos los objetivos se cumplan de manera oportuna.</p>
                  </div> 
                  <div class="col-md-1"></div>
                  <div class="col-md-3" style="align-content: center;">
                    <div>
                      <!-- <div style="border-radius: 150px; position: absolute; top: 214px;">
                        <span style=" background: #bd9250; padding-block-end: 114px; padding-inline-end: 137px; border-radius: 72px;">
                          
                        </span>
                      </div> -->
                      <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 16.png">
                    </div>
                  </div>  
                </div> 
                <div class="table-responsive text-nowrap">
                  <div class="row" style="margin: 0px;">  
                    <div class="col-md-12">
                      <div class="btn-group">
                        <div class="tabla_cronograma"></div>
                        <!-- <div class="">

                          <div class="row">  
                            <div class="col-md-4" align="center"></div>
                            <div class="col-md-4" align="center">
                              <select class="form-select" id="idactividad" onchange="btn_tabla_cronograma_actividad()" style="border-radius: 30px; background: #bd9250; font-size: 20px; text-align: center; width: auto;">
                                <option selected>Selecciona una opción</option>
                                <option value="1">Actividades a realizarse durante enero</option>
                                <option value="2">Actividades a realizarse durante febrero</option>
                                <option value="3">Actividades a realizarse durante marzo</option>
                                <option value="4">Actividades a realizarse durante abril</option>
                                <option value="5">Actividades a realizarse durante mayo</option>
                                <option value="6">Actividades a realizarse durante junio</option>
                                <option value="7">Actividades a realizarse durante julio</option>
                                <option value="8">Actividades a realizarse durante agosto</option>
                                <option value="9">Actividades a realizarse durante septiembre</option>
                                <option value="10">Actividades a realizarse durante octubre</option>
                                <option value="11">Actividades a realizarse durante noviembre</option>
                                <option value="12">Actividades a realizarse durante diciembre</option>
                              </select>  
                              <br>
                            </div>
                          </div> 
                          <div class="tabla_cronograma_actividad">
                            
                          </div>

                        </div> -->

                      </div>
                    </div>  
                  </div>
                 
                  <br><br>
                  <div class="row" style="margin: 0px;">  
                    <div class="col-md-12">
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="btn_add_debilidad_fortaleza()">Agregar una nueva debilidad o fortaleza a trabajar</button>
                    </div>
                  </div>  
                  <br>
                  <div class="row" style="margin: 0px;">  
                    <div class="col-md-12">
                      <div style="width: 2240px;" class="tabla_cronograma_d_f"></div>
                    </div>
                  </div>  
                </div> 
                <br>  
                <!--  -->
                <div style="padding-left: 20px; padding-right: 20px;">
                  <div class="row">
                      <!--  <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php //echo base_url() ?>Analisisfoda?con=1" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Estrategias FODA</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php // echo base_url() ?>Analisisfoda?con=2" ><span style="font-size: 20px;">Resumen de estrategias</span></a>
                      </div> -->
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(1)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Cronograma</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(2)"><span style="font-size: 20px;">Cumplimiento de actividades</span></a>
                      </div>
                      <div class="col-md-2" style="text-align:center"> 
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(3)"><span style="font-size: 20px;">Resumen</span></a>
                      </div>
                      <div class="col-md-2" style="text-align:center"> 
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="btn_exporta_cronograma()" >Exporta tu cronograma <span class="tf-icons mdi mdi-arrow-expand-up"></span></a>
                      </div>
                      <div class="col-md-2" style="text-align: right;">
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_cronograma_ocultar(3)" >Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                      </div>
                  </div> 
                  <br>   
                </div>  
                <!--  -->
              </div>  
            </div> 

          </div>  
        </div> 
        <div class="text_foda text_foda3_2" style="display: none;">
          <div class="card">
            <div class="card-body">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/graficos/Date_range_duotone.png" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Cumplimiento de actividades</h1>
                  </div> 
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div> 
                </div> 
              </div>
            </div>
            <div class="card-body" style="padding-top: 0px !important;">
              <div class="card academy-content shadow-none border">
                <!--  -->


                <div class="table-responsive text-nowrap">
                  <div class="row" style="margin: 0px;">  
                    <div class="col-md-12">
                      <div class="btn-group">
                        <div class="">
                          <br>
                          <?php /*
                            <div class="row">
                              <div class="col-sm-2">
                                <select  class="form-control" id="anio">
                                  <?php
                                    for($i=date('o'); $i>=2020; $i--){
                                        if ($i == date('o'))
                                            echo '<option value="'.$i.'" selected>'.$i.'</option>';
                                        else
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                    }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-4" >
                                <select class="form-select" id="idactividad" onchange="btn_tabla_cronograma_actividad()" style="border-radius: 30px; background: #bd9250; font-size: 20px; text-align: center; width: auto;">
                                  <option selected>Selecciona una opción</option>
                                  <option value="1">Actividades a realizarse durante enero</option>
                                  <option value="2">Actividades a realizarse durante febrero</option>
                                  <option value="3">Actividades a realizarse durante marzo</option>
                                  <option value="4">Actividades a realizarse durante abril</option>
                                  <option value="5">Actividades a realizarse durante mayo</option>
                                  <option value="6">Actividades a realizarse durante junio</option>
                                  <option value="7">Actividades a realizarse durante julio</option>
                                  <option value="8">Actividades a realizarse durante agosto</option>
                                  <option value="9">Actividades a realizarse durante septiembre</option>
                                  <option value="10">Actividades a realizarse durante octubre</option>
                                  <option value="11">Actividades a realizarse durante noviembre</option>
                                  <option value="12">Actividades a realizarse durante diciembre</option>
                                </select>  
                                <br>
                              </div>
                            </div>
                          */ ?> 
                          <div class="tabla_cronograma_actividad">
                            
                          </div>
                        </div>        
                      </div>
                    </div>  
                  </div>
                 
                </div> 
                <br>  

                <div style="padding-left: 20px; padding-right: 20px;">
                  <div class="row">
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(1)"><span style="font-size: 20px;">Cronograma</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(2)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Cumplimiento de actividades</span></a>
                      </div>
                      <div class="col-md-2" style="text-align:center"> 
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(3)"><span style="font-size: 20px;">Resumen</span></a>
                      </div>
                      <div class="col-md-2" style="text-align:center"> 
                          <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="btn_exporta_cumplimiento()" >Cumplimiento de actividades<span class="tf-icons mdi mdi-arrow-expand-up"></span></a>
                      </div>
                      <div class="col-md-2" style="text-align: right;">
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_cronograma_ocultar(3)" >Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                      </div>
                  </div> 
                  <br>   
                </div>  
                <!--  -->
              </div>  
            </div> 
            <!---->
          </div>  
        </div>  
        <!--  -->
        <div class="text_foda text_foda3_3" style="display: none;">
          <div class="card">
            <div class="card-body">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px; margin-top: 0px;">
                  <div class="col-md-2" align="center" style="text-align: -webkit-center;">
                    <img src="<?php echo base_url() ?>images/graficos/Date_range_duotone.png" style="width: 172px;">
                  </div>  
                  <div class="col-md-9" style="align-content: center;">
                    <h1>Resumen de tu plan</h1>
                    <h4>Revisa las fechas clave, tareas asignadas y responsables de cada proyecto para mantener todo en marcha.</h4>
                  </div> 
                  <div class="col-md-1" style="text-align: right;"> 
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="#footer_txt"><span class="tf-icons mdi mdi-arrow-down-thick"></span></a>
                  </div> 
                </div> 
              </div>
            </div>
            <!--  -->
            <div class="card-body" style="padding-top: 0px !important;">
              <div class="card academy-content shadow-none border">
                <div class="row g-3" style="margin: 12px;">
                  <div class="col-md-8">
                    <h2>Organiza y revisa tu plan de acción</h2>
                    <p style="text-align: justify; font-size: 18px;">Aquí podrás visualizar todas las actividades, sus fechas y los responsables asignados. Revisa que todo esté alineado con tus objetivos y realiza los ajustes necesarios para mantener tu plan en curso.</p>
                  </div> 
                  <div class="col-md-1"></div>
                  <div class="col-md-3" style="align-content: center;">
                    <div>
                      <!-- <div style="border-radius: 150px; position: absolute; top: 214px;">
                        <span style=" background: #bd9250; padding-block-end: 114px; padding-inline-end: 137px; border-radius: 72px;">
                          
                        </span>
                      </div> -->
                      <img style="width: 100%;" src="<?php echo base_url() ?>images/graficos/Illustration 16.png">
                    </div>
                  </div>  
                </div>
              </div>
             </div>    
            <!-- <div class="row" style="margin-left: 8px;">  
              <div class="col-md-12">
                <div class="btn_anios_resumen"></div>
                
              </div>
            </div>   -->    
            <div class="card-body" style="padding-top: 12px !important;">
              <div class="card academy-content shadow-none border">
                <!--  -->
                  <div class="row" style="margin: 0px;">  
                    <div class="col-md-12">
                      <div class="tabla_cronograma_resumen"></div>
                    </div>
                  </div>  
                  <br>
                  <div class="row">
                      <div class="col-md-1"></div>
                      <!--  <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php //echo base_url() ?>Analisisfoda?con=1" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Estrategias FODA</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" href="<?php // echo base_url() ?>Analisisfoda?con=2" ><span style="font-size: 20px;">Resumen de estrategias</span></a>
                      </div> -->
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(1)"><span style="font-size: 20px;">Cronograma</span></a>
                      </div>
                      <div class="col-md-3">
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(2)"><span style="font-size: 20px;">Cumplimiento de actividades</span></a>
                      </div>
                      <div class="col-md-3" style="text-align:center"> 
                          <a style="cursor:pointer" onclick="siguiente_cronograma_ocultar(3)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Resumen</span></a>
                      </div>
                      <!-- <div class="col-md-2" style="text-align: right;">
                        <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="btn_exporta_cronograma()" >Exporta resumen <span class="tf-icons mdi mdi-arrow-expand-up"></span></a>
                      </div> -->
                  </div> 
                  <br>   
                <!--  -->
              </div>
            </div>

          </div>    
        </div>  
      </div>

    </div>  
  </div>  


</div>   


<?php /* ?>
<div class="container-xxl flex-grow-1 container-p-y">
  <div class="row gy-4">
    <!-- Gamification Card -->
    <div class="col-md-12 col-lg-6">
      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(82 113 255) !important; border: 2px solid #5271ff;">
        <div style="top: -55px !important; position: absolute; left: -69px;">
            <img src="<?php echo base_url(); ?>public/img/op4/op.svg">
        </div> 
        <br><br><br>
        <div class="d-flex align-items-end row">
          <div class="col-md-12 order-1 order-md-2">
            <div class="card-body p-4">
              <form class="form" method="post" role="form" id="form_registro" novalidate>
                <div class="row g-3">
                  <div class="col-md-10">
                    <div class="form-floating form-floating-outline mb-4">
                      <textarea type="text" name="concepto" id="concepto" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                      <label for="nombre">Concepto</label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro">
                      <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                    </button>
                  </div>  
                </div> 
              </form>
              <div class="txt_fortaleza">

                

              </div>
            </div>
          </div>
        </div>

      </div>
      <div style="text-align: right; top: -56px !important; position: relative; left: 12px;">
        <button type="button" class="btn btn-sm me-3 waves-effect btn_f" data-bs-toggle="modal"
                    data-bs-target="#modal_fortaleza" style="font-size: 27px;">F</button>
      </div>
    </div>
    <!--/ Gamification Card -->

    <!-- Statistics Total Order -->
    <div class="col-md-12 col-lg-6">
      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(254 11 155) !important; border: 2px solid #fe0b9b;">
        <div style="top: -55px !important; position: absolute; text-align: right; right: -62px;">
            <img src="<?php echo base_url(); ?>public/img/op4/op2.svg">
        </div>
        <br><br><br>
        <div class="d-flex align-items-end row">

          <div class="col-md-12 order-1 order-md-2">
            <div class="card-body p-4">
              <form class="form" method="post" role="form" id="form_registro_o" novalidate>
                <div class="row g-3">
                  <div class="col-md-10">
                    <div class="form-floating form-floating-outline mb-4">
                      <textarea type="text" name="concepto" id="concepto_o" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                      <label for="nombre">Concepto</label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_o">
                      <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                    </button>
                  </div>  
                </div> 
              </form>
              <div class="txt_oportunidades">

                

              </div>
            </div>
          </div>
        </div>
      </div>
      <div style="top: -56px !important; position: relative; left: 4px;">
        <button type="button" class="btn btn-sm me-3 waves-effect btn_o" data-bs-toggle="modal"
                    data-bs-target="#modal_oportunidades" style="font-size: 27px;">O</button>
      </div>
    </div>

  </div>
  <br><br>
  <div class="row gy-4">
    <!-- Gamification Card -->
    <div class="col-md-12 col-lg-6">
      
      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(244 108 20) !important; border: 2px solid #f46c14;">
        <div style="top: -55px !important; position: absolute;  left: -69px;">
            <img src="<?php // echo base_url(); ?>public/img/op4/op3.svg">
        </div>
        <div style="top: 3px !important; position: absolute; text-align: right; right: -14px;">
          <button type="button" class="btn btn-sm me-3 waves-effect btn_d" data-bs-toggle="modal"
                      data-bs-target="#modal_debilidades" style="font-size: 27px;">D</button>
        </div>
        <br><br><br>
        <div class="d-flex align-items-end row">
          <div class="col-md-12 order-1 order-md-2">
            <div class="card-body p-4">
              <form class="form" method="post" role="form" id="form_registro_d" novalidate>
                <div class="row g-3">
                  <div class="col-md-10">
                    <div class="form-floating form-floating-outline mb-4">
                      <textarea type="text" name="concepto" id="concepto_d" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                      <label for="nombre">Concepto</label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_d">
                      <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                    </button>
                  </div>  
                </div> 
              </form>
              <div class="txt_debilidades">

                

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--/ Gamification Card -->

    <!-- Statistics Total Order -->
    <div class="col-md-12 col-lg-6">
      <div class="card h-100" style="box-shadow: 0 0.125rem 0.625rem 0 rgb(162 5 234) !important; border: 2px solid #a205ea;">
        <div style="top: -38px !important; position: absolute; text-align: right; right: -62px;">
            <img src="<?php //echo base_url(); ?>public/img/op4/op4.svg">
        </div>
        <div style="top: 3px !important; position: absolute; left: 2px;">
          <button type="button" class="btn btn-sm me-3 waves-effect btn_a" data-bs-toggle="modal"
                      data-bs-target="#modal_amenazas" style="font-size: 27px;">A</button>
        </div>
        <br><br><br>
        <div class="d-flex align-items-end row">
          <div class="col-md-12 order-1 order-md-2">
            <div class="card-body p-4">
              <form class="form" method="post" role="form" id="form_registro_a" novalidate>
                <div class="row g-3">
                  <div class="col-md-10">
                    <div class="form-floating form-floating-outline mb-4">
                      <textarea type="text" name="concepto" id="concepto_a" class="form-control colorlabel_white recordable rinited js-auto-size" required></textarea>
                      <label for="nombre">Concepto</label>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_registro_a">
                      <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                    </button>
                  </div>  
                </div> 
              </form>
              <div class="txt_amenazas">

                

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
 <php */ ?> 

<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_fortaleza"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4 style="color: black">Fortaleza</h4>
              <p style="text-align: justify;">Son los atributos internos y positivos que diferencian a la empresa de otros competidores.<br> 
              Considera aspectos como la experiencia del personal, activos intangibles, tecnología, ubicación geográfica, etc.
              <br><br>
              Enumera los recursos y capacidades internas que le dan ventaja a tu empresa, sobre tus competidores</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_oportunidades"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4 style="color: black">Oportunidades</h4>
              <p style="text-align: justify;">Son los factores externos que la empresa puede aprovechar para alcanzar sus objetivos. Estas pueden ser tendencias de mercado, cambios regulatorios, cambios demográficos, asociaciones estratégicas, entre otros.
              <br><br>
              Enumera las oportunidades que observes en el mercado, que te pueden beneficiar en un futuro</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_debilidades"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4 style="color: black">Debilidades</h4>
              <p style="text-align: justify;">Son los aspectos internos que limitan o reducen la capacidad de la organización para alcanzar sus objetivos. <br> 
              Esto puede incluir recursos insuficientes, falta de experiencia, procesos ineficientes, etc.
              <br><br>
              Enumera las desventajas que tienes vs tus competidores</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

<div
  class="modal-onboarding modal fade animate__animated"
  id="modal_amenazas"
  tabindex="-1"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header border-0">
        <button
          type="button"
          class="btn-close"
          data-bs-dismiss="modal"
          aria-label="Close"></button>
      </div>
      <div
        id="modal_fortalezas"
        class="carousel slide pb-4 mb-2"
        data-bs-interval="false">
        <div class="p-4">
          <div class="row g-3">
            <div class="col-md-12">
              <h4 style="color: black">Amenazas</h4>
              <p style="text-align: justify;">Son los factores externos que podrían causar problemas o daños a la empresa, que pueden poner en peligro la posición o el desempeño de la entidad.<br> 
              Pueden ser la competencia, cambios en la legislación, riesgos económicos, entre otros.
              <br><br>
              Enumera las amenazas que observas en tu mercado</p>
            </div>
          </div>    
        </div>
      </div>
    </div>
  </div>
</div>

