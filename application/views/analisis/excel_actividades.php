<?php
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_actividades".$fecha.".xls");
?>

  <table  border="1">
      <thead>
          <tr>
              <th>Debilidad a cambiar Fortaleza a mejorar</th>
              <th>Factor a manejar</th>
              <th>Prioridad</th>
              <th>Plan de trabajo general</th>
              <th>Plan de trabajo especifico</th>
              <th>Responsable</th>
              <th>Fecha de inicio</th>
              <th>Fecha de termino</th>
              <th>Tiempo estimado para concluir el objetivo especifico</th>
          </tr> 
      </thead>
      <tbody>
          <?php
          $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($idcliente,1);
          $totalm=0;
          foreach ($result_tm as $x){
              $totalm=$x->total;
          }

          $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($idcliente,1);
          $totali=0;
          foreach ($result_ti as $x){
              $totali=$x->total;
          }

          $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($idcliente,1);
          $totals=0;
          foreach ($result_ts as $x){
              $totals=$x->total;
          }
          $html='';
          if($totalm!=0 || $totali!=0 || $totals!=0){
              $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($idcliente,1);
              foreach ($result_merc as $x){
                  $html.='<tr>
                    <td >'.utf8_decode($x->titulo).'</td>
                    <td >'.utf8_decode($x->pregunta).'</td>
                    <td >Alta</td>
                    <td >'.utf8_decode($x->preg1).'</td>
                    <td >'.utf8_decode($x->preg2).'</td>
                    <td >'.utf8_decode($x->preg3).'</td>
                    <td >'.utf8_decode($x->preg4).'</td>
                    <td >'.utf8_decode($x->preg5).'</td>
                    <td >'.utf8_decode($x->preg6).'</td>
                  </tr>';
                  $result_merc_cro=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                  foreach ($result_merc_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  }             
              }
              $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($idcliente,1);
              foreach ($result_inter as $int){
                  $html.='<tr>
                    <td >'.utf8_decode($int->titulo).'</td>
                    <td >'.utf8_decode($int->pregunta).'</td>
                    <td >Alta</td>
                    <td >'.utf8_decode($int->preg1).'</td>
                    <td >'.utf8_decode($int->preg2).'</td>
                    <td >'.utf8_decode($int->preg3).'</td>
                    <td >'.utf8_decode($int->preg4).'</td>
                    <td >'.utf8_decode($int->preg5).'</td>
                    <td >'.utf8_decode($int->preg6).'</td>
                  </tr>';
                  $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                  foreach ($result_inter_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }
              $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($idcliente,1);
              foreach ($result_est as $est){
                  $html.='<tr>
                    <td >'.utf8_decode($est->titulo).'</td>
                    <td >'.utf8_decode($est->pregunta).'</td>
                    <td >Alta</td>
                    <td >'.utf8_decode($est->preg1).'</td>
                    <td >'.utf8_decode($est->preg2).'</td>
                    <td >'.utf8_decode($est->preg3).'</td>
                    <td >'.utf8_decode($est->preg4).'</td>
                    <td >'.utf8_decode($est->preg5).'</td>
                    <td >'.utf8_decode($est->preg6).'</td>
                  </tr>';
                  $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                  foreach ($result_est_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }
          }

          
          $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($idcliente,2);
          $totalm=0;
          foreach ($result_tm as $x){
              $totalm=$x->total;
          }

          $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($idcliente,2);
          $totali=0;
          foreach ($result_ti as $x){
              $totali=$x->total;
          }

          $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($idcliente,2);
          $totals=0;
          foreach ($result_ts as $x){
              $totals=$x->total;
          }

          if($totalm!=0 || $totali!=0 || $totals!=0){
              $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($idcliente,2);
              foreach ($result_merc as $x){
                  $html.='<tr>
                    <td >'.utf8_decode($x->titulo).'</td>
                    <td >'.utf8_decode($x->pregunta).'</td>
                    <td >Media</td>
                    <td >'.utf8_decode($x->preg1).'</td>
                    <td >'.utf8_decode($x->preg2).'</td>
                    <td >'.utf8_decode($x->preg3).'</td>
                    <td >'.utf8_decode($x->preg4).'</td>
                    <td >'.utf8_decode($x->preg5).'</td>
                    <td >'.utf8_decode($x->preg6).'</td>
                  </tr>';
                  $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                  foreach ($result_merc as $x){
                      $html.='<tr class="pre_cro_'.$x->id.'_1">
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($x->preg1).'</td>
                        <td >'.utf8_decode($x->preg2).'</td>
                        <td >'.utf8_decode($x->preg3).'</td>
                        <td >'.utf8_decode($x->preg4).'</td>
                        <td >'.utf8_decode($x->preg5).'</td>
                        <td >'.utf8_decode($x->preg6).'</td>
                      </tr>';
                  } 
              }
              $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($idcliente,2);
              foreach ($result_inter as $int){
                  $html.='<tr>
                    <td >'.utf8_decode($int->titulo).'</td>
                    <td >'.utf8_decode($int->pregunta).'</td>
                    <td >Media</td>
                    <td >'.utf8_decode($int->preg1).'</td>
                    <td >'.utf8_decode($int->preg2).'</td>
                    <td >'.utf8_decode($int->preg3).'</td>
                    <td >'.utf8_decode($int->preg4).'</td>
                    <td >'.utf8_decode($int->preg5).'</td>
                    <td >'.utf8_decode($int->preg6).'</td>
                  </tr>';
                  $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                  foreach ($result_inter_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }
              $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($idcliente,2);
              foreach ($result_est as $est){
                  $html.='<tr>
                    <td >'.utf8_decode($est->titulo).'</td>
                    <td >'.utf8_decode($int->pregunta).'</td>
                    <td >Media</td>
                    <td >'.utf8_decode($est->preg1).'</td>
                    <td >'.utf8_decode($est->preg2).'</td>
                    <td >'.utf8_decode($est->preg3).'</td>
                    <td >'.utf8_decode($est->preg4).'</td>
                    <td >'.utf8_decode($est->preg5).'</td>
                    <td >'.utf8_decode($est->preg6).'</td>
                  </tr>';
                  $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                  foreach ($result_est_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }

          }
          
          $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($idcliente,3);
          $totalm=0;
          foreach ($result_tm as $x){
              $totalm=$x->total;
          }

          $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($idcliente,3);
          $totali=0;
          foreach ($result_ti as $x){
              $totali=$x->total;
          }

          $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($idcliente,3);
          $totals=0;
          foreach ($result_ts as $x){
              $totals=$x->total;
          }

          if($totalm!=0 || $totali!=0 || $totals!=0){
              $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($idcliente,3);
              foreach ($result_merc as $x){
                  $html.='<tr>
                    <td >'.utf8_decode($x->titulo).'</td>
                    <td >'.utf8_decode($x->pregunta).'</td>
                    <td >Baja</td>
                    <td >'.utf8_decode($x->preg1).'</td>
                    <td >'.utf8_decode($x->preg2).'</td>
                    <td >'.utf8_decode($x->preg3).'</td>
                    <td >'.utf8_decode($x->preg4).'</td>
                    <td >'.utf8_decode($x->preg5).'</td>
                    <td >'.utf8_decode($x->preg6).'</td>
                  </tr>';
                  $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                  foreach ($result_merc as $x){
                      $html.='<tr class="pre_cro_'.$x->id.'_1">
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($x->preg1).'</td>
                        <td >'.utf8_decode($x->preg2).'</td>
                        <td >'.utf8_decode($x->preg3).'</td>
                        <td >'.utf8_decode($x->preg4).'</td>
                        <td >'.utf8_decode($x->preg5).'</td>
                        <td >'.utf8_decode($x->preg6).'</td>
                      </tr>';

                  } 
              }

              $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($idcliente,3);
              foreach ($result_inter as $int){
                  $html.='<tr>
                    <td >'.utf8_decode($int->titulo).'</td>
                    <td >'.utf8_decode($int->pregunta).'</td>
                    <td >Baja</td>
                    <td >'.utf8_decode($int->preg1).'</td>
                    <td >'.utf8_decode($int->preg2).'</td>
                    <td >'.utf8_decode($int->preg3).'</td>
                    <td >'.utf8_decode($int->preg4).'</td>
                    <td >'.utf8_decode($int->preg5).'</td>
                    <td >'.utf8_decode($int->preg6).'</td>
                  </tr>';
                  $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                  foreach ($result_inter_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }
              $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($idcliente,3);
              foreach ($result_est as $est){
                  $html.='<tr>
                    <td >'.utf8_decode($est->titulo).'</td>
                    <td >'.utf8_decode($est->pregunta).'</td>
                    <td >Baja</td>
                    <td >'.utf8_decode($est->preg1).'</td>
                    <td >'.utf8_decode($est->preg2).'</td>
                    <td >'.utf8_decode($est->preg3).'</td>
                    <td >'.utf8_decode($est->preg4).'</td>
                    <td >'.utf8_decode($est->preg5).'</td>
                    <td >'.utf8_decode($est->preg6).'</td>
                  </tr>';
                  $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                  foreach ($result_est_cro as $cro){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($cro->preg1).'</td>
                        <td >'.utf8_decode($cro->preg2).'</td>
                        <td >'.utf8_decode($cro->preg3).'</td>
                        <td >'.utf8_decode($cro->preg4).'</td>
                        <td >'.utf8_decode($cro->preg5).'</td>
                        <td >'.utf8_decode($cro->preg6).'</td>
                      </tr>';
                  } 
              }
          }
          
          $result=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza($idcliente);
          $total=0;
          foreach ($result as $x){
              $total=$x->total;
          }
          if($total!=0){
              $result_cronog=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza',array('idcliente'=>$idcliente,'activo'=>1));
              foreach ($result_cronog->result() as $x){
                $pri='';
                if($x->prioridad==1){
                  $pri='Alta';
                }else if($x->prioridad==2){
                  $pri='Media';
                }else if($x->prioridad==3){
                  $pri='Baja';
                }
                  $html.='<tr>
                    <td >'.utf8_decode($x->debilidad_cambiar).'</td>
                    <td >'.utf8_decode($x->factor_manejar).'</td>
                    <td >'.utf8_decode($pri).'</td>
                    <td >'.utf8_decode($x->preg1).'</td>
                    <td >'.utf8_decode($x->preg2).'</td>
                    <td >'.utf8_decode($x->preg3).'</td>
                    <td >'.utf8_decode($x->preg4).'</td>
                    <td >'.utf8_decode($x->preg5).'</td>
                    <td >'.utf8_decode($x->preg6).'</td>
                  </tr>';
                  $result_cronogd=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza_detalle',array('idcronograma_debilidad_fortaleza'=>$x->id,'activo'=>1));
                  foreach ($result_cronogd->result() as $crod){
                      $html.='<tr>
                        <td ></td>
                        <td ></td>
                        <td ></td>
                        <td >'.utf8_decode($crod->preg1).'</td>
                        <td >'.utf8_decode($crod->preg2).'</td>
                        <td >'.utf8_decode($crod->preg3).'</td>
                        <td >'.utf8_decode($crod->preg4).'</td>
                        <td >'.utf8_decode($crod->preg5).'</td>
                        <td >'.utf8_decode($crod->preg6).'</td>
                      </tr>';
                  } 
              }
          }
          echo $html;
          ?>
      </tbody>
  </table>



