<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModeloCostosventas extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    /*
    function get_ventashistoricas($idv,$idcliente,$mes){
        $strq = "SELECT vh.*
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.id=$idv AND v.idcliente=$idcliente AND vh.mes=$mes AND vh.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }
    */


    function get_ventasingresos($idcliente,$anio){
        $strq = "SELECT mes,anio,sum(cantidad) as cantidad from(
                        SELECT vh.mes,vh.anio_6 as anio,vh.cantidad_6 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_6='$anio' AND vh.activo=1
                        
                        UNION 
                        
                        SELECT vh.mes,vh.anio_5 as anio,vh.cantidad_5 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_5='$anio' AND vh.activo=1
                        
                        UNION 
                        
                        SELECT vh.mes,vh.anio_4 as anio,vh.cantidad_4 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_4='$anio' AND vh.activo=1
                        
                        UNION 
                        
                        SELECT vh.mes,vh.anio_3 as anio,vh.cantidad_3 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_3='$anio' AND vh.activo=1
                        
                        UNION 
                        
                        SELECT vh.mes,vh.anio_2 as anio,vh.cantidad_2 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_2='$anio' AND vh.activo=1
                        
                        UNION 
                        
                        SELECT vh.mes,vh.anio_1 as anio,vh.cantidad_1 as cantidad
                        FROM ventashistoricas AS v
                        LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
                        WHERE v.activo=1 AND v.idcliente='$idcliente'  AND vh.anio_1='$anio' AND vh.activo=1
                ) as datos  GROUP by mes"; 
        $query = $this->db->query($strq);
        return $query;
    }
    /*
    function get_resumen($id,$idcliente,$anio){
        $strq = "SELECT vr.*
            FROM ventashistoricas_resumen AS r
            LEFT JOIN ventashistoricas_resumen_detalles AS vr ON vr.idventashistoricas_resumen=r.id
            WHERE vr.idventashistoricas=$id AND r.idcliente=$idcliente AND r.anio=$anio AND r.activo=1 AND vr.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_ventas_cantidad($idcliente,$anio,$id){
        $strq = "SELECT SUM(vh.cantidad_1) AS cantidad_1
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.activo=1 AND v.idcliente=$idcliente AND vh.anio_1=$anio AND vh.activo=1 AND vh.idventashistoricas=$id"; 
        $query = $this->db->query($strq);
        return $query->row();
    }
    */
    function obtener_info_costosventas($idser,$anio){
        $strq="SELECT cvpdll.* 
                FROM costosventas_pro_dll as cvpdll
                INNER JOIN costosventas_pro AS cvp on cvp.id=cvpdll.idpro
                WHERE cvpdll.activo=1 AND cvp.activo=1 AND cvpdll.anio='$anio' AND cvp.id_linea_servicio='$idser'";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtener_info_costosventas_sum($idser,$anio){
        $strq="SELECT cvpdll.anio,cvpdll.mes,SUM(cvpdll.valor) as valor 
                FROM costosventas_pro_dll as cvpdll
                INNER JOIN costosventas_pro AS cvp on cvp.id=cvpdll.idpro
                WHERE cvpdll.activo=1 AND cvp.activo=1 AND cvpdll.anio='$anio' AND cvp.id_linea_servicio='$idser'
                GROUP BY cvpdll.anio,cvpdll.mes;";
        $query = $this->db->query($strq);
        return $query;
    }
}
