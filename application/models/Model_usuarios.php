<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_usuarios extends CI_Model {
    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    function get_list($params){
        $columnsx = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.correo',
            3=>'p.telefono',
            4=>'u.Usuario',
            5=>'u.acceso',
            6=>'per.nombre as perfil',
            7=>'u.UsuarioID'
        );
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.correo',
            3=>'p.telefono',
            4=>'u.Usuario',
            5=>'p.motivo',
            6=>'per.nombre',
        );
        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId','left');
        $this->db->join('perfiles per','per.perfilId=u.perfilId','left');
        $where = array('p.activo'=>1,'u.tipo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_list($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.correo',
            3=>'p.telefono',
            4=>'u.Usuario',
            5=>'u.acceso',
            6=>'per.nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('usuarios u','u.personalId=p.personalId','left');
        $this->db->join('perfiles per','per.perfilId=u.perfilId','left');
        $where = array('p.activo'=>1,'u.tipo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


}