<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_ventashistoricas extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_ventashistoricas($idv,$idcliente,$mes){
        $strq = "SELECT vh.*
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.id=$idv AND v.idcliente=$idcliente AND vh.mes=$mes AND vh.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }


    function get_ventasingresos($idcliente,$mes){
        $strq = "SELECT SUM(vh.cantidad_6) AS cantidad_6,SUM(vh.cantidad_5) AS cantidad_5,SUM(vh.cantidad_4) AS cantidad_4,SUM(vh.cantidad_3) AS cantidad_3,SUM(vh.cantidad_2) AS cantidad_2,SUM(vh.cantidad_1) AS cantidad_1
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.activo=1 AND v.idcliente=$idcliente AND vh.mes=$mes AND vh.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_resumen($id,$idcliente,$anio){
        $strq = "SELECT vr.*
            FROM ventashistoricas_resumen AS r
            LEFT JOIN ventashistoricas_resumen_detalles AS vr ON vr.idventashistoricas_resumen=r.id
            WHERE vr.idventashistoricas=$id AND r.idcliente=$idcliente AND r.anio=$anio AND r.activo=1 AND vr.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_ventas_cantidad($idcliente,$anio,$id){
        $strq = "SELECT SUM(vh.cantidad_1) AS cantidad_1
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.activo=1 AND v.idcliente=$idcliente AND vh.anio_1=$anio AND vh.activo=1 AND vh.idventashistoricas=$id"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_ventas_promediob($idcliente,$anio,$id,$mes){
        $strq = "SELECT vh.promedio,vh.cantidad_1
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_detalles AS vh ON vh.idventashistoricas=v.id
            WHERE v.activo=1 AND v.idcliente=$idcliente AND vh.anio_1=$anio AND vh.activo=1 AND vh.idventashistoricas=$id AND vh.mes=$mes"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_proyeccion_venta($idcliente,$id,$num_tabla){
        $strq = "SELECT vp.*
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_proyeccion_venta AS vp ON vp.idventashistoricas=v.id
            WHERE v.idcliente=$idcliente AND vp.idventashistoricas=$id AND vp.num_tabla=$num_tabla AND v.activo=1 AND vp.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

    function get_proyeccion_venta_detalle($idcliente,$id,$num_tabla,$mes){
        $strq = "SELECT vp.*
            FROM ventashistoricas AS v
            LEFT JOIN ventashistoricas_proyeccion_venta_detalles AS vp ON vp.idventashistoricas=v.id
            WHERE v.idcliente=$idcliente AND vp.idventashistoricas=$id AND vp.num_tabla=$num_tabla AND vp.mes=$mes AND v.activo=1 AND vp.activo=1"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

}
