<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        $this->hora_actual = date('G:i:s');
    }

    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }

    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }

    function getselect_tabla($table){
        $this->db->select('*');
        $this->db->from($table);
        $query=$this->db->get(); 
        return $query;
    }

    function getselectwheren_2($select,$table,$where){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function getdeletewheren3($table,$where){
        $this->db->delete($table,$where);
    }

    function getdeletewheren2($table,$where){
        
        $strq = "DELETE from $table WHERE idpago=$where";
        $query = $this->db->query($strq);
    }

    function getselectwherenall($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }

    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE estatus=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclike2($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE status=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclike3($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }

    function updatestock3($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2,$idname3,$id3){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 and $idname3=$id3 ";
        $query = $this->db->query($strq);
        //return $id;
    }

    function updatestock4($Tabla,$value,$masmeno,$value2,$idname1,$id1){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1";
        $query = $this->db->query($strq);
        //return $id;
    }

    function viewprefacturaslis($contrato,$tipo){
        if ($tipo>0) {
            if ($tipo==1) {
                $wheretipo=' and facpre.statuspago=0';
            }else{
                $wheretipo=' and facpre.statuspago=1';
            }
            
        }else{
            $wheretipo='';
        }
        $strq = "SELECT * 
                    FROM factura_prefactura as facpre
                    inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                    WHERE facpre.contratoId=$contrato $wheretipo"; 
        $query = $this->db->query($strq);
        return $query;
    }
    
    function viewprefacturaslis_general($contrato){
        $strq = "SELECT facpre.*,fac.*,facprep.*,sum(facprep.pago) as pagot, max(facprep.fecha) as fechamax
                FROM factura_prefactura as facpre
                inner JOIN f_facturas as fac on fac.FacturasId=facpre.facturaId
                left join factura_prefactura_pagos as facprep on facprep.facId=facpre.facId 
                WHERE facpre.contratoId=$contrato GROUP by fac.FacturasId

                "; 
        $query = $this->db->query($strq);
        return $query;
    }

    

    



 

  
    
    function descripcionfactura($factura){
        $strq = "SELECT * FROM f_facturas_servicios where FacturasId=$factura";
        $query = $this->db->query($strq);
        $descripcion='';
        foreach ($query->result() as $item) {
            $descripcion.='<div>'.$item->Descripcion2.'</div>';
        }
        return $descripcion;
    }


    public function asignaciona_p_e($idasig)    {
        $sql = "SELECT e.modelo,r.serie
                FROM asignacion_ser_poliza_a_e AS a
                INNER JOIN polizasCreadas_has_detallesPoliza AS r ON r.id=a.idequipo
                INNER JOIN equipos AS e ON e.id=r.idEquipo
                WHERE a.asignacionId = $idasig";
        $query = $this->db->query($sql);
        return $query;
    }

 
   
    public function doc_cliente_tel_cel($id){
        $sql="SELECT
            ct.tel_local,
            cc.celular
        FROM clientes AS acl
        INNER JOIN cliente_has_telefono AS ct ON ct.idCliente=acl.id
        INNER JOIN cliente_has_celular AS cc ON cc.idCliente=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->db->query($sql);
        return $query->row();
    }
    public function doc_cliente_tel_cel2($id){
        $sql="SELECT
            ct.telefono,
            ct.celular,
            ct.email
        FROM clientes AS acl
        INNER JOIN cliente_datoscontacto AS ct ON ct.clienteId=acl.id
        WHERE acl.id = $id
        GROUP BY acl.id
        ";
        $query=$this->db->query($sql);
        return $query->row();
    }





    /*
    function getlistfoliostatus($cliente){

    */
    //==============================================
   
    //==============================================

    function nombreunidadfacturacion($id){
        $sql="SELECT nombre
                from f_unidades
                WHERE UnidadId='$id'";
        $query=$this->db->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        return $nombre;
    }
    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad,
                dt.iva
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }

    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId
            ";
        $query=$this->db->query($sql);
        return $query;
    }
  

    function total_facturas($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' Estado = 1 ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_facturas 
                WHERE $estado and activo =1 and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalcomplementos($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' Estado = 1 ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_complementopago 
                WHERE activo=1 and $estado and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function ultimoFolio() {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }

    function topclientesfacturados(){
        $strq = "SELECT clienteId,Nombre,Rfc,sum(total) as total FROM `f_facturas` WHERE Estado=1 and activo=1 GROUP BY clienteId ORDER BY `total` DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query; 
    }
    function sumadelmesfacturados($fini,$ffin){
        $strq = "SELECT sum(total) as total FROM f_facturas WHERE Estado=1 and activo=1 AND fechatimbre BETWEEN '$fini 00:00:00' AND '$ffin 23:59:59'";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }
    function sumapagadocomplemento($factura){
        $strq = "SELECT sum(comd.ImpPagado) as total FROM `f_complementopago_documento` as comd INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId WHERE comd.facturasId=$factura and com.Estado=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }

    public function get_cliente($buscar){
        $strq="SELECT clienteId,razon_social
                FROM clientes 
                WHERE activo = 1 AND razon_social like '%$buscar%'
                UNION
                SELECT clienteId,Nombre AS Nombre
                FROM f_facturas 
                WHERE activo = 1 AND Folio like '%$buscar%'"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function total_facturas_mes_num($anio,$mes){

        $strq = "SELECT COUNT(*) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_ventas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    public function get_cliente_top_10_fecha($anio,$mes){
        $strq="SELECT c.razon_social, COUNT(*) AS reg, SUM(f.total) AS total FROM f_facturas AS f
            INNER JOIN clientes AS c ON c.clienteId=f.clienteId
            WHERE f.activo=1 AND f.Estado=1 AND MONTH(f.fechatimbre)=$mes AND YEAR(f.fechatimbre)=$anio GROUP BY f.clienteId ORDER BY SUM(f.total) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_cliente_top_10(){
        $strq="SELECT c.razon_social, COUNT(*) AS reg, SUM(f.total) AS total FROM f_facturas AS f
            INNER JOIN clientes AS c ON c.clienteId=f.clienteId
            WHERE f.activo=1 AND f.Estado=1 GROUP BY f.clienteId ORDER BY SUM(f.total) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_vacunas_top_10_fecha($anio,$mes){
        $strq="SELECT c.Descripcion2 AS Descripcion, SUM(c.Cantidad) AS reg, SUM(c.Importe) AS total FROM f_facturas AS f
            INNER JOIN f_facturas_servicios AS c ON c.FacturasId=f.FacturasId
            WHERE f.activo=1 AND f.Estado=1 AND MONTH(f.fechatimbre)=$mes AND YEAR(f.fechatimbre)=$anio GROUP BY c.Descripcion2 ORDER BY SUM(c.Importe) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_vacunas_top_10(){
        $strq="SELECT c.Descripcion2 AS Descripcion, SUM(c.Cantidad) AS reg, SUM(c.Importe) AS total FROM f_facturas AS f
            INNER JOIN f_facturas_servicios AS c ON c.FacturasId=f.FacturasId
            WHERE f.activo=1 AND f.Estado=1 GROUP BY c.Descripcion2 ORDER BY SUM(c.Importe) DESC LIMIT 10"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getclienterazonsociallike($search){
        
        $strq = "SELECT clidf.*, cli.desactivar,cli.motivo,cli.dias_saldo,
                cli.diaspago,
                cli.saldopago 
                FROM clientes as cli
                INNER JOIN cliente_fiscales as clidf on clidf.idcliente=cli.id
                WHERE cli.activo=1 AND clidf.activo=1 and clidf.razon_social like '%$search%' order by clidf.razon_social asc";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    
    function getsearchproductoslike($search){
        $strq = "SELECT p.*, ps.stock,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8 and p.tipo=2),0) AS lote8,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8 and p.tipo=1),0) as serie8 
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid=8 and p.tipo=0
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_tipo_like($search){
        $strq = "SELECT p.*, ps.stock,
            IFNULL((SELECT sum(cantidad) FROM productos_sucursales_lote WHERE activo=1 AND productoid=p.id AND sucursalid=8 and p.tipo=2),0) AS lote8,
            IFNULL((SELECT count(*) FROM productos_sucursales_serie WHERE activo=1 and disponible=1 AND productoid=p.id AND sucursalid=8 and p.tipo=1),0) as serie8 
            from productos p 
            join productos_sucursales ps on ps.productoid=p.id and sucursalid=8
            where (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductoslike_compras($search){
        $strq = "SELECT * from productos where (nombre like '%$search%' or idProducto like '%$search%') and (activo=1 or activo='Y') and referencia like '%3%' order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_ventas_like($search,$suc){
        $strq = "SELECT p.*, IFNULL(concat('Lote: ',psl.lote,' ','Caducidad: ',psl.caducidad,' (','',psl.cantidad,')'),'') as lote, IFNULL(psl.id,'0') as id_prod_suc, psl.caducidad,
            IFNULL(concat('Serie: ',pss.serie),'') as serie, IFNULL(pss.id,'0') as id_prod_suc_serie
            from productos p
            left join productos_sucursales_lote psl on psl.productoid=p.id and psl.activo=1 and psl.sucursalid=$suc and psl.cantidad>0
            left join productos_sucursales_serie pss on pss.productoid=p.id and pss.activo=1 and pss.disponible=1 and pss.sucursalid=$suc
            where referencia like '%2%' and (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by psl.caducidad asc, nombre asc "; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_solicita_like($search){
        $strq = "SELECT p.*
            from productos p
            where referencia like '%2%' and (nombre like '%$search%' or idProducto like '%$search%') and (p.activo=1 or p.activo='Y') 
            order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function getsearchproductos_ventas_series_like($search){
        $strq = "SELECT * from productos where tipo=1 and (nombre like '%$search%' or idProducto like '%$search%') and (activo=1 or activo='Y') order by nombre asc"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function searchRecargas_like($search){
        $strq = "SELECT * from recargas where codigo like '%$search%' AND tipo=1 and estatus=1"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function searchRentas_like($search){
        $strq = "SELECT * from servicios where clave like '%$search%' AND activo=1 or descripcion like '%$search%' AND activo=1"; 
        $query = $this->db->query($strq);
        return $query;
    }

    function obtenerformapagoventa($idventa){

        $strq = "SELECT * FROM `venta_erp_formaspagos` WHERE idventa=$idventa ORDER BY monto DESC LIMIT 1";
        $query = $this->db->query($strq);
        $formapago='';
        foreach ($query->result() as $row) {
            $formapago=$row->formapago;
        }

        return $formapago; 
    }

    function infoventapre($id){
        $strq = "SELECT v.*,clif.razon_social 
        FROM venta_erp as v 
        INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial 
        WHERE v.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    function infocotizacion($id){
        $strq = "SELECT v.*,clif.razon_social 
        FROM cotizacion as v 
        INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial 
        WHERE v.id=$id"; 
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclikeunidadsat($search){
        $strq = "SELECT * from f_unidades WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclikeconceptosa($search){
        $strq = "SELECT * from f_servicios WHERE activo=1 and (Clave like '%".$search."%' or nombre like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function getseleclikeproveedor($search){
        $strq = "SELECT * from proveedores WHERE activo=1 and (nombre like '%".$search."%' or codigo like '%".$search."%' )";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function get_alertas_traspasos(){
        $strq="SELECT h.id,h.tipo,p.nombre,sx.name_suc AS name_sucx,sy.name_suc AS name_sucy
            FROM historial_alerta AS h
            LEFT JOIN sucursal AS sx ON sx.id=h.sucursaldestino
            LEFT JOIN sucursal AS sy ON sy.id=h.sucursalorigen
            LEFT JOIN personal AS p ON p.personalId=h.idpersonal
            WHERE h.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_alertas_total(){
        $strq="SELECT COUNT(*) AS total
            FROM historial_alerta AS h
            WHERE h.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function genSelect($tabla){
        $query = $this->db->get($tabla);
        return $query->result();
    }
    function generararqueosinfo($suc,$fecha){
        $strq="SELECT MAX(ar.folio) as folio FROM arqueos as ar WHERE ar.sucursal=$suc AND ar.activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    function generararqueosql($emple,$suc,$fechainicio){
        if($emple>0){
            $where_emple=" and ven.personalid='$emple' ";
        }else{
            $where_emple="";
        }
        $strq="SELECT fmp.clave as clavefm, fmp.formapago_alias,ban.id as bancoid, ban.ban_clave,IFNULL(ban.name_ban,'') as name_ban, SUM(vfmp.monto) as monto
                FROM f_formapago as fmp
                INNER JOIN venta_erp_formaspagos as vfmp ON vfmp.formapago=fmp.clave
                INNER JOIN venta_erp as ven on ven.id=vfmp.idventa AND ven.sucursal=$suc $where_emple AND ven.reg BETWEEN '$fechainicio 00:00:00' AND '$fechainicio 23:59:59'
                LEFT JOIN bancos as ban on ban.id=vfmp.banco 
                WHERE ven.activo=1
                GROUP BY fmp.clave,ban.id;"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function info_arqueo_dll($idarqueo){
        $strq="SELECT arqdll.id,fform.formapago_alias,IFNULL(ban.name_ban,'') as banco,arqdll.monto,arqdll.monto_confirm,arqdll.clavefm,arqdll.bancoid
            FROM arqueos_dll arqdll
            INNER JOIN f_formapago fform ON fform.clave=arqdll.clavefm
            LEFT JOIN bancos as ban on ban.id=arqdll.bancoid
            WHERE arqdll.activo=1 AND arqdll.idarqueo=$idarqueo"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function fomapago($clave){
        $strq="SELECT *
                FROM f_formapago
            
                WHERE clave='$clave' "; 
        $query = $this->db->query($strq);
        $query=$query->row();
        return $query;
    }
    function getbanco($clave){
        $strq="SELECT *
                FROM bancos
            
                WHERE id='$clave' "; 
        $query = $this->db->query($strq);
        
        return $query;
    }
    function generareportexz($idarqueo,$tipo,$view){
        //http://localhost/semit_erp/Cortecaja?cortexz=1&idarqueo=5#tab21
        $result_ar =  $this->getselectwheren('arqueos',array('id'=>$idarqueo));
        $result_ar= $result_ar->row();
        $folioarqueo=$result_ar->folio;
        $fecha_arqueo=$result_ar->fecha;
        $ar_idsucursal=$result_ar->sucursal;
        $ar_reg=$result_ar->reg;
        //============================================================================================
        $result_suc =  $this->getselectwheren('sucursal',array('id'=>$result_ar->sucursal));
        $result_suc=$result_suc->row();
        $sucursal=$result_suc->name_suc;
        //============================================================================================
        $result_per =  $this->getselectwheren('personal',array('personalId'=>$result_ar->personal));
        $result_per=$result_per->row();
        $personal=$result_per->nombre;
        //============================================================================================
        $result_dll =  $this->info_arqueo_dll($idarqueo);
        $result_dll = $result_dll->result();
        $apartura_de_caja=0;//generar la consul para traer los cortes de las cajas del personal que pertenescan a una determinada sucursal (tabla 'turnos')
        //==========================================
            $this->updateCatalogo('turnos',array('estatus'=>2,'idpersonal_c'=>$result_ar->personal,'fecha_c'=>$fecha_arqueo,'hora_c'=>$this->hora_actual),array('estatus'=>1,'fecha'=>$fecha_arqueo,'sucursal'=>$ar_idsucursal));
        //==========================================
            $apartura_de_caja=$this->obtenersaldoencaja($ar_idsucursal,$fecha_arqueo);
            $apartura_de_caja=floatval($apartura_de_caja);
        //==========================================
            $monto_credito=$this->obtenermontoscredito($ar_idsucursal,$fecha_arqueo);
        //==========================================
        $html='<style type="text/css">
        .c_blue{color:#152342;}
        .c_green{color:#c6d420;}
        .t_c{text-align:center;}
        .t_bold{font-weight:bold;}
        .t_size_12{font-size:12px;}
        .table_report{width: 100%;}
        .texc{text-align: center;}
        .texe{text-align: end;}
        .sbordertop{border-top: 1px solid #152342;        }
        .sborderbottom{border-bottom: 1px solid #152342;        }
       </style>';
        if($tipo==0){
            $html.='<table border="0" aling="center" class="table_report  c_blue">
                        <tr><td class="texc"><b>Cierre de caja z</b></td></tr>
                        <tr><td class="texc"><b>SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</b></td></tr>
                        <tr><td class="texc"><b>'.$sucursal.'</b></td></tr>
                        <tr><td class="texc"><b>Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</b></td></tr>
                        <tr><td class="texc"><b>Cierre de cajas Z de la Sucursal '.$sucursal.'</b></td></tr>
                        <tr><td>Fecha de cierre: '.$ar_reg.'</td></tr>
                    </table>';
            $html.='<table border="0" class="table_report"><tbody><tr><td class="sborderbottom "><b>MOVIMIENTOS DE CAJAS</b></td></tr></tbody></table>';
            $html.='<table border="0" class="table_report">
                     <tr>
                        <td colspan="4">Estacion de trabajo: '.$sucursal.'</td>
                     </tr>
                     <tr>
                        <td colspan="4">Apertura de caja</td>
                     </tr>
                     <tr>
                        <td>EF</td>
                        <td>Efectivo</td>
                        <td></td>
                        <td>$'.number_format(($apartura_de_caja),2,'.',',').'</td>
                     </tr>
                     <tr>
                        <td></td>
                        <td></td>
                        <td>Total de concepto</td>
                        <td>$'.number_format(($apartura_de_caja),2,'.',',').'</td>
                     </tr>
                     <tr>
                        <td colspan="4"></td>
                     </tr>
                     <tr>
                        <td colspan="4">Venta</td>
                     </tr>
                     ';
                     $total_concepto=0;
                     foreach ($result_dll as $item) {
                        if($item->clavefm=='01'){
                            $monto_MV=$item->monto-$apartura_de_caja;
                        }else{
                            $monto_MV=$item->monto;
                        }
                        if($item->clavefm=='00'){

                        }else{
                            $total_concepto=$total_concepto+$monto_MV;
                        }
                        
                        $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td></td><td>$ '.number_format(($monto_MV),2,'.',',').'</td></tr>';
                     }
                     if($monto_credito>0){
                        $html.='<tr><td>Credito</td><td></td><td>$ '.number_format(($monto_credito),2,'.',',').'</td></tr>';
                     }
                     $html.='<tr><td></td><td>Total Concepto: </td><td>$ '.number_format($total_concepto,2,'.',',').'</td></tr>';
                     $html.='<tr><td></td><td>Total estacion: </td><td>$ '.number_format(($total_concepto+$apartura_de_caja),2,'.',',').'</td></tr>';
                     $html.='<tr><td></td><td>GRAN TOTAL: </td><td>$ '.number_format(($total_concepto+$apartura_de_caja),2,'.',',').'</td></tr>';

                     $html.='<tr><td></td><td></td><td></td></tr>';
                     $html.='<tr><td></td><td>Total sin apertura: </td><td>$ '.number_format($total_concepto,2,'.',',').'</td></tr>';
            $html.='</table>';
        }
        if($tipo==1){
            $html.='<table border="0" aling="center" class="table_report  c_blue">
                        <tr><td class="texc"><b>Arqueo de caja</b></td></tr>
                        <tr><td class="texc"><b>SERVICIOS Y EQUIPOS MEDICOS INTERNACIONALES DE TOLUCA</b></td></tr>
                        <tr><td class="texc"><b>Calle: Av Benito Juerez Col 08 CP 52140 Estado de Mexico</b></td></tr>
                        <tr><td><b>No. corte: <span class="c_green">'.$folioarqueo.'</span></b></td></tr>
                        <tr><td><b>Empleado: <span class="c_green">'.$personal.'</span></b></td></tr>
                        <tr><td><b>Sucursal: <span class="c_green">'.$sucursal.'</span></b></td></tr>
                    </table>';
            $html.='<table border="0" class="table_report">
                        <tr><td colspan="2">Arqueo de caja</td><td></td></tr>
                        <tr><td width="20%"></td><td width="60%">';
                        $html.='<table border="0" class="table_report">';
                        $montog=0;
                        $montoxz=0;
                        foreach ($result_dll as $item) {
                            if($item->clavefm=='00'){

                            }else{
                                $montog=$montog+$item->monto;
                                $montoxz=$montoxz+$item->monto_confirm;
                            }
                            

                            $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($item->monto),2,'.',',').'</td></tr>';
                        }
                        $html.='<tr><td class="texe">Total con apertura de caja: </td><td>$ '.number_format(($montog),2,'.',',').'</td></tr>';
                        $html.='<tr><td class="texe">Total corte XZ: </td><td>$ '.number_format(($montoxz),2,'.',',').'</td></tr>';

                        $html.='<tr><td class="texe sbordertop">Dif. Arqueo vs corte: </td><td class="sbordertop">$ '.number_format(($montog-$montoxz),2,'.',',').'</td></tr>';
                        $html.='</table>';

                        $html.='</td width="20%"><td></td></tr>
                    </table>';
            $html.='<table border="0" class="table_report">
                        <tr><td colspan="2">Ingresado</td><td></td></tr>
                        <tr><td width="20%"></td><td width="60%">';
                        $html.='<table border="0" class="table_report">';
                        $total_corte_ciego=0;
                        foreach ($result_dll as $item) {
                            if($item->clavefm=='00'){

                            }else{
                                $total_corte_ciego=$total_corte_ciego+$item->monto_confirm;
                            }

                            $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($item->monto_confirm),2,'.',',').'</td></tr>';
                        }
                        $html.='<tr><td class="texe ">Total corte ciego: </td><td class="">$ '.number_format(($total_corte_ciego),2,'.',',').'</td></tr>';
                        $html.='</table>';

                        $html.='</td width="20%"><td></td></tr>
                    </table>';
            $html.='<table border="0" class="table_report">
                        <tr><td colspan="2">Movimientos</td><td></td></tr>
                        <tr><td width="20%"></td><td width="60%">';
                        $html.='<table border="0" class="table_report">';
                        $html.='<tr><td colspan="2"></td></tr>';
                        log_message('error','apartura_de_caja: '.$apartura_de_caja);
                        $html.='<tr><td class="sborderbottom">APERTURA DE CAJA</td><td class="sborderbottom">$ '.number_format(($apartura_de_caja),2,'.',',').'</td></tr>';
                        
                        $html.='<tr><td colspan="2"></td></tr>';
                        $html.='<tr><td colspan="2" class="sborderbottom">VENTAS</td></tr>';
                        foreach ($result_dll as $item) {
                            
                            if($item->clavefm=='01'){
                                $monto_sin_caja=$item->monto-$apartura_de_caja;
                            }else{
                                $monto_sin_caja=$item->monto;
                            }
                            $html.='<tr><td>'.$item->formapago_alias.' '.$item->banco.'</td><td>$ '.number_format(($monto_sin_caja),2,'.',',').'</td></tr>';
                        }
                            $html.='<tr><td colspan="2" class="sborderbottom"></td></tr>';

                            $html.='<tr><td class="texe">Total sin apertura de caja: </td><td>$ '.number_format(($montog-$apartura_de_caja),2,'.',',').'</td></tr>';//AGREGA EL DESCUENTO DEL CORTE DE CAJA
                            $html.='<tr><td class="texe">Total corte XZ: </td><td>$ '.number_format(($montoxz),2,'.',',').'</td></tr>';
                            $html.='<tr><td class="texe sbordertop">Dif. Arqueo vs corte: </td><td class="sbordertop">$ '.number_format((($montog-$apartura_de_caja)-$montoxz),2,'.',',').'</td></tr>';

                        $html.='</table>';

                        $html.='</td width="20%"><td></td></tr>
                    </table>';

                    $html.='<table border="0" class="table_report"><tr><td class="sborderbottom "><b>DETALLES DE FORMA DE PAGO</b></td></tr></table>';
                    $gran_total=0; $montototal=0;
                    $res_formp=$this->formaspagosvgroupby();
                    foreach ($res_formp->result() as $itemfp) {
                        $name_formapago=$itemfp->formapago_alias.' '.$itemfp->name_ban;
                        $id_fompago=$itemfp->formapago;
                        $id_banco=$itemfp->banco;
                        $resul_pagos = $this->optenciondepagosventas($ar_idsucursal,$fecha_arqueo,$id_fompago,$id_banco);
                        $montototal_fomp=0;
                        if($resul_pagos->num_rows()>0){
                            $html.='<table border="0" class="table_report"><tr><td class="sborderbottom "><b>'.$name_formapago.'</b></td></tr></table>';
                            $html.='<table border="0" class="table_report">';
                                    if($id_fompago=='01'){
                                        $html.='<tr><td>Efectivo</td><td></td><td></td><td>$ '.number_format($apartura_de_caja,2,'.',',').'</td></tr>';
                                    }
                                foreach ($resul_pagos->result() as $itemfp_table) {
                                    $montototal_fomp=$montototal_fomp+$itemfp_table->monto;
                                    $montototal=$montototal+$itemfp_table->monto;
                                    $html.='<tr><td>'.$itemfp_table->tipo.'</td><td>'.$itemfp_table->razon_social.'</td><td>'.$itemfp_table->folio.'</td><td>$ '.number_format($itemfp_table->monto,2,'.',',').'</td></tr>';
                                }
                                $html.='<tr><td></td><td colspan="2" class="texe"><b>'.$name_formapago.'</b></td><td>$ '.number_format($montototal_fomp,2,'.',',').'</td></tr>
                                <!--<tr><td></td><td colspan="2" class="texe"><b>GRAN TOTAL</b></td><td>$ '.number_format($gran_total,2,'.',',').'</td></tr>-->';
                            $html.='</table>';
                            $gran_total=$gran_total+$montototal;
                        }
                    }
                    $html.='<table border="0" class="table_report">
                            <tr>
                                <td width="40%"></td></td><td width="40%" colspan="3" class="texe"><b>GRAN TOTAL</b></td><td>$ '.number_format($gran_total,2,'.',',').'</td>
                            </tr>
                        </table>';
                    $html.='<table border="0" class="table_report"><tr><td class="sborderbottom "><b>DETALLES DE ARTICULOS</b></td></tr></table>';
                    $resul_productos = $this->obtenciondeproductosventa($ar_idsucursal,$fecha_arqueo);
                    $html.='<table border="0" class="table_report"><thead><tr><th>Código Articulo</th><th>Descripción</th><th>Cantidad</th><th>Monto</th><th>NombreGrupo</th></tr></thead><tbody>';
                        foreach ($resul_productos->result() as $itempr) {
                            if($itempr->tipo_prod==1) { $catego=$itempr->categoria; $idProducto=$itempr->idProducto; $nombre=$itempr->nombre." SERIE: ".$itempr->serie; }
                            if($itempr->tipo_prod==2) { $catego=$itempr->categoria; $idProducto=$itempr->idProducto; $nombre=$itempr->nombre." LOTE: ".$itempr->lote; }
                            if($itempr->tipo_prod==0) { $catego="OXÍGENO"; $idProducto=$itempr->codigo; $nombre="Recarga de cilindro de oxígeno ".$itempr->codigo." de ".$itempr->capacidad." L"; }
                            $html.='<tr><td>'.$idProducto.'</td><td>'.$nombre.'</td><td>'.$itempr->cantidad.'</td><td>$ '.number_format($itempr->precio_unitario,2,'.',',').'</td><td>'.$catego.'</td></tr>';
                        }
                    $html.='</tbody></table>';
            $html.='';
        }
        if($view==0){
            echo $html;
        }else{
            return $html;
        }
        
    }
    public function formaspagosvgroupby(){
        $strq = "SELECT vfp.formapago,vfp.banco,ffp.formapago_alias,IFNULL(ban.name_ban,'')as name_ban
                FROM venta_erp_formaspagos as vfp
                INNER JOIN f_formapago as ffp on ffp.clave=vfp.formapago
                LEFT JOIN bancos as ban on ban.id=vfp.banco
                GROUP BY vfp.formapago,vfp.banco";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function optenciondepagosventas($suc,$fecha,$fomp,$ban){
        $strq = "SELECT 'venta' as tipo,clif.razon_social,v.folio,vfp.monto,vfp.formapago,vfp.banco
                FROM venta_erp as v
                INNER JOIN cliente_fiscales as clif on clif.id=v.id_razonsocial
                INNER JOIN venta_erp_formaspagos as vfp on vfp.idventa=v.id
                WHERE v.activo=1 AND v.sucursal='$suc' AND v.reg>='$fecha 00:00:00' AND v.reg<='$fecha 23:59:59' AND vfp.formapago='$fomp' AND vfp.banco='$ban'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function obtenciondeproductosventa($suc,$fecha){
        $strq="SELECT pro.idProducto,pro.nombre,vd.cantidad,vd.precio_unitario,cat.categoria, IFNULL(pro.tipo,0) as tipo_prod, vd.tipo, 
            IFNULL(r.codigo,'') as codigo, IFNULL(r.capacidad,0) as capacidad, 
            IFNULL(psl.lote,'') as lote, IFNULL(pss.serie,'') as serie
            FROM venta_erp as v
            INNER JOIN venta_erp_detalle AS vd on vd.idventa=v.id
            LEFT JOIN productos as pro on pro.id=vd.idproducto and vd.tipo=0
            LEFT JOIN recargas AS r ON r.id=vd.idproducto and vd.tipo=1
            LEFT JOIN productos_sucursales_serie AS pss ON pss.id=vd.id_ps_serie and pss.activo=1 
            LEFT JOIN productos_sucursales_lote AS psl ON psl.id=vd.id_ps_lote and psl.activo=1
            LEFT JOIN categoria as cat on cat.categoriaId=pro.categoria
            WHERE v.activo=1 AND vd.activo=1 AND v.sucursal='$suc' AND v.reg>='$fecha 00:00:00' AND v.reg<='$fecha 23:59:59'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function get_usuarios()
    {
         $strq="SELECT p.personalId,p.nombre,u.Usuario,p.puesto,p.fechabaja,p.motivo,per.nombre as perfil,s.name_suc,u.acceso,u.motivo_desactivar
            FROM personal AS p
            INNER JOIN usuarios AS u ON u.personalId=p.personalId
            INNER JOIN perfiles AS per ON per.perfilId=u.perfilId
            INNER JOIN sucursal AS s ON s.id=u.sucursal
            WHERE p.estatus=1 AND p.personalId!=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

     public function get_clientes()
    {
         $strq="SELECT p.id,p.nombre,p.usuario,p.motivo,p.desactivar,p.saldo,s.clave
            FROM clientes AS p
            INNER JOIN sucursal AS s ON s.id=p.sucursal
            INNER JOIN cliente_fiscales AS cl ON cl.idcliente=p.id
            WHERE p.activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    function obtenersaldoencaja($suc,$fecha){
        /*
        if($per>0){
            $where_per=" and t.idpersonal='$per' ";
        }else{
            $where_per="";
        }
        $strq = "SELECT t.monto,t.fecha, usu.sucursal 
                FROM turnos as t 
                INNER JOIN personal as per on per.personalId=t.idpersonal
                INNER JOIN usuarios as usu on usu.personalId=per.personalId
                WHERE usu.sucursal='$suc'  $where_per AND t.fecha='$fecha';";
        */
                //$strq="SELECT * FROM `turnos` WHERE sucursal='$suc' AND fecha='$fecha' and estatus=1"
                $strq="SELECT * FROM `turnos` WHERE sucursal='$suc' AND fecha='$fecha' ";
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->monto>0){
                $monto=$item->monto;
            }
        }

        return $monto;
    }
    function listacotizaciones($suc,$fechaactual){
        $strq="SELECT *,DATE_ADD(reg, INTERVAL 7 DAY) as finvigencia
                FROM `cotizacion` 
                WHERE sucursal='$suc' AND preventa=0 AND activo=1 AND '$fechaactual' < DATE_ADD(reg, INTERVAL 7 DAY)"; 
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenermontosdevoluciones($suc,$fecha){
        $strq="SELECT SUM(monto) as total FROM bitacora_devoluciones WHERE idsucursal='$suc' AND fecha='$fecha'";
        //log_message('error',$strq);
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->total>0){
                $monto=$item->total;
            }
            // code...
        }
        return $monto;
    }
    function obtenermontoscredito($suc,$fecha){
        $strq="SELECT sum(total) as total FROM venta_erp as v 
                WHERE v.tipo_venta=1 AND v.activo=1 AND sucursal='$suc' AND reg BETWEEN '$fecha 00:00:00' AND '$fecha 23:59:59'";
        $query = $this->db->query($strq);
        $monto=0;
        foreach ($query->result() as $item) {
            if($item->total>0){
                $monto=$item->total;
            }
            // code...
        }
        return $monto;
    }
    function obtenerdatossucursalfacturaventa($idfactura){
        $strq="SELECT s.* FROM venta_fk_factura as vf 
            INNER JOIN venta_erp as v on v.id=vf.idventa
            INNER JOIN sucursal as s on s.id=v.sucursal
            WHERE vf.idfactura='$idfactura'";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenersucursalesfactura(){
        $strq="SELECT * FROM sucursal WHERE id=4 OR id=5 or id=3 or id=2";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenerultimosdigitosventa($idfactura){
        $strq="SELECT vfm.* 
        FROM venta_erp_formaspagos as vfm
        INNER JOIN venta_fk_factura as vfac on vfac.idventa=vfm.idventa
        WHERE vfm.ultimosdigitos!='' AND vfac.idfactura='$idfactura'";
        $query = $this->db->query($strq);
        return $query;
    }
    function obtenermontostotalespormes($cliente,$anio){
        $strq="SELECT 
                me.idmes,
                me.mesname,
                SUM(vra.monto) as monto
            FROM meses as me
            LEFT JOIN ventas_reales_anio as vra on vra.idcliente='$cliente' AND vra.anio='$anio' AND vra.mes=me.idmes
            GROUP BY me.idmes;";
        $query = $this->db->query($strq);
        return $query;
    }
    function v_proyeccion($idser,$anio){
        $strq="SELECT vpv.anio,vpvd.* 
                FROM ventashistoricas_proyeccion_venta as vpv
                INNER JOIN ventashistoricas_proyeccion_venta_detalles as vpvd on vpvd.idventashistoricas_proyecion_venta=vpv.id
                WHERE vpv.activo=1 and vpvd.activo=1 and vpv.idventashistoricas='$idser' AND vpv.anio='$anio'";
        $query = $this->db->query($strq);
        return $query;
    }
    function sum_proyeccion($idser,$anio){
        $strq="SELECT anio,mes,SUM(valor) as valor 
                FROM costosventas_proyectados 
                WHERE anio='$anio' AND idpro>0 and activo=1 AND idser ='$idser'
                GROUP BY mes";
        $query = $this->db->query($strq);
        return $query;
    }
    


}