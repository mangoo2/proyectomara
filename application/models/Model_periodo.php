<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_periodo extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_list($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.anio',
            2=>'p.mes',
            3=>'p.nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('periodo p');
        if($params['anio']!=0){
        	if($params['mes']!=0){
                $where = array('p.activo'=>1,'p.anio'=>$params['anio'],'p.mes'=>$params['mes']);
            }else{
                $where = array('p.activo'=>1,'p.anio'=>$params['anio']);
            }
        }else{
        	if($params['mes']!=0){
                $where = array('p.activo'=>1,'p.mes'=>$params['mes']);
            }else{
                $where = array('p.activo'=>1);	
            }
        }
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $columns = array( 
            0=>'p.id',
            1=>'p.anio',
            2=>'p.mes',
            3=>'p.nombre'
        );
        $this->db->select('COUNT(*) as total');
        $this->db->from('periodo p');
        
        if($params['anio']!=0){
        	if($params['mes']!=0){
                $where = array('p.activo'=>1,'p.anio'=>$params['anio'],'p.mes'=>$params['mes']);
            }else{
                $where = array('p.activo'=>1,'p.anio'=>$params['anio']);
            }
        }else{
        	if($params['mes']!=0){
                $where = array('p.activo'=>1,'p.mes'=>$params['mes']);
            }else{
                $where = array('p.activo'=>1);	
            }
        }
        
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

}