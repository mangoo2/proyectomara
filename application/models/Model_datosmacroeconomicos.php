<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_datosmacroeconomicos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    

    function get_macroeconomicos($idcliente,$orden,$tipo,$anio){
        $strq = "SELECT * 
            FROM datosmacroeconomicos
            WHERE idcliente=$idcliente AND orden=$orden AND tipo=$tipo AND anio=$anio"; 
        $query = $this->db->query($strq);
        return $query->row();
    }

}
