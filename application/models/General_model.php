<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function insert_batch($Tabla,$data){
        $this->db->insert_batch($Tabla, $data);
    }
    
    public function get_select($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    public function edit_recordw($where,$data,$table){
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);
    }

    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }

    function getselectwhererow($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->row();
    }

    function getselect_tabla($table){
        $this->db->select('*');
        $this->db->from($table);
        $query=$this->db->get(); 
        return $query;
    }

    public function delete_record($table,$cos,$id){
        $this->db->where($cos,$id);
        return $this->db->delete($table);
    }
}