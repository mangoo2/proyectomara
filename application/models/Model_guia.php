<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_guia extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_total_competidores($id){
        $strq="SELECT COUNT(*) AS total FROM  guia_competidor WHERE idcliente=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_total_oportunidades($id){
        $strq="SELECT COUNT(*) AS total FROM  foda_oportunidad WHERE activo=1 AND tipo=1 AND idcliente=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_total_fortaleza($id){
        $strq="SELECT COUNT(*) AS total FROM  foda_fortaleza WHERE activo=1 AND tipo=1 AND idcliente=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
}