<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_cliente extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    
    public function getDatosCPEstado_gruop($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado,ep.mnpio");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $this->db->group_by("ep.codigo");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDatosCPEstado($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $query=$this->db->get();
        return $query->result();
    }

    function get_list($params){
        $columns = array( 
            0=>'c.id',
            1=>'c.nombre',
            2=>'c.ap_paterno',
            3=>'c.ap_materno',
            4=>'c.email',
            5=>'c.telefono',
            6=>'c.celular',
            7=>'c.razon_social',
            8=>'c.giro',
            9=>'u.Usuario',
            10=>'p.nombre'
        );

        $columnsx = array( 
            0=>'c.id',
            1=>'c.nombre',
            2=>'c.ap_paterno',
            3=>'c.ap_materno',
            4=>'c.email',
            5=>'c.telefono',
            6=>'c.celular',
            7=>'c.razon_social',
            8=>'c.giro',
            9=>'u.Usuario',
            10=>'c.suspender',
            11=>'(SELECT DATE_FORMAT(h.reg, "%d/%m/%Y - %r" ) AS reg FROM historial_session AS h
                where h.UsuarioID=u.UsuarioID
                ORDER BY h.histoId DESC LIMIT 1) AS reg',
            12=>'p.nombre AS curso'    
        );


        $select="";
        foreach ($columnsx as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes c');
        $this->db->join('usuarios u','u.idcliente=c.id','left');
        $this->db->join('periodo p','p.id=c.idperiodo','left');
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if($params['coml1']!=''){
            $this->db->like('c.nombre',$params['coml1']);
        } 
        if($params['coml2']!=0){
            $where = array('c.idperiodo'=>$params['coml2']);
            $this->db->where($where);
        } 
        if($params['coml9']!=2){
            $where = array('c.suspender'=>$params['coml9']);
            $this->db->where($where);
        } 
        if($params['coml3']!=''){
            $this->db->like('c.email',$params['coml3']);
        }
        if($params['coml4']!=''){
            $this->db->like('c.telefono',$params['coml4']);
        }
        if($params['coml5']!=''){
            $this->db->like('c.celular',$params['coml5']);
        }
        if($params['coml6']!=''){
            $this->db->like('c.razon_social',$params['coml6']);
        }
        if($params['coml7']!=''){
            $this->db->like('c.giro',$params['coml7']);
        }
        if($params['coml8']!=''){
            $this->db->like('u.Usuario',$params['coml8']);
        }

   /*     if(isset($params['coml2'])){
            if($params['coml2']!=''){
                $where = array('c.nombre'=>$params['coml2']);
                $this->db->where($where);
            } 
        }*/
        
/*        

coml3
coml4
coml5
coml6
coml7
coml8
*/

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_list($params){
        $columns = array( 
            0=>'c.id',
            1=>'c.nombre',
            2=>'c.ap_paterno',
            3=>'c.ap_materno',
            4=>'c.email',
            5=>'c.telefono',
            6=>'c.celular',
            7=>'c.razon_social',
            8=>'c.giro',
            9=>'u.Usuario',
            10=>'p.nombre'
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('clientes c');
        $this->db->join('usuarios u','u.idcliente=c.id','left');
        $this->db->join('periodo p','p.id=c.idperiodo','left');
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_menu(){
        $strq="SELECT m.Nombre,mc.idmenu
            FROM menu_cliente AS mc
            LEFT JOIN menu AS m ON m.MenuId=mc.idmenu"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_submenu($id,$idr){
        if($idr==0){
            $strq="SELECT ms.MenusubId,ms.Nombre,0 AS mnp,m.Nombre AS menu
            FROM menu_sub AS ms  
             LEFT JOIN menu AS m ON m.MenuId=ms.MenuId
            WHERE ms.MenuId=$id AND ms.visible=1 AND ms.submenutipo=0";  
        }else{
            $strq="SELECT ms.MenusubId,ms.Nombre,pd.MenusubId AS mnp,m.Nombre AS menu
            FROM menu_sub AS ms
            LEFT JOIN menu AS m ON m.MenuId=ms.MenuId
            LEFT JOIN perfiles_detalles AS pd ON pd.MenusubId=ms.MenusubId AND pd.idcliente=$idr
            WHERE ms.MenuId=$id AND ms.visible=1 AND ms.submenutipo=0"; 
        }
        
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_submenu_permiso($id){
        $strq="SELECT ms.MenusubId,ms.Nombre,m.Nombre AS menu
            FROM  perfiles_detalles AS pd
            INNER JOIN menu_sub AS ms ON ms.MenusubId=pd.MenusubId
             LEFT JOIN menu AS m ON m.MenuId=ms.MenuId
            WHERE pd.idcliente=$id AND ms.visible=1 AND ms.submenutipo=0"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

}