<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class Model_guia_seleccion extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_total_mercados(){
        $strq="SELECT COUNT(*) AS total FROM  guia_seleccion_preguntas_tipo WHERE activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_total_factores(){
        $strq="SELECT COUNT(*) AS total FROM  guia_seleccion_fortalezas_preguntas_tipo WHERE activo=1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    /// resumen
    public function get_total_seleccion_fortalezas($orden){
        $strq="SELECT COUNT(*) AS total FROM  guia_seleccion_fortalezas_preguntas_detalles WHERE resumen=1 AND orden=$orden"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_total_seleccion_mercado($orden){
        $strq="SELECT COUNT(*) AS total FROM  guia_seleccion_preguntas_detalles WHERE resumen=1 AND orden=$orden"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_total_seleccion_resumen_estado($orden){
        $strq="SELECT COUNT(*) AS total FROM  guia_seleccion_fortalezas_estado_resultados_detalles WHERE resumen=1 AND orden=$orden"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_mercadoresultados_total($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_mercadoresultados($idcliente){
        $sql = "SELECT  g.titulo,psp.idpregunta
                FROM guia_seleccion_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_mercadoresultados_detalles($idcliente,$idpregunta){
        $sql = "SELECT  psp.pregunta
                FROM guia_seleccion_preguntas_respuesta AS gs
                LEFT JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente AND psp.idpregunta=$idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //////////////////////////////////////
    public function get_internosultados_total($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_internosultados($idcliente){
        $sql = "SELECT  g.titulo,psp.idpregunta
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_internosultados_detalles($idcliente,$idpregunta){
        $sql = "SELECT  psp.pregunta
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente AND psp.idpregunta=$idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }
    /////////////
    public function get_pri_mercadoresultados_total($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_mercadoresultados($idcliente){
        $sql = "SELECT  g.titulo,psp.idpregunta
                FROM guia_seleccion_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_mercadoresultados_detalles($idcliente,$idpregunta){
        $sql = "SELECT  psp.pregunta,psp.id
                FROM guia_seleccion_preguntas_respuesta AS gs
                LEFT JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles 
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente AND psp.idpregunta=$idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }
    //////////////////////////////////////
    public function get_pri_internosultados_total($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados($idcliente){
        $sql = "SELECT  g.titulo,psp.idpregunta
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados_detalles($idcliente,$idpregunta){
        $sql = "SELECT  psp.pregunta,psp.id
                FROM guia_seleccion_fortalezas_preguntas_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                WHERE gs.resp1=2 AND gs.idcliente=$idcliente AND psp.idpregunta=$idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    //////////////////////////////////////
    public function get_pri_estadosultados_total($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles
                INNER JOIN guia_seleccion_fortalezas_estado_resultados AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_estadosultados($idcliente){
        $sql = "SELECT  g.titulo,psp.idpregunta
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles
                INNER JOIN guia_seleccion_fortalezas_estado_resultados AS g ON g.id=psp.idpregunta 
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente GROUP BY psp.idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_estadosultados_detalles($idcliente,$idpregunta){
        $sql = "SELECT  psp.pregunta,psp.id
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta AS gs
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles
                WHERE gs.resp1=1 AND gs.idcliente=$idcliente AND psp.idpregunta=$idpregunta";
        $query = $this->db->query($sql);
        return $query->result();
    }

    ///////////////////////////////////
    public function get_pri_mercadoresultados_total_alta($idcliente,$tipo){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_preguntas_respuesta_detalles AS gs
                INNER JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_mercadoresultados_detalles_alta($idcliente,$tipo){
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_preguntas_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro ON cro.idguia_seleccion_preguntas_respuesta_detalles=gs.id AND cro.tipo=1 
                LEFT JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles 
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_mercadoresultados_detalles_alta_cronograma($id){
        $sql = "SELECT cro.id,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro
                WHERE cro.tipo=0 AND cro.activo=1 AND cro.idguia_seleccion_preguntas_respuesta_detalles=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados_total_alta($idcliente,$tipo){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles 
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados_detalles_alta($idcliente,$tipo){
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles=gs.id AND cro.tipo=1 
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados_detalles_alta_cronograma($id){
        $sql = "SELECT cro.id,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro
                WHERE cro.tipo=0 AND cro.activo=1 AND cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_estadosultados_total_alta($idcliente,$tipo){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles 
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_estadosultados_detalles_alta($idcliente,$tipo){
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles=gs.id AND cro.tipo=1 
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles
                INNER JOIN guia_seleccion_fortalezas_estado_resultados AS g ON g.id=psp.idpregunta 
                WHERE gs.resultado=$tipo AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_estadosultados_detalles_alta_cronograma($id){
        $sql = "SELECT cro.id,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro
                WHERE cro.tipo=0 AND cro.activo=1 AND cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_cronograma_debilidad_fortaleza($idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM cronograma_debilidad_fortaleza
                WHERE activo=1 AND idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_pri_mercadoresultados_actividad($mes,$anio,$idcliente){
        $sql = "SELECT COUNT(*) AS total
                FROM guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_preguntas_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_preguntas_respuesta_detalles
                LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma_actividad AS act ON act.idguia_seleccion_preguntas_respuesta_detalles_cronograma=cro.id AND act.activo=1 AND act.mes=$mes
                WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_pri_internoesultados_actividad($mes,$anio,$idcliente){
        $sql = "SELECT COUNT(*) AS total
                FROM guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles
                LEFT JOIN guia_seleccion_fortalezas_ps_re_detalles_cronograma_actividad AS act ON act.idguia_seleccion_fortalezas_ps_re_detalles_cronograma=cro.id AND act.activo=1 AND act.mes=$mes
                WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_pri_estadoesultados_actividad($mes,$anio,$idcliente){
        $sql = "SELECT  COUNT(*) AS total
                FROM guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles
                LEFT JOIN guia_seleccion_fortalezas_estado_r_r_d_cron_act AS act ON act.idguia_seleccion_fortalezas_estado_r_r_detalles_cronograma=cro.id AND act.activo=1 AND act.mes=$mes
                WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_cronograma_debilidad_fortaleza_actividad($mes,$anio,$idcliente){
        $sql = "SELECT COUNT(*) AS total
                FROM cronograma_debilidad_fortaleza As cro
                LEFT JOIN cronograma_debilidad_fortaleza_actividad AS act ON act.idcronograma_debilidad_fortaleza=cro.id AND act.activo=1 AND act.mes=$mes
                WHERE cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_cronograma_debilidad_fortaleza_detalle_actividad($mes,$anio,$idcliente){
        $sql = "SELECT COUNT(*) AS total
                FROM cronograma_debilidad_fortaleza_detalle AS cro
                INNER JOIN cronograma_debilidad_fortaleza AS gs ON gs.id = cro.idcronograma_debilidad_fortaleza
                LEFT JOIN cronograma_debilidad_fortaleza_detalle_actividad AS act ON act.idcronograma_debilidad_fortaleza_detalle=cro.id AND act.activo=1 AND act.mes=$mes
                WHERE cro.activo=1 AND gs.activo=1 AND  gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function get_pri_mercadoresultados_actividad($idcliente){
        $sql = "SELECT cro.id,cro.preg2,act.mes,act.semana1,act.semana2,act.semana3,act.semana4,act.semana5,gs.resultado,cro.preg4,cro.preg5
                FROM guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_preguntas_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_preguntas_respuesta_detalles
                LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma_actividad AS act ON act.idguia_seleccion_preguntas_respuesta_detalles_cronograma=cro.id AND act.activo=1
                WHERE cro.activo=1 AND gs.idcliente=$idcliente GROUP BY cro.id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internoesultados_actividad($idcliente){
        $sql = "SELECT cro.id,cro.preg2,act.mes,act.semana1,act.semana2,act.semana3,act.semana4,act.semana5,gs.resultado,cro.preg4,cro.preg5
                FROM guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles
                LEFT JOIN guia_seleccion_fortalezas_ps_re_detalles_cronograma_actividad AS act ON act.idguia_seleccion_fortalezas_ps_re_detalles_cronograma=cro.id AND act.activo=1
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function get_pri_estadoesultados_actividad($idcliente){
        $sql = "SELECT cro.id,cro.preg2,act.mes,act.semana1,act.semana2,act.semana3,act.semana4,act.semana5,gs.resultado,cro.preg4,cro.preg5
                FROM guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs ON gs.id = cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles
                LEFT JOIN guia_seleccion_fortalezas_estado_r_r_d_cron_act AS act ON act.idguia_seleccion_fortalezas_estado_r_r_detalles_cronograma=cro.id AND act.activo=1
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_cronograma_debilidad_fortaleza_actividad($idcliente){
        $sql = "SELECT cro.id,cro.preg2,act.mes,act.semana1,act.semana2,act.semana3,act.semana4,act.semana5,cro.prioridad,cro.preg4,cro.preg5
                FROM cronograma_debilidad_fortaleza As cro
                LEFT JOIN cronograma_debilidad_fortaleza_actividad AS act ON act.idcronograma_debilidad_fortaleza=cro.id AND act.activo=1
                WHERE cro.activo=1 AND cro.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function get_cronograma_debilidad_fortaleza_detalle_actividad($idcliente){
        $sql = "SELECT cro.id,cro.preg2,act.mes,act.semana1,act.semana2,act.semana3,act.semana4,act.semana5,gs.prioridad,cro.preg4,cro.preg5
                FROM cronograma_debilidad_fortaleza_detalle AS cro
                INNER JOIN cronograma_debilidad_fortaleza AS gs ON gs.id = cro.idcronograma_debilidad_fortaleza
                LEFT JOIN cronograma_debilidad_fortaleza_detalle_actividad AS act ON act.idcronograma_debilidad_fortaleza_detalle=cro.id AND act.activo=1
                WHERE cro.activo=1 AND gs.activo=1 AND  gs.idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function get_pri_mercadoresultados_detalles_anio_mes($idcliente){

        /*$sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_preguntas_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro ON cro.idguia_seleccion_preguntas_respuesta_detalles=gs.id
                LEFT JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles 
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";*/
        /*if($mes<10){
            $mes='0'.$mes;  
        }*/
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_preguntas_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro ON cro.idguia_seleccion_preguntas_respuesta_detalles=gs.id
                LEFT JOIN guia_seleccion_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_preguntas_detalles 
                INNER JOIN guia_seleccion_preguntas AS g ON g.id=psp.idpregunta 
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";
        /*
                AND cro.preg4<=if(MONTH(cro.preg4)=$mes AND YEAR(cro.preg4)=$anio,cro.preg4,'$anio-$mes-01')
                AND cro.preg5>=if(MONTH(cro.preg5)=$mes AND YEAR(cro.preg5)=$anio,cro.preg5,'$anio-$mes-30')        */

        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_pri_internosultados_detalles_anio_mes($idcliente){
        /*if($mes<10){
            $mes='0'.$mes;  
        }*/
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles=gs.id 
                INNER JOIN guia_seleccion_fortalezas_preguntas_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_preguntas_detalles
                INNER JOIN guia_seleccion_fortalezas_preguntas AS g ON g.id=psp.idpregunta
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";

                /*AND cro.preg4<=if(MONTH(cro.preg4)=$mes AND YEAR(cro.preg4)=$anio,cro.preg4,'$anio-$mes-01')
                AND cro.preg5>=if(MONTH(cro.preg5)=$mes AND YEAR(cro.preg5)=$anio,cro.preg5,'$anio-$mes-30')*/
                /*WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio*/
        $query = $this->db->query($sql);
        return $query->result();
    }

     public function get_pri_estadosultados_detalles_anio_mes($idcliente){
        /*if($mes<10){
            $mes='0'.$mes;  
        }*/
        $sql = "SELECT  psp.pregunta,gs.id,g.titulo,gs.anio,gs.semestre,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs
                LEFT JOIN guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles=gs.id
                INNER JOIN guia_seleccion_fortalezas_estado_resultados_detalles AS psp ON psp.id=gs.idguia_seleccion_fortalezas_estado_resultados_detalles
                INNER JOIN guia_seleccion_fortalezas_estado_resultados AS g ON g.id=psp.idpregunta 
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";

                 /*
                AND cro.preg4<=if(MONTH(cro.preg4)=$mes AND YEAR(cro.preg4)=$anio,cro.preg4,'$anio-$mes-01')
                AND cro.preg5>=if(MONTH(cro.preg5)=$mes AND YEAR(cro.preg5)=$anio,cro.preg5,'$anio-$mes-30')*/
                /*
                WHERE cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";*/
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    //
    public function get_cronograma_debilidad_fortaleza_anio_mes($idcliente){
        // if($mes<10){
        //     $mes='0'.$mes;  
        // }
        $sql = "SELECT cro.debilidad_cambiar,cro.factor_manejar,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM cronograma_debilidad_fortaleza As cro
                WHERE cro.activo=1 AND cro.idcliente=$idcliente";

                /*AND cro.preg4<=if(MONTH(cro.preg4)=$mes AND YEAR(cro.preg4)=$anio,cro.preg4,'$anio-$mes-01')
                AND cro.preg5>=if(MONTH(cro.preg5)=$mes AND YEAR(cro.preg5)=$anio,cro.preg5,'$anio-$mes-30')*/
                /*
                WHERE cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND cro.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";*/
        $query = $this->db->query($sql);
        return $query->result();
    }


    public function get_cronograma_debilidad_fortaleza_detalle_anio_mes($idcliente){
        /*if($mes<10){
            $mes='0'.$mes;  
        }*/
        $sql = "SELECT  gs.debilidad_cambiar,gs.factor_manejar,cro.preg1,cro.preg2,cro.preg3,cro.preg4,cro.preg5,cro.preg6
                FROM cronograma_debilidad_fortaleza_detalle AS cro
                INNER JOIN cronograma_debilidad_fortaleza AS gs ON gs.id = cro.idcronograma_debilidad_fortaleza
                LEFT JOIN cronograma_debilidad_fortaleza_detalle_actividad AS act ON act.idcronograma_debilidad_fortaleza_detalle=cro.id AND act.activo=1 
                WHERE cro.activo=1 AND gs.idcliente=$idcliente";
                // AND act.mes=$mes 
                /*AND cro.preg4<=if(MONTH(cro.preg4)=$mes AND YEAR(cro.preg4)=$anio,cro.preg4,'$anio-$mes-01')
                AND cro.preg5>=if(MONTH(cro.preg5)=$mes AND YEAR(cro.preg5)=$anio,cro.preg5,'$anio-$mes-30')*/
                /*
                WHERE cro.activo=1 AND gs.activo=1 AND  gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio OR cro.activo=1 AND gs.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg4) <=$mes  AND YEAR(cro.preg4) <= $anio AND MONTH(cro.preg4) >=$mes  AND YEAR(cro.preg4) >= $anio OR cro.activo=1 AND gs.activo=1 AND gs.idcliente=$idcliente AND MONTH(cro.preg5) <=$mes  AND YEAR(cro.preg5) <= $anio AND MONTH(cro.preg5) >=$mes  AND YEAR(cro.preg5) >= $anio";*/
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_total_debilidad_fortaleza($idcliente){
        $sql = "SELECT COUNT(*) AS total
                FROM cronograma_debilidad_fortaleza
                WHERE activo=1 AND idcliente=$idcliente";
        $query = $this->db->query($sql);
        return $query->result();
    }

   /* public function get_cronograma_debilidad_fortaleza_resumen_anio($idcliente){
        $sql = "SELECT id, preg4, preg5,preg6,tipo FROM (
                    SELECT  cro.id,cro.preg4,cro.preg5,cro.preg6, '1' AS tipo
                    FROM guia_seleccion_preguntas_respuesta_detalles AS gs
                    LEFT JOIN guia_seleccion_preguntas_respuesta_detalles_cronograma AS cro ON cro.idguia_seleccion_preguntas_respuesta_detalles=gs.id
                    WHERE cro.activo=1 AND gs.idcliente=$idcliente
                    GROUP BY YEAR(cro.preg5)
                    UNION
                    SELECT  cro.id,cro.preg4,cro.preg5,cro.preg6, '2' AS tipo
                    FROM guia_seleccion_fortalezas_preguntas_respuesta_detalles AS gs
                    LEFT JOIN guia_seleccion_fortalezas_ps_re_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_preguntas_respuesta_detalles=gs.id
                    WHERE cro.activo=1 AND gs.idcliente=$idcliente
                    GROUP BY YEAR(cro.preg5)
                    UNION
                    SELECT  cro.id,cro.preg4,cro.preg5,cro.preg6, '3' AS tipo
                    FROM guia_seleccion_fortalezas_estado_resultados_respuesta_detalles AS gs
                    LEFT JOIN guia_seleccion_fortalezas_estado_r_r_detalles_cronograma AS cro ON cro.idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles=gs.id
                    WHERE cro.activo=1 AND gs.idcliente=$idcliente
                    GROUP BY YEAR(cro.preg5)
                    UNION
                    SELECT  cro.id,cro.preg4,cro.preg5,cro.preg6, '4' AS tipo
                    FROM cronograma_debilidad_fortaleza AS cro
                    WHERE cro.activo=1 AND cro.idcliente=$idcliente
                    GROUP BY YEAR(cro.preg5)
                    UNION
                    SELECT  cro.id,cro.preg4,cro.preg5,cro.preg6, '5' AS tipo
                    FROM cronograma_debilidad_fortaleza AS gs
                    LEFT JOIN cronograma_debilidad_fortaleza_detalle AS cro ON cro.idcronograma_debilidad_fortaleza=gs.id
                    WHERE cro.activo=1 AND gs.idcliente=$idcliente
                    GROUP BY YEAR(cro.preg5)
                   ) a
                GROUP BY YEAR(preg5)
                ";
        $query = $this->db->query($sql);
        return $query->result();
    }*/
    
}
    