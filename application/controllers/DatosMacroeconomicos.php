<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DatosMacroeconomicos extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_datosmacroeconomicos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,8);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=6;
        $data['btn_active_sub']=8;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('macroeconomicos/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('macroeconomicos/indexjs');
    }

    function get_datos_macroeconomicos()
    {   
        $anio = $this->input->post('anio');
        $m_1_1=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,1,1,$anio);
        $m_1_2=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,1,2,$anio);
        $m_1_3=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,1,3,$anio);
        $m_1_4=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,1,4,$anio);
        
        $m_2_1=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,2,1,$anio);
        $m_2_2=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,2,2,$anio);
        $m_2_3=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,2,3,$anio);
        $m_2_4=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,2,4,$anio);

        $m_3_1=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,3,1,$anio);
        $m_3_2=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,3,2,$anio);
        $m_3_3=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,3,3,$anio);
        $m_3_4=$this->Model_datosmacroeconomicos->get_macroeconomicos($this->idcliente,3,4,$anio);
        $anio2 = $anio+1;
        $anio3 = $anio2+1;
        $anio4 = $anio3+1;
        $html='<div class="row">
              <div class="col-sm-12">
                <table class="table table-bordered table_info" id="tabla_datos_1">
                  <thead>
                    <tr>
                      <th rowspan="2"></th>
                      <th rowspan="2" class="th_c" style="text-align-last: center;">Año '.$anio.'</th>
                      <th rowspan="2" class="th_c" style="text-align-last: center;">Proyección '.$anio2.'</th>
                      <th colspan="2" class="th_c" style="text-align-last: center;">Fuentes de información</th>
                    </tr>
                    <tr>
                      <th class="th_c" style="text-align-last: center;">Fuente</th>
                      <th class="th_c" style="text-align-last: center;">Página Web</th>
                    </tr>
                    <tr class="tr_1">
                      <th class="th_c">Inflación 
                        <input type="hidden" id="id_x" value="'; if(isset($m_1_1->id)){ $html.=$m_1_1->id; }else{ $html.=0; } $html.='" />
                        <input type="hidden" id="orden_x" value="1" />
                        <input type="hidden" id="tipo_x" value="1" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio2.'" />
                      </th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_anio_x" value="'; if(isset($m_1_1->concepto_anio)){ $html.=$m_1_1->concepto_anio; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_1_1->concepto_proyeccion)){ $html.=$m_1_1->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_1_1->fuente)){ $html.=$m_1_1->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_1_1->pagina)){ $html.=$m_1_1->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_1">
                      <th class="th_c">PIB 
                        <input type="hidden" id="id_x" value="'; if(isset($m_1_2->id)){ $html.=$m_1_2->id; }else{ $html.=0; } $html.='" />
                        <input type="hidden" id="orden_x" value="1" />
                        <input type="hidden" id="tipo_x" value="2" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio2.'" />
                      </th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_anio_x" value="'; if(isset($m_1_2->concepto_anio)){ $html.=$m_1_2->concepto_anio; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_1_2->concepto_proyeccion)){ $html.=$m_1_2->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_1_2->fuente)){ $html.=$m_1_2->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_1_2->pagina)){ $html.=$m_1_2->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_1">
                      <th class="th_c">Tasa Bancaria 
                        <input type="hidden" id="id_x" value="'; if(isset($m_1_3->id)){ $html.=$m_1_3->id; }else{ $html.=0; } $html.='" />
                        <input type="hidden" id="orden_x" value="1" />
                        <input type="hidden" id="tipo_x" value="3" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio2.'" />
                      </th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_anio_x" value="'; if(isset($m_1_3->concepto_anio)){ $html.=$m_1_3->concepto_anio; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_1_3->concepto_proyeccion)){ $html.=$m_1_3->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_1_3->fuente)){ $html.=$m_1_3->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_1_3->pagina)){ $html.=$m_1_3->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_1">
                      <th class="th_c">Leasing 
                        <input type="hidden" id="id_x" value="'; if(isset($m_1_4->id)){ $html.=$m_1_4->id; }else{ $html.=0; } $html.='" />
                        <input type="hidden" id="orden_x" value="1" />
                        <input type="hidden" id="tipo_x" value="4" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio2.'" />
                      </th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_anio_x" value="'; if(isset($m_1_4->concepto_anio)){ $html.=$m_1_4->concepto_anio; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_1_4->concepto_proyeccion)){ $html.=$m_1_4->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_1_4->fuente)){ $html.=$m_1_4->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_1_4->pagina)){ $html.=$m_1_4->pagina; } $html.='"  /></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <br>
                <table class="table table-bordered table_info" id="tabla_datos_2">
                  <thead>
                    <tr>
                      <th rowspan="2"></th>
                      <th rowspan="2" class="th_c" style="text-align-last: center;">Proyección '.$anio3.'</th>
                      <th colspan="2" class="th_c" style="text-align-last: center;">Fuentes de información</th>
                    </tr>
                    <tr>
                      <th class="th_c" style="text-align-last: center;">Fuente</th>
                      <th class="th_c" style="text-align-last: center;">Página Web</th>
                    </tr>
                    <tr class="tr_2">
                      <th class="th_c">Inflación 
                        <input type="hidden" id="id_x" value="'; if(isset($m_2_1->id)){ $html.=$m_2_1->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="2" />
                        <input type="hidden" id="tipo_x" value="1" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio3.'" />
                      </th>
                      <th class="td_input"> <input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_2_1->concepto_anio)){ $html.=$m_2_1->concepto_anio; } $html.='"  /> <input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_2_1->concepto_proyeccion)){ $html.=$m_2_1->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_2_1->fuente)){ $html.=$m_2_1->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_2_1->pagina)){ $html.=$m_2_1->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_2">
                      <th class="th_c">PIB 
                        <input type="hidden" id="id_x" value="'; if(isset($m_2_2->id)){ $html.=$m_2_2->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="2" />
                        <input type="hidden" id="tipo_x" value="2" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio3.'" />
                      </th>
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_2_2->concepto_anio)){ $html.=$m_2_2->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_2_2->concepto_proyeccion)){ $html.=$m_2_2->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_2_2->fuente)){ $html.=$m_2_2->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_2_2->pagina)){ $html.=$m_2_2->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_2">
                      <th class="th_c">Tasa Bancaria 
                        <input type="hidden" id="id_x" value="'; if(isset($m_2_3->id)){ $html.=$m_2_3->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="2" />
                        <input type="hidden" id="tipo_x" value="3" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio3.'" />
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_2_3->concepto_anio)){ $html.=$m_2_3->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_2_3->concepto_proyeccion)){ $html.=$m_2_3->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_2_3->fuente)){ $html.=$m_2_3->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_2_3->pagina)){ $html.=$m_2_3->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_2">
                      <th class="th_c">Leasing 
                        <input type="hidden" id="id_x" value="'; if(isset($m_2_4->id)){ $html.=$m_2_4->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="2" />
                        <input type="hidden" id="tipo_x" value="4" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio3.'" />
                      </th>
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_2_4->concepto_anio)){ $html.=$m_2_4->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_2_4->concepto_proyeccion)){ $html.=$m_2_4->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_2_4->fuente)){ $html.=$m_2_4->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_2_4->pagina)){ $html.=$m_2_4->pagina; } $html.='"  /></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <br>
                <table class="table table-bordered table_info" id="tabla_datos_3">
                  <thead>
                    <tr>
                      <th rowspan="2"></th>
                      <th rowspan="2" class="th_c" style="text-align-last: center;">Proyección '.$anio4.'</th>
                      <th colspan="2" class="th_c" style="text-align-last: center;">Fuentes de información</th>
                    </tr>
                    <tr>
                      <th class="th_c" style="text-align-last: center;">Fuente</th>
                      <th class="th_c" style="text-align-last: center;">Página Web</th>
                    </tr>
                    <tr class="tr_3">
                      <th class="th_c">Inflación 
                        <input type="hidden" id="id_x" value="'; if(isset($m_3_1->id)){ $html.=$m_3_1->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="3" />
                        <input type="hidden" id="tipo_x" value="1" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio4.'" />
                      </th>
                      <th class="td_input"> <input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_3_1->concepto_anio)){ $html.=$m_3_1->concepto_anio; } $html.='"  /> <input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_3_1->concepto_proyeccion)){ $html.=$m_3_1->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_3_1->fuente)){ $html.=$m_3_1->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_3_1->pagina)){ $html.=$m_3_1->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_3">
                      <th class="th_c">PIB 
                        <input type="hidden" id="id_x" value="'; if(isset($m_3_2->id)){ $html.=$m_3_2->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="3" />
                        <input type="hidden" id="tipo_x" value="2" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio4.'" />
                      </th>
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_3_2->concepto_anio)){ $html.=$m_3_2->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_3_2->concepto_proyeccion)){ $html.=$m_3_2->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_3_2->fuente)){ $html.=$m_3_2->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_3_2->pagina)){ $html.=$m_3_2->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_3">
                      <th class="th_c">Tasa Bancaria 
                        <input type="hidden" id="id_x" value="'; if(isset($m_3_3->id)){ $html.=$m_3_3->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="3" />
                        <input type="hidden" id="tipo_x" value="3" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio4.'" />
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_3_3->concepto_anio)){ $html.=$m_3_3->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_3_3->concepto_proyeccion)){ $html.=$m_3_3->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_3_3->fuente)){ $html.=$m_3_3->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_3_3->pagina)){ $html.=$m_3_3->pagina; } $html.='"  /></th>
                    </tr>
                    <tr class="tr_3">
                      <th class="th_c">Leasing 
                        <input type="hidden" id="id_x" value="'; if(isset($m_3_4->id)){ $html.=$m_3_4->id; } $html.='" />
                        <input type="hidden" id="orden_x" value="3" />
                        <input type="hidden" id="tipo_x" value="4" />
                        <input type="hidden" id="anio_x" value="'.$anio.'" />
                        <input type="hidden" id="proyeccion_x" value="'.$anio4.'" />
                      </th>
                      <th class="td_input"><input type="hidden" class="form-control" id="concepto_anio_x" value="'; if(isset($m_3_4->concepto_anio)){ $html.=$m_3_4->concepto_anio; } $html.='"  / class="td_input"><input type="text" class="form-control" id="concepto_proyeccion_x" value="'; if(isset($m_3_4->concepto_proyeccion)){ $html.=$m_3_4->concepto_proyeccion; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="fuente_x" value="'; if(isset($m_3_4->fuente)){ $html.=$m_3_4->fuente; } $html.='"  /></th>
                      <th class="td_input"><input type="text" class="form-control" id="pagina_x" value="'; if(isset($m_3_4->pagina)){ $html.=$m_3_4->pagina; } $html.='"  /></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>';
        echo $html;
    }

    function registro_datos_macroeconomicos(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idcliente']=$this->idcliente;
            $data['orden']=$DATA[$i]->orden; 
            $data['tipo']=$DATA[$i]->tipo;
            $data['anio']=$DATA[$i]->anio;
            $data['proyeccion']=$DATA[$i]->proyeccion;
            $data['concepto_anio']=$DATA[$i]->concepto_anio;
            $data['concepto_proyeccion']=$DATA[$i]->concepto_proyeccion;
            $data['fuente']=$DATA[$i]->fuente;
            $data['pagina']=$DATA[$i]->pagina;
            if($DATA[$i]->id==0){
                $id=$this->General_model->add_record('datosmacroeconomicos',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'datosmacroeconomicos');
            }
            
        }
    }

}