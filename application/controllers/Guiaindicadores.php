<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guiaindicadores extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_guia_seleccion');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,23);// perfil y id del submenu
            if ($permiso==0) {
                //redirect('Login');
            }
        }else{
            //redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=2;
        $data['btn_active_sub']=23;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('guiaindicadores/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('guiaindicadores/indexjs');
    }

    public function add_data(){
        $data=$this->input->post();  
        $id=$data['id'];      
        $data['reg']=$this->fechahora;
        $data['idpersonal']=$this->idpersonal; 
        if($id==0){
            $id=$this->General_model->add_record('guiaindicadores',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'guiaindicadores');
        }
        echo $id;
    }


    public function editar_factor()
    {   
        $id=$this->input->post('id');
        if($id==0){
            // Datos generales
            $id = 0;
            $factor='';
        }else{
            $resul=$this->General_model->getselectwhere('guiaindicadores','id',$id);
            foreach ($resul as $item) {
                $id=$item->id;
                $factor=$item->factor;
            }
        }
        $html='<div class="row">
                <div class="col-md-4">
                  <input type="hidden" name="id" id="idfactor" value="'.$id.'">
                  <div class="form-floating form-floating-outline">
                    <input type="text" class="form-control" id="factor" name="factor" value="'.$factor.'" placeholder="" required  />
                    <label for="factor">Factor</label>
                  </div>
                </div>
                <div class="col-md-4">';
                    if($id==0){
                        $html.='<button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Guardar</button>';
                    }else{
                        $html.='<button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro">Editar</button>';
                    }
                $html.='</div>  
              </div>  ';

        echo $html;
    }

    public function get_texto_factor_estrategias()
    {
        $html='<div class="accordion" id="collapsibleSection">';
          $get_factor=$this->General_model->get_select('guiaindicadores',array('activo'=>1));
          foreach ($get_factor->result() as $x){
            $html.='<div class="card accordion-item">
            <h2 class="accordion-header" id="item_'.$x->id.'">
              <button
                class="accordion-button collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#item_x'.$x->id.'"
                aria-expanded="false"
                aria-controls="item_x'.$x->id.'"
                onclick="get_estrategias('.$x->id.')"
                >
                '.$x->factor.'
              </button>
            </h2>
            <div
              id="item_x'.$x->id.'"
              class="accordion-collapse collapse"
              aria-labelledby="item_'.$x->id.'"
              data-bs-parent="#collapsibleSection">
              <div class="accordion-body">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-4" style="text-align: -webkit-right;">
                      <h5>'.$x->factor.'</h5>
                    </div>
                    <div class="col-md-2" style="text-align: -webkit-right;">
                      <button type="button" class="btn btn-icon btn-outline-primary" onclick="text_factor('.$x->id.')">
                        <span class="tf-icons mdi mdi-pencil"></span>
                      </button>
                      <button type="button" class="btn btn-icon btn-outline-secondary" onclick="eliminar_registro_factor('.$x->id.')">
                        <span class="tf-icons mdi mdi-delete-empty"></span>
                      </button>
                    </div>  
                </div>
                <div class="text_estrategia_form_'.$x->id.'">
                    <div class="row">
                        <div class="col-md-4">
                          <div class="form-floating form-floating-outline">
                            <input type="hidden" id="id_estraegia_'.$x->id.'" value="'.$x->id.'">
                            <input type="hidden" id="id_registro_'.$x->id.'" value="0">
                            <input type="text" class="form-control" id="estrategia_'.$x->id.'" placeholder="" required  />
                            <label for="factor">Estrategia</label>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro_'.$x->id.'" onclick="guardar_estrategia('.$x->id.')">Guardar</button>
                        </div>  
                    </div>  
                </div>    
                <div class="text_estrategia_tabla_'.$x->id.'">
                </div>
              </div>
            </div>
          </div>';
          }
        $html.='</div>';
        echo $html;
    }

    public function delete_record_factor(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'guiaindicadores');
    }

    public function add_data_estrategias(){
        $idguiaindicadores=$this->input->post('idguiaindicadores');
        $id=$this->input->post('id');  
        $estrategia=$this->input->post('estrategia');
        $data['idguiaindicadores']=$idguiaindicadores;
        $data['estrategia']=$estrategia;      
        if($id==0){
            $id=$this->General_model->add_record('guiaindicadores_estrategias',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'guiaindicadores_estrategias');
        }
        echo $id;
    }

    public function get_estrategias_all()
    {   
        $id=$this->input->post('id');  
        $html='<div class="row">
                        <div class="table-responsive text-nowrap"><br>
                            <table class="table">
                              <thead>
                                <tr>
                                  <th style="color: #bd9250;">Estrategias</th>
                                  <th style="color: #bd9250;">Acciones</th>
                                </tr>
                              </thead>
                              <tbody class="table-border-bottom-0">';
                                $get_factor=$this->General_model->get_select('guiaindicadores_estrategias',array('activo'=>1,'idguiaindicadores'=>$id));
                                foreach ($get_factor->result() as $x){
                                    $html.='<tr class="row_t_'.$x->id.'">
                                       <td>'.$x->estrategia.'</td>
                                       <td><button type="button" class="btn btn-icon btn-outline-primary" onclick="text_factor_estrategia('.$x->id.','.$id.')">
                                            <span class="tf-icons mdi mdi-pencil"></span>
                                          </button>
                                          <button type="button" class="btn btn-icon btn-outline-secondary" onclick="eliminar_registro_factor_estrategia('.$x->id.','.$id.')">
                                            <span class="tf-icons mdi mdi-delete-empty"></span>
                                          </button></td>
                                    <tr>';
                                }
                              $html.='</tbody>
                            </table>
                        </div>
                    </div>';
        echo $html;
    }

    public function editar_estrategias()
    {   
        $idguiaindicadores=$this->input->post('idguiaindicadores');
        $id=$this->input->post('id');
        if($id==0){
            $id = 0;
            $estrategia='';
        }else{
            $resul=$this->General_model->getselectwhere('guiaindicadores_estrategias','id',$id);
            foreach ($resul as $item) {
                $id=$item->id;
                $estrategia=$item->estrategia;
            }
        }
        $html='<div class="row">
            <div class="col-md-4">
              <div class="form-floating form-floating-outline">
                <input type="hidden" id="id_registro_'.$idguiaindicadores.'" value="'.$id.'">
                <input type="text" class="form-control" id="estrategia_'.$idguiaindicadores.'" placeholder="" value="'.$estrategia.'"  />
                <label for="factor">Estrategia</label>
              </div>
            </div>
            <div class="col-md-4">';
            if($id==0){
                $html.='<button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro_'.$idguiaindicadores.'" onclick="guardar_estrategia('.$idguiaindicadores.')">Guardar</button>';
            }else{
                $html.='<button type="submit" class="btn btn-primary me-sm-3 me-1 btn_registro_'.$idguiaindicadores.'" onclick="guardar_estrategia('.$idguiaindicadores.')">Editar</button>';
            }
            $html.='</div>  
        </div> ';
        echo $html;
    }

    public function delete_record_factor_estrategia(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'guiaindicadores_estrategias');
    }
}

