<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronograma_actividades extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_guia');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,24);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=7;
        $data['btn_active_sub']=24;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cronograma_actividades/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('cronograma_actividades/indexjs');
    }

    function registro($id=0)
    {   
        $data['btn_active']=7;
        $data['btn_active_sub']=24;
        if($id==0){ 
            $data['id']=0;
            $data['idguiaindicadores']=0;
            $data['idguiaindicadores_estrategias']=0;
        }else{ 
            $result=$this->General_model->get_select('cronograma_aterrizaje',array('id'=>$id,'idcliente'=>$this->idcliente));
            foreach ($result->result() as $item) {
                $data['id']=$item->id;
                $data['idguiaindicadores']=$item->idguiaindicadores;
                $data['idguiaindicadores_estrategias']=$item->idguiaindicadores_estrategias;
            }
        }
        $data['get_guiaindicadores']=$this->General_model->get_select('guiaindicadores',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cronograma_actividades/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('cronograma_actividades/formjs');
    }

    public function get_estrategias__all()
    {   
        $id=$this->input->post('id');
        $idguiaindicadores_estrategias=$this->input->post('idguiaindicadores_estrategias');
        $html='<div class="form-floating form-floating-outline mb-4">
                <select class="form-select" id="idestrategias" name="idguiaindicadores_estrategias" onchange="text_select_pan_trabajo()">
                  <option selected>Selecciona una opción</option>';
                    $get_estrategias=$this->General_model->get_select('guiaindicadores_estrategias',array('activo'=>1,'idguiaindicadores'=>$id));
                    foreach ($get_estrategias->result() as $x){
                      $html.='<option value="'.$x->id.'"'; if($x->id==$idguiaindicadores_estrategias){$html.='selected';}  $html.='>'.$x->estrategia.'</option>';
                    }
                $html.='</select>
                <label for="cfdi">Debilidad a cambiar / Fortaleza a mejorar</label>
              </div>';
        echo $html;
    }

    public function add_data(){
        $data=$this->input->post();
        $id=$data['id'];
        if($id==0){
            $data['idcliente']=$this->idcliente;
            $id=$this->General_model->add_record('cronograma_aterrizaje',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'cronograma_aterrizaje');
        }
        echo $id;
    }
    
}