<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('General_model');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Model_guia_seleccion');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
        }
        date_default_timezone_set("America/Mexico_City");
        $this->mes_act=date('n');
    }
    public function index(){
        $data['btn_active']=1;
        $data['btn_active_sub']=1;
        $data['mes']=$this->mes_act;
        if($this->idcliente==0){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar',$data);
            $this->load->view('inicio');
            $this->load->view('templates/footer');
            $this->load->view('iniciojs');
        }else{
      
            $v1=0;$v2=0;$v3=0;$v4=0;$v5=0;
            $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente);
            foreach ($result_merc as $x){                    
                $v1=1;
            }
            $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente);
            foreach ($result_inter as $int){
                $v2=1;
            }
            $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente);
            foreach ($result_est as $est){
                $v3=1;
            }
            $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente);
            foreach ($result_cron as $xn){
                $v4=1;
            }
            $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente);
            foreach ($result_cronn as $xnn){  
                $v5=1;
            }
            if($v1==1 || $v2==1 || $v3==1 || $v4==1 || $v5==1){
                header("Location: ".base_url()."Analisisfoda?con=3");    
            }else{
                header("Location: ".base_url()."Guia_detectar_oportunidades?tipo=1");    
            }
            
        }
    }

    function searchproductos(){
        $pro = $this->input->get('search');
        $results=$this->ModeloCatalogos->getsearchproductoslike($pro);
        //echo $results;
        echo json_encode($results->result());
    }

    function get_tabla_verificador()
    {    
        $codigo=$this->input->post('codigo');
        $tipo=$this->input->post('tipo');
        if($tipo==1){
            $get_productos=$this->General_model->getselectwhere('productos','codigoBarras',$codigo);
        }else{
            $get_productos=$this->General_model->getselectwhere('productos','id',$codigo);
        }
        $html='<table class="table table-sm" id="table_datos">
                    <thead>
                        <tr style="background: #009bdb;">
                            <th scope="col" style="color: white !important;  font-size: 15px;"></th>
                            <th scope="col" style="color: white; font-size: 15px;">Nombre/Descripción</th>
                            <th scope="col" style="color: white; font-size: 15px;">Stock sucursal</th>
                            <th scope="col" style="color: white; font-size: 15px;">Precio</th>
                        </tr>
                    </thead>
                    <tbody>';
                        foreach($get_productos as $x){
                            $result=$this->General_model->get_productos_files($x->id);
                            $img=''; 
                            foreach ($result as $z){
                                $img='<img style="width: 100px" src="'.base_url().'uploads/productos/'.$z->file.'"';    
                            }
                            $html.='<tr>
                                <td>'.$img.'</td>
                                <td>'.$x->nombre.'</td>
                                <td>'.$x->stock.'</td>
                                <td>'.$x->precio_con_iva.'</td>
                            </tr>';
                        }
                    $html.='</tbody>
                </table>';
        echo $html;
    }
    
    function get_alertas()
    {
        $result=$this->ModeloCatalogos->get_alertas_traspasos();
        $html='';
        foreach ($result as $x){
            if($x->tipo==1){
                $html.='<li class="noti-primary">
                    <div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/notifi.png"></span>
                      <div class="media-body">
                        <p>Solicitud de traspasos</p>
                      </div>
                    </div>
                    <div>
                      <h5><b style="color: #019cde;">Usuario:</b><b>'.$x->nombre.'</b></h5>
                      <h5><b style="color: #019cde;">Sucursal destino:</b><b>'.$x->name_sucx.'</b></h5>
                      <h5><b style="color: #019cde;">Sucursal origen:</b><b>'.$x->name_sucy.'</b></h5>
                      <a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Contrasena?valor='.$x->id.'">Generar contraseña</a>
                    </div>
                  </li>';
            }else{
                $html.='<li class="noti-primary">
                    <div class="media"><span class="notification-bg bg-light-primary"><img style="width: 50px;" src="'.base_url().'public/img/notifi2.png"></span>
                      <div class="media-body">
                        <p>Solicitud de descuento</p>
                      </div>
                    </div>
                    <div>
                      <h5><b style="color: #019cde;">Usuario:</b><b>'.$x->nombre.'</b></h5>
                      <h5><b style="color: #019cde;">Sucursal:</b><b>'.$x->name_sucy.'</b></h5>
                      <a type="button" class="btn realizarventa btn-sm" style="width: 100%;" href="'.base_url().'Contrasena?valor='.$x->id.'">Generar contraseña</a>
                    </div>
                  </li>';
            }
        }
        echo $html;
    }

    function get_alertas_total()
    {
        $result=$this->ModeloCatalogos->get_alertas_total();
        $total=0;
        foreach ($result as $x){
            $total=$x->total;
        }
        echo $total;
    }

    function add_cantidad_sucursal()
    {
        $get_productos=$this->General_model->get_table('productos');
        foreach ($get_productos as $x){
            $idproductos=$x->id;
            $get_sucursal=$this->General_model->get_table('sucursal');
            foreach ($get_sucursal as $s){
                $productos_sucursales=$this->General_model->getselectwhereall('productos_sucursales',array('productoid'=>$idproductos,'sucursalid'=>$s->id));
                $validar=0;
                foreach ($productos_sucursales as $ps){
                    $validar=1;
                }
                if($validar==0){
                    $data['productoid']=$idproductos;
                    $data['sucursalid']=$s->id;                    
                    $data['stock']=0;
                    $data['stockmin']=0;
                    $data['stockmax']=0;
                    $data['precio']=0;
                    $data['incluye_iva']=0;
                    $data['ubicacion']='';
                    $data['desc_monto']=0;
                    $data['desc_tipo_descuento']=0;
                    $data['iva']=0;
                    $this->General_model->add_record('productos_sucursales',$data);
                }
            }
        }
    }
}