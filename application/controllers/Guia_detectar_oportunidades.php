<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guia_detectar_oportunidades extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_guia');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,21);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=7;
        $data['btn_active_sub']=21;
        $data['btn_active_sub_m']=30;
        $data['get_periodo']=$this->General_model->get_select('periodo',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('guia_detectar/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('guia_detectar/indexjs');
    }

    function get_oportunidades()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_oportunidad',array('idcliente'=>$this->idcliente,'activo'=>1));
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-12">
                  <div class="form-floating form-floating-outline mb-4">
                    <textarea type="text" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                  </div>
                </div> 
            </div>';
        }

        echo $html;
    }

    function get_amenazas()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_amenazas',array('idcliente'=>$this->idcliente,'activo'=>1));
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-12">
                  <div class="form-floating form-floating-outline mb-4">
                    <textarea type="text" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                    <label for="nombre">Concepto</label>
                  </div>
                </div> 
            </div>';
        }

        echo $html;
    }

    function get_fortalezas()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_fortaleza',array('idcliente'=>$this->idcliente,'activo'=>1));
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-12">
                  <div class="form-floating form-floating-outline mb-4">
                    <textarea type="text" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                    <label for="nombre">Concepto</label>
                  </div>
                </div>
            </div>';
        }

        echo $html;
    }

    function get_debilidades()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_debilidades',array('idcliente'=>$this->idcliente,'activo'=>1));
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-12">
                  <div class="form-floating form-floating-outline mb-4">
                    <textarea type="text" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                    <label for="nombre">Concepto</label>
                  </div>
                </div>

            </div>';
        }

        echo $html;
    }

    public function add_competidor(){
        $nombre=$this->input->post('nombre');;
        $data['nombre']=$nombre;
        $data['idcliente']=$this->idcliente;
        $id=$this->General_model->add_record('guia_competidor',$data);
        echo $id;
    }
    

    function get_tabla_guia()
    {   
        $result=$this->General_model->getselectwheren('guia_competidor',array('idcliente'=>$this->idcliente,'activo'=>1));
        $resultx=$this->Model_guia->get_total_competidores($this->idcliente);
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }
        $html='<div class="clonetr"><table class="table" id="clonetr_data4"></table></div><div class="table-responsive text-nowrap tabla_guia_com">
            <table class="table table-bordered table_info" id="tabla_datos"  style="white-space: normal !important;">
              <tbody>';
              $html.='<tr>
                  <th colspan="'.$total_r.'">Guía de los colores: <span style="color:red">El rojo son amenazas para mi</span> / <span style="color:green">El verde son oportunidades para mi</span></th>
                </tr>
                <tr class="encabezado" id="encabezado" style="vertical-align: text-top;">  
                  <th style="width:30%"><div style="color: white; font-size: 22px; text-transform: capitalize;">Competidores</div></th>';
                  foreach ($result->result() as $x){
                    $html.='<th class="td_input row_c_'.$x->id.'" data-competidor="'.$x->nombre.'" style="width:11%"><div style="width: max-content;">
                    <textarea rows="1" class="form-control js-auto-size" id="nombrec'.$x->id.'" oninput="editar_competidor('.$x->id.')" style="height: 40px !important;">'.$x->nombre.'</textarea></div>
                          </th>';
                  }

                  /*<a class="btn_cafe" onclick="edit_competidor('.$x->id.')">
                            <span class="tf-icons mdi mdi-pencil"></span>
                          </a>
                   <a class="btn_gris" onclick="eliminar_competidor('.$x->id.')">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                           </a>*/
                $html.='</tr>';
                $result_p=$this->General_model->getselectwheren('guia_preguntas',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td style="text-align: center;" colspan="8"><div class="div-linea"></div></td>
                    </tr>';
                    $html.='<tr style="width:45%">
                      <td ><div class="th_c" style="width: max-content;">'.$p->titulo.'</div></td>
                      <td class="" colspan="'.$total_r.'"><br><span style="color:#272726">-</span></td>
                    </tr>';
                    $result_p=$this->General_model->getselectwheren('guia_preguntas_detalles',array('idpregunta'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;
                        $html.='<tr style="vertical-align: text-top;">
                            <td >'.$pd->pregunta.'</td>';
                            foreach ($result->result() as $x){
                                $html.='<td class="td_input">';
                                    if($tipo==1){
                                        $pr1=$x->pre_1;
                                        $pr2=$x->pre_2;
                                        $pr5=$x->pre_5;
                                        $pr6=$x->pre_6;
                                        $pr7=$x->pre_7;
                                        $pr8=$x->pre_8;
                                        $pr11=$x->pre_11;
                                        $pr14=$x->pre_14;
                                        $pr17=$x->pre_17;
                                        $pr19=$x->pre_19;
                                        $pr21=$x->pre_21;
                                        $pr24=$x->pre_24;
                                        $pr25=$x->pre_25;
                                        $pr26=$x->pre_26;
                                        $pr29=$x->pre_29;
                                        $pr31=$x->pre_31;
                                        $pr32=$x->pre_32;
                                        $pr33=$x->pre_33;
                                        $style_p1='';

                                        if($idpd==1){
                                            if($pr1=='1'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr1=='0'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==2){
                                            if($pr2=='0'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr2=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==5){
                                            if($pr5=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr5=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==6){ 
                                            if($pr6=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr6=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==7){ 
                                            if($pr7=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr7=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==8){ 
                                            if($pr8=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr8=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==11){ 
                                            if($pr11=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr11=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==14){ 
                                            if($pr14=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr14=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==17){ 
                                            if($pr17=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr17=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==19){ 
                                            if($pr19=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr19=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==21){ 
                                            if($pr21=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr21=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==24){ 
                                            if($pr24=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr24=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==25){ 
                                            if($pr25=='2'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else if($pr25=='1'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==26){ 
                                            if($pr26=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr26=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else if($pr26=='0'){
                                                $style_p='style="background: #f5bd00; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==29){ 
                                            if($pr29=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr29=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==31){ 
                                            if($pr31=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr31=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==32){ 
                                            if($pr32=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr32=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }else if($idpd==33){ 
                                            if($pr33=='2'){
                                                $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                            }else if($pr33=='1'){
                                                $style_p='style="background: #e31a1a; color:white; border-radius: 30px;"';
                                            }else{
                                                $style_p='style="border-radius: 30px;"';
                                            }
                                        }
                                        $html.='<select class="form-select" id="pre_'.$x->id.'_'.$idpd.'" onchange="pregunta_competidor('.$x->id.','.$idpd.')" '.$style_p.'><option value="-1">Selecciona una opción</option>';
                                        $result_pdo=$this->General_model->getselectwheren('guia_preguntas_detalles_opciones',array('idguia'=>$idpd,'activo'=>1));
                                        foreach ($result_pdo->result() as $pdo){
                                            $selected_p='';
                                            //var_dump($pr1.'.==//'.$pdo->valor);
                                            if($idpd==1){ if($pr1==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==2){ if($pr2==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==5){ if($pr5==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==6){ if($pr6==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==7){ if($pr7==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==8){ if($pr8==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==11){ if($pr11==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==14){ if($pr14==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==17){ if($pr17==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==19){ if($pr19==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==21){ if($pr21==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==24){ if($pr24==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==25){ if($pr25==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==26){ if($pr26==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==29){ if($pr29==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==31){ if($pr31==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==32){ if($pr32==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idpd==33){ if($pr33==$pdo->valor){ $selected_p='selected'; }
                                            }
                                                
                                            $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                        }
                                        $html.='</select>';
                                        
                                    }else{
                                        $pr3=$x->pre_3;
                                        $pr4=$x->pre_4;
                                        $pr9=$x->pre_9;
                                        $pr10=$x->pre_10;
                                        $pr12=$x->pre_12;
                                        $pr13=$x->pre_13;
                                        $pr15=$x->pre_15;
                                        $pr16=$x->pre_16;
                                        $pr18=$x->pre_18;
                                        $pr20=$x->pre_20;
                                        $pr22=$x->pre_22;
                                        $pr23=$x->pre_23;
                                        $pr27=$x->pre_27;
                                        $pr28=$x->pre_28;
                                        $pr29=$x->pre_29;
                                        $pr30=$x->pre_30;

                                        $respuesta_p='';
                                        if($idpd==3){ $respuesta_p=$pr3; 
                                        }else if($idpd==4){ $respuesta_p=$pr4; 
                                        }else if($idpd==9){ $respuesta_p=$pr9;
                                        }else if($idpd==10){ $respuesta_p=$pr10; 
                                        }else if($idpd==12){ $respuesta_p=$pr12;
                                        }else if($idpd==13){ $respuesta_p=$pr13;
                                        }else if($idpd==15){ $respuesta_p=$pr15;
                                        }else if($idpd==16){ $respuesta_p=$pr16;
                                        }else if($idpd==18){ $respuesta_p=$pr18;
                                        }else if($idpd==20){ $respuesta_p=$pr20;
                                        }else if($idpd==22){ $respuesta_p=$pr22;
                                        }else if($idpd==23){ $respuesta_p=$pr23;
                                        }else if($idpd==27){ $respuesta_p=$pr27;
                                        }else if($idpd==28){ $respuesta_p=$pr28;
                                        }else if($idpd==29){ $respuesta_p=$pr29;
                                        }else if($idpd==30){ $respuesta_p=$pr30;
                                        }
                                        //$html.='<input type="text" class="form-control" id="pre_'.$x->id.'_'.$idpd.'" value="'.$respuesta_p.'" oninput="pregunta_competidor_input('.$x->id.','.$idpd.')" />';
                                        $html.='<div style="width: max-content;"><textarea rows="1" class="form-control js-auto-size" id="pre_'.$x->id.'_'.$idpd.'" oninput="pregunta_competidor_input('.$x->id.','.$idpd.')">'.$respuesta_p.'</textarea></div>';
                                    }
                                $html.='</td>';
                            }
                        $html.='</tr>';
                    }
                }
              $html.='</tbody>
            </table>
            <br>';
            $result_p=$this->General_model->getselectwheren('guia_competidor_detalles',array('idcliente'=>$this->idcliente));
            $pre1='';
            $pre2='';
            $pre3='';
            $pre4='';
            foreach ($result_p->result() as $p){
                $pre1=$p->pre1;
                $pre2=$p->pre2;
                $pre3=$p->pre3;
                $pre4=$p->pre4;
            }
            $html.='<table class="table table-bordered table_info"  style="white-space: normal !important;">
              <thead>
                <tr style="vertical-align: text-top;">
                  <td style="width:30%"><div class="th_c">¿Qué factores que ofrece tu competencia se pueden eliminar, porque el mercado no los valora?</div></td>
                  <td style="width:70%"><textarea rows="1" id="pre_com_1" class="form-control js-auto-size" oninput="pregunta_compe_deta(1)" >'.$pre1.'</textarea></td>
                </tr>
                <tr>
                  <td><br></td>
                  <td><br></td>
                </tr>
                <tr style="vertical-align: text-top;">
                  <td style="width:30%"><div class="th_c">¿Qué factores que ofrece tu competencia se pueden reducir, porque el mercado no los valora?</div></td>
                  <td style="width:70%"><textarea rows="1" id="pre_com_2" class="form-control js-auto-size" oninput="pregunta_compe_deta(2)">'.$pre2.'</textarea></td>
                </tr>
                <tr>
                  <td><br></td>
                  <td><br></td>
                </tr>
                <tr style="vertical-align: text-top;">
                  <td style="width:30%"><div class="th_c">¿Qué factores se pueden crear (algo que nadie nunca haya ofrecido), porque harían una gran diferencia en el mercado? (identificalos, independientemente del precio o costo)</div></td>
                  <td style="width:70%"><textarea rows="1" id="pre_com_3" class="form-control js-auto-size" oninput="pregunta_compe_deta(3)">'.$pre3.'</textarea></td>
                </tr>
                <tr>
                  <td><br></td>
                  <td><br></td>
                </tr>
                <tr style="vertical-align: text-top;"> 
                  <td style="width:30%"><div class="th_c">¿Qué factores se pueden incrementar, porque el mercado los valora? (aunque nadie los ofrece)</div></td>
                  <td style="width:70%"><textarea rows="1" id="pre_com_4" class="form-control js-auto-size" oninput="pregunta_compe_deta(4)">'.$pre4.'</textarea></td>
                </tr>
              </thead>  
            </table>
            <br>
            <div style="padding-left: 20px; padding-right: 20px;">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="text-align:center">
                        <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(1)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Tus competidores</span></a>
                    </div>
                    <div class="col-md-3" style="text-align:center"> 
                        <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(2)"><span style="font-size: 20px;">Tu posición en el mercado</span></a>
                    </div>
                    <div class="col-md-3" style="text-align:center">
                         &nbsp; &nbsp;<a style="cursor:pointer" onclick="siguiente_competidores_ocultar(3)"><span style="font-size: 20px;">Tu entorno externo</span></a><!--&nbsp;&nbsp; <span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                    </div>
                    <div class="col-md-2" style="text-align: right;">
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_competidores_ocultar(2)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                    </div>
                </div>    
            </div>    
          </div>';
    
        echo $html;
    }

    public function delete_record_competidor(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'guia_competidor');
    }

    public function edit_data_competidor(){
        $nombre=$this->input->post('nombre');
        $id=$this->input->post('id');
        $datos['nombre']=$nombre;
        $this->General_model->edit_record('id',$id,$datos,'guia_competidor');
        echo $id;
    }

    public function edit_pregunta(){
        $id=$this->input->post('id');
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $data = array('pre_'.$idpregunta=>$respuesta);
        $this->General_model->edit_record('id',$id,$data,'guia_competidor');
    }

    function get_tabla_guia_fortaleza()
    {   
        $html='<div class="table-responsive text-nowrap">
            <table class="table table-bordered table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>
                <tr>';
                  /*<th colspan="3">Guía de los colores: <span style="color:red">El rojo son amenazas para mi</span> / <span style="color:green">El verde son oportunidades para mi</span></th>';
                    $html.='';*/
                $html.='</tr>
              </thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_preguntas_fortaleza',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td style="text-align: center;" colspan="4"><div class="div-linea"></div></td>
                    </tr>';
                    $html.='<tr>
                      <td><div class="th_c" style="width: max-content;">'.$p->titulo.'</div></td>
                      <td colspan="2"></td>
                    </tr>';
                    $result_p=$this->General_model->getselectwheren('guia_preguntas_fortaleza_detalles',array('idpregunta_fortaleza'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;

                        $result_f=$this->General_model->getselectwheren('guia_preguntas_fortaleza_cliente',array('    idguia_preguntas_fortaleza_detalle'=>$idpd,'idcliente'=>$this->idcliente));
                        $respuesta='';
                        foreach ($result_f->result() as $f){
                            $respuesta=$f->respuesta;
                        }
                        
                        $html.='<tr style="vertical-align: text-top;">
                            <td style="width: 600px;">'.$pd->pregunta.'</td>';
                            $html.='<td class="td_input">';
                                if($tipo==1){
                                    $style_p='';
                                    if($respuesta=='1'){
                                        $style_p='style="background: #70ad47; color:white; border-radius: 30px; width: min-content;"';
                                    }else if($respuesta=='0'){
                                        $style_p='style="background: #e31a1a; color:white; border-radius: 30px; width: min-content;"';
                                    }else{
                                        $style_p='style="color:white; border-radius: 30px; width: min-content;"';
                                    }
                                    $html.='<select class="form-select" id="pre_f_'.$idpd.'" onchange="pregunta_fortaleza('.$idpd.')" '.$style_p.'>
                                        <option value="-1">Selecciona una opción</option>';
                                    $result_pdo=$this->General_model->getselectwheren('guia_preguntas_fortaleza_detalles_opciones',array('idguia'=>$idpd,'activo'=>1));
                                    foreach ($result_pdo->result() as $pdo){
                                        $selected_p='';
                                        if($respuesta==$pdo->valor){ $selected_p='selected'; }
                                        $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                    }
                                    $html.='</select>';
                                    
                                }else{
                                    //$html.='<input type="text" class="form-control" id="pre_f_'.$idpd.'" value="'.$respuesta.'" oninput="pregunta_fortaleza_input('.$idpd.')" />';
                                    $html.='<textarea rows="1" style="width: min-content;" class="form-control js-auto-size" id="pre_f_'.$idpd.'" oninput="pregunta_fortaleza_input('.$idpd.')">'.$respuesta.'</textarea>';
                                }
                            $html.='</td>';
                        $html.='</tr>';
                    }
                }
              $html.='</tbody>
            </table>
          </div><br><br>
          <div style="padding-left: 20px; padding-right: 20px;">
                <div class="row">
                    <div class="col-md-1" style="text-align: right;">
                        <!--<span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                    </diV>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(1)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Apalancamiento interno</span></a>
                    </div>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(2)"><span style="font-size: 20px;">Factores internos</span></a>
                    </div>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(3)"><span style="font-size: 20px;">Tu entorno interno</span></a>
                    </div>
                    <div class="col-md-2" style="text-align: right;">
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_analisis_ocultar(2)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                    </div>
                </div>    
          </div>';

        echo $html;
    }

    public function edit_pregunta_fortaleza(){
        $id=$this->input->post('id');
        $respuesta=$this->input->post('respuesta');
        $data = array('idguia_preguntas_fortaleza_detalle'=>$id,'idcliente'=>$this->idcliente,'respuesta'=>$respuesta,);
        $result=$this->General_model->getselectwheren('guia_preguntas_fortaleza_cliente',array('    idguia_preguntas_fortaleza_detalle'=>$id,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $this->General_model->add_record('guia_preguntas_fortaleza_cliente',$data);
        }else{
            $this->General_model->edit_record('id',$id_rg,$data,'guia_preguntas_fortaleza_cliente');    
        }
        
    }

    function get_tabla_guia_cliente()
    {   
        /*
        $html.='<tr>
                  <th colspan="3">Guía de los colores: <span style="color:red">El rojo son amenazas para mi</span> / <span style="color:green">El verde son oportunidades para mi</span></th>';
                    
                $html.='</tr>';
        */
        $html='<div class="table-responsive text-nowrap">
            <table class="table table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>';
              $html.='</thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_preguntas_cliente',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td><div class="th_c" style="width: max-content;">'.$p->titulo.'</div></td>
                      <td></td>
                    </tr>';
                    $result_p=$this->General_model->getselectwheren('guia_preguntas_cliente_detalles',array('idpregunta_cliente'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;

                        $result_c=$this->General_model->getselectwheren('guia_preguntas_cliente_respuesta',array('    idguia_preguntas_cliente_detalle'=>$idpd,'idcliente'=>$this->idcliente));
                        $respuesta='';
                        foreach ($result_c->result() as $f){
                            $respuesta=$f->respuesta;
                        }
                        
                        $html.='<tr>
                            <td >'.$pd->pregunta.'</td>';
                            $html.='<td class="td_input">';
                                if($tipo==1){
                                    $style_p='';
                                    if($respuesta=='1'){
                                        $style_p='style="background: #70ad47; color: white; border-radius: 30px; width: auto;"';
                                    }else if($respuesta=='0'){
                                        $style_p='style="background: #e31a1a; color: white; border-radius: 30px; width: auto;"';
                                    }else{
                                        $style_p='style="color: white; border-radius: 30px; width: auto;"';
                                    }
                                    $html.='<select class="form-select" id="pre_c_'.$idpd.'" onchange="pregunta_cliente('.$idpd.')" '.$style_p.'>
                                        <option value="-1">Selecciona una opción</option>';
                                    $result_pdo=$this->General_model->getselectwheren('guia_preguntas_cliente_detalles_opciones',array('idguia'=>$idpd,'activo'=>1));
                                    foreach ($result_pdo->result() as $pdo){
                                        $selected_p='';
                                        if($respuesta==$pdo->valor){ $selected_p='selected'; }
                                        $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                    }
                                    $html.='</select>';
                                    
                                }else{
   
                                }
                            $html.='</td>';
                        $html.='</tr>';
                    }
                }
              $html.='</tbody>
            </table>
          </div>';
    
        echo $html;
    }

    public function edit_pregunta_cliente(){
        $id=$this->input->post('id');
        $respuesta=$this->input->post('respuesta');
        $data = array('idguia_preguntas_cliente_detalle'=>$id,'idcliente'=>$this->idcliente,'respuesta'=>$respuesta,);
        $result=$this->General_model->getselectwheren('guia_preguntas_cliente_respuesta',array('idguia_preguntas_cliente_detalle'=>$id,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $this->General_model->add_record('guia_preguntas_cliente_respuesta',$data);
        }else{
            $this->General_model->edit_record('id',$id_rg,$data,'guia_preguntas_cliente_respuesta');    
        }
        
    }

    function get_tabla_guia_cliente_instrucciones()
    {   
        $html='<div class="table-responsive text-nowrap">
            <table class="table table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>';
                /*$html.='<tr>
                  <th colspan="3">Instrucciones: Si  respondiste que "No" a alguna de las preguntas anteriores, esta sección te puede servir como base para generar los "insights" de tu cliente potencial</th>';
                    $html.='';
                $html.='</tr>';*/
              $html.='</thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_preguntas_cliente_detalles',array('idpregunta_cliente'=>0,'activo'=>1,'formulario'=>2));
                foreach ($result_p->result() as $pd){
                    $idpd=$pd->id;
                    $tipo=$pd->tipo;

                    $result_c=$this->General_model->getselectwheren('guia_preguntas_cliente_respuesta',array('idguia_preguntas_cliente_detalle'=>$idpd,'idcliente'=>$this->idcliente));
                    $respuesta='';
                    foreach ($result_c->result() as $f){
                        $respuesta=$f->respuesta;
                    }
                    
                    $html.='<tr>
                        <td style="width: 100%;"><div class="th_c" >'.$pd->pregunta.'</div><br>';
                            if($tipo==1){     
                            }else if($tipo==0){
                                $html.='<textarea rows="1" type="text" id="pre_cf_'.$idpd.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_cliente_formulario('.$idpd.')">'.$respuesta.'</textarea>';
                            }else if($tipo==3){
                                $t_vc='';
                                $t_v='';
                                $t_v2='';
                                $t_vc2='';
                                //var_dump($respuesta);
                                if(intval($respuesta)==1){
                                    //$t_v='checked=""';
                                    $t_vc='radioimgv';
                                    $t_v='style=""';
                                }else if(intval($respuesta)==2){
                                    $t_v2='style=""';
                                    $t_vc2='radioimgr';
                                }
                                $html.='<div class="row">
                                    <div class="col-md-5"></div>
                                    <div class="col-md-1">
                                        <div class="form-check form-check-success">
                                          <input name="pre_cf_'.$idpd.'" class="form-check-input radioimgr'.$idpd.' '.$t_vc.'" type="radio" onclick="pregunta_cliente_formulario_ckeck('.$idpd.',1)" value="1" id="pre_cf1_'.$idpd.'" '.$t_v.'>
                                          <label class="form-check-label" for="pre_cf1_'.$idpd.'">Si</label>
                                        </div>
                                    </diV>
                                    <div class="col-md-1">
                                        <div class="form-check form-check-danger">
                                          <input class="form-check-input radioimgr'.$idpd.' '.$t_vc2.'" type="radio" '.$t_v2.' name="pre_cf_'.$idpd.'" id="pre_cf2_'.$idpd.'" onclick="pregunta_cliente_formulario_ckeck('.$idpd.',2)" value="2" >
                                          <label class="form-check-label" for="pre_cf2_'.$idpd.'">No</label>
                                        </div>
                                    </div>
                                </div>';
                            }
                        $html.='</td>';
                    $html.='</tr>';
                }
              $html.='</tbody>
            </table>
          </div>';
    
        echo $html;
    }

    public function edit_compe_detalle(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $result=$this->General_model->getselectwheren('guia_competidor_detalles',array('idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idcliente'=>$this->idcliente,'pre'.$idpregunta=>$respuesta);
            $this->General_model->add_record('guia_competidor_detalles',$data);
        }else{
            $data = array('pre'.$idpregunta=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_competidor_detalles');    
        }   
    }

    function get_competidores()
    {
        $resultx=$this->Model_guia->get_total_competidores($this->idcliente);
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }
        $validar=0;
        if($total_r==5){
            $validar=0;
        }
        if($validar==0){
            for ($i = $total_r; $i <=4; $i++) {
                $data['idcliente']=$this->idcliente;
                $id=$this->General_model->add_record('guia_competidor',$data);
            }
        }  
    }


    function validar_oportunidades_existe()
    {
        $resultx=$this->Model_guia->get_total_oportunidades($this->idcliente);
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }

        if($total_r==0){
            $data['idcliente']=$this->idcliente;
            $data['tipo']=1;
            $id=$this->General_model->add_record('foda_oportunidad',$data);
        } 
    }

    function validar_fortaleza_existe()
    {
        $resultx=$this->Model_guia->get_total_fortaleza($this->idcliente);
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }

        if($total_r==0){
            $data['idcliente']=$this->idcliente;
            $data['tipo']=1;
            $id=$this->General_model->add_record('foda_fortaleza',$data);
        } 
    }

}