<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioCliente extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->anio = date('Y');
        $this->mes = date('m');
        $this->idcliente=$this->session->userdata('idcliente');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            /*$permiso=$this->Login_model->getviewpermiso($this->perfilid,6);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }*/
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=5;
        $data['btn_active_sub']=6;
        //$data['perfil']=$this->perfilid;    
        $data['cl']=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
        //var_dump($data['cl']);die;
        $resul=$this->General_model->getselectwhere('clientes','id',$this->idcliente);
        foreach ($resul as $item) {
            $data['id']=$item->id;
            $data['nombre']=$item->nombre;
            $data['ap_paterno']=$item->ap_paterno;
            $data['ap_materno']=$item->ap_materno;
            $data['email']=$item->email;
            $data['telefono']=$item->telefono;
            $data['celular']=$item->celular;
            $data['fecha_operacion']=$item->fecha_operacion;
            // datos empresa
            $data['tipo_persona']=$item->tipo_persona;
            $data['razon_social']=$item->razon_social;
            $data['rfc']=$item->rfc;
            $data['regimen_fiscal']=$item->regimen_fiscal;
            $data['cfdi']=$item->cfdi;
            $data['codigo_postal']=$item->codigo_postal;
            $data['calle_numero']=$item->calle_numero;
            $data['municipio']=$item->municipio;
            $data['colonia']=$item->colonia;
            $data['ciudad']=$item->ciudad;
            $data['estado']=$item->estado;  
            $data['giro']=$item->giro;  
            $data['idperiodo']=$item->idperiodo;  
            $data['foto']=$item->foto;
            // inicio de sesion
            $data['UsuarioID']=0;
            $data['usuario']='';
            $data['contrasena']='';
            $data['contrasena2']='';
            $resul=$this->General_model->getselectwhere('usuarios','idcliente',$this->idcliente);
            foreach ($resul as $item) {
                $data['UsuarioID']=$item->UsuarioID;   
                $data['usuario']=$item->Usuario;
                $data['contrasena']='x1f3[5]7w78{';     
                $data['contrasena2']='x1f3[5]7w78{'; 
            }
        }
        $data['uso_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $data['get_periodo']=$this->General_model->get_select('periodo',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('clienteusuario/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('clienteusuario/indexjs');
    }

    public function add_data(){
        $data=$this->input->post();
        $id=$this->idcliente;
        unset($data["usuario"]);
        unset($data["contrasena"]);
        unset($data["contrasena2"]);

        if(isset($data['tipo_persona'])){
            $data['tipo_persona']=$data['tipo_persona'];
        }else{
            $data['tipo_persona']=0;
        }
        
        $this->General_model->edit_record('id',$id,$data,'clientes');
        echo $id;
    }

    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        $validar=0;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($datos['contrasena']);
            $validar=0;
        }else{
            $datous['contrasena'] = $datos['contrasena'];
            $validar=1;
        }
        $id=$datos['UsuarioID'];
        $datous['Usuario'] = $datos['usuario'];
        $where = array('UsuarioID'=>$id);
        $this->General_model->edit_recordw($where,$datous,'usuarios');
        echo $validar;
    }


    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->Model_cliente->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->Model_cliente->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->Model_cliente->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="cliente";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('id',$id,$array,'clientes');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

}    