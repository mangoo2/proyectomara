<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_periodo');
         $this->load->model('Model_cliente');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,3);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        $data['get_periodo']=$this->General_model->get_select('periodo',array('activo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('cliente/indexjs');
    }

    public function registro($id=0){
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        if($id==0){
            // Datos generales
            $data['id'] = 0;
            $data['nombre']='';
            $data['ap_paterno']='';
            $data['ap_materno']='';
            $data['email']='';
            $data['telefono']='';
            $data['celular']='';
            $data['fecha_operacion']='';
            $data['suspender']=1;
            // Datos de empresa 
            $data['tipo_persona']='';
            $data['razon_social']='';
            $data['rfc']='';
            $data['regimen_fiscal']='';
            $data['cfdi']='';
            $data['codigo_postal']='';
            $data['calle_numero']='';
            $data['municipio'] = '';
            $data['colonia']='';
            $data['ciudad']='';
            $data['estado']='';
            $data['giro']='';
            // inicio sesion
            $data['UsuarioID']=0;
            $data['usuario']='';
            $data['contrasena']='';
            $data['contrasena2']='';
            $data['idperiodo']=0;
        }else{
            $resul=$this->General_model->getselectwhere('clientes','id',$id);
            foreach ($resul as $item) {
                $data['id']=$item->id;
                $data['nombre']=$item->nombre;
                $data['ap_paterno']=$item->ap_paterno;
                $data['ap_materno']=$item->ap_materno;
                $data['email']=$item->email;
                $data['telefono']=$item->telefono;
                $data['celular']=$item->celular;
                $data['fecha_operacion']=$item->fecha_operacion;
                $data['suspender']=$item->suspender;
                // datos empresa
                $data['tipo_persona']=$item->tipo_persona;
                $data['razon_social']=$item->razon_social;
                $data['rfc']=$item->rfc;
                $data['regimen_fiscal']=$item->regimen_fiscal;
                $data['cfdi']=$item->cfdi;
                $data['codigo_postal']=$item->codigo_postal;
                $data['calle_numero']=$item->calle_numero;
                $data['municipio']=$item->municipio;
                $data['colonia']=$item->colonia;
                $data['ciudad']=$item->ciudad;
                $data['estado']=$item->estado;  
                $data['giro']=$item->giro;  
                $data['idperiodo']=$item->idperiodo;  
                // inicio de sesion
                $data['UsuarioID']=0;
                $data['usuario']='';
                $data['contrasena']='';
                $data['contrasena2']='';
                $resul=$this->General_model->getselectwhere('usuarios','idcliente',$id);
                foreach ($resul as $item) {
                    $data['UsuarioID']=$item->UsuarioID;   
                    $data['usuario']=$item->Usuario;
                    $data['contrasena']='x1f3[5]7w78{';     
                    $data['contrasena2']='x1f3[5]7w78{'; 
                }
            }
        }   
        $data['uso_cfdi']=$this->General_model->get_select('f_uso_cfdi',array('activo'=>1));
        $data['get_periodo']=$this->General_model->get_select('periodo',array('activo'=>1));
        $data['get_menu']=$this->Model_cliente->get_menu();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('cliente/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('cliente/formjs');
    }
    
    public function add_data(){
        $data=$this->input->post();
        $id=$data['id'];
        if(isset($data['correo_enviar'])){
            $correo_enviar=1;
        }else{
            $correo_enviar=0;
        }
        unset($data["correo_enviar"]);
        unset($data["usuario"]);
        unset($data["contrasena"]);
        unset($data["contrasena2"]);
        if(isset($data['suspender'])){
            $data['suspender']=0;
        }else{
            $data['suspender']=1;
        }
        if(isset($data['tipo_persona'])){
            $data['tipo_persona']=$data['tipo_persona'];
        }else{
            $data['tipo_persona']=0;
        }
        
        $data['reg']=$this->fechahora;
        $data['idpersonal']=$this->idpersonal; 

        if($id==0){
            $id=$this->General_model->add_record('clientes',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'clientes');
        }
        
        echo $id;
    }

    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($datos['contrasena']);
        }else{
            $datous['contrasena'] = $datos['contrasena'];
            $datous['txtuser'] = $pss_verifica;
        }
        $id=$datos['UsuarioID'];
        $datous['idcliente'] = $datos['idcliente'];
        $datous['Usuario'] = $datos['usuario'];
        $datous['perfilId'] = 2;
        $datous['personalId']= $this->idpersonal;
        $datous['acceso']=$datos['suspender'];
        $datous['tipo']=0;
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datous,'usuarios');
        }else{
            $this->General_model->add_record('usuarios',$datous);
        }   
    
    }


    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->Model_cliente->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->Model_cliente->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->Model_cliente->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->Model_cliente->get_list($params);
        $totaldata= $this->Model_cliente->total_list($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'clientes');
        $this->General_model->edit_record('idcliente',$id,$data,'usuarios');
    }

    function get_submenu()
    {
        $id=$this->input->post('idmenu');
        $idreg=$this->input->post('idreg');
        $result=$this->Model_cliente->get_submenu($id,$idreg);
        $resultc=$this->Model_cliente->get_submenu_permiso($idreg);
        $html='<div class="row ">
            <div class="col-md-6 col-12 mb-md-0 mb-4">
                <p>Menú</p>
                <ul class="list-group list-group-flush" id="handle-list-1">';
                foreach ($result as $x) {
                    if($x->MenusubId==$x->mnp){
                    }else{
                        $html.='<li class="list-group-item lh-1 d-flex justify-content-between align-items-center li_p">
                          <span class="d-flex justify-content-between align-items-center">
                            <i class="drag-handle cursor-move mdi mdi-menu align-text-bottom me-2"></i>
                            <span>'.$x->Nombre.' / '.$x->menu.'</span>
                          </span>
                          <input type="hidden" id="idsubmenux" value="'.$x->MenusubId.'">
                        </li>';
                    }
                }
            $html.='</ul>
            </div>
            <div class="col-md-6 col-12 mb-md-0 mb-4 menu_txt_p">
                <p>Acceso al Menú</p>
                <ul class="list-group list-group-flush ul_p" id="handle-list-2">';
                if($idreg!=0){
                    foreach ($resultc as $c) {
                        $html.='<li class="list-group-item lh-1 d-flex justify-content-between align-items-center li_p">
                          <span class="d-flex justify-content-between align-items-center">
                            <i class="drag-handle cursor-move mdi mdi-menu align-text-bottom me-2"></i>
                            <span>'.$c->Nombre.' / '.$c->menu.'</span>
                          </span>
                          <input type="hidden" id="idsubmenux" value="'.$c->MenusubId.'">
                        </li>';
                    }

                }
            $html.='</ul>
            </div>
        </div>';
        echo $html;    
    }

    function registro_datos_permisos(){
        $datos = $this->input->post('data');
        $idcliente = $this->input->post('idcliente');
        $DATA = json_decode($datos);
        if($idcliente!=0){
            $this->General_model->delete_record('perfiles_detalles','idcliente',$idcliente);
        }
        for ($i=0;$i<count($DATA);$i++) {   
            $data['perfilId']=2;
            $data['MenusubId']=$DATA[$i]->idsubmenu; 
            $data['idcliente']=$DATA[$i]->idcliente;
            if($DATA[$i]->idsubmenu==30){
                $array1 = array('perfilId' => 2,'MenusubId'=>21,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array1);
                $array2 = array('perfilId' => 2,'MenusubId'=>25,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array2);
                $array3 = array('perfilId' => 2,'MenusubId'=>26,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array3);
                $array4 = array('perfilId' => 2,'MenusubId'=>27,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array4);
            }else if($DATA[$i]->idsubmenu==31){
                $array1 = array('perfilId' => 2,'MenusubId'=>19,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array1);
                $array2 = array('perfilId' => 2,'MenusubId'=>28,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array2);
                $array3 = array('perfilId' => 2,'MenusubId'=>29,'idcliente'=>$idcliente);
                $this->General_model->add_record('perfiles_detalles',$array3);
            }
            $id=$this->General_model->add_record('perfiles_detalles',$data);
        }
    }

    function envio_correo(){
        $id = $this->input->post('id');
        $strqfac="SELECT * FROM clientes  WHERE id=$id";
        $query_fac = $this->db->query($strqfac);
        $query_fac=$query_fac->row();
        $correo=$query_fac->email;
        //var_dump($query_fac);
        $strqus="SELECT * FROM usuarios WHERE idcliente=$id";
        $query_us = $this->db->query($strqus);
        $query_us=$query_us->row();
        //var_dump($query_us);
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            $this->load->library('email', NULL, 'ci_email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.mangoo.systems'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'mara@mangoo.systems';

            //Nuestra contraseña
            $config["smtp_pass"] = 'zNp5(Tc^G!Zh';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            $asunto='Mara Perez';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('mara@mangoo.systems',$asunto);
             
              /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
               */
            //======================
            $this->email->bcc('andres@mangoo.mx');
            $this->email->to($correo);
            //$this->email->bcc('leon1255555@gmail.com');

            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            

            $message='<div>
                <div style="border-radius: 15px; background: #272726; padding: 25px 25px 0px 25px; width: 80%; max-width: 545px;;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                        <div style="width: 100%; float: left;">
                            <div style="text-align:center">
                                <img src="'.base_url().'images/MVP Logo Mara.png" style="max-height: 100px;">
                                <br>
                                <h2 style="color:white; font-family: sans-serif;">¡Bienvenido a tu plataforma!</h2>
                                <img src="'.base_url().'images/waving-hand-svgrepo-com.svg" style="max-height: 100px;">
                                <br>
                            </div>
                        </div>

                        <div style="width: 100%; color:white; text-align: justify;">
                            <p style="font-size:15px; font-family: sans-serif;">Es un gusto enorme para mi poder ser parte de tu crecimiento. Juntos crearemos una planeación y estrategia infalible que potenciará tu negocio o empresa al siguiente nivel.<p>
                        </div>
                        <div style="width: 100%; text-align: center;">
                            <a href="https://maraperez.mangoo.systems/Login" style="color: white; background: #bd9250; padding: 9px; border-radius: 10px; padding-left: 25px; padding-right: 25px; text-decoration: auto; font-family: sans-serif;">Mi sesión</a>
                        </div>
                        
                        <div style="width: 100%; color:white; text-align: justify;">
                            <p style="font-size:15px; font-family: sans-serif;">Usuario:'.$query_us->Usuario.'<p>
                        </div>
                        <div style="width: 100%; color:white; text-align: justify;">
                            <p style="font-size:15px; font-family: sans-serif;">Contraseña:'.$query_us->txtuser.'<p>
                        </div>
                        <div style="width: 100%; color:white; text-align: center;">
                            <p style="font-size:20px; font-family: sans-serif;">La EXCELENCIA, <span style="color: #bd9250">no se negocia</span><p>
                        </div>
                        
                    </div></div>';

        $this->email->message($message);
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
            var_dump('Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
            var_dump('No se a enviado el email');
        }
        //==================
    }


    public function correo_aux(){        
        $this->load->view('cliente/correo');
    }

}