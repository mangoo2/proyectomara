<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_usuarios');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){   
        $data['btn_active']=2;
        $data['btn_active_sub']=2;     
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('usuario/index');
        $this->load->view('templates/footer');
        $this->load->view('usuario/indexjs');
    }

    public function registro($id=0){        
        $data['btn_active']=2;
        $data['btn_active_sub']=2;  
        if($id==0){
            $data['titulo']='Alta de nuevo usuario';
            // Datos generales
            $data['personalId'] = 0;
            $data['nombre']='';
            $data['correo']='';
            $data['telefono']='';
            $data['file']='';

            $data['UsuarioID']=0;
            $data['perfilId']=0;
            $data['usuario']='';
            $data['contrasena']='';
            $data['contrasena2']='';
        }else{
            $data['titulo']='Edición de usuario';
            $resul=$this->General_model->getselectwhere('personal','personalId',$id);
            foreach ($resul as $item) {
                $data['personalId']=$item->personalId;
                $data['nombre']=$item->nombre;
                $data['correo']=$item->correo;
                $data['telefono']=$item->telefono;      
                $data['file']=$item->file;     
                // inicio de sesion
                $data['UsuarioID']=0;
                $data['perfilId']=0;
                $data['usuario']='';
                $data['contrasena']='';
                $data['contrasena2']='';
                $resul=$this->General_model->getselectwheren('usuarios',array('personalId'=>$id,'tipo'=>1));
                foreach ($resul->result() as $item) {
                    $data['UsuarioID']=$item->UsuarioID;   
                    $data['perfilId']=$item->perfilId;   
                    $data['usuario']=$item->Usuario;
                    $data['contrasena']='x1f3[5]7w78{';     
                    $data['contrasena2']='x1f3[5]7w78{'; 
                }
            }
        }      
        $data['perfil']=$this->General_model->getselectwheren('perfiles',array('estatus'=>1,'tipo'=>1));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('usuario/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('usuario/formjs',$data);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function add_data(){
        $data=$this->input->post();
        $personalId=$data['personalId'];
        unset($data['personalId']);
        unset($data['UsuarioID']);
        unset($data['perfilId']);
        unset($data['usuario']);
        unset($data['contrasena']);
        unset($data['contrasena2']);
        if($personalId==0){
            $id=$this->General_model->add_record('personal',$data);
        }else{
            $id=$this->General_model->edit_record('personalId',$personalId,$data,'personal');
            $id=$personalId;
        }
        echo $id;
    }

    public function add_data_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'x1f3[5]7w78{'){
           unset($datos['contrasena']);
        }else{
            $datous['contrasena'] = $datos['contrasena'];
        }
        $id=$datos['UsuarioID'];
        $datous['personalId'] = $datos['personalId'];
        $datous['Usuario'] = $datos['usuario'];
        $datous['perfilId'] = $datos['perfilId'];
        $datous['acceso']=1;
        $datous['tipo']=1;
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datous,'usuarios');
        }else{
            $this->General_model->add_record('usuarios',$datous);
        }   
   
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->Model_usuarios->get_list($params);
        $totaldata= $this->Model_usuarios->total_list($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function delete_record(){
        $personalId=$this->input->post('id');
        $UsuarioID=$this->input->post('idusu');
        $data = array('activo'=>0);
        $this->General_model->edit_record('personalId',$personalId,$data,'personal');
        $this->General_model->edit_record('UsuarioID',$UsuarioID,$data,'usuarios');
        
    }

    public function update_record(){
        $id=$this->input->post('id');
        $data = array('acceso'=>0);
        $this->General_model->edit_record('UsuarioID',$id,$data,'usuarios');
        
    }

    public function activar_record(){
        $id=$this->input->post('id');
        $data = array('acceso'=>1);
        $this->General_model->edit_record('UsuarioID',$id,$data,'usuarios');
        
    }

    function cargafiles(){
        $id=$this->input->post('id');
        $folder="personal";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('file'=>$newfile);
          $this->General_model->edit_record('personalId',$id,$array,'personal');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

}    