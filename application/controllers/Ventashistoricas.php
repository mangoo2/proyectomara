<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventashistoricas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_ventashistoricas');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,18);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=6;
        $data['btn_active_sub']=18;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ventashistoricas/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventashistoricas/indexjs');
    }

    function get_datos_ventashistoricas()
    {   
        $result=$this->General_model->getselectwheren('ventashistoricas',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html='<div class="nav-align-left mb-4">
          <ul class="nav nav-pills me-3" role="tablist">';
            $cont=0;
            foreach ($result->result() as $p){
              $html.='<li class="nav-item">
                <button
                  type="button"
                  class="nav-link"
                  role="tab"
                  data-bs-toggle="tab"
                  data-bs-target="#navs_'.$p->id.'"
                  aria-controls="navs_'.$p->id.'"
                  aria-selected="true"
                  onclick="get_ventashistoricasdetalles('.$p->id.')">
                  <span class="tt_p_'.$p->id.'">'.$p->nombre.'</span>
                </button>
              </li>';
              $cont++;
            }
          $html.='</ul>
          <div class="tab-content">';
            $contx=0;
            foreach ($result->result() as $px){ 
              $html.='<div class="tab-pane fade" id="navs_'.$px->id.'" role="tabpanel">
                <h4 class="txt_titulo">Linea Servicio / Producto: <span class="tt_p_'.$px->id.'">'.$px->nombre.'</span> &nbsp;&nbsp; <a class="btn btn-outline-secondary waves-effect" onclick="edit_reg()"><span class="tf-icons mdi mdi-pencil"></span></a></h4>
                <div class="txt_titulo_edit" style="display:none">
                  <div class="row g-3">
                    <div class="col-md-4">
                      <div class="form-floating form-floating-outline mb-4">
                        <input type="text" class="form-control" id="nombre_'.$px->id.'" value="'.$px->nombre.'" placeholder="" required  />
                        <label for="nombre">Linea Servicio / Producto:</label>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <a class="btn btn-outline-secondary waves-effect" onclick="edit_registro('.$px->id.')">Editar</a>
                    </div>
                  </div>  
                </div>
                <br>
                <div class="text_tabla_ventas_historicas_detalles text_tabla_ventas_historicas_detalles_'.$px->id.'"></div>
              </div>';
              $contx++;
            }
          $html.='</div>
          </div>';
        echo $html;
    }

    function get_ventas_historicas_detalles()
    {
      $id = $this->input->post('id');
      $c=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
      $anio_cli=date('Y',strtotime($c->fecha_operacion));
      $anio_aux6=$anio_cli-6;
      $anio_aux5=$anio_cli-5;
      $anio_aux4=$anio_cli-4;
      $anio_aux3=$anio_cli-3;
      $anio_aux2=$anio_cli-2;
      $anio_aux1=$anio_cli-1;
      $m1=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,1);
      $m2=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,2);
      $m3=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,3);
      $m4=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,4);
      $m5=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,5);
      $m6=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,6);
      $m7=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,7);
      $m8=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,8);
      $m9=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,9);
      $m10=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,10);
      $m11=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,11);
      $m12=$this->Model_ventashistoricas->get_ventashistoricas($id,$this->idcliente,12);
      //if(isset($m1)){
        $html='<input type="hidden" id="idventashistoricas_x" value="'.$id.'" />
          <table class="table table-bordered table_info" id="tabla_datos_1">
                  <thead>
                    <tr>
                      <th class="th_grey">Mes/Año</th>
                      <th class="th_grey">'.$anio_aux6.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">'.$anio_aux5.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">'.$anio_aux4.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">'.$anio_aux3.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">'.$anio_aux2.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">'.$anio_aux1.'</th>
                      <th class="th_grey"></th>
                      <th class="th_grey">Promedio</th>
                      <th class="th_grey"></th>
                    </tr>';

                    $col5_1m=0;
                    $col4_1m=0;
                    $col3_1m=0;
                    $col2_1m=0;
                    $col1_1m=0;
                    $pv1 = 0;
                    if(isset($m12->cantidad_6)){
                      /*if($m12->cantidad_6!=0){
                        $col5_1r=$m1->cantidad_5-$m12->cantidad_6;  
                        $col5_1d=$col5_1r/$m12->cantidad_6;
                        $col5_1m=$col5_1d*100;
                      }
                      
                      if($m12->cantidad_5!=0){
                        $col4_1r=$m1->cantidad_4-$m12->cantidad_5;  
                        $col4_1d=$col4_1r/$m12->cantidad_5;
                        $col4_1m=$col4_1d*100;
                      }
                      
                      if($m12->cantidad_4!=0){
                        $col3_1r=$m1->cantidad_3-$m12->cantidad_4;  
                        $col3_1d=$col3_1r/$m12->cantidad_4;
                        $col3_1m=$col3_1d*100;
                      }

                      if($m12->cantidad_3!=0){
                        $col2_1r=$m1->cantidad_2-$m12->cantidad_3;  
                        $col2_1d=$col2_1r/$m12->cantidad_3;
                        $col2_1m=$col2_1d*100;
                      }

                      
                      if($m12->cantidad_2!=0){
                        $col1_1r=$m1->cantidad_1-$m12->cantidad_2;  
                        $col1_1d=$col1_1r/$m12->cantidad_2;
                        $col1_1m=$col1_1d*100;
                      }

                      $array1=[];
                      if($m1->cantidad_6!=0){
                        array_push($array1,$m1->cantidad_6);
                      }
                      if($m1->cantidad_5!=0){
                        array_push($array1,$m1->cantidad_5);
                      }
                      if($m1->cantidad_4!=0){
                        array_push($array1,$m1->cantidad_4);
                      }
                      if($m1->cantidad_3!=0){
                        array_push($array1,$m1->cantidad_3);
                      }
                      if($m1->cantidad_2!=0){
                        array_push($array1,$m1->cantidad_2);
                      }
                      if($m1->cantidad_1!=0){
                        array_push($array1,$m1->cantidad_1);
                      }
                      $pv1 = array_sum($array1)/count($array1);*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Enero<input type="hidden" class="id_x1" class="id_x1" id="id_x" value="'; if(isset($m1->id)){ $html.=$m1->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="1" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m1->cantidad_6)){ $html.=$m1->cantidad_6; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c"></th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m1->cantidad_5)){ $html.=$m1->cantidad_5; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col5_1m">'.round($col5_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m1->cantidad_4)){ $html.=$m1->cantidad_4; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col4_1m">'.round($col4_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m1->cantidad_3)){ $html.=$m1->cantidad_3; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col3_1m">'.round($col3_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m1->cantidad_2)){ $html.=$m1->cantidad_2; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col2_1m">'.round($col2_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m1->cantidad_1)){ $html.=$m1->cantidad_1; } $html.='" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col1_1m">'.round($col1_1m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv1).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m1->promedio)){ $html.=$m1->promedio; } $html.='" /></th>
                      <th class="th_c"></th>            
                    </tr>';
                    
                    $col6_2m=0;
                    $col5_2m=0;
                    $col4_2m=0;
                    $col3_2m=0;
                    $col2_2m=0;
                    $col1_2m=0;
                    $pv2 = 0;
                    $f2_m=0;
                      /*if($m1->cantidad_6!=0){
                        $col6_2r=$m2->cantidad_6-$m1->cantidad_6;
                        $col6_2d=$col6_2r/$m1->cantidad_6;
                        $col6_2m=$col6_2d*100;  
                      }

                      
                      if($m1->cantidad_5!=0){
                        $col5_2r=$m2->cantidad_5-$m1->cantidad_5;
                        $col5_2d=$col5_2r/$m1->cantidad_5;
                        $col5_2m=$col5_2d*100;
                      }

                      
                      if($m1->cantidad_4!=0){
                        $col4_2r=$m2->cantidad_4-$m1->cantidad_4;
                        $col4_2d=$col4_2r/$m1->cantidad_4;
                        $col4_2m=$col4_2d*100;
                      }

                      
                      if($m1->cantidad_3!=0){
                        $col3_2r=$m2->cantidad_3-$m1->cantidad_3;
                        $col3_2d=$col3_2r/$m1->cantidad_3;
                        $col3_2m=$col3_2d*100;
                      }

                      
                      if($m1->cantidad_2!=0){
                        $col2_2r=$m2->cantidad_2-$m1->cantidad_2;
                        $col2_2d=$col2_2r/$m1->cantidad_2;
                        $col2_2m=$col2_2d*100;
                      }

                      
                      if($m1->cantidad_1!=0){
                        $col1_2r=$m2->cantidad_1-$m1->cantidad_1;
                        $col1_2d=$col1_2r/$m1->cantidad_1;
                        $col1_2m=$col1_2d*100;
                      }

                      $array2=[];
                      if($m2->cantidad_6!=0){
                        array_push($array2,$m2->cantidad_6);
                      }
                      if($m2->cantidad_5!=0){
                        array_push($array2,$m2->cantidad_5);
                      }
                      if($m2->cantidad_4!=0){
                        array_push($array2,$m2->cantidad_4);
                      }
                      if($m2->cantidad_3!=0){
                        array_push($array2,$m2->cantidad_3);
                      }
                      if($m2->cantidad_2!=0){
                        array_push($array2,$m2->cantidad_2);
                      }
                      if($m2->cantidad_1!=0){
                        array_push($array2,$m2->cantidad_1);
                      }
                      $pv2 = array_sum($array2)/count($array2);
                      
                      $f2_m=0;
                      if($pv1!=0){
                        $f2_r=$pv2-$pv1;
                        $f2_d=$f2_r/$pv1;
                        $f2_m=$f2_d*100;
                      }*/

                    $html.='<tr class="tr_1">
                      <th class="th_grey">Febrero<input type="hidden" class="id_x2" id="id_x" value="'; if(isset($m2->id)){ $html.=$m2->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="2" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m2->cantidad_6)){ $html.=$m2->cantidad_6; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col6_2m">'.round($col6_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m2->cantidad_5)){ $html.=$m2->cantidad_5; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col5_2m">'.round($col5_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m2->cantidad_4)){ $html.=$m2->cantidad_4; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col4_2m">'.round($col4_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m2->cantidad_3)){ $html.=$m2->cantidad_3; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col3_2m">'.round($col3_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m2->cantidad_2)){ $html.=$m2->cantidad_2; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col2_2m">'.round($col2_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m2->cantidad_1)){ $html.=$m2->cantidad_1; } $html.='" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col1_2m">'.round($col1_2m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv2).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m2->promedio)){ $html.=$m2->promedio; } $html.='" /></th>
                      <th class="th_c f2_m">'.round($f2_m).'%</th>    
                    </tr>';
                    
                    $col6_3m=0;
                    $col5_3m=0;
                    $col4_3m=0;
                    $col3_3m=0;
                    $col2_3m=0;
                    $col1_3m=0;

                    $pv3 = 0;
                    $f3_m=0;
                    if(isset($m3->cantidad_6)){
                      /*if($m2->cantidad_6!=0){
                        $col6_3r=$m3->cantidad_6-$m2->cantidad_6;
                        $col6_3d=$col6_3r/$m2->cantidad_6;
                        $col6_3m=$col6_3d*100;  
                      }

                      if($m2->cantidad_5!=0){
                        $col5_3r=$m3->cantidad_5-$m2->cantidad_5;
                        $col5_3d=$col5_3r/$m2->cantidad_5;
                        $col5_3m=$col5_3d*100;
                      }

                      if($m2->cantidad_4!=0){
                        $col4_3r=$m3->cantidad_4-$m2->cantidad_4;
                        $col4_3d=$col4_3r/$m2->cantidad_4;
                        $col4_3m=$col4_3d*100;
                      }

                      if($m2->cantidad_3!=0){
                        $col3_3r=$m3->cantidad_3-$m2->cantidad_3;
                        $col3_3d=$col3_3r/$m2->cantidad_3;
                        $col3_3m=$col3_3d*100;
                      }

                      if($m2->cantidad_2!=0){
                        $col2_3r=$m3->cantidad_2-$m2->cantidad_2;
                        $col2_3d=$col2_3r/$m2->cantidad_2;
                        $col2_3m=$col2_3d*100;
                      }

                      if($m2->cantidad_1!=0){
                        $col1_3r=$m3->cantidad_1-$m2->cantidad_1;
                        $col1_3d=$col1_3r/$m2->cantidad_1;
                        $col1_3m=$col1_3d*100;
                      }

                      $array3=[];
                      if($m3->cantidad_6!=0){
                        array_push($array3,$m3->cantidad_6);
                      }
                      if($m3->cantidad_5!=0){
                        array_push($array3,$m3->cantidad_5);
                      }
                      if($m3->cantidad_4!=0){
                        array_push($array3,$m3->cantidad_4);
                      }
                      if($m3->cantidad_3!=0){
                        array_push($array3,$m3->cantidad_3);
                      }
                      if($m3->cantidad_2!=0){
                        array_push($array3,$m3->cantidad_2);
                      }
                      if($m3->cantidad_1!=0){
                        array_push($array3,$m3->cantidad_1);
                      }
                      $pv3 = array_sum($array3)/count($array3);
                      
                      $f3_m=0;
                      if($pv2!=0){
                        $f3_r=$pv3-$pv2;
                        $f3_d=$f3_r/$pv2;
                        $f3_m=$f3_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Marzo<input type="hidden" class="id_x3" id="id_x" value="'; if(isset($m3->id)){ $html.=$m3->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="3" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m3->cantidad_6)){ $html.=$m3->cantidad_6; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col6_3m">'.round($col6_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m3->cantidad_5)){ $html.=$m3->cantidad_5; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col5_3m">'.round($col5_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m3->cantidad_4)){ $html.=$m3->cantidad_4; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col4_3m">'.round($col4_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m3->cantidad_3)){ $html.=$m3->cantidad_3; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col3_3m">'.round($col3_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m3->cantidad_2)){ $html.=$m3->cantidad_2; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col2_3m">'.round($col2_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m3->cantidad_1)){ $html.=$m3->cantidad_1; } $html.='" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col1_3m">'.round($col1_3m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv3).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m3->promedio)){ $html.=$m3->promedio; } $html.='" /></th>
                      <th class="th_c f3_m">'.round($f3_m).'%</th>    
                    </tr>';
                    
                    $col6_4m=0;
                    $col5_4m=0;
                    $col4_4m=0;
                    $col3_4m=0;
                    $col2_4m=0;
                    $col1_4m=0;

                    $pv4 = 0;
                    $f4_m = 0;
                    if(isset($m3->cantidad_6)){
                      /*if($m3->cantidad_6!=0){
                        $col6_4r=$m4->cantidad_6-$m3->cantidad_6;
                        $col6_4d=$col6_4r/$m3->cantidad_6;
                        $col6_4m=$col6_4d*100;
                      }

                      if($m3->cantidad_5!=0){
                        $col5_4r=$m4->cantidad_5-$m3->cantidad_5;
                        $col5_4d=$col5_4r/$m3->cantidad_5;
                        $col5_4m=$col5_4d*100;
                      }

                      if($m3->cantidad_4!=0){
                        $col4_4r=$m4->cantidad_4-$m3->cantidad_4;
                        $col4_4d=$col4_4r/$m3->cantidad_4;
                        $col4_4m=$col4_4d*100;
                      }

                      if($m3->cantidad_3!=0){
                        $col3_4r=$m4->cantidad_3-$m3->cantidad_3;
                        $col3_4d=$col3_4r/$m3->cantidad_3;
                        $col3_4m=$col3_4d*100;
                      }

                      if($m3->cantidad_2!=0){
                        $col2_4r=$m4->cantidad_2-$m3->cantidad_2;
                        $col2_4d=$col2_4r/$m3->cantidad_2;
                        $col2_4m=$col2_4d*100;
                      }

                      if($m3->cantidad_1!=0){
                        $col1_4r=$m4->cantidad_1-$m3->cantidad_1;
                        $col1_4d=$col1_4r/$m3->cantidad_1;
                        $col1_4m=$col1_4d*100;
                      }
                       
                      $array4=[];
                      if($m4->cantidad_6!=0){
                        array_push($array4,$m4->cantidad_6);
                      }
                      if($m4->cantidad_5!=0){
                        array_push($array4,$m4->cantidad_5);
                      }
                      if($m4->cantidad_4!=0){
                        array_push($array4,$m4->cantidad_4);
                      }
                      if($m4->cantidad_3!=0){
                        array_push($array4,$m4->cantidad_3);
                      }
                      if($m4->cantidad_2!=0){
                        array_push($array4,$m4->cantidad_2);
                      }
                      if($m4->cantidad_1!=0){
                        array_push($array4,$m4->cantidad_1);
                      }
                      $pv4 = array_sum($array4)/count($array4);
                      
                      $f4_m=0;
                      if($pv3!=0){
                        $f4_r=$pv4-$pv3;
                        $f4_d=$f4_r/$pv3;
                        $f4_m=$f4_d*100;
                      }*/
                    }

                    $html.='<tr class="tr_1">
                      <th class="th_grey">Abril<input type="hidden" class="id_x4" id="id_x" value="'; if(isset($m4->id)){ $html.=$m4->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="4" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m4->cantidad_6)){ $html.=$m4->cantidad_6; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col6_4m">'.round($col6_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m4->cantidad_5)){ $html.=$m4->cantidad_5; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col5_4m">'.round($col5_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m4->cantidad_4)){ $html.=$m4->cantidad_4; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col4_4m">'.round($col4_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m4->cantidad_3)){ $html.=$m4->cantidad_3; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col3_4m">'.round($col3_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m4->cantidad_2)){ $html.=$m4->cantidad_2; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col2_4m">'.round($col2_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m4->cantidad_1)){ $html.=$m4->cantidad_1; } $html.='" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col1_4m">'.round($col1_4m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv4).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m4->promedio)){ $html.=$m4->promedio; } $html.='" /></th>
                      <th class="th_c f4_m">'.round($f4_m).'%</th>    
                    </tr>';

                    $col6_5m=0;
                    $col5_5m=0;
                    $col4_5m=0;
                    $col3_5m=0;
                    $col2_5m=0;
                    $col1_5m=0;
                    $pv5 = 0;
                    $f5_m = 0;
                    if(isset($m5->cantidad_6)){
                      /*if($m4->cantidad_6!=0){ 
                        $col6_5r=$m5->cantidad_6-$m4->cantidad_6;
                        $col6_5d=$col6_5r/$m4->cantidad_6;
                        $col6_5m=$col6_5d*100;
                      }

                      
                      if($m4->cantidad_5!=0){ 
                        $col5_5r=$m5->cantidad_5-$m4->cantidad_5;
                        $col5_5d=$col5_5r/$m4->cantidad_5;
                        $col5_5m=$col5_5d*100;
                      }

                      
                      if($m4->cantidad_4!=0){ 
                        $col4_5r=$m5->cantidad_4-$m4->cantidad_4;
                        $col4_5d=$col4_5r/$m4->cantidad_4;
                        $col4_5m=$col4_5d*100;
                      }

                      
                      if($m4->cantidad_3!=0){ 
                        $col3_5r=$m5->cantidad_3-$m4->cantidad_3;
                        $col3_5d=$col3_5r/$m4->cantidad_3;
                        $col3_5m=$col3_5d*100;
                      }

                      
                      if($m4->cantidad_2!=0){ 
                        $col2_5r=$m5->cantidad_2-$m4->cantidad_2;
                        $col2_5d=$col2_5r/$m4->cantidad_2;
                        $col2_5m=$col2_5d*100;
                      }

                      
                      if($m4->cantidad_1!=0){ 
                        $col1_5r=$m5->cantidad_1-$m4->cantidad_1;
                        $col1_5d=$col1_5r/$m4->cantidad_1;
                        $col1_5m=$col1_5d*100;
                      }

                      $array5=[];
                      if($m5->cantidad_6!=0){
                        array_push($array5,$m5->cantidad_6);
                      }
                      if($m5->cantidad_5!=0){
                        array_push($array5,$m5->cantidad_5);
                      }
                      if($m5->cantidad_4!=0){
                        array_push($array5,$m5->cantidad_4);
                      }
                      if($m5->cantidad_3!=0){
                        array_push($array5,$m5->cantidad_3);
                      }
                      if($m5->cantidad_2!=0){
                        array_push($array5,$m5->cantidad_2);
                      }
                      if($m5->cantidad_1!=0){
                        array_push($array5,$m5->cantidad_1);
                      }
                      $pv5 = array_sum($array5)/count($array5);

                      $f5_m=0;
                      if($pv4!=0){
                        $f5_r=$pv5-$pv4;
                        $f5_d=$f5_r/$pv4;
                        $f5_m=$f5_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Mayo<input type="hidden" class="id_x5" id="id_x" value="'; if(isset($m5->id)){ $html.=$m5->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="5" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m5->cantidad_6)){ $html.=$m5->cantidad_6; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col6_5m">'.round($col6_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m5->cantidad_5)){ $html.=$m5->cantidad_5; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col5_5m">'.round($col5_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m5->cantidad_4)){ $html.=$m5->cantidad_4; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col4_5m">'.round($col4_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m5->cantidad_3)){ $html.=$m5->cantidad_3; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col3_5m">'.round($col3_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m5->cantidad_2)){ $html.=$m5->cantidad_2; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col2_5m">'.round($col2_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m5->cantidad_1)){ $html.=$m5->cantidad_1; } $html.='" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col1_5m">'.round($col1_5m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv5).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m5->promedio)){ $html.=$m5->promedio; } $html.='" /></th>
                      <th class="th_c f5_m">'.round($f5_m).'%</th>    
                    </tr>';
                    
                    $col6_6m=0;
                    $col5_6m=0;
                    $col4_6m=0;
                    $col3_6m=0;
                    $col2_6m=0;
                    $col1_6m=0;
                    $pv6 = 0;
                    $f6_m = 0;
                    if(isset($m6->cantidad_6)){
                      /*if($m5->cantidad_6!=0){
                        $col6_6r=$m6->cantidad_6-$m5->cantidad_6;
                        $col6_6d=$col6_6r/$m5->cantidad_6;
                        $col6_6m=$col6_6d*100;
                      }

                      if($m5->cantidad_5!=0){
                        $col5_6r=$m6->cantidad_5-$m5->cantidad_5;
                        $col5_6d=$col5_6r/$m5->cantidad_5;
                        $col5_6m=$col5_6d*100;
                      }

                      if($m5->cantidad_4!=0){
                        $col4_6r=$m6->cantidad_4-$m5->cantidad_4;
                        $col4_6d=$col4_6r/$m5->cantidad_4;
                        $col4_6m=$col4_6d*100;
                      }
                      
                      if($m5->cantidad_3!=0){
                        $col3_6r=$m6->cantidad_3-$m5->cantidad_3;
                        $col3_6d=$col3_6r/$m5->cantidad_3;
                        $col3_6m=$col3_6d*100;
                      }

                      if($m5->cantidad_2!=0){
                        $col2_6r=$m6->cantidad_2-$m5->cantidad_2;
                        $col2_6d=$col2_6r/$m5->cantidad_2;
                        $col2_6m=$col2_6d*100;
                      }

                      if($m5->cantidad_1!=0){
                        $col1_6r=$m6->cantidad_1-$m5->cantidad_1;
                        $col1_6d=$col1_6r/$m5->cantidad_1;
                        $col1_6m=$col1_6d*100;
                      }
                      
                      $array6=[];
                      if($m6->cantidad_6!=0){
                        array_push($array6,$m6->cantidad_6);
                      }
                      if($m6->cantidad_5!=0){
                        array_push($array6,$m6->cantidad_5);
                      }
                      if($m6->cantidad_4!=0){
                        array_push($array6,$m6->cantidad_4);
                      }
                      if($m6->cantidad_3!=0){
                        array_push($array6,$m6->cantidad_3);
                      }
                      if($m6->cantidad_2!=0){
                        array_push($array6,$m6->cantidad_2);
                      }
                      if($m6->cantidad_1!=0){
                        array_push($array6,$m6->cantidad_1);
                      }
                      $pv6 = array_sum($array6)/count($array6);

                      $f6_m=0;
                      if($pv5!=0){
                        $f6_r=$pv6-$pv5;
                        $f6_d=$f6_r/$pv5;
                        $f6_m=$f6_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Junio<input type="hidden" class="id_x6" id="id_x" value="'; if(isset($m6->id)){ $html.=$m6->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="6" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m6->cantidad_6)){ $html.=$m6->cantidad_6; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col6_6m">'.round($col6_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m6->cantidad_5)){ $html.=$m6->cantidad_5; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col5_6m">'.round($col5_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m6->cantidad_4)){ $html.=$m6->cantidad_4; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col4_6m">'.round($col4_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m6->cantidad_3)){ $html.=$m6->cantidad_3; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col3_6m">'.round($col3_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m6->cantidad_2)){ $html.=$m6->cantidad_2; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col2_6m">'.round($col2_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m6->cantidad_1)){ $html.=$m6->cantidad_1; } $html.='" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col1_6m">'.round($col1_6m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv6).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m6->promedio)){ $html.=$m6->promedio; } $html.='" /></th>
                      <th class="th_c f6_m">'.round($f6_m).'%</th>    
                    </tr>';

                    $col6_7m=0;
                    $col5_7m=0;
                    $col4_7m=0;
                    $col3_7m=0;
                    $col2_7m=0;
                    $col1_7m=0;
                    $pv7 = 0;
                    $f7_m=0;
                    if(isset($m7->cantidad_6)){
                      /*if($m6->cantidad_6!=0){
                        $col6_7r=$m7->cantidad_6-$m6->cantidad_6;
                        $col6_7d=$col6_7r/$m6->cantidad_6;
                        $col6_7m=$col6_7d*100;
                      }

                      if($m6->cantidad_5!=0){
                        $col5_7r=$m7->cantidad_5-$m6->cantidad_5;
                        $col5_7d=$col5_7r/$m6->cantidad_5;
                        $col5_7m=$col5_7d*100;
                      }

                      if($m6->cantidad_4!=0){
                        $col4_7r=$m7->cantidad_4-$m6->cantidad_4;
                        $col4_7d=$col4_7r/$m6->cantidad_4;
                        $col4_7m=$col4_7d*100;
                      }

                      if($m6->cantidad_3!=0){
                        $col3_7r=$m7->cantidad_3-$m6->cantidad_3;
                        $col3_7d=$col3_7r/$m6->cantidad_3;
                        $col3_7m=$col3_7d*100;
                      }

                      if($m6->cantidad_2!=0){
                        $col2_7r=$m7->cantidad_2-$m6->cantidad_2;
                        $col2_7d=$col2_7r/$m6->cantidad_2;
                        $col2_7m=$col2_7d*100;
                      }

                      if($m6->cantidad_1!=0){
                        $col1_7r=$m7->cantidad_1-$m6->cantidad_1;
                        $col1_7d=$col1_7r/$m6->cantidad_1;
                        $col1_7m=$col1_7d*100;
                      }

                      $array7=[];
                      if($m7->cantidad_6!=0){
                        array_push($array7,$m7->cantidad_6);
                      }
                      if($m7->cantidad_5!=0){
                        array_push($array7,$m7->cantidad_5);
                      }
                      if($m7->cantidad_4!=0){
                        array_push($array7,$m7->cantidad_4);
                      }
                      if($m7->cantidad_3!=0){
                        array_push($array7,$m7->cantidad_3);
                      }
                      if($m7->cantidad_2!=0){
                        array_push($array7,$m7->cantidad_2);
                      }
                      if($m7->cantidad_1!=0){
                        array_push($array7,$m7->cantidad_1);
                      }
                      $pv7 = array_sum($array7)/count($array7);

                      $f7_m=0;
                      if($pv6!=0){
                        $f7_r=$pv7-$pv6;
                        $f7_d=$f7_r/$pv6;
                        $f7_m=$f7_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Julio<input type="hidden" class="id_x7" id="id_x" value="'; if(isset($m7->id)){ $html.=$m7->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="7" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m7->cantidad_6)){ $html.=$m7->cantidad_6; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col6_7m">'.round($col6_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m7->cantidad_5)){ $html.=$m7->cantidad_5; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col5_7m">'.round($col5_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m7->cantidad_4)){ $html.=$m7->cantidad_4; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col4_7m">'.round($col4_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m7->cantidad_3)){ $html.=$m7->cantidad_3; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col3_7m">'.round($col3_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m7->cantidad_2)){ $html.=$m7->cantidad_2; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col2_7m">'.round($col2_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m7->cantidad_1)){ $html.=$m7->cantidad_1; } $html.='" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col1_7m">'.round($col1_7m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv7).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m7->promedio)){ $html.=$m7->promedio; } $html.='" /></th>
                      <th class="th_c f7_m">'.round($f7_m).'%</th>    
                    </tr>';

                    $col6_8m=0;
                    $col5_8m=0;
                    $col4_8m=0;
                    $col3_8m=0;
                    $col2_8m=0;
                    $col1_8m=0;
                    $pv8 = 0;
                    $f8_m = 0;

                    if(isset($m8->cantidad_6)){
                      /*if($m7->cantidad_6!=0){
                        $col6_8r=$m8->cantidad_6-$m7->cantidad_6;
                        $col6_8d=$col6_8r/$m7->cantidad_6;
                        $col6_8m=$col6_8d*100;
                      }

                      if($m7->cantidad_5!=0){
                        $col5_8r=$m8->cantidad_5-$m7->cantidad_5;
                        $col5_8d=$col5_8r/$m7->cantidad_5;
                        $col5_8m=$col5_8d*100;
                      }

                      if($m7->cantidad_4!=0){
                        $col4_8r=$m8->cantidad_4-$m7->cantidad_4;
                        $col4_8d=$col4_8r/$m7->cantidad_4;
                        $col4_8m=$col4_8d*100;
                      }

                      if($m7->cantidad_3!=0){
                        $col3_8r=$m8->cantidad_3-$m7->cantidad_3;
                        $col3_8d=$col3_8r/$m7->cantidad_3;
                        $col3_8m=$col3_8d*100;
                      }
                      
                      if($m7->cantidad_2!=0){
                        $col2_8r=$m8->cantidad_2-$m7->cantidad_2;
                        $col2_8d=$col2_8r/$m7->cantidad_2;
                        $col2_8m=$col2_8d*100;
                      }

                      if($m7->cantidad_1!=0){
                        $col1_8r=$m8->cantidad_1-$m7->cantidad_1;
                        $col1_8d=$col1_8r/$m7->cantidad_1;
                        $col1_8m=$col1_8d*100;
                      }

                      $array8=[];
                      if($m8->cantidad_6!=0){
                        array_push($array8,$m8->cantidad_6);
                      }
                      if($m8->cantidad_5!=0){
                        array_push($array8,$m8->cantidad_5);
                      }
                      if($m8->cantidad_4!=0){
                        array_push($array8,$m8->cantidad_4);
                      }
                      if($m8->cantidad_3!=0){
                        array_push($array8,$m8->cantidad_3);
                      }
                      if($m8->cantidad_2!=0){
                        array_push($array8,$m8->cantidad_2);
                      }
                      if($m8->cantidad_1!=0){
                        array_push($array8,$m8->cantidad_1);
                      }
                      $pv8 = array_sum($array8)/count($array8);

                      $f8_m=0;
                      if($pv7!=0){
                        $f8_r=$pv8-$pv7;
                        $f8_d=$f8_r/$pv7;
                        $f8_m=$f8_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Agosto<input type="hidden" class="id_x8" id="id_x" value="'; if(isset($m8->id)){ $html.=$m8->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="8" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m8->cantidad_6)){ $html.=$m8->cantidad_6; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col6_8m">'.round($col6_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m8->cantidad_5)){ $html.=$m8->cantidad_5; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col5_8m">'.round($col5_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m8->cantidad_4)){ $html.=$m8->cantidad_4; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col4_8m">'.round($col4_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m8->cantidad_3)){ $html.=$m8->cantidad_3; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col3_8m">'.round($col3_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m8->cantidad_2)){ $html.=$m8->cantidad_2; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col2_8m">'.round($col2_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m8->cantidad_1)){ $html.=$m8->cantidad_1; } $html.='" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col1_8m">'.round($col1_8m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv8).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m8->promedio)){ $html.=$m8->promedio; } $html.='" /></th>
                      <th class="th_c f8_m">'.round($f8_m).'%</th>    
                    </tr>';

                    $col6_9m=0;
                    $col5_9m=0;
                    $col4_9m=0;
                    $col3_9m=0;
                    $col2_9m=0;
                    $col1_9m=0;
                    $pv9 = 0;
                    $f9_m = 0;

                    if(isset($m8->cantidad_6)){
                      /*if($m8->cantidad_6!=0){
                        $col6_9r=$m9->cantidad_6-$m8->cantidad_6;
                        $col6_9d=$col6_9r/$m8->cantidad_6;
                        $col6_9m=$col6_9d*100;
                      }

                      if($m8->cantidad_5!=0){
                        $col5_9r=$m9->cantidad_5-$m8->cantidad_5;
                        $col5_9d=$col5_9r/$m8->cantidad_5;
                        $col5_9m=$col5_9d*100;
                      }

                      if($m8->cantidad_4!=0){
                        $col4_9r=$m9->cantidad_4-$m8->cantidad_4;
                        $col4_9d=$col4_9r/$m8->cantidad_4;
                        $col4_9m=$col4_9d*100;
                      }

                      if($m8->cantidad_3!=0){
                        $col3_9r=$m9->cantidad_3-$m8->cantidad_3;
                        $col3_9d=$col3_9r/$m8->cantidad_3;
                        $col3_9m=$col3_9d*100;
                      }

                      if($m8->cantidad_2!=0){
                        $col2_9r=$m9->cantidad_2-$m8->cantidad_2;
                        $col2_9d=$col2_9r/$m8->cantidad_2;
                        $col2_9m=$col2_9d*100;
                      }

                      if($m8->cantidad_1!=0){
                        $col1_9r=$m9->cantidad_1-$m8->cantidad_1;
                        $col1_9d=$col1_9r/$m8->cantidad_1;
                        $col1_9m=$col1_9d*100;
                      }
                      
                      $array9=[];
                      if($m9->cantidad_6!=0){
                        array_push($array9,$m9->cantidad_6);
                      }
                      if($m9->cantidad_5!=0){
                        array_push($array9,$m9->cantidad_5);
                      }
                      if($m9->cantidad_4!=0){
                        array_push($array9,$m9->cantidad_4);
                      }
                      if($m9->cantidad_3!=0){
                        array_push($array9,$m9->cantidad_3);
                      }
                      if($m9->cantidad_2!=0){
                        array_push($array9,$m9->cantidad_2);
                      }
                      if($m9->cantidad_1!=0){
                        array_push($array9,$m9->cantidad_1);
                      }
                      $pv9 = array_sum($array9)/count($array9);

                      $f9_m=0;
                      if($pv8!=0){
                        $f9_r=$pv9-$pv8;
                        $f9_d=$f9_r/$pv8;
                        $f9_m=$f9_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Septiembre<input type="hidden" class="id_x9" id="id_x" value="'; if(isset($m9->id)){ $html.=$m9->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="9" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m9->cantidad_6)){ $html.=$m9->cantidad_6; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col6_9m">'.round($col6_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m9->cantidad_5)){ $html.=$m9->cantidad_5; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col5_9m">'.round($col5_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m9->cantidad_4)){ $html.=$m9->cantidad_4; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col4_9m">'.round($col4_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m9->cantidad_3)){ $html.=$m9->cantidad_3; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col3_9m">'.round($col3_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m9->cantidad_2)){ $html.=$m9->cantidad_2; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col2_9m">'.round($col2_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m9->cantidad_1)){ $html.=$m9->cantidad_1; } $html.='" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col1_9m">'.round($col1_9m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv9).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m9->promedio)){ $html.=$m9->promedio; } $html.='" /></th>
                      <th class="th_c f9_m">'.round($f9_m).'%</th>    
                    </tr>';

                    $col6_10m=0;
                    $col5_10m=0;
                    $col4_10m=0;
                    $col3_10m=0;
                    $col2_10m=0;
                    $col1_10m=0;
                    $pv10 = 0;
                    $f10_m=0;
                    if(isset($m10->cantidad_6)){
                      /*if($m9->cantidad_6!=0){
                        $col6_10r=$m10->cantidad_6-$m9->cantidad_6;
                        $col6_10d=$col6_10r/$m9->cantidad_6;
                        $col6_10m=$col6_10d*100;
                      }

                      if($m9->cantidad_5!=0){
                        $col5_10r=$m10->cantidad_5-$m9->cantidad_5;
                        $col5_10d=$col5_10r/$m9->cantidad_5;
                        $col5_10m=$col5_10d*100;
                      }

                      if($m9->cantidad_4!=0){
                        $col4_10r=$m10->cantidad_4-$m9->cantidad_4;
                        $col4_10d=$col4_10r/$m9->cantidad_4;
                        $col4_10m=$col4_10d*100;
                      }

                      if($m9->cantidad_3!=0){
                        $col3_10r=$m10->cantidad_3-$m9->cantidad_3;
                        $col3_10d=$col3_10r/$m9->cantidad_3;
                        $col3_10m=$col3_10d*100;
                      }

                      if($m9->cantidad_2!=0){
                        $col2_10r=$m10->cantidad_2-$m9->cantidad_2;
                        $col2_10d=$col2_10r/$m9->cantidad_2;
                        $col2_10m=$col2_10d*100;
                      }

                      if($m9->cantidad_1!=0){
                        $col1_10r=$m10->cantidad_1-$m9->cantidad_1;
                        $col1_10d=$col1_10r/$m9->cantidad_1;
                        $col1_10m=$col1_10d*100;
                      }
                      
                      $array10=[];
                      if($m10->cantidad_6!=0){
                        array_push($array10,$m10->cantidad_6);
                      }
                      if($m10->cantidad_5!=0){
                        array_push($array10,$m10->cantidad_5);
                      }
                      if($m10->cantidad_4!=0){
                        array_push($array10,$m10->cantidad_4);
                      }
                      if($m10->cantidad_3!=0){
                        array_push($array10,$m10->cantidad_3);
                      }
                      if($m10->cantidad_2!=0){
                        array_push($array10,$m10->cantidad_2);
                      }
                      if($m10->cantidad_1!=0){
                        array_push($array10,$m10->cantidad_1);
                      }
                      $pv10 = array_sum($array10)/count($array10);

                      $f10_m=0;
                      if($pv9!=0){
                        $f10_r=$pv10-$pv9;
                        $f10_d=$f10_r/$pv9;
                        $f10_m=$f10_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Octubre<input type="hidden" class="id_x10" id="id_x" value="'; if(isset($m10->id)){ $html.=$m10->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="10" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m10->cantidad_6)){ $html.=$m10->cantidad_6; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col6_10m">'.round($col6_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m10->cantidad_5)){ $html.=$m10->cantidad_5; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col5_10m">'.round($col5_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m10->cantidad_4)){ $html.=$m10->cantidad_4; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col4_10m">'.round($col4_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m10->cantidad_3)){ $html.=$m10->cantidad_3; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col3_10m">'.round($col3_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m10->cantidad_2)){ $html.=$m10->cantidad_2; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col2_10m">'.round($col2_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m10->cantidad_1)){ $html.=$m10->cantidad_1; } $html.='" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col1_10m">'.round($col1_10m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv10).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m10->promedio)){ $html.=$m10->promedio; } $html.='" /></th>
                      <th class="th_c f10_m">'.round($f10_m).'%</th>    
                    </tr>';

                    $col6_11m=0;
                    $col5_11m=0;
                    $col4_11m=0;
                    $col3_11m=0;
                    $col2_11m=0;
                    $col1_11m=0;
                    $pv11 =0;
                    $f11_m=0;
                    if(isset($m10->cantidad_6)){
                      /*if($m10->cantidad_6!=0){
                        $col6_11r=$m11->cantidad_6-$m10->cantidad_6;
                        $col6_11d=$col6_11r/$m10->cantidad_6;
                        $col6_11m=$col6_11d*100;
                      }

                      if($m10->cantidad_5!=0){
                        $col5_11r=$m11->cantidad_5-$m10->cantidad_5;
                        $col5_11d=$col5_11r/$m10->cantidad_5;
                        $col5_11m=$col5_11d*100;
                      }

                      if($m10->cantidad_4!=0){
                        $col4_11r=$m11->cantidad_4-$m10->cantidad_4;
                        $col4_11d=$col4_11r/$m10->cantidad_4;
                        $col4_11m=$col4_11d*100;
                      }

                      if($m10->cantidad_3!=0){
                        $col3_11r=$m11->cantidad_3-$m10->cantidad_3;
                        $col3_11d=$col3_11r/$m10->cantidad_3;
                        $col3_11m=$col3_11d*100;
                      }

                      if($m10->cantidad_2!=0){
                        $col2_11r=$m11->cantidad_2-$m10->cantidad_2;
                        $col2_11d=$col2_11r/$m10->cantidad_2;
                        $col2_11m=$col2_11d*100;
                      }

                      if($m10->cantidad_1!=0){
                        $col1_11r=$m11->cantidad_1-$m10->cantidad_1;
                        $col1_11d=$col1_11r/$m10->cantidad_1;
                        $col1_11m=$col1_11d*100;
                      }
                      
                      $array11=[];
                      if($m11->cantidad_6!=0){
                        array_push($array11,$m11->cantidad_6);
                      }
                      if($m11->cantidad_5!=0){
                        array_push($array11,$m11->cantidad_5);
                      }
                      if($m11->cantidad_4!=0){
                        array_push($array11,$m11->cantidad_4);
                      }
                      if($m11->cantidad_3!=0){
                        array_push($array11,$m11->cantidad_3);
                      }
                      if($m11->cantidad_2!=0){
                        array_push($array11,$m11->cantidad_2);
                      }
                      if($m11->cantidad_1!=0){
                        array_push($array11,$m11->cantidad_1);
                      }
                      $pv11 = array_sum($array11)/count($array11);

                      $f11_m=0;
                      if($pv10!=0){
                        $f11_r=$pv11-$pv10;
                        $f11_d=$f11_r/$pv10;
                        $f11_m=$f11_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Noviembre<input type="hidden" class="id_x11" id="id_x" value="'; if(isset($m11->id)){ $html.=$m11->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="11" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m11->cantidad_6)){ $html.=$m11->cantidad_6; } $html.='" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col6_11m">'.round($col6_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m11->cantidad_5)){ $html.=$m11->cantidad_5; } $html.='" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col5_11m">'.round($col5_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m11->cantidad_4)){ $html.=$m11->cantidad_4; } $html.='" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col4_11m">'.round($col4_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m11->cantidad_3)){ $html.=$m11->cantidad_3; } $html.='" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col3_11m">'.round($col3_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m11->cantidad_2)){ $html.=$m11->cantidad_2; } $html.='" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col2_11m">'.round($col2_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m11->cantidad_1)){ $html.=$m11->cantidad_1; } $html.='" oninput="calcular_promedio(11)" /></th>                      
                      <th class="th_c col1_11m">'.round($col1_11m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv11).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m11->promedio)){ $html.=$m11->promedio; } $html.='" /></th>
                      <th class="th_c f11_m">'.round($f11_m).'%</th>    
                    </tr>';

                    $col6_12m=0;
                    $col5_12m=0;
                    $col4_12m=0;
                    $col3_12m=0;
                    $col2_12m=0;
                    $col1_12m=0;
                    $pv12 =
                    $f12_m=0;

                    if(isset($m12->cantidad_6)){
                      /*if($m11->cantidad_6!=0){
                        $col6_12r=$m12->cantidad_6-$m11->cantidad_6;
                        $col6_12d=$col6_12r/$m11->cantidad_6;
                        $col6_12m=$col6_12d*100;
                      }

                      if($m11->cantidad_5!=0){
                        $col5_12r=$m12->cantidad_5-$m11->cantidad_5;
                        $col5_12d=$col5_12r/$m11->cantidad_5;
                        $col5_12m=$col5_12d*100;
                      }

                      if($m11->cantidad_4!=0){
                        $col4_12r=$m12->cantidad_4-$m11->cantidad_4;
                        $col4_12d=$col4_12r/$m11->cantidad_4;
                        $col4_12m=$col4_12d*100;
                      }

                      if($m11->cantidad_3!=0){
                        $col3_12r=$m12->cantidad_3-$m11->cantidad_3;
                        $col3_12d=$col3_12r/$m11->cantidad_3;
                        $col3_12m=$col3_12d*100;
                      }

                      if($m11->cantidad_2!=0){
                        $col2_12r=$m12->cantidad_2-$m11->cantidad_2;
                        $col2_12d=$col2_12r/$m11->cantidad_2;
                        $col2_12m=$col2_12d*100;
                      }

                      if($m11->cantidad_1!=0){
                        $col1_12r=$m12->cantidad_1-$m11->cantidad_1;
                        $col1_12d=$col1_12r/$m11->cantidad_1;
                        $col1_12m=$col1_12d*100;
                      }
                      
                      $array12=[];
                      if($m12->cantidad_6!=0){
                        array_push($array12,$m12->cantidad_6);
                      }
                      if($m12->cantidad_5!=0){
                        array_push($array12,$m12->cantidad_5);
                      }
                      if($m12->cantidad_4!=0){
                        array_push($array12,$m12->cantidad_4);
                      }
                      if($m12->cantidad_3!=0){
                        array_push($array12,$m12->cantidad_3);
                      }
                      if($m12->cantidad_2!=0){
                        array_push($array12,$m12->cantidad_2);
                      }
                      if($m12->cantidad_1!=0){
                        array_push($array12,$m12->cantidad_1);
                      }
                      $pv12 = array_sum($array12)/count($array12);

                      $f12_m=0;
                      if($pv11!=0){
                        $f12_r=$pv12-$pv11;
                        $f12_d=$f12_r/$pv11;
                        $f12_m=$f12_d*100;
                      }*/
                    }
                    $html.='<tr class="tr_1">
                      <th class="th_grey">Diciembre<input type="hidden" class="id_x12" id="id_x" value="'; if(isset($m12->id)){ $html.=$m12->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="12" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="form-control" id="cantidad_6x" value="'; if(isset($m12->cantidad_6)){ $html.=$m12->cantidad_6; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col6_12m">'.round($col6_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="form-control" id="cantidad_5x" value="'; if(isset($m12->cantidad_5)){ $html.=$m12->cantidad_5; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col5_12m">'.round($col5_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="form-control" id="cantidad_4x" value="'; if(isset($m12->cantidad_4)){ $html.=$m12->cantidad_4; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col4_12m">'.round($col4_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="form-control" id="cantidad_3x" value="'; if(isset($m12->cantidad_3)){ $html.=$m12->cantidad_3; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col3_12m">'.round($col3_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="form-control" id="cantidad_2x" value="'; if(isset($m12->cantidad_2)){ $html.=$m12->cantidad_2; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col2_12m">'.round($col2_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="form-control" id="cantidad_1x" value="'; if(isset($m12->cantidad_1)){ $html.=$m12->cantidad_1; } $html.='" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col1_12m">'.round($col1_12m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv12).'</span><input type="hidden" id="promedio_x" value="'; if(isset($m12->promedio)){ $html.=$m12->promedio; } $html.='" /></th>
                      <th class="th_c f12_m">'.round($f12_m).'%</th>    
                    </tr>';
                    // suma de anios
                    $suma_col6=0;
                    $suma_col5=0;
                    $suma_col4=0;
                    $suma_col3=0;
                    $suma_col2=0;
                    $suma_col1=0;
                    /*if(isset($m12->cantidad_6)){
                      $suma_col6=$m1->cantidad_6+$m2->cantidad_6+$m3->cantidad_6+$m4->cantidad_6+$m5->cantidad_6+$m6->cantidad_6+$m7->cantidad_6+$m8->cantidad_6+$m9->cantidad_6+$m10->cantidad_6+$m11->cantidad_6+$m12->cantidad_6;
                      $suma_col5=$m1->cantidad_5+$m2->cantidad_5+$m3->cantidad_5+$m4->cantidad_5+$m5->cantidad_5+$m6->cantidad_5+$m7->cantidad_5+$m8->cantidad_5+$m9->cantidad_5+$m10->cantidad_5+$m11->cantidad_5+$m12->cantidad_5;
                      $suma_col4=$m1->cantidad_4+$m2->cantidad_4+$m3->cantidad_4+$m4->cantidad_4+$m5->cantidad_4+$m6->cantidad_4+$m7->cantidad_4+$m8->cantidad_4+$m9->cantidad_4+$m10->cantidad_4+$m11->cantidad_4+$m12->cantidad_4;
                      $suma_col3=$m1->cantidad_3+$m2->cantidad_3+$m3->cantidad_3+$m4->cantidad_3+$m5->cantidad_3+$m6->cantidad_3+$m7->cantidad_3+$m8->cantidad_3+$m9->cantidad_3+$m10->cantidad_3+$m11->cantidad_3+$m12->cantidad_3;
                      $suma_col2=$m1->cantidad_2+$m2->cantidad_2+$m3->cantidad_2+$m4->cantidad_2+$m5->cantidad_2+$m6->cantidad_2+$m7->cantidad_2+$m8->cantidad_2+$m9->cantidad_2+$m10->cantidad_2+$m11->cantidad_2+$m12->cantidad_2;
                      $suma_col1=$m1->cantidad_1+$m2->cantidad_1+$m3->cantidad_1+$m4->cantidad_1+$m5->cantidad_1+$m6->cantidad_1+$m7->cantidad_1+$m8->cantidad_1+$m9->cantidad_1+$m10->cantidad_1+$m11->cantidad_1+$m12->cantidad_1;
                    }*/
                      // suma de promedios
                    $mch6 =0;
                    $mch5 =0;
                    $mch4 =0;
                    $mch3 =0;
                    $mch2 =0;
                    $mch1 =0;
                    /*$promh6=[$col6_2m,$col6_3m,$col6_4m,$col6_5m,$col6_6m,$col6_7m,$col6_8m,$col6_9m,$col6_10m,$col6_11m,$col6_12m];
                    $mch6 = array_sum($promh6)/count($promh6);

                    $promh5=[$col5_2m,$col5_3m,$col5_4m,$col5_5m,$col5_6m,$col5_7m,$col5_8m,$col5_9m,$col5_10m,$col5_11m,$col5_12m];
                    $mch5 = array_sum($promh5)/count($promh5);

                    $promh4=[$col4_2m,$col4_3m,$col4_4m,$col4_5m,$col4_6m,$col4_7m,$col4_8m,$col4_9m,$col4_10m,$col4_11m,$col4_12m];
                    $mch4 = array_sum($promh4)/count($promh4);

                    $promh3=[$col3_2m,$col3_3m,$col3_4m,$col3_5m,$col3_6m,$col3_7m,$col3_8m,$col3_9m,$col3_10m,$col3_11m,$col3_12m];
                    $mch3 = array_sum($promh3)/count($promh3);

                    $promh2=[$col2_2m,$col2_3m,$col2_4m,$col2_5m,$col2_6m,$col2_7m,$col2_8m,$col2_9m,$col2_10m,$col2_11m,$col2_12m];
                    $mch2 = array_sum($promh2)/count($promh2);

                    $promh1=[$col1_2m,$col1_3m,$col1_4m,$col1_5m,$col1_6m,$col1_7m,$col1_8m,$col1_9m,$col1_10m,$col1_11m,$col1_12m];
                    $mch1 = array_sum($promh1)/count($promh1);*/
                    
                    $suma_pv=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+$pv9+$pv10+$pv11+$pv12;

                    $prvaf1=[$f2_m,$f3_m,$f4_m,$f5_m,$f6_m,$f7_m,$f8_m,$f9_m,$f10_m,$f11_m,$f12_m];
                    $prvf1 = array_sum($prvaf1)/count($prvaf1);

                    $html.='<tr>
                      <th class="th_grey">Total <br>general</th>
                      <th class="th_c t_col6">'.number_format(round($suma_col6),2,'.',',').'</th>
                      <th class="th_c mch6">'.round($mch6).'%</th>
                      <th class="th_c t_col5">'.number_format(round($suma_col5) ,2,'.',',').'</th>
                      <th class="th_c mch5">'.round($mch5).'%</th>
                      <th class="th_c t_col4">'.number_format(round($suma_col4),2,'.',',').'</th>
                      <th class="th_c mch4">'.round($mch4).'%</th>
                      <th class="th_c t_col3">'.number_format(round($suma_col3),2,'.',',').'</th>
                      <th class="th_c mch3">'.round($mch3).'%</th>
                      <th class="th_c t_col2">'.number_format(round($suma_col2),2,'.',',').'</th>
                      <th class="th_c mch2">'.round($mch2).'%</th>
                      <th class="th_c t_col1">'.number_format(round($suma_col1),2,'.',',').'</th>
                      <th class="th_c mch1">'.round($mch1).'%</th>
                      <th class="th_c suma_pv">'.round($suma_pv).'</th>
                      <th class="th_c prvf1">'.round($prvf1).'%</th>            
                    </tr>';

                    $ft5_m=0;
                    if($suma_col6!=0){
                      $ft5_r=$suma_col5-$suma_col6;
                      $ft5_d=$ft5_r/$suma_col6;
                      $ft5_m=$ft5_d*100;
                    }

                    $ft4_m=0;
                    if($suma_col5!=0){
                      $ft4_r=$suma_col4-$suma_col5;
                      $ft4_d=$ft4_r/$suma_col5;
                      $ft4_m=$ft4_d*100;
                    }

                    $ft3_m=0;
                    if($suma_col4!=0){
                      $ft3_r=$suma_col3-$suma_col4;
                      $ft3_d=$ft3_r/$suma_col4;
                      $ft3_m=$ft3_d*100;
                    }

                    $ft2_m=0;
                    if($suma_col3!=0){
                      $ft2_r=$suma_col2-$suma_col3;
                      $ft2_d=$ft2_r/$suma_col3;
                      $ft2_m=$ft2_d*100;
                    }

                    $ft1_m=0;
                    if($suma_col2!=0){
                      $ft1_r=$suma_col1-$suma_col2;
                      $ft1_d=$ft1_r/$suma_col2;
                      $ft1_m=$ft1_d*100;
                    }

                    $html.='<tr>
                      <th class="th_border"></th>
                      <th class="th_border"></th>
                      <th class="th_border"></th>
                      <th class="th_c ft5_m">'.round($ft5_m).'%</th>
                      <th class="th_border"></th>
                      <th class="th_c ft4_m">'.round($ft4_m).'%</th>
                      <th class="th_border"></th>
                      <th class="th_c ft3_m">'.round($ft3_m).'%</th>
                      <th class="th_border"></th>
                      <th class="th_c ft2_m">'.round($ft2_m).'%</th>
                      <th class="th_border"></th>
                      <th class="th_c ft1_m">'.round($ft1_m).'%</th>
                      <th class="th_border"></th>
                      <th class="th_border"></th>
                      <th class="th_border"></th>            
                    </tr>
                  </thead>
                </table><div class="row my-4">  
                  <div class="col-xl-12">
                    <div>
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" onclick="guardar_registro()">Guardar</button>
                      <button type="button" class="btn btn-danger me-sm-3 me-1" onclick="eliminar_registro('.$id.')">Eliminar</button>
                    </div>    
                  </div>
                </div>';
      /*}else{
        $html='<input type="hidden" id="idventashistoricas_x" value="'.$id.'" />
          <table class="table table-bordered" id="tabla_datos_1">
                  <thead>
                    <tr>
                      <th class="th_c">Mes/Año</th>
                      <th class="th_c">'.$anio_aux6.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">'.$anio_aux5.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">'.$anio_aux4.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">'.$anio_aux3.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">'.$anio_aux2.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">'.$anio_aux1.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">Promedio</th>
                      <th class="th_c"></th>
                    </tr>';
                    
                    $html.='<tr class="tr_1">
                      <th class="th_c">Enero<input type="hidden" class="id_x1" id="id_x" value="0" /><input type="hidden" id="mes_x" value="1" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(1)" oninput="calcular_promedio()" /></th>
                      <th class="th_c"></th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col5_1m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col4_1m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col3_1m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col2_1m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(1)" /></th>
                      <th class="th_c col1_1m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c"></th>            
                    </tr>';
                    
                    $html.='<tr class="tr_1">
                      <th class="th_c">Febrero<input type="hidden" class="id_x2" id="id_x" value="0" /><input type="hidden" id="mes_x" value="2" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col6_2m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col5_2m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col4_2m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col3_2m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col2_2m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(2)" /></th>
                      <th class="th_c col1_2m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f2_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Marzo<input type="hidden" class="id_x3" id="id_x" value="0" /><input type="hidden" id="mes_x" value="3" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col6_3m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col5_3m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col4_3m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col3_3m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col2_3m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(3)" /></th>
                      <th class="th_c col1_3m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f3_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Abril<input type="hidden" class="id_x4" id="id_x" value="0" /><input type="hidden" id="mes_x" value="4" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col6_4m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col5_4m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col4_4m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col3_4m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col2_4m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(4)" /></th>
                      <th class="th_c col1_4m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f4_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Mayo<input type="hidden" class="id_x5" id="id_x" value="0" /><input type="hidden" id="mes_x" value="5" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col6_5m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col5_5m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col4_5m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col3_5m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col2_5m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(5)" /></th>
                      <th class="th_c col1_5m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f5_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Junio<input type="hidden" class="id_x6" id="id_x" value="0" /><input type="hidden" id="mes_x" value="6" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col6_6m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col5_6m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col4_6m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col3_6m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col2_6m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(6)" /></th>
                      <th class="th_c col1_6m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f6_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Julio<input type="hidden" class="id_x7" id="id_x" value="0" /><input type="hidden" id="mes_x" value="7" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col6_7m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col5_7m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col4_7m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col3_7m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col2_7m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(7)" /></th>
                      <th class="th_c col1_7m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f7_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Agosto<input type="hidden" class="id_x8" id="id_x" value="0" /><input type="hidden" id="mes_x" value="8" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col6_8m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col5_8m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col4_8m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col3_8m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col2_8m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(8)" /></th>
                      <th class="th_c col1_8m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f8_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Septiembre<input type="hidden" class="id_x9" id="id_x" value="0" /><input type="hidden" id="mes_x" value="9" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col6_9m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col5_9m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col4_9m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col3_9m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col2_9m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(9)" /></th>
                      <th class="th_c col1_9m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f9_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Octubre<input type="hidden" class="id_x10" id="id_x" value="0" /><input type="hidden" id="mes_x" value="10" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col6_10m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col5_10m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col4_10m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col3_10m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col2_10m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(10)" /></th>
                      <th class="th_c col1_10m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f10_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Noviembre<input type="hidden" class="id_x11" id="id_x" value="0" /><input type="hidden" id="mes_x" value="11" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col6_11m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col5_11m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col4_11m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col3_11m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(11)" /></th>
                      <th class="th_c col2_11m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(11)" /></th>                      
                      <th class="th_c col1_11m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f11_m">0%</th>    
                    </tr>';

                    $html.='<tr class="tr_1">
                      <th class="th_c">Diciembre<input type="hidden" class="id_x12" id="id_x" value="0" /><input type="hidden" id="mes_x" value="12" /></th>
                      <th><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" class="input_vh" id="cantidad_6x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col6_12m">0%</th>
                      <th><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" class="input_vh" id="cantidad_5x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col5_12m">0%</th>
                      <th><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" class="input_vh" id="cantidad_4x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col4_12m">0%</th>
                      <th><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" class="input_vh" id="cantidad_3x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col3_12m">0%</th>
                      <th><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" class="input_vh" id="cantidad_2x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col2_12m">0%</th>
                      <th><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" class="input_vh" id="cantidad_1x" value="0" oninput="calcular_promedio(12)" /></th>
                      <th class="th_c col1_12m">0%</th>
                      <th class="th_c thpm">0</th>
                      <th class="th_c f12_m">0%</th>    
                    </tr>';
                    // suma de anios

                    $html.='<tr>
                      <th class="th_c">Total <br>general</th>
                      <th class="th_c t_col6">0</th>
                      <th class="th_c mch6">0%</th>
                      <th class="th_c t_col5">0</th>
                      <th class="th_c mch5">0%</th>
                      <th class="th_c t_col4">0</th>
                      <th class="th_c mch4">0%</th>
                      <th class="th_c t_col3">0</th>
                      <th class="th_c mch3">0%</th>
                      <th class="th_c t_col2">0</th>
                      <th class="th_c mch2">0%</th>
                      <th class="th_c t_col1">0</th>
                      <th class="th_c mch1">0%</th>
                      <th class="th_c suma_pv">0</th>
                      <th class="th_c prvf1">0%</th>            
                    </tr>';

                    $html.='<tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th class="th_c ft5_m">0%</th>
                      <th></th>
                      <th class="th_c ft4_m">0%</th>
                      <th></th>
                      <th class="th_c ft3_m">0%</th>
                      <th></th>
                      <th class="th_c ft2_m">0%</th>
                      <th></th>
                      <th class="th_c ft1_m">0%</th>
                      <th></th>
                      <th></th>
                      <th></th>            
                    </tr>
                  </thead>
                </table><div class="row my-4">  
                  <div class="col-xl-12">
                    <div>
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro" onclick="guardar_registro()">Guardar</button>
                      <button type="button" class="btn btn-danger me-sm-3 me-1" onclick="eliminar_registro('.$id.')">Eliminar</button>
                    </div>    
                  </div>
                </div>';
      }*/

      echo $html;          
    }

    public function add_data(){
        $data=$this->input->post();
        $data['reg']=$this->fechahora;
        $data['idcliente']=$this->idcliente; 
        $id=$this->General_model->add_record('ventashistoricas',$data);
    }

    function registro_datos_ventashistoricasdetalles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idventashistoricas']=$DATA[$i]->idventashistoricas;
            $data['mes']=$DATA[$i]->mes; 
            $data['anio_6']=$DATA[$i]->anio_6;
            $data['cantidad_6']=$DATA[$i]->cantidad_6;
            $data['anio_5']=$DATA[$i]->anio_5;
            $data['cantidad_5']=$DATA[$i]->cantidad_5;
            $data['anio_4']=$DATA[$i]->anio_4;
            $data['cantidad_4']=$DATA[$i]->cantidad_4;
            $data['anio_3']=$DATA[$i]->anio_3;
            $data['cantidad_3']=$DATA[$i]->cantidad_3;
            $data['anio_2']=$DATA[$i]->anio_2;
            $data['cantidad_2']=$DATA[$i]->cantidad_2;
            $data['anio_1']=$DATA[$i]->anio_1;
            $data['cantidad_1']=$DATA[$i]->cantidad_1;
            $data['promedio']=$DATA[$i]->promedio;
            if($DATA[$i]->id==0){
                $id=$this->General_model->add_record('ventashistoricas_detalles',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'ventashistoricas_detalles');
            }
            
        }
    }
    
    public function delete_record(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'ventashistoricas');
    }

    public function update_nombre(){
        $id=$this->input->post('id');
        $nombre=$this->input->post('nombre');
        $data = array('nombre'=>$nombre);
        $this->General_model->edit_record('id',$id,$data,'ventashistoricas');
    }

    function inser_registro_ventashistoricasdetalles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $id=0;
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idventashistoricas']=$DATA[$i]->idventashistoricas;
            $data['mes']=$DATA[$i]->mes; 
            $data['anio_6']=$DATA[$i]->anio_6;
            $data['cantidad_6']=$DATA[$i]->cantidad_6;
            $data['anio_5']=$DATA[$i]->anio_5;
            $data['cantidad_5']=$DATA[$i]->cantidad_5;
            $data['anio_4']=$DATA[$i]->anio_4;
            $data['cantidad_4']=$DATA[$i]->cantidad_4;
            $data['anio_3']=$DATA[$i]->anio_3;
            $data['cantidad_3']=$DATA[$i]->cantidad_3;
            $data['anio_2']=$DATA[$i]->anio_2;
            $data['cantidad_2']=$DATA[$i]->cantidad_2;
            $data['anio_1']=$DATA[$i]->anio_1;
            $data['cantidad_1']=$DATA[$i]->cantidad_1;
            $data['promedio']=$DATA[$i]->promedio;
            if($DATA[$i]->id==0){
                $id=$this->General_model->add_record('ventashistoricas_detalles',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'ventashistoricas_detalles');
                $id=$DATA[$i]->id;
            }
        }
        echo $id;
    }

    function get_ventas_ingresos()
    {
      $id = 1;
      $c=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
      $anio_cli=date('Y',strtotime($c->fecha_operacion));
      $anio_aux6=$anio_cli-6;
      $anio_aux5=$anio_cli-5;
      $anio_aux4=$anio_cli-4;
      $anio_aux3=$anio_cli-3;
      $anio_aux2=$anio_cli-2;
      $anio_aux1=$anio_cli-1;
      $m1=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,1);
      $m2=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,2);
      $m3=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,3);
      $m4=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,4);
      $m5=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,5);
      $m6=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,6);
      $m7=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,7);
      $m8=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,8);
      $m9=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,9);
      $m10=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,10);
      $m11=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,11);
      $m12=$this->Model_ventashistoricas->get_ventasingresos($this->idcliente,12);
      //if(isset($m1)){
        $html='<input type="hidden" id="idventashistoricas_x" value="'.$id.'" />
          <table class="table table-bordered table_info" id="tabla_datos_1">
                  <thead>
                    <tr>
                      <th class="th_c">Mes/Año</th>
                      <th class="th_c anio_aux6" data-anio="'.$anio_aux6.'">'.$anio_aux6.'</th>
                      <th class="th_c"></th>
                      <th class="th_c anio_aux5" data-anio="'.$anio_aux5.'">'.$anio_aux5.'</th>
                      <th class="th_c"></th>
                      <th class="th_c anio_aux4" data-anio="'.$anio_aux4.'">'.$anio_aux4.'</th>
                      <th class="th_c"></th>
                      <th class="th_c anio_aux3" data-anio="'.$anio_aux3.'">'.$anio_aux3.'</th>
                      <th class="th_c"></th>
                      <th class="th_c anio_aux2" data-anio="'.$anio_aux2.'">'.$anio_aux2.'</th>
                      <th class="th_c"></th>
                      <th class="th_c anio_aux1" data-anio="'.$anio_aux1.'">'.$anio_aux1.'</th>
                      <th class="th_c"></th>
                      <th class="th_c">Promedio</th>
                      <th class="th_c"></th>
                    </tr>';

                    $col5_1m=0;
                    $col4_1m=0;
                    $col3_1m=0;
                    $col2_1m=0;
                    $col1_1m=0;
                    $pv1 = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Enero<input type="hidden" class="id_x1" class="id_x1" id="id_x" value="'; if(isset($m1->id)){ $html.=$m1->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="1" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m1->cantidad_6)){ $html.=$m1->cantidad_6; } $html.='" /></th>
                      <th class="th_c"></th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m1->cantidad_5)){ $html.=$m1->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_1m">'.round($col5_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m1->cantidad_4)){ $html.=$m1->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_1m">'.round($col4_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m1->cantidad_3)){ $html.=$m1->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_1m">'.round($col3_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m1->cantidad_2)){ $html.=$m1->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_1m">'.round($col2_1m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m1->cantidad_1)){ $html.=$m1->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_1m">'.round($col1_1m).'%</th>
                      <th class="th_c thpm"><span class="thpm">'.round($pv1).'</span</th>
                      <th class="th_c"></th>            
                    </tr>';
                    
                    $col6_2m=0;
                    $col5_2m=0;
                    $col4_2m=0;
                    $col3_2m=0;
                    $col2_2m=0;
                    $col1_2m=0;
                    $pv2 = 0;
                    $f2_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Febrero<input type="hidden" class="id_x2" id="id_x" value="'; if(isset($m2->id)){ $html.=$m2->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="2" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m2->cantidad_6)){ $html.=$m2->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_2m">'.round($col6_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m2->cantidad_5)){ $html.=$m2->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_2m">'.round($col5_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m2->cantidad_4)){ $html.=$m2->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_2m">'.round($col4_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m2->cantidad_3)){ $html.=$m2->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_2m">'.round($col3_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m2->cantidad_2)){ $html.=$m2->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_2m">'.round($col2_2m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m2->cantidad_1)){ $html.=$m2->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_2m">'.round($col1_2m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv2).'</span></th>
                      <th class="th_c f2_m">'.round($f2_m).'%</th>    
                    </tr>';
                    
                    $col6_3m=0;
                    $col5_3m=0;
                    $col4_3m=0;
                    $col3_3m=0;
                    $col2_3m=0;
                    $col1_3m=0;

                    $pv3 = 0;
                    $f3_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Marzo<input type="hidden" class="id_x3" id="id_x" value="'; if(isset($m3->id)){ $html.=$m3->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="3" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m3->cantidad_6)){ $html.=$m3->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_3m">'.round($col6_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m3->cantidad_5)){ $html.=$m3->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_3m">'.round($col5_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m3->cantidad_4)){ $html.=$m3->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_3m">'.round($col4_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m3->cantidad_3)){ $html.=$m3->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_3m">'.round($col3_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m3->cantidad_2)){ $html.=$m3->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_3m">'.round($col2_3m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m3->cantidad_1)){ $html.=$m3->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_3m">'.round($col1_3m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv3).'</span></th>
                      <th class="th_c f3_m">'.round($f3_m).'%</th>    
                    </tr>';
                    
                    $col6_4m=0;
                    $col5_4m=0;
                    $col4_4m=0;
                    $col3_4m=0;
                    $col2_4m=0;
                    $col1_4m=0;

                    $pv4 = 0;
                    $f4_m = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Abril<input type="hidden" class="id_x4" id="id_x" value="'; if(isset($m4->id)){ $html.=$m4->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="4" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m4->cantidad_6)){ $html.=$m4->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_4m">'.round($col6_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m4->cantidad_5)){ $html.=$m4->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_4m">'.round($col5_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m4->cantidad_4)){ $html.=$m4->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_4m">'.round($col4_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m4->cantidad_3)){ $html.=$m4->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_4m">'.round($col3_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m4->cantidad_2)){ $html.=$m4->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_4m">'.round($col2_4m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m4->cantidad_1)){ $html.=$m4->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_4m">'.round($col1_4m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv4).'</span></th>
                      <th class="th_c f4_m">'.round($f4_m).'%</th>    
                    </tr>';

                    $col6_5m=0;
                    $col5_5m=0;
                    $col4_5m=0;
                    $col3_5m=0;
                    $col2_5m=0;
                    $col1_5m=0;
                    $pv5 = 0;
                    $f5_m = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Mayo<input type="hidden" class="id_x5" id="id_x" value="'; if(isset($m5->id)){ $html.=$m5->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="5" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m5->cantidad_6)){ $html.=$m5->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_5m">'.round($col6_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m5->cantidad_5)){ $html.=$m5->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_5m">'.round($col5_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m5->cantidad_4)){ $html.=$m5->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_5m">'.round($col4_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m5->cantidad_3)){ $html.=$m5->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_5m">'.round($col3_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m5->cantidad_2)){ $html.=$m5->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_5m">'.round($col2_5m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m5->cantidad_1)){ $html.=$m5->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_5m">'.round($col1_5m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv5).'</span></th>
                      <th class="th_c f5_m">'.round($f5_m).'%</th>    
                    </tr>';
                    
                    $col6_6m=0;
                    $col5_6m=0;
                    $col4_6m=0;
                    $col3_6m=0;
                    $col2_6m=0;
                    $col1_6m=0;
                    $pv6 = 0;
                    $f6_m = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Junio<input type="hidden" class="id_x6" id="id_x" value="'; if(isset($m6->id)){ $html.=$m6->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="6" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m6->cantidad_6)){ $html.=$m6->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_6m">'.round($col6_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m6->cantidad_5)){ $html.=$m6->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_6m">'.round($col5_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m6->cantidad_4)){ $html.=$m6->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_6m">'.round($col4_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m6->cantidad_3)){ $html.=$m6->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_6m">'.round($col3_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m6->cantidad_2)){ $html.=$m6->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_6m">'.round($col2_6m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m6->cantidad_1)){ $html.=$m6->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_6m">'.round($col1_6m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv6).'</span></th>
                      <th class="th_c f6_m">'.round($f6_m).'%</th>    
                    </tr>';

                    $col6_7m=0;
                    $col5_7m=0;
                    $col4_7m=0;
                    $col3_7m=0;
                    $col2_7m=0;
                    $col1_7m=0;
                    $pv7 = 0;
                    $f7_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Julio<input type="hidden" class="id_x7" id="id_x" value="'; if(isset($m7->id)){ $html.=$m7->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="7" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m7->cantidad_6)){ $html.=$m7->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_7m">'.round($col6_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m7->cantidad_5)){ $html.=$m7->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_7m">'.round($col5_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m7->cantidad_4)){ $html.=$m7->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_7m">'.round($col4_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m7->cantidad_3)){ $html.=$m7->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_7m">'.round($col3_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m7->cantidad_2)){ $html.=$m7->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_7m">'.round($col2_7m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m7->cantidad_1)){ $html.=$m7->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_7m">'.round($col1_7m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv7).'</span></th>
                      <th class="th_c f7_m">'.round($f7_m).'%</th>    
                    </tr>';

                    $col6_8m=0;
                    $col5_8m=0;
                    $col4_8m=0;
                    $col3_8m=0;
                    $col2_8m=0;
                    $col1_8m=0;
                    $pv8 = 0;
                    $f8_m = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Agosto<input type="hidden" class="id_x8" id="id_x" value="'; if(isset($m8->id)){ $html.=$m8->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="8" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m8->cantidad_6)){ $html.=$m8->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_8m">'.round($col6_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m8->cantidad_5)){ $html.=$m8->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_8m">'.round($col5_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m8->cantidad_4)){ $html.=$m8->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_8m">'.round($col4_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m8->cantidad_3)){ $html.=$m8->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_8m">'.round($col3_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m8->cantidad_2)){ $html.=$m8->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_8m">'.round($col2_8m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m8->cantidad_1)){ $html.=$m8->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_8m">'.round($col1_8m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv8).'</span></th>
                      <th class="th_c f8_m">'.round($f8_m).'%</th>    
                    </tr>';

                    $col6_9m=0;
                    $col5_9m=0;
                    $col4_9m=0;
                    $col3_9m=0;
                    $col2_9m=0;
                    $col1_9m=0;
                    $pv9 = 0;
                    $f9_m = 0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Septiembre<input type="hidden" class="id_x9" id="id_x" value="'; if(isset($m9->id)){ $html.=$m9->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="9" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m9->cantidad_6)){ $html.=$m9->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_9m">'.round($col6_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m9->cantidad_5)){ $html.=$m9->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_9m">'.round($col5_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m9->cantidad_4)){ $html.=$m9->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_9m">'.round($col4_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m9->cantidad_3)){ $html.=$m9->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_9m">'.round($col3_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m9->cantidad_2)){ $html.=$m9->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_9m">'.round($col2_9m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m9->cantidad_1)){ $html.=$m9->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_9m">'.round($col1_9m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv9).'</span></th>
                      <th class="th_c f9_m">'.round($f9_m).'%</th>    
                    </tr>';

                    $col6_10m=0;
                    $col5_10m=0;
                    $col4_10m=0;
                    $col3_10m=0;
                    $col2_10m=0;
                    $col1_10m=0;
                    $pv10 = 0;
                    $f10_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Octubre<input type="hidden" class="id_x10" id="id_x" value="'; if(isset($m10->id)){ $html.=$m10->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="10" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m10->cantidad_6)){ $html.=$m10->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_10m">'.round($col6_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m10->cantidad_5)){ $html.=$m10->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_10m">'.round($col5_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m10->cantidad_4)){ $html.=$m10->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_10m">'.round($col4_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m10->cantidad_3)){ $html.=$m10->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_10m">'.round($col3_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m10->cantidad_2)){ $html.=$m10->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_10m">'.round($col2_10m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m10->cantidad_1)){ $html.=$m10->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_10m">'.round($col1_10m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv10).'</span></th>
                      <th class="th_c f10_m">'.round($f10_m).'%</th>    
                    </tr>';

                    $col6_11m=0;
                    $col5_11m=0;
                    $col4_11m=0;
                    $col3_11m=0;
                    $col2_11m=0;
                    $col1_11m=0;
                    $pv11 =0;
                    $f11_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Noviembre<input type="hidden" class="id_x11" id="id_x" value="'; if(isset($m11->id)){ $html.=$m11->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="11" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m11->cantidad_6)){ $html.=$m11->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_11m">'.round($col6_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m11->cantidad_5)){ $html.=$m11->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_11m">'.round($col5_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m11->cantidad_4)){ $html.=$m11->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_11m">'.round($col4_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m11->cantidad_3)){ $html.=$m11->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_11m">'.round($col3_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m11->cantidad_2)){ $html.=$m11->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_11m">'.round($col2_11m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m11->cantidad_1)){ $html.=$m11->cantidad_1; } $html.='" /></th>                      
                      <th class="th_c col1_11m">'.round($col1_11m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv11).'</span></th>
                      <th class="th_c f11_m">'.round($f11_m).'%</th>    
                    </tr>';

                    $col6_12m=0;
                    $col5_12m=0;
                    $col4_12m=0;
                    $col3_12m=0;
                    $col2_12m=0;
                    $col1_12m=0;
                    $pv12 =
                    $f12_m=0;

                    $html.='<tr class="tr_1">
                      <th class="th_c">Diciembre<input type="hidden" class="id_x12" id="id_x" value="'; if(isset($m12->id)){ $html.=$m12->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="mes_x" value="12" /></th>
                      <th class="th_input"><input type="hidden" id="anio6_x" value="'.$anio_aux6.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux6.'" id="cantidad_6x" value="'; if(isset($m12->cantidad_6)){ $html.=$m12->cantidad_6; } $html.='" /></th>
                      <th class="th_c col6_12m">'.round($col6_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio5_x" value="'.$anio_aux5.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux5.'" id="cantidad_5x" value="'; if(isset($m12->cantidad_5)){ $html.=$m12->cantidad_5; } $html.='" /></th>
                      <th class="th_c col5_12m">'.round($col5_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio4_x" value="'.$anio_aux4.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux4.'" id="cantidad_4x" value="'; if(isset($m12->cantidad_4)){ $html.=$m12->cantidad_4; } $html.='" /></th>
                      <th class="th_c col4_12m">'.round($col4_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio3_x" value="'.$anio_aux3.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux3.'" id="cantidad_3x" value="'; if(isset($m12->cantidad_3)){ $html.=$m12->cantidad_3; } $html.='" /></th>
                      <th class="th_c col3_12m">'.round($col3_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio2_x" value="'.$anio_aux2.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux2.'" id="cantidad_2x" value="'; if(isset($m12->cantidad_2)){ $html.=$m12->cantidad_2; } $html.='" /></th>
                      <th class="th_c col2_12m">'.round($col2_12m).'%</th>
                      <th class="th_input"><input type="hidden" id="anio1_x" value="'.$anio_aux1.'" /><input type="number" readonly class="form-control v_anio_'.$anio_aux1.'" id="cantidad_1x" value="'; if(isset($m12->cantidad_1)){ $html.=$m12->cantidad_1; } $html.='" /></th>
                      <th class="th_c col1_12m">'.round($col1_12m).'%</th>
                      <th class="th_c"><span class="thpm">'.round($pv12).'</span></th>
                      <th class="th_c f12_m">'.round($f12_m).'%</th>    
                    </tr>';
                    // suma de anios
                    $suma_col6=0;
                    $suma_col5=0;
                    $suma_col4=0;
                    $suma_col3=0;
                    $suma_col2=0;
                    $suma_col1=0;

                    $mch6 =0;
                    $mch5 =0;
                    $mch4 =0;
                    $mch3 =0;
                    $mch2 =0;
                    $mch1 =0;

                    
                    $suma_pv=$pv1+$pv2+$pv3+$pv4+$pv5+$pv6+$pv7+$pv8+$pv9+$pv10+$pv11+$pv12;

                    $prvaf1=[$f2_m,$f3_m,$f4_m,$f5_m,$f6_m,$f7_m,$f8_m,$f9_m,$f10_m,$f11_m,$f12_m];
                    $prvf1 = array_sum($prvaf1)/count($prvaf1);

                    $html.='<tr>
                      <th class="th_c">Total <br>general</th>
                      <th class="th_c t_col6">'.number_format(round($suma_col6),2,'.',',').'</th>
                      <th class="th_c mch6">'.round($mch6).'%</th>
                      <th class="th_c t_col5">'.number_format(round($suma_col5) ,2,'.',',').'</th>
                      <th class="th_c mch5">'.round($mch5).'%</th>
                      <th class="th_c t_col4">'.number_format(round($suma_col4),2,'.',',').'</th>
                      <th class="th_c mch4">'.round($mch4).'%</th>
                      <th class="th_c t_col3">'.number_format(round($suma_col3),2,'.',',').'</th>
                      <th class="th_c mch3">'.round($mch3).'%</th>
                      <th class="th_c t_col2">'.number_format(round($suma_col2),2,'.',',').'</th>
                      <th class="th_c mch2">'.round($mch2).'%</th>
                      <th class="th_c t_col1">'.number_format(round($suma_col1),2,'.',',').'</th>
                      <th class="th_c mch1">'.round($mch1).'%</th>
                      <th class="th_c suma_pv">'.round($suma_pv).'</th>
                      <th class="th_c prvf1">'.round($prvf1).'%</th>            
                    </tr>';

                    $ft5_m=0;
                    if($suma_col6!=0){
                      $ft5_r=$suma_col5-$suma_col6;
                      $ft5_d=$ft5_r/$suma_col6;
                      $ft5_m=$ft5_d*100;
                    }

                    $ft4_m=0;
                    if($suma_col5!=0){
                      $ft4_r=$suma_col4-$suma_col5;
                      $ft4_d=$ft4_r/$suma_col5;
                      $ft4_m=$ft4_d*100;
                    }

                    $ft3_m=0;
                    if($suma_col4!=0){
                      $ft3_r=$suma_col3-$suma_col4;
                      $ft3_d=$ft3_r/$suma_col4;
                      $ft3_m=$ft3_d*100;
                    }

                    $ft2_m=0;
                    if($suma_col3!=0){
                      $ft2_r=$suma_col2-$suma_col3;
                      $ft2_d=$ft2_r/$suma_col3;
                      $ft2_m=$ft2_d*100;
                    }

                    $ft1_m=0;
                    if($suma_col2!=0){
                      $ft1_r=$suma_col1-$suma_col2;
                      $ft1_d=$ft1_r/$suma_col2;
                      $ft1_m=$ft1_d*100;
                    }

                    $html.='<tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th class="th_c ft5_m">'.round($ft5_m).'%</th>
                      <th></th>
                      <th class="th_c ft4_m">'.round($ft4_m).'%</th>
                      <th></th>
                      <th class="th_c ft3_m">'.round($ft3_m).'%</th>
                      <th></th>
                      <th class="th_c ft2_m">'.round($ft2_m).'%</th>
                      <th></th>
                      <th class="th_c ft1_m">'.round($ft1_m).'%</th>
                      <th></th>
                      <th></th>
                      <th></th>            
                    </tr>
                  </thead>
                </table>';


      echo $html;          
    }

    function get_resumen_presupuesto()
    { 

      $id = $this->input->post('id');
      $c=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
      $anio_cli=date('Y',strtotime($c->fecha_operacion));
      $suma_anio=$anio_cli+3; 

        $html='<div class="nav-align-left mb-4">
          <ul class="nav nav-pills me-3" role="tablist">';
            for($i=$anio_cli; $i<=$suma_anio; $i++){ 
              $vhr=$this->General_model->getselectwhererow('ventashistoricas_resumen',array('idcliente'=>$this->idcliente,'anio'=>$i,'activo'=>1));
              $idx=0;
              if(isset($vhr->id)){ $idx=$vhr->id; }else{ $idx=0; }
              $html.='<li class="nav-item"><input type="hidden" class="id_r_'.$i.'" value="'.$idx.'">
                <button
                  type="button"
                  class="nav-link"
                  role="tab"
                  data-bs-toggle="tab"
                  data-bs-target="#navs_'.$i.'"
                  aria-controls="navs_'.$i.'"
                  aria-selected="true"
                  onclick="get_ventasresumendetalles('.$i.')">
                  <span class="tt_p_'.$i.'">'.$i.'</span>
                </button>
              </li>';
            }
          $html.='</ul>';

          $html.='<div class="tab-content">';
            for($ix=$anio_cli; $ix<=$suma_anio; $ix++){ 
              $html.='<div class="tab-pane fade" id="navs_'.$ix.'" role="tabpanel">
                <h4 class="txt_titulo">Año:'.$ix.'</h4>
                <br>
                <div class="text_tabla_ventas_resumen_detalles text_tabla_ventas_resumen_detalles_'.$ix.'"></div>
              </div>';
            }
          $html.='</div>
          </div>';
      echo $html;
    }

    function get_ventas_resumen_detalles()
    {
      $anio = $this->input->post('anio');
      $c=$this->General_model->getselectwhererow('ventashistoricas_resumen',array('idcliente'=>$this->idcliente,'anio'=>$anio,'activo'=>1));
      $aniox=substr($anio,-2);
        $html='<input type="hidden" id="id_ventashistoricas_resumenr" value="'.$c->id.'" />
          <table class="table table-bordered table_info" id="tabla_datos_resumen">
                  <thead>
                    <tr>
                      <th class="th_c">Ventas</th>
                      <th colspan="2" class="th_c">Ene-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Feb-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Mar-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Abr-'.$aniox.'</th>
                      <th colspan="2" class="th_c">May-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Jun-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Jul-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Ago-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Sep-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Oct-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Nov-'.$aniox.'</th>
                      <th colspan="2" class="th_c">Dic-'.$aniox.'</th>
                      <th class="th_c">Total año</th>
                    </tr>';

                    $result_vh=$this->General_model->getselectwheren('ventashistoricas',array('idcliente'=>$this->idcliente,'activo'=>1));
                    $contx=1;
                    foreach ($result_vh->result() as $vh){
                      $rd=$this->Model_ventashistoricas->get_resumen($vh->id,$this->idcliente,$anio);
                      $html.='<tr class="tr_r">
                        <th class="th_c">'.$vh->nombre.'<input type="hidden" class="idventashistoricas_r" id="idventashistoricas_r" value="'.$vh->id.'"><input type="hidden" class="id_rd'.$contx.'" id="id_r" value="'; if(isset($rd->id)){ $html.=$rd->id; }else{ $html.=0; } $html.='" /></th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_1r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad1)){ $html.=$rd->cantidad1; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc1">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_2r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad2)){ $html.=$rd->cantidad2; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc2">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_3r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad3)){ $html.=$rd->cantidad3; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc3">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_4r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad4)){ $html.=$rd->cantidad4; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc4">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_5r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad5)){ $html.=$rd->cantidad5; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc5">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_6r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad6)){ $html.=$rd->cantidad6; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc6">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_7r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad7)){ $html.=$rd->cantidad7; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc7">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_8r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad8)){ $html.=$rd->cantidad8; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc8">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_9r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad9)){ $html.=$rd->cantidad9; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc9">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_10r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad10)){ $html.=$rd->cantidad10; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc10">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_11r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad11)){ $html.=$rd->cantidad11; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc11">0%</th>
                        <th class="th_input"><input type="number" class="form-control" id="cantidad_12r" oninput="calcular_resumen('.$contx.')" value="'; if(isset($rd->cantidad12)){ $html.=$rd->cantidad12; }else{ $html.=0;} $html.='" /></th>
                        <th class="th_c pc12">0%</th>
                        <th class="th_c totalanio"></th>            
                      </tr>';
                      $contx++;
                    }

                    $html.='<tr>
                      <th class="th_c">Total</th>
                      <th class="th_c suma_1">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_2">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_3">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_4">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_5">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_6">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_7">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_8">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_9">0</th>            
                      <th class="th_c">100%</th>
                      <th class="th_c suma_10">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_11">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c suma_12">0</th>
                      <th class="th_c">100%</th>
                      <th class="th_c">0</th>
                    </tr>';

                    $html.='
                  </thead>
                </table>';

      echo $html;          
    }

    public function add_data_resumen(){
      $anio=$this->input->post('anio');
      $data['anio']=$anio;
      $data['idcliente']=$this->idcliente;
      $id=$this->General_model->add_record('ventashistoricas_resumen',$data);
      echo $id;
    }

    function inser_registro_resumen(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $id=0;
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idventashistoricas_resumen']=$DATA[$i]->idventashistoricas_resumen;
            $data['idventashistoricas']=$DATA[$i]->idventashistoricas; 
            $data['cantidad1']=$DATA[$i]->cantidad1;
            $data['cantidad2']=$DATA[$i]->cantidad2;
            $data['cantidad3']=$DATA[$i]->cantidad3;
            $data['cantidad4']=$DATA[$i]->cantidad4;
            $data['cantidad5']=$DATA[$i]->cantidad5;
            $data['cantidad6']=$DATA[$i]->cantidad6;
            $data['cantidad7']=$DATA[$i]->cantidad7;
            $data['cantidad8']=$DATA[$i]->cantidad8;
            $data['cantidad9']=$DATA[$i]->cantidad9;
            $data['cantidad10']=$DATA[$i]->cantidad10;
            $data['cantidad11']=$DATA[$i]->cantidad11;
            $data['cantidad12']=$DATA[$i]->cantidad12;
            if($DATA[$i]->id==0){
                $id=$this->General_model->add_record('ventashistoricas_resumen_detalles',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'ventashistoricas_resumen_detalles');
                $id=$DATA[$i]->id;
            }
        }
        echo $id;
    }

    function get_datos_proyeccion()
    {   
        $result=$this->General_model->getselectwheren('ventashistoricas',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html='<div class="nav-align-left mb-4">
          <ul class="nav nav-pills me-3" role="tablist">';
            $cont=0;
            foreach ($result->result() as $p){
              $html.='<li class="nav-item">
                <button
                  type="button"
                  class="nav-link"
                  role="tab"
                  data-bs-toggle="tab"
                  data-bs-target="#navsp_'.$p->id.'"
                  aria-controls="navsp_'.$p->id.'"
                  aria-selected="true"
                  onclick="get_proyeccion_ventas_detalles('.$p->id.')">
                  <span class="tt_pp_'.$p->id.'">'.$p->nombre.'</span>
                </button>
              </li>';
              $cont++;
            }
          $html.='</ul>
          <div class="tab-content">';
            $contx=0;
            foreach ($result->result() as $px){ 
              $html.='<div class="tab-pane fade" id="navsp_'.$px->id.'" role="tabpanel">
                <h4 class="txt_titulo">Linea Servicio / Producto: '.$px->nombre.'</h4>
                <br>
                <div class="text_tabla_proyeccion_ventas_detalles text_tabla_proyeccion_ventas_detalles_'.$px->id.'"></div>
              </div>';
              $contx++;
            }
          $html.='</div>
          </div>';
        echo $html;
    }
    
    function get_proyeccion_ventas_detalles()
    {
      $id = $this->input->post('id');
      $c=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
      $anio_cli=date('Y',strtotime($c->fecha_operacion));
      $anio_aux3=$anio_cli-3;
      $anio_aux2=$anio_cli-2;
      $anio_aux1=$anio_cli-1;
      
      $anio_2=$anio_cli+1;
      $anio_3=$anio_cli+2;
      $m1=$this->Model_ventashistoricas->get_ventas_cantidad($this->idcliente,$anio_aux1,$id);

      $p1=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,1);
      $p2=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,2);
      $p3=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,3);
      $p4=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,4);
      $p5=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,5);
      $p6=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,6);
      $p7=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,7);
      $p8=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,8);
      $p9=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,9);
      $p10=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,10);
      $p11=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,11);
      $p12=$this->Model_ventashistoricas->get_ventas_promediob($this->idcliente,$anio_aux1,$id,12);

      $tg=0;
      if(isset($m1->cantidad_1)){ $tg=$m1->cantidad_1; }else{ $tg=0; }
      
      $pr1=0;
      $pr2=0;
      $pr3=0;
      $pr4=0;
      $pr5=0;
      $pr6=0;
      $pr7=0;
      $pr8=0;
      $pr9=0;
      $pr10=0;
      $pr11=0;
      $pr12=0;

      if(isset($p1->promedio)){ $pr1=$p1->promedio; }else{ $pr1=0; }
      if(isset($p2->promedio)){ $pr2=$p2->promedio; }else{ $pr2=0; }
      if(isset($p3->promedio)){ $pr3=$p3->promedio; }else{ $pr3=0; }
      if(isset($p4->promedio)){ $pr4=$p4->promedio; }else{ $pr4=0; }
      if(isset($p5->promedio)){ $pr5=$p5->promedio; }else{ $pr5=0; }
      if(isset($p6->promedio)){ $pr6=$p6->promedio; }else{ $pr6=0; }
      if(isset($p7->promedio)){ $pr7=$p7->promedio; }else{ $pr7=0; }
      if(isset($p8->promedio)){ $pr8=$p8->promedio; }else{ $pr8=0; }
      if(isset($p9->promedio)){ $pr9=$p9->promedio; }else{ $pr9=0; }
      if(isset($p10->promedio)){ $pr10=$p10->promedio; }else{ $pr10=0; }
      if(isset($p11->promedio)){ $pr11=$p11->promedio; }else{ $pr11=0; }
      if(isset($p12->promedio)){ $pr12=$p12->promedio; }else{ $pr12=0; }

      $ca1=0;
      $ca2=0; 
      $ca3=0;
      $ca4=0;
      $ca5=0;
      $ca6=0;
      $ca7=0;
      $ca8=0;
      $ca9=0;
      $ca10=0;
      $ca11=0;
      $ca12=0;
      
      if(isset($p1->cantidad_1)){ $ca1=$p1->cantidad_1; }else{ $ca1=0; }
      if(isset($p2->cantidad_1)){ $ca2=$p2->cantidad_1; }else{ $ca2=0; }
      if(isset($p3->cantidad_1)){ $ca3=$p3->cantidad_1; }else{ $ca3=0; }
      if(isset($p4->cantidad_1)){ $ca4=$p4->cantidad_1; }else{ $ca4=0; }
      if(isset($p5->cantidad_1)){ $ca5=$p5->cantidad_1; }else{ $ca5=0; }
      if(isset($p6->cantidad_1)){ $ca6=$p6->cantidad_1; }else{ $ca6=0; }
      if(isset($p7->cantidad_1)){ $ca7=$p7->cantidad_1; }else{ $ca7=0; }
      if(isset($p8->cantidad_1)){ $ca8=$p8->cantidad_1; }else{ $ca8=0; }
      if(isset($p9->cantidad_1)){ $ca9=$p9->cantidad_1; }else{ $ca9=0; }
      if(isset($p10->cantidad_1)){ $ca10=$p10->cantidad_1; }else{ $ca10=0; }
      if(isset($p11->cantidad_1)){ $ca11=$p11->cantidad_1; }else{ $ca11=0; }
      if(isset($p12->cantidad_1)){ $ca12=$p12->cantidad_1; }else{ $ca12=0; }
     
      $html='<input type="hidden" id="idventashistoricas_x" value="'.$id.'" />
          <table class="table table-bordered table_info">
            <thead>
              <tr>
                <th class="th_border">';
                  $pre_t_1=$this->Model_ventashistoricas->get_proyeccion_venta($this->idcliente,$id,1);

                  $pv_t1_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,1);
                  $pv_t2_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,2);
                  $pv_t3_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,3);
                  $pv_t4_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,4);
                  $pv_t5_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,5);
                  $pv_t6_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,6);
                  $pv_t7_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,7);
                  $pv_t8_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,8);
                  $pv_t9_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,9);
                  $pv_t10_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,10);
                  $pv_t11_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,11);
                  $pv_t12_1=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,1,12);

                  $html.='<input type="hidden" id="id_vhpv" value="'; if(isset($pre_t_1->id)){ $html.=$pre_t_1->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="anio_t1" value="'; if(isset($pre_t_1->anio)){ $html.=$pre_t_1->anio; }else{ $html.=$anio_cli; } $html.='" />
                    <table class="table table-bordered table_info" id="tabla_p_1"> 
                    <thead>
                      <tr>
                        <th class="th_border"></th>                     
                        <th class="th_purple" colspan="5">Presupuesto '.$anio_cli.'</th>
                      </tr>
                      <tr>
                        <th class="th_border"></th>                     
                        <th class="th_grey">Opción A</th>
                        <th class="th_grey">Opción B</th>
                        <th class="th_grey" colspan="2">Opción C</th>
                        <th class="th_grey" rowspan="2">¿Qué opción<br>seleccionaste?</th>
                      </tr>
                      <tr>
                        <th class="th_border"></th>                     
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey" colspan="2">Proyectado</th>
                      </tr>
                      <tr>
                        <th class="th_grey">Mes/Año</th>                     
                        <th class="th_grey">'.$anio_cli.'</th>
                        <th class="th_grey">'.$anio_cli.'</th>
                        <th class="th_grey">%</th>
                        <th class="th_grey">'.$anio_cli.'</th>
                        <th class="th_orange th_input">
                          <select class="form-select" id="sct1" onchange="calcular_proyeccion1()">
                            <option value="1" '; if(isset($pre_t_1->opcion)){ if($pre_t_1->opcion==1){ $html.='selected'; }} $html.='>A</option>
                            <option value="2" '; if(isset($pre_t_1->opcion)){ if($pre_t_1->opcion==2){ $html.='selected'; }} $html.='>B</option>
                            <option value="3" '; if(isset($pre_t_1->opcion)){ if($pre_t_1->opcion==3){ $html.='selected'; }} $html.='>C</option>
                          </select>
                        </th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Enero</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ></div><input type="hidden" id="pr_b1_1" value="'.$pr1.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t1_1->id)){ $html.=$pv_t1_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="1" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t1_1->opcion_porcentaje_c)){ $html.=$pv_t1_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca1.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Febrero</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr2.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t2_1->id)){ $html.=$pv_t2_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="2" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t2_1->opcion_porcentaje_c)){ $html.=$pv_t2_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca2.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Marzo</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr3.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t3_1->id)){ $html.=$pv_t3_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="3" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t3_1->opcion_porcentaje_c)){ $html.=$pv_t3_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca3.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Abril</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr4.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t4_1->id)){ $html.=$pv_t4_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="4" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t4_1->opcion_porcentaje_c)){ $html.=$pv_t4_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca4.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Mayo</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr5.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t5_1->id)){ $html.=$pv_t5_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="5" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t5_1->opcion_porcentaje_c)){ $html.=$pv_t5_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca5.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Junio</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr6.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t6_1->id)){ $html.=$pv_t6_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="6" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t6_1->opcion_porcentaje_c)){ $html.=$pv_t6_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca6.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Julio</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr7.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t7_1->id)){ $html.=$pv_t7_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="7" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t7_1->opcion_porcentaje_c)){ $html.=$pv_t7_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca7.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Agosto</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr8.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t8_1->id)){ $html.=$pv_t8_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="8" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t8_1->opcion_porcentaje_c)){ $html.=$pv_t8_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca8.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Septiembre</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr9.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t9_1->id)){ $html.=$pv_t9_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="9" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t9_1->opcion_porcentaje_c)){ $html.=$pv_t9_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca9.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Octubre</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr10.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t10_1->id)){ $html.=$pv_t10_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="10" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t10_1->opcion_porcentaje_c)){ $html.=$pv_t10_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca10.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Noviembre</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr11.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t11_1->id)){ $html.=$pv_t11_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="11" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t11_1->opcion_porcentaje_c)){ $html.=$pv_t11_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca11.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr class="tr_pv">
                        <th class="th_grey">Diciembre</th>                     
                        <th class="th_grey_c"><div class="op_a1"></div><input type="hidden" id="inp_op_a1" ></th>
                        <th class="th_grey_c"><div class="op_b1"></div><input type="hidden" id="inp_op_b1" ><input type="hidden" id="pr_b1_1" value="'.$pr12.'" /><input type="hidden" id="id_pv_1" value="'; if(isset($pv_t12_1->id)){ $html.=$pv_t12_1->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv" value="12" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_1" value="'; if(isset($pv_t12_1->opcion_porcentaje_c)){ $html.=$pv_t12_1->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" /></th>
                        <th class="th_grey_c"><div class="op_c1"></div><input type="hidden" id="inp_op_c1" ><input type="hidden" id="ca_c1_1" value="'.$ca12.'" /></th>
                        <th class="th_purple"><div class="op_t1"></div><input type="hidden" id="inp_op_t1" ></th>
                      </tr>
                      <tr>
                        <th class="th_yellow th_height">Total<br>general</th>                     
                        <th class="th_yellow"><input type="hidden" id="tg_1" value="'.$tg.'" /><span class="tg_a_1">'.number_format($tg,2,'.',',').'</span></th>
                        <th class="th_yellow"><span class="tg_b_1"></span><input type="hidden" id="inptg_b_1" value="" /></th>
                        <th class="th_yellow" rowspan="2"></th>
                        <th class="th_yellow"><span class="tg_c_1"></span></th>
                        <th class="th_yellow" rowspan="2" style="place-content: space-evenly;"><div class="op_tg1"></div></th>
                      </tr>
                      <tr>
                        <th class="th_yellow">Incremento anual</th>                     
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_p_1" value="'; if(isset($pre_t_1->porcentaje_a)){ $html.=$pre_t_1->porcentaje_a; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" />%</div></th>
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_b_1" value="'; if(isset($pre_t_1->porcentaje_b)){ $html.=$pre_t_1->porcentaje_b; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion1()" />%</div></th>
                        <th class="th_yellow"></th>
                      </tr>';

                      $html.='
                    </thead>
                  </table>
                  <table class="table table-bordered table_info"> 
                    <thead>
                      <tr> 
                        <th class="th_yellow" rowspan="4" style="place-content: space-evenly;">Instrucciones</th>                 
                        <th class="th_yellow" colspan="4">Capturar SOLO UNA opción (A o B o C)</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="4">Opción A. Debes de indicar el incremento que proyectas en tus ventas anuales y ese total del año, se divide en 12 meses por igual</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="4">Opción B. Debes de indicar el incremento que proyectas en tus ventas anuales y ese mismo crecimiento se aplica a cada uno de los meses, tomando como base, las ventas de cada mes, del último año (año '.$anio_aux1.')</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="4">Opción C. Debes de indicar el incremento que quieres lograr cada mes, tomando como base las ventas del último año (año '.$anio_aux1.'), puedes tomar como referencia tu ciclicidad en cada mes (si creces o decreces)</th>
                      </tr>
                    </thead>
                  </table>    
                </th>
                <th style="place-content: start;" class="th_border">';
                  $pre_t_2=$this->Model_ventashistoricas->get_proyeccion_venta($this->idcliente,$id,2);
                  $pv_t1_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,1);
                  $pv_t2_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,2);
                  $pv_t3_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,3);
                  $pv_t4_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,4);
                  $pv_t5_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,5);
                  $pv_t6_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,6);
                  $pv_t7_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,7);
                  $pv_t8_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,8);
                  $pv_t9_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,9);
                  $pv_t10_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,10);
                  $pv_t11_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,11);
                  $pv_t12_2=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,2,12);

                  $html.='<input type="hidden" id="id_vhpv2" value="'; if(isset($pre_t_2->id)){ $html.=$pre_t_2->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="anio_t2" value="'; if(isset($pre_t_2->anio)){ $html.=$pre_t_2->anio; }else{ $html.=$anio_2; } $html.='" />
                  <table class="table table-bordered table_info" id="tabla_p_2">
                    <thead>
                      <tr>                    
                        <th class="th_purple" colspan="5">Presupuesto '.$anio_2.'</th>
                      </tr>
                      <tr>                    
                        <th class="th_grey">Opción A</th>
                        <th class="th_grey">Opción B</th>
                        <th class="th_grey" colspan="2">Opción C</th>
                        <th class="th_grey" rowspan="2">¿Qué opción<br>seleccionaste?</th>
                      </tr>
                      <tr>                  
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey" colspan="2">Proyectado</th>
                      </tr>
                      <tr>                   
                        <th class="th_grey">'.$anio_2.'</th>
                        <th class="th_grey">'.$anio_2.'</th>
                        <th class="th_grey">%</th>
                        <th class="th_grey">'.$anio_2.'</th>
                        <th class="th_orange th_input">
                          <select class="form-select" id="sct2" onchange="calcular_proyeccion2()">
                            <option value="1" '; if(isset($pre_t_2->opcion)){ if($pre_t_2->opcion==1){ $html.='selected'; }} $html.='>A</option>
                            <option value="2" '; if(isset($pre_t_2->opcion)){ if($pre_t_2->opcion==2){ $html.='selected'; }} $html.='>B</option>
                            <option value="3" '; if(isset($pre_t_2->opcion)){ if($pre_t_2->opcion==3){ $html.='selected'; }} $html.='>C</option>
                          </select>
                        </th>
                      </tr>
                      <tr class="tr_pv2">                     
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b1_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t1_2->id)){ $html.=$pv_t1_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="1" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t1_2->opcion_porcentaje_c)){ $html.=$pv_t1_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c1_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                    
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b2_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t2_2->id)){ $html.=$pv_t2_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="2" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t2_2->opcion_porcentaje_c)){ $html.=$pv_t2_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c2_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b3_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t3_2->id)){ $html.=$pv_t3_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="3" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t3_2->opcion_porcentaje_c)){ $html.=$pv_t3_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c3_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                 
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b4_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t4_2->id)){ $html.=$pv_t4_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="4" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t4_2->opcion_porcentaje_c)){ $html.=$pv_t4_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c4_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b5_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t5_2->id)){ $html.=$pv_t5_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="5" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t5_2->opcion_porcentaje_c)){ $html.=$pv_t5_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c5_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b6_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t6_2->id)){ $html.=$pv_t6_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="6" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t6_2->opcion_porcentaje_c)){ $html.=$pv_t6_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c6_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                 
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b7_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t7_2->id)){ $html.=$pv_t7_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="7" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t7_2->opcion_porcentaje_c)){ $html.=$pv_t7_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c7_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                   
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b8_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t8_2->id)){ $html.=$pv_t8_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="8" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t8_2->opcion_porcentaje_c)){ $html.=$pv_t8_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c8_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b9_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t9_2->id)){ $html.=$pv_t9_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="9" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t9_2->opcion_porcentaje_c)){ $html.=$pv_t9_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c9_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b10_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t10_2->id)){ $html.=$pv_t10_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="10" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t10_2->opcion_porcentaje_c)){ $html.=$pv_t10_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c10_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                  
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b11_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t11_2->id)){ $html.=$pv_t11_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="11" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t11_2->opcion_porcentaje_c)){ $html.=$pv_t11_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c11_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr class="tr_pv2">                    
                        <th class="th_grey_c"><div class="op_a2"></div><input type="hidden" id="inp_op_a2" ></th>
                        <th class="th_grey_c"><div class="op_b2"></div><input type="hidden" id="inp_op_b2" ><input type="hidden" class="can_b_2" id="can_b12_2" value="" /><input type="hidden" id="id_pv_2" value="'; if(isset($pv_t12_2->id)){ $html.=$pv_t12_2->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv2" value="12" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_2" value="'; if(isset($pv_t12_2->opcion_porcentaje_c)){ $html.=$pv_t12_2->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" /></th>
                        <th class="th_grey_c"><div class="op_c2 op_c12_2"></div><input type="hidden" id="inp_op_c2" ></th>
                        <th class="th_purple"><div class="op_t2"></div><input type="hidden" id="inp_op_t2" ></th>
                      </tr>
                      <tr>              
                        <th class="th_yellow th_height"><span class="tg_a_2"></span><input type="hidden" class="form-control" id="inptg_a_2" value="0" /></th>
                        <th class="th_yellow"><span class="tg_b_2"></span><input type="hidden" id="inptg_b_2" value="" /></th>
                        <th class="th_yellow" rowspan="2"></th>
                        <th class="th_yellow"><span class="tg_c_2"></span><input type="hidden" class="form-control" id="inptg_c_2" value="0" /></th>
                        <th class="th_yellow" rowspan="2" style="place-content: space-evenly;"><div class="op_tg2"></div></th>
                      </tr>
                      <tr>                
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_p_2" value="'; if(isset($pre_t_2 ->porcentaje_a)){ $html.=$pre_t_2->porcentaje_a; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" />%</div></th>
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_b_2" value="'; if(isset($pre_t_2->porcentaje_b)){ $html.=$pre_t_2->porcentaje_b; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion2()" />%</div></th>
                        <th class="th_yellow" style="place-content: space-evenly;"><span class="incp_c_2">0%</span></th>
                      </tr>';
                      $html.='
                    </thead>
                  </table>
                  <table class="table table-bordered table_info"> 
                    <thead>
                      <tr>                
                        <th class="th_yellow" colspan="5">Capturar SOLO UNA opción (A o B o C)</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="5">Opción A. Debes de indicar el incremento que proyectas en tus ventas anuales y ese total anual, del año se divide en 12 meses por igual</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="5">Opción B. Debes de indicar el incremento que proyectas en tus ventas anuales y ese mismo crecimiento se aplica a cada uno de los meses, tomando como base, las ventas de cada mes, del último año (año '.$anio_cli.')</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="4">Opción C. Debes de indicar el incremento que quieres lograr cada mes, tomando como base las ventas del último año (año '.$anio_cli.'), puedes tomar como referencia tu ciclicidad en cada mes (si creces o decreces)</th>
                      </tr>
                    </thead>
                  </table>   
                </th>
                <th style="place-content: start;" class="th_border">';
                  $pre_t_3=$this->Model_ventashistoricas->get_proyeccion_venta($this->idcliente,$id,3);
                  $pv_t1_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,1);
                  $pv_t2_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,2);
                  $pv_t3_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,3);
                  $pv_t4_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,4);
                  $pv_t5_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,5);
                  $pv_t6_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,6);
                  $pv_t7_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,7);
                  $pv_t8_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,8);
                  $pv_t9_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,9);
                  $pv_t10_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,10);
                  $pv_t11_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,11);
                  $pv_t12_3=$this->Model_ventashistoricas->get_proyeccion_venta_detalle($this->idcliente,$id,3,12);

                  $html.='<input type="hidden" id="id_vhpv3" value="'; if(isset($pre_t_3->id)){ $html.=$pre_t_3->id; }else{ $html.=0; } $html.='" /><input type="hidden" id="anio_t3" value="'; if(isset($pre_t_3->anio)){ $html.=$pre_t_3->anio; }else{ $html.=$anio_3; } $html.='" />
                  <table class="table table-bordered table_info" id="tabla_p_3">
                    <thead>
                      <tr>                    
                        <th class="th_purple" colspan="5">Presupuesto '.$anio_3.'</th>
                      </tr>
                      <tr>                    
                        <th class="th_grey">Opción A</th>
                        <th class="th_grey">Opción B</th>
                        <th class="th_grey" colspan="2">Opción C</th>
                        <th class="th_grey" rowspan="2">¿Qué opción<br>seleccionaste?</th>
                      </tr>
                      <tr>                  
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey">Proyectado</th>
                        <th class="th_grey" colspan="2">Proyectado</th>
                      </tr>
                      <tr>                   
                        <th class="th_grey">'.$anio_3.'</th>
                        <th class="th_grey">'.$anio_3.'</th>
                        <th class="th_grey">%</th>
                        <th class="th_grey">'.$anio_3.'</th>
                        <th class="th_orange th_input">
                          <select class="form-select" id="sct3" onchange="calcular_proyeccion3()">
                            <option value="1" '; if(isset($pre_t_3->opcion)){ if($pre_t_3->opcion==1){ $html.='selected'; }} $html.='>A</option>
                            <option value="2" '; if(isset($pre_t_3->opcion)){ if($pre_t_3->opcion==2){ $html.='selected'; }} $html.='>B</option>
                            <option value="3" '; if(isset($pre_t_3->opcion)){ if($pre_t_3->opcion==3){ $html.='selected'; }} $html.='>C</option>
                          </select>
                        </th>
                      </tr>
                      <tr class="tr_pv3">                     
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b1_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t1_3->id)){ $html.=$pv_t1_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="1" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t1_3->opcion_porcentaje_c)){ $html.=$pv_t1_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c1_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                    
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b2_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t2_3->id)){ $html.=$pv_t2_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="2" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t2_3->opcion_porcentaje_c)){ $html.=$pv_t2_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c3_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b3_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t3_3->id)){ $html.=$pv_t3_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="3" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t3_3->opcion_porcentaje_c)){ $html.=$pv_t3_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c3_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                 
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b4_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t4_3->id)){ $html.=$pv_t4_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="4" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t4_3->opcion_porcentaje_c)){ $html.=$pv_t4_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c4_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b5_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t5_3->id)){ $html.=$pv_t5_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="5" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t5_3->opcion_porcentaje_c)){ $html.=$pv_t5_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c5_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b6_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t6_3->id)){ $html.=$pv_t6_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="6" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t6_3->opcion_porcentaje_c)){ $html.=$pv_t6_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c6_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                 
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b7_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t7_3->id)){ $html.=$pv_t7_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="7" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t7_3->opcion_porcentaje_c)){ $html.=$pv_t7_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c7_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                   
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b8_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t8_3->id)){ $html.=$pv_t8_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="8" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t8_3->opcion_porcentaje_c)){ $html.=$pv_t8_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c8_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b9_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t9_3->id)){ $html.=$pv_t9_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="9" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t9_3->opcion_porcentaje_c)){ $html.=$pv_t9_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c9_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b10_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t10_3->id)){ $html.=$pv_t10_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="10" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t10_3->opcion_porcentaje_c)){ $html.=$pv_t10_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c10_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                  
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b11_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t11_3->id)){ $html.=$pv_t11_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="11" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t11_3->opcion_porcentaje_c)){ $html.=$pv_t11_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c11_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr class="tr_pv3">                    
                        <th class="th_grey_c"><div class="op_a3"></div><input type="hidden" id="inp_op_a3" ></th>
                        <th class="th_grey_c"><div class="op_b3"></div><input type="hidden" id="inp_op_b3" ><input type="hidden" class="can_b_3" id="can_b12_3" value="" /><input type="hidden" id="id_pv_3" value="'; if(isset($pv_t12_3->id)){ $html.=$pv_t12_3->id; }else{ $html.=0; } $html.='"><input type="hidden" id="mes_pv3" value="12" /></th>
                        <th class="th_orange th_input"><input type="number" class="form-control" id="inc_c_3" value="'; if(isset($pv_t12_3->opcion_porcentaje_c)){ $html.=$pv_t12_3->opcion_porcentaje_c; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" /></th>
                        <th class="th_grey_c"><div class="op_c3 op_c12_3"></div><input type="hidden" id="inp_op_c3" ></th>
                        <th class="th_purple"><div class="op_t3"></div><input type="hidden" id="inp_op_t3" ></th>
                      </tr>
                      <tr>              
                        <th class="th_yellow th_height"><span class="tg_a_3"></span><input type="hidden" class="form-control" id="inptg_a_3" value="0" /></th>
                        <th class="th_yellow"><span class="tg_b_3"></span></th>
                        <th class="th_yellow" rowspan="2"></th>
                        <th class="th_yellow"><span class="tg_c_3"></span></th>
                        <th class="th_yellow" rowspan="2" style="place-content: space-evenly;"><div class="op_tg3"></div></th>
                      </tr>
                      <tr>                
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_p_3" value="'; if(isset($pre_t_3 ->porcentaje_a)){ $html.=$pre_t_3->porcentaje_a; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" />%</div></th>
                        <th class="th_yellow"><div class="btn-group" style="place-items: center;"><input type="number" class="form-control" id="inc_b_3" value="'; if(isset($pre_t_3 ->porcentaje_b)){ $html.=$pre_t_3->porcentaje_b; }else{ $html.=0; } $html.='" oninput="calcular_proyeccion3()" />%</div></th>
                        <th class="th_yellow" style="place-content: space-evenly;"><span class="incp_c_3">0%</span></th>
                      </tr>';
                      $html.='
                    </thead>
                  </table>
                  <table class="table table-bordered table_info"> 
                    <thead>
                      <tr>                
                        <th class="th_yellow" colspan="5">Capturar SOLO UNA opción (A o B o C)</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="5">Opción A. Debes de indicar el incremento que proyectas en tus ventas anuales y ese total anual, del año se divide en 12 meses por igual</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="5">Opción B. Debes de indicar el incremento que proyectas en tus ventas anuales y ese mismo crecimiento se aplica a cada uno de los meses, tomando como base, las ventas de cada mes, del último año (año '.$anio_3.')</th>
                      </tr>
                      <tr> 
                        <th class="th_yellow" colspan="4">Opción C. Debes de indicar el incremento que quieres lograr cada mes, tomando como base las ventas del último año (año '.$anio_3.'), puedes tomar como referencia tu ciclicidad en cada mes (si creces o decreces)</th>
                      </tr>
                    </thead>
                  </table>   
                </th>
              </tr>
            </thead>
          </table><div class="row my-4">  
            <div class="col-xl-12">
              <div>
                <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro_proyeccion" onclick="add_tablas()">Guardar</button>
              </div>    
            </div>
          </div>';

      echo $html;          
    }

    public function add_data_presupuesto(){
        $datax=$this->input->post();
        $id=$datax['id'];
        $data['idventashistoricas']=$datax['idventashistoricas'];
        $data['opcion']=$datax['opcion'];
        $data['porcentaje_a']=$datax['porcentaje_a'];
        $data['porcentaje_b']=$datax['porcentaje_b'];
        $data['anio']=$datax['anio'];
        $data['num_tabla']=$datax['num_tabla'];
        if($id==0){
            $id=$this->General_model->add_record('ventashistoricas_proyeccion_venta',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'ventashistoricas_proyeccion_venta');
        }
        echo $id;
    }

    function registro_datos_proyecion_venta_detalles(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {   
            $data['idventashistoricas_proyecion_venta']=$DATA[$i]->idventashistoricas_proyecion_venta;
            $data['idventashistoricas']=$DATA[$i]->idventashistoricas; 
            $data['num_tabla']=$DATA[$i]->num_tabla;
            $data['mes']=$DATA[$i]->mes;
            $data['opcion_a']=$DATA[$i]->opcion_a;
            $data['opcion_b']=$DATA[$i]->opcion_b;
            $data['opcion_c']=$DATA[$i]->opcion_c;
            $data['opcion_porcentaje_c']=$DATA[$i]->opcion_porcentaje_c;
            $data['opcion_seleccionada']=$DATA[$i]->opcion_seleccionada;
            if($DATA[$i]->id==0){
                $id=$this->General_model->add_record('ventashistoricas_proyeccion_venta_detalles',$data);
            }else{
                $this->General_model->edit_record('id',$DATA[$i]->id,$data,'ventashistoricas_proyeccion_venta_detalles');
            }
            
        }
    }

}
/// 