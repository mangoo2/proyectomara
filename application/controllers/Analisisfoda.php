<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisisfoda extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_guia_seleccion');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->fecha = date('Y-m-d');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,19);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=7;
        $data['btn_active_sub']=19;
        $data['btn_active_sub_m']=31;
        $data['get_periodo']=$this->General_model->get_select('periodo',array('activo'=>1));
        $data['get_cli']=$this->General_model->getselectwhererow('clientes',array('id'=>$this->idcliente));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('analisis/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('analisis/indexjs');
    }

    public function add_data(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $id=$this->General_model->add_record('foda_fortaleza',$data);
        echo $id;
    }

    public function add_data_f(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $data['tipo']=1;
        $id=$this->General_model->add_record('foda_fortaleza',$data);
        echo $id;
    }

    public function add_data_o(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $id=$this->General_model->add_record('foda_oportunidad',$data);
        echo $id;
    }

    public function add_data_o_m(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $data['tipo']=1;
        $id=$this->General_model->add_record('foda_oportunidad',$data);
        echo $id;
    }

    public function add_data_d(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $id=$this->General_model->add_record('foda_debilidades',$data);
        echo $id;
    }

    public function add_data_a(){
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $id=$this->General_model->add_record('foda_amenazas',$data);
        echo $id;
    }

    public function edit_data_f(){
        $concepto=$this->input->post('concepto');
        $id=$this->input->post('id');
        $datos['concepto']=$concepto;
        $this->General_model->edit_record('id',$id,$datos,'foda_fortaleza');
        echo $id;
    }

    public function edit_data_o(){
        $concepto=$this->input->post('concepto');
        $id=$this->input->post('id');
        $datos['concepto']=$concepto;
        $this->General_model->edit_record('id',$id,$datos,'foda_oportunidad');
        echo $id;
    }

    public function edit_data_d(){
        $concepto=$this->input->post('concepto');
        $id=$this->input->post('id');
        $datos['concepto']=$concepto;
        $this->General_model->edit_record('id',$id,$datos,'foda_debilidades');
        echo $id;
    }

    public function edit_data_a(){
        $concepto=$this->input->post('concepto');
        $id=$this->input->post('id');
        $datos['concepto']=$concepto;
        $this->General_model->edit_record('id',$id,$datos,'foda_amenazas');
        echo $id;
    }

    public function delete_record_f(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'foda_fortaleza');
    }

    public function delete_record_o(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'foda_oportunidad');
    }

    public function delete_record_d(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'foda_debilidades');
    }

    public function delete_record_a(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'foda_amenazas');
    }

    function get_fortalezas()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_fortaleza',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html.='<div class="row_fortaleza">';
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-10">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <textarea rows="1" type="text" id="concepto_f_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                  </div>
                </div>
                <div class="col-md-2">
                    <div style="display: flex;">
                        <div class="btn_f_'.$x->id.'">
                            <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light icon_f_'.$x->id.'" onclick="btn_editar_f('.$x->id.')">
                                <span class="tf-icons mdi mdi-pencil mdi-24px"></span>
                            </button>
                        </div>&nbsp;&nbsp;
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_fo('.$x->id.')">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                        </button>  
                    </div>    
                </diV>
            </div>';
        }
        $html.='</div>';
        echo $html;
    }

    function get_oportunidades()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_oportunidad',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html.='<div class="row_oportunidades">';
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-10">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <textarea rows="1" type="text" id="concepto_o_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <div class="btn_o_'.$x->id.'">
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light icon_o_'.$x->id.'" onclick="btn_editar_o('.$x->id.')">
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>
                        </button>
                    </div>&nbsp;&nbsp;
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_op('.$x->id.')">
                        <span class="tf-icons mdi mdi-delete-empty"></span>
                    </button>  
                  </div>
                </div>
            </div>';
        }
        $html.='</div>';
        echo $html;
    }

    function get_debilidades()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_debilidades',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html.='<div class="row_debilidad">';
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-10">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <textarea rows="1"  type="text" id="concepto_d_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                  </div>
                </div> 
                <div class="col-md-2">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <div class="btn_d_'.$x->id.'">
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light icon_d_'.$x->id.'" onclick="btn_editar_d('.$x->id.')">
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>
                        </button>
                    </div>&nbsp;&nbsp;  
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_de('.$x->id.')">
                        <span class="tf-icons mdi mdi-delete-empty"></span>
                    </button> 
                  </div>
                </div> 
            </div>';
        }
        $html.='</div>';
        echo $html;
    }

    function get_amenazas()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_amenazas',array('idcliente'=>$this->idcliente,'activo'=>1));
        $html.='<div class="row_amenazas">';
        foreach ($result->result() as $x){
            $html.='<div class="row g-3">
                <div class="col-md-10">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <textarea rows="1" type="text" id="concepto_a_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" disabled>'.$x->concepto.'</textarea>
                  </div>
                </div>  
                <div class="col-md-2">
                  <div class="form-floating form-floating-outline mb-4" style="display: flex;">
                    <div class="btn_a_'.$x->id.'">
                        <button type="button" class="btn rounded-pill btn-icon btn-outline-primary demo waves-effect waves-light icon_a_'.$x->id.'" onclick="btn_editar_a('.$x->id.')">
                            <span class="tf-icons mdi mdi-pencil mdi-24px"></span>
                        </button>
                    </div>&nbsp;&nbsp;
                    <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_am('.$x->id.')">
                        <span class="tf-icons mdi mdi-delete-empty"></span>
                    </button> 
                  </div>
                </div>  
            </div>';
        }
        $html.='</div>';
        echo $html;
    }
    
    public function update_foda(){
        $data = array('estatusfoda'=>1);
        $this->General_model->edit_record('id',$this->idcliente,$data,'clientes');
    }

    public function edit_pregunta_resumen(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $tipo=$this->input->post('tipo'); 
        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles',array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$tipo=>$respuesta);
            $this->General_model->add_record('guia_seleccion_preguntas_respuesta_detalles',$data);
        }else{
            $data = array('resp'.$tipo=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles');    
        }
        
    }

    function calculo_resumen1()
    {
        $idpregunta=$this->input->post('idpregunta');
        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles',array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $html='';
        foreach ($result->result() as $x){
            $pr1=$x->resp1;
            $pr2=$x->resp2;
            $pr3=$x->resp3;
            $pr4=$x->resp4;
            $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
            $costo=0;
            $riesgo=0;
            $complejidad=0;
            $beneficios=0;
            foreach ($result_c->result() as $c){
                $costo=$c->costo;
                $riesgo=$c->riesgo;
                $complejidad=$c->complejidad;
                $beneficios=$c->beneficios;
            }
            
            $mult1=$pr1*$costo;
            $mult2=$pr2*$riesgo;
            $mult3=$pr3*$complejidad;
            $mult4=$pr4*$beneficios;
            $div1=$mult1/100;
            $div2=$mult2/100;
            $div3=$mult3/100;
            $div4=$mult4/100;

            //var_dump($div1);
            //var_dump($div2);
            //var_dump($div3);
            //var_dump($div4);

            $suma=$div1+$div2+$div3+$div4;
 
            $valor=0;
            if($suma>=1 && $suma<=1.39){
                $html.='<div class="resum_c3">Alta</div>';
                $valor=1;
            }else if($suma>=1.4 && $suma<=2.34){
                $html.='<div class="resum_c2">Media</div>';  
                $valor=2;
            }else if($suma>=2.34 && $suma<=3){
                $html.='<div class="resum_c1">Baja</div>';
                $valor=3;
            }

            $data = array('resultado'=>$valor);
            $this->General_model->edit_record('idguia_seleccion_preguntas_detalles',$idpregunta,$data,'guia_seleccion_preguntas_respuesta_detalles');

        }
        echo $html;
    }

    public function edit_pregunta_riesgo(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $tipo=$this->input->post('tipo'); 
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta_detalles',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$tipo=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_preguntas_respuesta_detalles',$data);
        }else{
            $data = array('resp'.$tipo=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_preguntas_respuesta_detalles');    
        }
        
    }

    function calculo_resumen2()
    {
        $idpregunta=$this->input->post('idpregunta');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta_detalles',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $html='';
        foreach ($result->result() as $x){
            $pr1=$x->resp1;
            $pr2=$x->resp2;
            $pr3=$x->resp3;
            $pr4=$x->resp4;
            $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
            $costo=0;
            $riesgo=0;
            $complejidad=0;
            $beneficios=0;
            foreach ($result_c->result() as $c){
                $costo=$c->costo;
                $riesgo=$c->riesgo;
                $complejidad=$c->complejidad;
                $beneficios=$c->beneficios;
            }
            
            $mult1=$pr1*$costo;
            $mult2=$pr2*$riesgo;
            $mult3=$pr3*$complejidad;
            $mult4=$pr4*$beneficios;
            $div1=$mult1/100;
            $div2=$mult2/100;
            $div3=$mult3/100;
            $div4=$mult4/100;
            $suma=$div1+$div2+$div3+$div4;
            $valor=0;

            if($suma>=1 && $suma<=1.39){
                $html.='<div class="resum_c3">Alta</div>';
                $valor=1;
            }else if($suma>=1.4 && $suma<=2.34){
                $html.='<div class="resum_c2">Media</div>'; 
                $valor=2; 
            }else if($suma>=2.34 && $suma<=3){
                $html.='<div class="resum_c1">Baja</div>';
                $valor=3;
            }

            $data = array('resultado'=>$valor);
            $this->General_model->edit_record('idguia_seleccion_fortalezas_preguntas_detalles',$idpregunta,$data,'guia_seleccion_fortalezas_preguntas_respuesta_detalles');
        }

        echo $html;
    }

    public function edit_pregunta_fortaleza_resultado(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $tipo=$this->input->post('tipo'); 
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta_detalles',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$tipo=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_estado_resultados_respuesta_detalles',$data);
        }else{
            $data = array('resp'.$tipo=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta_detalles');    
        }
        
    }

    function calculo_resumen3()
    {
        $idpregunta=$this->input->post('idpregunta');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta_detalles',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $html='';
        foreach ($result->result() as $x){
            $pr1=$x->resp1;
            $pr2=$x->resp2;
            $pr3=$x->resp3;
            $pr4=$x->resp4;
            $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
            $costo=0;
            $riesgo=0;
            $complejidad=0;
            $beneficios=0;
            foreach ($result_c->result() as $c){
                $costo=$c->costo;
                $riesgo=$c->riesgo;
                $complejidad=$c->complejidad;
                $beneficios=$c->beneficios;
            }
            
            $mult1=$pr1*$costo;
            $mult2=$pr2*$riesgo;
            $mult3=$pr3*$complejidad;
            $mult4=$pr4*$beneficios;
            $div1=$mult1/100;
            $div2=$mult2/100;
            $div3=$mult3/100;
            $div4=$mult4/100;
            $suma=$div1+$div2+$div3+$div4;
            $valor=0;

            if($suma>=1 && $suma<=1.39){
                $html.='<div class="resum_c3">Alta</div>';
                $valor=1;
            }else if($suma>=1.4 && $suma<=2.34){
                $html.='<div class="resum_c2">Media</div>';
                $valor=2;  
            }else if($suma>=2.34 && $suma<=3){
                $html.='<div class="resum_c1">Baja</div>';
                $valor=3;
            }

            $data = array('resultado'=>$valor);
            $this->General_model->edit_record('idguia_seleccion_fortalezas_estado_resultados_detalles',$idpregunta,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta_detalles');
        }

        echo $html;
    }

    function get_tabla_resumen_estrategias_prioriza_2_m()
    {   
        
        $html='<div>
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">
              <tbody>';
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,1);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,1);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,1);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }



                if($totalm!=0 || $totali!=0 || $totals!=0){
                    $html.='<tr>
                      <td ><div><h3>Debilidades de prioridad <span style="color:red">alta</span> a trabajar</h3></div></td>
                    </tr>';

                    $html.='<tr>
                        <td><div class="row">';
                            $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,1);
                            foreach ($result_merc as $x){
                                $anio=$x->anio;
                                $semestre=$x->semestre;
                 
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #5b2326; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$x->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #e31a1a !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Alta</div>';
                                            
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';

                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black; margin-bottom: 4px;" class="form-select" id="id_m_a_'.$x->id.'"  onchange="b_edit_resp_m_a('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black;" class="form-select" id="id_m_s_'.$x->id.'"  onchange="b_edit_resp_m_s('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/

                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #47282d !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades del mercado / '.$x->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }
                            // interno
                            $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,1);
                            foreach ($result_inter as $int){
                                $anio=$int->anio;
                                $semestre=$int->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #5b2326; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$int->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #e31a1a !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Alta</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black; margin-bottom: 4px;" class="form-select" id="id_i_a_'.$int->id.'"  onchange="b_edit_resp_i_a('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black;" class="form-select" id="id_i_s_'.$int->id.'"  onchange="b_edit_resp_i_s('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #47282d !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu análisis interno / '.$int->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            // estado
                            $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,1);
                            foreach ($result_est as $est){
                                $anio=$est->anio;
                                $semestre=$est->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #5b2326; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$est->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #e31a1a !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Alta</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black; margin-bottom: 4px;" class="form-select" id="id_s_a_'.$est->id.'"  onchange="b_edit_resp_s_a('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #e31a1a; color: black;" class="form-select" id="id_s_s_'.$est->id.'"  onchange="b_edit_resp_s_s('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #47282d !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu estado de resultados / '.$est->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            $html.='</div><br><div class="div-linea"></div>';
                        $html.='</td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
        echo $html;
    }
    

     function get_tabla_resumen_estrategias_prioriza_2_i()
    {   
        
        $html='<div>
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">
              <tbody>';
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,2);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,2);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,2);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }



                if($totalm!=0 || $totali!=0 || $totals!=0){
                    $html.='<tr>
                      <td ><div><h3>Debilidades de prioridad <span style="color:#f5bd00">media</span> a trabajar</h3></div></td>
                    </tr>';

                    $html.='<tr>
                        <td><div class="row">';
                            $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,2);
                            foreach ($result_merc as $x){
                                $anio=$x->anio;
                                $semestre=$x->semestre;
                                
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #544722; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$x->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Media</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black; margin-bottom: 4px;" class="form-select" id="id_m_a_'.$x->id.'"  onchange="b_edit_resp_m_a('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black;" class="form-select" id="id_m_s_'.$x->id.'"  onchange="b_edit_resp_m_s('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #453c2e !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades del mercado / '.$x->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }
                            // interno
                            $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,2);
                            foreach ($result_inter as $int){
                                $anio=$int->anio;
                                $semestre=$int->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #544722; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$int->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Media</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black; margin-bottom: 4px;" class="form-select" id="id_i_a_'.$int->id.'"  onchange="b_edit_resp_i_a('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black;" class="form-select" id="id_i_s_'.$int->id.'"  onchange="b_edit_resp_i_s('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #453c2e !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu análisis interno / '.$int->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            // estado
                            $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,2);
                            foreach ($result_est as $est){
                                $anio=$est->anio;
                                $semestre=$est->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #544722; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$est->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Media</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black; margin-bottom: 4px;" class="form-select" id="id_s_a_'.$est->id.'"  onchange="b_edit_resp_s_a('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #f5bd00; color: black;" class="form-select" id="id_s_s_'.$est->id.'"  onchange="b_edit_resp_s_s('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #453c2e !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu estado de resultados / '.$est->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            $html.='</div><br><div class="div-linea"></div>';
                        $html.='</td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
        echo $html;
    }

     function get_tabla_resumen_estrategias_prioriza_2_e()
    {   
        
        $html='<div>
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">
              <tbody>';
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,3);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,3);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,3);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }



                if($totalm!=0 || $totali!=0 || $totals!=0){
                    $html.='<tr>
                      <td ><div><h3>Debilidades de prioridad <span style="color:#70ad47">baja</span> a trabajar</h3></div></td>
                    </tr>';

                    $html.='<tr>
                        <td><div class="row">';
                            $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,3);
                            foreach ($result_merc as $x){
                                $anio=$x->anio;
                                $semestre=$x->semestre;

                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #1a7255; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$x->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #70ad47 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Baja</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black; margin-bottom: 4px;" class="form-select" id="id_m_a_'.$x->id.'"  onchange="b_edit_resp_m_a('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black;" class="form-select" id="id_m_s_'.$x->id.'"  onchange="b_edit_resp_m_s('.$x->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #234c40 !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades del mercado / '.$x->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }
                            // interno
                            $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,3);
                            foreach ($result_inter as $int){
                                $anio=$int->anio;
                                $semestre=$int->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #1a7255; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$int->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #70ad47 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Baja</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black; margin-bottom: 4px;" class="form-select" id="id_i_a_'.$int->id.'"  onchange="b_edit_resp_i_a('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black;" class="form-select" id="id_i_s_'.$int->id.'"  onchange="b_edit_resp_i_s('.$int->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #234c40 !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu análisis interno / '.$int->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            // estado
                            $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,3);
                            foreach ($result_est as $est){
                                $anio=$est->anio;
                                $semestre=$est->semestre;
                                $html.='<div class="col-md-3"><br>
                                    <div style="padding: 12px; background: #1a7255; border-radius: 36px; height: 95%;">
                                        <div>
                                            <div style="font-size: 18px; text-align: -webkit-center;">'.$est->pregunta.'</div>
                                            <div style="font-size: 18px;">Prioridad:</div>
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #70ad47 !important; font-size: 17px; color:black !important; text-align: -webkit-center;!important;">Baja</div>';
                                            /*$html.='<div style="font-size: 18px;">Plazo de implementación:</div>';
                                            $html.='<div class="row">  
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black; margin-bottom: 4px;" class="form-select" id="id_s_a_'.$est->id.'"  onchange="b_edit_resp_s_a('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                            $html.='<option value="2024" '; if($anio==2024){ $html.='selected'; } $html.='>2024</option>';
                                                            $html.='<option value="2025" '; if($anio==2025){ $html.='selected'; } $html.='>2025</option>';
                                                            $html.='<option value="2026" '; if($anio==2026){ $html.='selected'; } $html.='>2026</option>';
                                                            $html.='<option value="2027" '; if($anio==2027){ $html.='selected'; } $html.='>2027</option>';
                                                            $html.='<option value="2028" '; if($anio==2028){ $html.='selected'; } $html.='>2028</option>';
                                                            $html.='<option value="2029" '; if($anio==2029){ $html.='selected'; } $html.='>2029</option>';
                                                            $html.='<option value="2030" '; if($anio==2030){ $html.='selected'; } $html.='>2030</option>';
                                                        $html.='</select>';
                                                    $html.='</div>
                                                        <div class="col-md-6">';
                                                            $html.='<select style="border-radius: 30px; background: #70ad47; color: black;" class="form-select" id="id_s_s_'.$est->id.'"  onchange="b_edit_resp_s_s('.$est->id.')" ><option valor="0">Selecciona una opción</option>';
                                                                $html.='<option value="1" '; if($semestre==1){ $html.='selected'; } $html.='>Trimestre 1</option>';
                                                                $html.='<option value="2" '; if($semestre==2){ $html.='selected'; } $html.='>Trimestre 2</option>';
                                                                $html.='<option value="3" '; if($semestre==3){ $html.='selected'; } $html.='>Trimestre 3</option>';
                                                                $html.='<option value="4" '; if($semestre==4){ $html.='selected'; } $html.='>Trimestre 4</option>';
                                                        $html.='</select>';
                                                $html.='</div>
                                                    </div>';*/
                                            $html.='
                                        </div><br>
                                        <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #234c40 !important; font-size: 17px;  text-align: -webkit-center; max-width: none !important;">Debilidades de tu estado de resultados / '.$est->titulo.'</div>
                                    </div>
                                    
                                </div>';
                            }

                            $html.='</div><br><div class="div-linea"></div>';
                        $html.='</td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
        echo $html;
    }

    public function edit_resp_m_a(){
        $id_rg=$this->input->post('id');
        $anio=$this->input->post('anio');
        $data = array('anio'=>$anio);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles');    
    }

    public function edit_resp_m_s(){
        $id_rg=$this->input->post('id');
        $semestre=$this->input->post('semestre');
        $data = array('semestre'=>$semestre);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles');    
    }

    public function edit_resp_i_a(){
        $id_rg=$this->input->post('id');
        $anio=$this->input->post('anio');
        $data = array('anio'=>$anio);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_preguntas_respuesta_detalles');    
    }

    public function edit_resp_i_s(){
        $id_rg=$this->input->post('id');
        $semestre=$this->input->post('semestre');
        $data = array('semestre'=>$semestre);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_preguntas_respuesta_detalles');    
    }

    public function edit_resp_s_a(){
        $id_rg=$this->input->post('id');
        $anio=$this->input->post('anio');
        $data = array('anio'=>$anio);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta_detalles');    
    }

    public function edit_resp_s_s(){
        $id_rg=$this->input->post('id');
        $semestre=$this->input->post('semestre');
        $data = array('semestre'=>$semestre);
        $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta_detalles');    
    }

    function get_tabla_cronograma()
    {   
        $html='<div class="table-responsive text-nowrap">
            <table style="white-space: normal !important;">
              <tbody>';
/*                $html.='<tr>
                  <td style="text-align: center;" colspan="5" ><div class="div-linea"></div></td>
                </tr>';*/
                $html.='<tr>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Prioridad</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;"style="width: 255px;">Plan de trabajo general</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                </tr>';
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,1);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,1);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,1);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }
                if($totalm!=0 || $totali!=0 || $totals!=0){
                    
                    $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,1);
                    foreach ($result_merc as $x){
                        $html.='<tr  style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_1" oninput="b_edit_resp('.$x->id.',1,1)">'.$x->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_2" oninput="b_edit_resp('.$x->id.',1,2)">'.$x->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_3" oninput="b_edit_resp('.$x->id.',1,3)">'.$x->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_'.$x->id.'_1_4" oninput="b_edit_resp('.$x->id.',1,4)" value="'.$x->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_'.$x->id.'_1_5" oninput="b_edit_resp('.$x->id.',1,5)" value="'.$x->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_6" oninput="b_edit_resp('.$x->id.',1,6)">'.$x->preg6.'</textarea>
                            <div class="th_c id_pre_'.$x->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$x->id.',1)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                
                        $result_merc_cro=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                        foreach ($result_merc_cro as $cro){
                            $html.='<tr  style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_1">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_1_1" oninput="b_edit_resp_cro('.$cro->id.',1,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_1_2" oninput="b_edit_resp_cro('.$cro->id.',1,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_1_3" oninput="b_edit_resp_cro('.$cro->id.',1,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_1_4" oninput="b_edit_resp_cro('.$cro->id.',1,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_1_5" oninput="b_edit_resp_cro('.$cro->id.',1,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_1_6" oninput="b_edit_resp_cro('.$cro->id.',1,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',1)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        }                        
                     
                    }
                    
                    $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,1);
                    foreach ($result_inter as $int){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_1" oninput="b_edit_resp('.$int->id.',2,1)">'.$int->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_2" oninput="b_edit_resp('.$int->id.',2,2)">'.$int->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_3" oninput="b_edit_resp('.$int->id.',2,3)">'.$int->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_4" oninput="b_edit_resp('.$int->id.',2,4)" value="'.$int->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_5" oninput="b_edit_resp('.$int->id.',2,5)" value="'.$int->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_6" oninput="b_edit_resp('.$int->id.',2,6)">'.$int->preg6.'</textarea>
                            <div class="th_c id_pre_'.$int->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$int->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$int->id.',2)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                        foreach ($result_inter_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_2">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_1" oninput="b_edit_resp_cro('.$cro->id.',2,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_2" oninput="b_edit_resp_cro('.$cro->id.',2,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_3" oninput="b_edit_resp_cro('.$cro->id.',2,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_4" oninput="b_edit_resp_cro('.$cro->id.',2,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_5" oninput="b_edit_resp_cro('.$cro->id.',2,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_6" oninput="b_edit_resp_cro('.$cro->id.',2,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',2)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }
                    $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,1);
                    foreach ($result_est as $est){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_1" oninput="b_edit_resp('.$est->id.',3,1)">'.$est->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_2" oninput="b_edit_resp('.$est->id.',3,2)">'.$est->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_3" oninput="b_edit_resp('.$est->id.',3,3)">'.$est->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_4" oninput="b_edit_resp('.$est->id.',3,4)" value="'.$est->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_5" oninput="b_edit_resp('.$est->id.',3,5)" value="'.$est->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_6" oninput="b_edit_resp('.$est->id.',3,6)">'.$est->preg6.'</textarea>
                            <div class="th_c id_pre_'.$est->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$est->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$est->id.',3)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                        foreach ($result_est_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_3">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_1" oninput="b_edit_resp_cro('.$cro->id.',3,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_2" oninput="b_edit_resp_cro('.$cro->id.',3,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_3" oninput="b_edit_resp_cro('.$cro->id.',3,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_4" oninput="b_edit_resp_cro('.$cro->id.',3,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_5" oninput="b_edit_resp_cro('.$cro->id.',3,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_6" oninput="b_edit_resp_cro('.$cro->id.',3,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',3)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }

                }
                 
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,2);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,2);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,2);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }

                if($totalm!=0 || $totali!=0 || $totals!=0){
                    $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,2);
                    foreach ($result_merc as $x){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_1" oninput="b_edit_resp('.$x->id.',1,1)">'.$x->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_2" oninput="b_edit_resp('.$x->id.',1,2)">'.$x->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_3" oninput="b_edit_resp('.$x->id.',1,3)">'.$x->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_4" oninput="b_edit_resp('.$x->id.',1,4)" value="'.$x->preg4.'"></textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_5" oninput="b_edit_resp('.$x->id.',1,5)" value="'.$x->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_6" oninput="b_edit_resp('.$x->id.',1,6)">'.$x->preg6.'</textarea>
                            <div class="th_c id_pre_'.$x->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$x->id.',1)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                        foreach ($result_merc as $x){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$x->id.'_1">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_1" oninput="b_edit_resp_cro('.$x->id.',1,1)">'.$x->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_2" oninput="b_edit_resp_cro('.$x->id.',1,2)">'.$x->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_3" oninput="b_edit_resp_cro('.$x->id.',1,3)">'.$x->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$x->id.'_1_4" oninput="b_edit_resp_cro('.$x->id.',1,4)" value="'.$x->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$x->id.'_1_5" oninput="b_edit_resp_cro('.$x->id.',1,5)" value="'.$x->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_6" oninput="b_edit_resp_cro('.$x->id.',1,6)">'.$x->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$x->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$x->id.',1)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }
                    $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,2);
                    foreach ($result_inter as $int){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                          <td <div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_1" oninput="b_edit_resp('.$int->id.',2,1)">'.$int->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_2" oninput="b_edit_resp('.$int->id.',2,2)">'.$int->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_3" oninput="b_edit_resp('.$int->id.',2,3)">'.$int->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_4" oninput="b_edit_resp('.$int->id.',2,4)" value="'.$int->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_5" oninput="b_edit_resp('.$int->id.',2,5)" value="'.$int->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_6" oninput="b_edit_resp('.$int->id.',2,6)">'.$int->preg6.'</textarea>
                            <div class="th_c id_pre_'.$int->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$int->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$int->id.',2)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                        foreach ($result_inter_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_2">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_1" oninput="b_edit_resp_cro('.$cro->id.',2,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_2" oninput="b_edit_resp_cro('.$cro->id.',2,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_3" oninput="b_edit_resp_cro('.$cro->id.',2,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_4" oninput="b_edit_resp_cro('.$cro->id.',2,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_5" oninput="b_edit_resp_cro('.$cro->id.',2,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_6" oninput="b_edit_resp_cro('.$cro->id.',2,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',2)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }
                    $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,2);
                    foreach ($result_est as $est){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_1" oninput="b_edit_resp('.$est->id.',3,1)">'.$est->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_2" oninput="b_edit_resp('.$est->id.',3,2)">'.$est->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_3" oninput="b_edit_resp('.$est->id.',3,3)">'.$est->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_4" oninput="b_edit_resp('.$est->id.',3,4)" value="'.$est->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_5" oninput="b_edit_resp('.$est->id.',3,5)" value="'.$est->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_6" oninput="b_edit_resp('.$est->id.',3,6)">'.$est->preg6.'</textarea>
                            <div class="th_c id_pre_'.$est->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$est->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$est->id.',3)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                        foreach ($result_est_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_3">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_1" oninput="b_edit_resp_cro('.$cro->id.',3,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_2" oninput="b_edit_resp_cro('.$cro->id.',3,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_3" oninput="b_edit_resp_cro('.$cro->id.',3,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_4" oninput="b_edit_resp_cro('.$cro->id.',3,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_5" oninput="b_edit_resp_cro('.$cro->id.',3,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_6" oninput="b_edit_resp_cro('.$cro->id.',3,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',3)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }

                }
                
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total_alta($this->idcliente,3);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total_alta($this->idcliente,3);
                $totali=0;
                foreach ($result_ti as $x){
                    $totali=$x->total;
                }

                $result_ts=$this->Model_guia_seleccion->get_pri_estadosultados_total_alta($this->idcliente,3);
                $totals=0;
                foreach ($result_ts as $x){
                    $totals=$x->total;
                }

                if($totalm!=0 || $totali!=0 || $totals!=0){
                    $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta($this->idcliente,3);
                    foreach ($result_merc as $x){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_1" oninput="b_edit_resp('.$x->id.',1,1)">'.$x->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_2" oninput="b_edit_resp('.$x->id.',1,2)">'.$x->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_3" oninput="b_edit_resp('.$x->id.',1,3)">'.$x->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_4" oninput="b_edit_resp('.$x->id.',1,4)" value="'.$x->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_5" oninput="b_edit_resp('.$x->id.',1,5)" value="'.$x->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$x->id.'_1_6" oninput="b_edit_resp('.$x->id.',1,6)">'.$x->preg6.'</textarea>
                            <div class="th_c id_pre_'.$x->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$x->id.',1)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_alta_cronograma($x->id);
                        foreach ($result_merc as $x){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$x->id.'_1">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_1" oninput="b_edit_resp_cro('.$x->id.',1,1)">'.$x->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_2" oninput="b_edit_resp_cro('.$x->id.',1,2)">'.$x->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_3" oninput="b_edit_resp_cro('.$x->id.',1,3)">'.$x->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$x->id.'_1_4" oninput="b_edit_resp_cro('.$x->id.',1,4)" value="'.$x->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$x->id.'_1_5" oninput="b_edit_resp_cro('.$x->id.',1,5)" value="'.$x->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$x->id.'_1_6" oninput="b_edit_resp_cro('.$x->id.',1,6)">'.$x->preg6.'</textarea>
                            <div class="th_c id_pre_cro_'.$x->id.'_1_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div>
                            </div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$x->id.',1)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';

                        } 
                    }

                    $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta($this->idcliente,3);
                    foreach ($result_inter as $int){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_1" oninput="b_edit_resp('.$int->id.',2,1)">'.$int->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_2" oninput="b_edit_resp('.$int->id.',2,2)">'.$int->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_3" oninput="b_edit_resp('.$int->id.',2,3)">'.$int->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_4" oninput="b_edit_resp('.$int->id.',2,4)" value="'.$int->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_5" oninput="b_edit_resp('.$int->id.',2,5)" value="'.$int->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$int->id.'_2_6" oninput="b_edit_resp('.$int->id.',2,6)">'.$int->preg6.'</textarea>
                            <div class="th_c id_pre_'.$int->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$int->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$int->id.',2)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_inter_cro=$this->Model_guia_seleccion->get_pri_internosultados_detalles_alta_cronograma($int->id);
                        foreach ($result_inter_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_2">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_1" oninput="b_edit_resp_cro('.$cro->id.',2,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_2" oninput="b_edit_resp_cro('.$cro->id.',2,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_3" oninput="b_edit_resp_cro('.$cro->id.',2,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_4" oninput="b_edit_resp_cro('.$cro->id.',2,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_2_5" oninput="b_edit_resp_cro('.$cro->id.',2,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_2_6" oninput="b_edit_resp_cro('.$cro->id.',2,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_2_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',2)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }
                    $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta($this->idcliente,3);
                    foreach ($result_est as $est){
                        $html.='<tr style="vertical-align: text-top;">
                          <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                          <td ><div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_1" oninput="b_edit_resp('.$est->id.',3,1)">'.$est->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_2" oninput="b_edit_resp('.$est->id.',3,2)">'.$est->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_3" oninput="b_edit_resp('.$est->id.',3,3)">'.$est->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_4" oninput="b_edit_resp('.$est->id.',3,4)" value="'.$est->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_5" oninput="b_edit_resp('.$est->id.',3,5)" value="'.$est->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_'.$est->id.'_3_6" oninput="b_edit_resp('.$est->id.',3,6)">'.$est->preg6.'</textarea>
                            <div class="th_c id_pre_'.$est->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$est->preg6.'</div>
                            </div></td>
                          <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro('.$est->id.',3)">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button></div></td>
                        </tr>';
                        $result_est_cro=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_alta_cronograma($est->id);
                        foreach ($result_est_cro as $cro){
                            $html.='<tr style="vertical-align: text-top;" class="pre_cro_'.$cro->id.'_3">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_1" oninput="b_edit_resp_cro('.$cro->id.',3,1)">'.$cro->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_2" oninput="b_edit_resp_cro('.$cro->id.',3,2)">'.$cro->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_3" oninput="b_edit_resp_cro('.$cro->id.',3,3)">'.$cro->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_4" oninput="b_edit_resp_cro('.$cro->id.',3,4)" value="'.$cro->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_cro_'.$cro->id.'_3_5" oninput="b_edit_resp_cro('.$cro->id.',3,5)" value="'.$cro->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none" rows="1" class="form-control js-auto-size" id="id_pre_cro_'.$cro->id.'_3_6" oninput="b_edit_resp_cro('.$cro->id.',3,6)">'.$cro->preg6.'</textarea>
                                    <div class="th_c id_pre_cro_'.$cro->id.'_3_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$cro->preg6.'</div></div></td>
                              <td ><div class="padding1"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_cro('.$cro->id.',3)">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button></div></td>
                            </tr>';
                        } 
                    }
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
          //$html.='<div class="clonetr"><table class="table" id="clonetr_data4">'.$int->preg1.'</table></div>';
    
        echo $html;
    }

    public function b_edit_respuesta_cro(){
        $id_rg=$this->input->post('id');
        $pre=$this->input->post('pre');
        $tipo=$this->input->post('tipo');
        $num=$this->input->post('num');
        /*$vali=0; 
        if($pre>=$this->fecha){
            $vali=1;
        }else{
            $vali=0;
            $pre='';
        }*/

        if($tipo==1){
            $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles_cronograma',array('idguia_seleccion_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>1));
            $id_m=0;
            foreach ($result->result() as $x){
                $id_m=$x->id;
            }
            if($id_m==0){
                $data = array('idguia_seleccion_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>1,'preg'.$num=>$pre);
                $this->General_model->add_record('guia_seleccion_preguntas_respuesta_detalles_cronograma',$data);
            }else{
                $data = array('preg'.$num=>$pre);
                $this->General_model->edit_record('id',$id_m,$data,'guia_seleccion_preguntas_respuesta_detalles_cronograma');    
            }
        }else if($tipo==2){
            $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_ps_re_detalles_cronograma',array('idguia_seleccion_fortalezas_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>1));
            $id_m=0;
            foreach ($result->result() as $x){
                $id_m=$x->id;
            }
            if($id_m==0){
                $data = array('idguia_seleccion_fortalezas_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>1,'preg'.$num=>$pre);
                $this->General_model->add_record('guia_seleccion_fortalezas_ps_re_detalles_cronograma',$data);
            }else{
                $data = array('preg'.$num=>$pre);
                $this->General_model->edit_record('id',$id_m,$data,'guia_seleccion_fortalezas_ps_re_detalles_cronograma');    
            }
        }else if($tipo==3){
            $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_r_r_detalles_cronograma',array('idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles'=>$id_rg,'tipo'=>1));
            $id_m=0;
            foreach ($result->result() as $x){
                $id_m=$x->id;
            }
            if($id_m==0){
                $data = array('idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles'=>$id_rg,'tipo'=>1,'preg'.$num=>$pre);
                $this->General_model->add_record('guia_seleccion_fortalezas_estado_r_r_detalles_cronograma',$data);
            }else{
                $data = array('preg'.$num=>$pre);
                $this->General_model->edit_record('id',$id_m,$data,'guia_seleccion_fortalezas_estado_r_r_detalles_cronograma');    
            }  
        }
        //echo $vali;
    }

    public function b_add_respuesta_cro(){
        $id_rg=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        if($tipo==1){
            $data = array('idguia_seleccion_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>0);
            $this->General_model->add_record('guia_seleccion_preguntas_respuesta_detalles_cronograma',$data);
        }else if($tipo==2){
            $data = array('idguia_seleccion_fortalezas_preguntas_respuesta_detalles'=>$id_rg,'tipo'=>0);
            $this->General_model->add_record('guia_seleccion_fortalezas_ps_re_detalles_cronograma',$data);
        }else if($tipo==3){
            $data = array('idguia_seleccion_fortalezas_estado_resultados_respuesta_detalles'=>$id_rg,'tipo'=>0);
            $this->General_model->add_record('guia_seleccion_fortalezas_estado_r_r_detalles_cronograma',$data);
        }
    }

    public function b_edit_respuesta_cro_sub(){
        $id_rg=$this->input->post('id');
        $pre=$this->input->post('pre');
        $tipo=$this->input->post('tipo');
        $num=$this->input->post('num');
        if($tipo==1){
            $data = array('preg'.$num=>$pre);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles_cronograma');    
        }else if($tipo==2){
            $data = array('preg'.$num=>$pre);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_ps_re_detalles_cronograma');    
        }else if($tipo==3){
            $data = array('preg'.$num=>$pre);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_r_r_detalles_cronograma');    
        }
        
    }

    public function b_delete_respuesta_cro_sub(){
        $id_rg=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $data = array('activo'=>0);
        if($tipo==1){
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles_cronograma');    
        }else if($tipo==2){
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_ps_re_detalles_cronograma');    
        }else if($tipo==3){
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_r_r_detalles_cronograma');    
        }
        
    }


    public function fecha_validar()
    {   
        $f1=$this->input->post('f1');
        $f2=$this->input->post('f2');
        $f2f = date("d-m-Y",strtotime($f2."+ 2 days")); 
        date_default_timezone_set('America/Mexico_City');       
        $fechaInicio = new DateTime("$f1 23:59:59");
        $fechaFin = new DateTime("$f2f 00:00:00");
        $intervalo = $fechaInicio->diff($fechaFin);
        $anio='';
        $mes='';
        $dia='';
        if($intervalo->y!=0){
            $anio=$intervalo->y . " años, ";
        }
        if($intervalo->m!=0){
            $mes=$intervalo->m." meses y ";
        }
            $dia=$intervalo->d." dias";
        echo $anio.$mes.$dia;
    }

    public function b_add_cronograma_debilidad_fortaleza(){
        $data = array('idcliente'=>$this->idcliente);
        $this->General_model->add_record('cronograma_debilidad_fortaleza',$data);

    }

    function get_tabla_cronograma_debilidad_fortaleza()
    {   
        $html='';
        $result=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza($this->idcliente);
        $total=0;
        foreach ($result as $x){
            $total=$x->total;
        }
        if($total!=0){
            $html.='<div class="table-responsive text-nowrap">
            <table style="white-space: normal !important;">
              <tbody>';

                $html.='<tr class="menu_debilidad">
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Prioridad</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                  <td style="place-items: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                  <td></td>
                  <td></td>
                </tr>';
                    $result_cronog=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza',array('idcliente'=>$this->idcliente,'activo'=>1));
                    foreach ($result_cronog->result() as $x){
                        $html.='<tr style="vertical-align: text-top;" class="row_cro_df_'.$x->id.'">
                          <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_10" oninput="b_edit_resp_df('.$x->id.',10)">'.$x->debilidad_cambiar.'</textarea></div></td>
                          <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_11" oninput="b_edit_resp_df('.$x->id.',11)">'.$x->factor_manejar.'</textarea></div></td>
                          <td >
                              <select style="width: auto; border-radius: 30px; color: #636578; background-color: #a7a7a4;" class="form-select" id="id_pre_df_'.$x->id.'_12" onchange="b_edit_resp_df_select('.$x->id.',12)">
                              <option valor="0">Selecciona una opción</option>
                              <option value="1"'; if($x->prioridad==1){$html.='selected';} $html.='>Alta</option>
                              <option value="2"'; if($x->prioridad==2){$html.='selected';} $html.='>Media</option>
                              <option value="3"'; if($x->prioridad==3){$html.='selected';} $html.='>Baja</option>
                              </select>
                          </td>
                          <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_1" oninput="b_edit_resp_df('.$x->id.',1)">'.$x->preg1.'</textarea></div></td>
                          <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_2" oninput="b_edit_resp_df('.$x->id.',2)">'.$x->preg2.'</textarea></div></td>
                          <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_3" oninput="b_edit_resp_df('.$x->id.',3)">'.$x->preg3.'</textarea></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_df_'.$x->id.'_4" oninput="b_edit_resp_df('.$x->id.',4)" value="'.$x->preg4.'"></div></td>
                          <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_df_'.$x->id.'_5" oninput="b_edit_resp_df('.$x->id.',5)" value="'.$x->preg5.'"></div></td>
                          <td ><div class="padding1"><textarea style="display:none; width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_'.$x->id.'_6" oninput="b_edit_resp_df('.$x->id.',6)">'.$x->preg6.'</textarea>
                            <div class="th_c id_pre_df_'.$x->id.'_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$x->preg6.'</div>
                            </div></td>
                          <td ><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light btn_add_m" onclick="b_add_resp_cro_df_d('.$x->id.')">
                                    <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                                  </button>
                           </td>
                           <td >
                                  <button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_resp_cro_df('.$x->id.')">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button>
                           </td>
                        </tr>';
                        $result_cronogd=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza_detalle',array('idcronograma_debilidad_fortaleza'=>$x->id,'activo'=>1));
                        foreach ($result_cronogd->result() as $crod){
                            $html.='<tr style="vertical-align: text-top;" class="row_cro_df_d'.$crod->id.'">
                              <td ></td>
                              <td ></td>
                              <td ></td>
                              <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_d_'.$crod->id.'_1" oninput="b_edit_resp_df_d('.$crod->id.',1)">'.$crod->preg1.'</textarea></div></td>
                              <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_d_'.$crod->id.'_2" oninput="b_edit_resp_df_d('.$crod->id.',2)">'.$crod->preg2.'</textarea></div></td>
                              <td ><div class="padding1"><textarea style="width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_d_'.$crod->id.'_3" oninput="b_edit_resp_df_d('.$crod->id.',3)">'.$crod->preg3.'</textarea></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_df_d_'.$crod->id.'_4" oninput="b_edit_resp_df_d('.$crod->id.',4)" value="'.$crod->preg4.'"></div></td>
                              <td ><div class="padding1"><input type="date" class="form-control" id="id_pre_df_d_'.$crod->id.'_5" oninput="b_edit_resp_df_d('.$crod->id.',5)" value="'.$crod->preg5.'"></div></td>
                              <td ><div class="padding1"><textarea style="display:none; width: 255px;" rows="1" class="form-control js-auto-size" id="id_pre_df_d_'.$crod->id.'_6" oninput="b_edit_resp_df_d('.$crod->id.',6)">'.$crod->preg6.'</textarea>
                                <div class="th_c id_pre_df_d_'.$crod->id.'_6" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px;">'.$crod->preg6.'</div>
                                </div></td>
                              <td ><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_resp_cro_df_d('.$crod->id.')">
                                        <span class="tf-icons mdi mdi-delete-empty"></span>
                                    </button>
                               </td>
                               <td >
                               </td>
                            </tr>';
                        } 
                    }
            $html.='</tbody>
            </table>';
          $html.='</div>';
        }
        echo $html;    

    } 

    public function fecha_validar_df()
    {   
        $f1=$this->input->post('f1');
        $f2=$this->input->post('f2');
        $f2f = date("d-m-Y",strtotime($f2."+ 2 days")); 
        date_default_timezone_set('America/Mexico_City');       
        $fechaInicio = new DateTime("$f1 23:59:59");
        $fechaFin = new DateTime("$f2f 00:00:00");
        $intervalo = $fechaInicio->diff($fechaFin);
        $anio='';
        $mes='';
        $dia='';
        if($intervalo->y!=0){
            $anio=$intervalo->y . " años, ";
        }
        if($intervalo->m!=0){
            $mes=$intervalo->m." meses y ";
        }
            $dia=$intervalo->d." dias";
        echo $anio.$mes.$dia;
    }

    public function b_edit_respuesta_cro_debilidad_fortaleza(){
        $id_rg=$this->input->post('id');
        $pre=$this->input->post('pre');
        $num=$this->input->post('num');
        if($num==10){
            $data = array('debilidad_cambiar'=>$pre);
        }else if($num==11){
            $data = array('factor_manejar'=>$pre);
        }else if($num==12){
            $data = array('prioridad'=>$pre);
        }else{
            $data = array('preg'.$num=>$pre);
        }
        $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza');
    }

    public function b_delete_cro_debilidad_fortaleza(){
        $id_rg=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza');    
    }

    public function b_add_respuesta_cro_df_d(){
        $id_rg=$this->input->post('id');
        $data = array('idcronograma_debilidad_fortaleza'=>$id_rg);
        $this->General_model->add_record('cronograma_debilidad_fortaleza_detalle',$data);
    }

    public function b_edit_respuesta_cro_debilidad_fortaleza_detalles(){
        $id_rg=$this->input->post('id');
        $pre=$this->input->post('pre');
        $num=$this->input->post('num');
        $data = array('preg'.$num=>$pre);
        $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza_detalle');
    }

    public function b_delete_cro_debilidad_fortaleza_d(){
        $id_rg=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza_detalle');    
    }

    public function total_debilidad_fortaleza(){
        $result1=$this->Model_guia_seleccion->get_total_debilidad_fortaleza($this->idcliente);
        $valor=0;
        foreach ($result1 as $cro){
            $valor=$cro->total; 
        }
        echo $valor;
    }

    function get_tabla_cronograma_actividad()
    {   
        /*$id=$this->input->post('id');
        $anio=$this->input->post('anio');
        $txt_mes='';
        if($id==1){
            $txt_mes='Enero';
        }else if($id==2){
            $txt_mes='Febrero';
        }else if($id==3){
            $txt_mes='Marzo';
        }else if($id==4){
            $txt_mes='Abril';
        }else if($id==5){
            $txt_mes='Mayo';
        }else if($id==6){
            $txt_mes='Junio';
        }else if($id==8){
            $txt_mes='Julio';
        }else if($id==8){
            $txt_mes='Agosto';
        }else if($id==9){
            $txt_mes='Septiembre';
        }else if($id==10){
            $txt_mes='Octubre';
        }else if($id==11){
            $txt_mes='Noviembre';
        }else if($id==12){
            $txt_mes='Diciembre';
        }*/
        $html='<table style="white-space: normal !important;">
              <tbody>
                <tr>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Prioridad</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Actividad<span style="color: #ffffff00">_</span>a<span style="color: #ffffff00">_</span>Realizarse</div></div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Semana 1 <br> ¿Se<span style="color: #ffffff00">_</span>cumplió<span style="color: #ffffff00">_</span>la<span style="color: #ffffff00">_</span>actividad?</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Semana 2 <br> ¿Se<span style="color: #ffffff00">_</span>cumplió<span style="color: #ffffff00">_</span>la<span style="color: #ffffff00">_</span>actividad?</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Semana 3 <br> ¿Se<span style="color: #ffffff00">_</span>cumplió<span style="color: #ffffff00">_</span>la<span style="color: #ffffff00">_</span>actividad?</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Semana 4 <br> ¿Se<span style="color: #ffffff00">_</span>cumplió<span style="color: #ffffff00">_</span>la<span style="color: #ffffff00">_</span>actividad?</div></div></td>
                  <td style="text-align: center;"><div class="padding1"><div class="th_c" >Semana 5 <br> ¿Se<span style="color: #ffffff00">_</span>cumplió<span style="color: #ffffff00">_</span>la<span style="color: #ffffff00">_</span>actividad?</div></div></td>
                </tr>';
                $html.='<tr><td colspan="8"><br></td></tr>';
                $result1=$this->Model_guia_seleccion->get_pri_mercadoresultados_actividad($this->idcliente);
                foreach ($result1 as $cro){
                    $ba1='';
                    $ba2='';
                    $ba3='';
                    $ba4='';
                    $ba5='';
                    $prioridad_txt='';
                    if($cro->resultado==1){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_red"><div class="padding1"><div class="padding2">Alta</div></div></div></div>';
                    }else if($cro->resultado==2){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></div>';
                    }else if($cro->resultado==3){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></div></div>';
                    }

                    if($cro->semana1==1){
                        $ba1='background: #70ad47;';
                    }else if($cro->semana1==2){
                        $ba1='background: #e31a1a;';
                    }else{
                        $ba1='background: #a7a7a4;';
                    }
                    if($cro->semana2==1){
                        $ba2='background: #70ad47;';
                    }else if($cro->semana2==2){
                        $ba2='background: #e31a1a;';
                    }else{
                        $ba2='background: #a7a7a4;';
                    }
                    if($cro->semana3==1){
                        $ba3='background: #70ad47;';
                    }else if($cro->semana3==2){
                        $ba3='background: #e31a1a;';
                    }else{
                        $ba3='background: #a7a7a4;';
                    }
                    if($cro->semana4==1){
                        $ba4='background: #70ad47;';
                    }else if($cro->semana4==2){
                        $ba4='background: #e31a1a;';
                    }else{
                        $ba4='background: #a7a7a4;';
                    }
                    if($cro->semana5==1){
                        $ba5='background: #70ad47;';
                    }else if($cro->semana5==2){
                        $ba5='background: #e31a1a;';
                    }else{
                        $ba5='background: #a7a7a4;';
                    }
                    $html.='<tr>
                      <td><div class="padding1">'.$prioridad_txt.'</td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5)).'</div></div></td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.$cro->preg2.'</div></div></td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_1_'.$cro->id.'" onchange="resp_actividad('.$cro->id.',1)" style="'.$ba1.' color: white; border-radius: 30px; width: auto; ">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana1==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana1==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_2_'.$cro->id.'" onchange="resp_actividad('.$cro->id.',2)" style="'.$ba2.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana2==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana2==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_3_'.$cro->id.'" onchange="resp_actividad('.$cro->id.',3)" style="'.$ba3.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana3==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana3==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_4_'.$cro->id.'" onchange="resp_actividad('.$cro->id.',4)" style="'.$ba4.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana4==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana4==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_5_'.$cro->id.'" onchange="resp_actividad('.$cro->id.',5)" style="'.$ba5.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana5==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana5==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                    </tr>';
                }


                $result2=$this->Model_guia_seleccion->get_pri_internoesultados_actividad($this->idcliente);
                foreach ($result2 as $cro){
                    $ba1='';
                    $ba2='';
                    $ba3='';
                    $ba4='';
                    $ba5='';

                    if($cro->resultado==1){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_red"><div class="padding1"><div class="padding2">Alta</div></div></div></div>';
                    }else if($cro->resultado==2){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></div>';
                    }else if($cro->resultado==3){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></div></div>';
                    }

                    if($cro->semana1==1){
                        $ba1='background: #70ad47;';
                    }else if($cro->semana1==2){
                        $ba1='background: #e31a1a;';
                    }else{
                        $ba1='background: #a7a7a4;';
                    }
                    if($cro->semana2==1){
                        $ba2='background: #70ad47;';
                    }else if($cro->semana2==2){
                        $ba2='background: #e31a1a;';
                    }else{
                        $ba2='background: #a7a7a4;';
                    }
                    if($cro->semana3==1){
                        $ba3='background: #70ad47;';
                    }else if($cro->semana3==2){
                        $ba3='background: #e31a1a;';
                    }else{
                        $ba3='background: #a7a7a4;';
                    }
                    if($cro->semana4==1){
                        $ba4='background: #70ad47;';
                    }else if($cro->semana4==2){
                        $ba4='background: #e31a1a;';
                    }else{
                        $ba4='background: #a7a7a4;';
                    }
                    if($cro->semana5==1){
                        $ba5='background: #70ad47;';
                    }else if($cro->semana5==2){
                        $ba5='background: #e31a1a;';
                    }else{
                        $ba5='background: #a7a7a4;';
                    }
                    $html.='<tr>
                      <td><div class="padding1">'.$prioridad_txt.'</td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5)).'</div></div></td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.$cro->preg2.'</div></div></td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_i_1_'.$cro->id.'" onchange="resp_actividad_i('.$cro->id.',1)" style="'.$ba1.' color: white; border-radius: 30px; width: auto; ">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana1==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana1==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_i_2_'.$cro->id.'" onchange="resp_actividad_i('.$cro->id.',2)" style="'.$ba2.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana2==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana2==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_i_3_'.$cro->id.'" onchange="resp_actividad_i('.$cro->id.',3)" style="'.$ba3.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana3==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana3==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_i_4_'.$cro->id.'" onchange="resp_actividad_i('.$cro->id.',4)" style="'.$ba4.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana4==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana4==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_i_5_'.$cro->id.'" onchange="resp_actividad_i('.$cro->id.',5)" style="'.$ba5.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana5==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana5==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                    </tr>';
                }

                $result3=$this->Model_guia_seleccion->get_pri_estadoesultados_actividad($this->idcliente);
                foreach ($result3 as $cro){
                    $ba1='';
                    $ba2='';
                    $ba3='';
                    $ba4='';
                    $ba5='';

                    if($cro->resultado==1){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_red"><div class="padding1"><div class="padding2">Alta</div></div></div></div>';
                    }else if($cro->resultado==2){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></div>';
                    }else if($cro->resultado==3){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></div></div>';
                    }

                    if($cro->semana1==1){
                        $ba1='background: #70ad47;';
                    }else if($cro->semana1==2){
                        $ba1='background: #e31a1a;';
                    }else{
                        $ba1='background: #a7a7a4;';
                    }
                    if($cro->semana2==1){
                        $ba2='background: #70ad47;';
                    }else if($cro->semana2==2){
                        $ba2='background: #e31a1a;';
                    }else{
                        $ba2='background: #a7a7a4;';
                    }
                    if($cro->semana3==1){
                        $ba3='background: #70ad47;';
                    }else if($cro->semana3==2){
                        $ba3='background: #e31a1a;';
                    }else{
                        $ba3='background: #a7a7a4;';
                    }
                    if($cro->semana4==1){
                        $ba4='background: #70ad47;';
                    }else if($cro->semana4==2){
                        $ba4='background: #e31a1a;';
                    }else{
                        $ba4='background: #a7a7a4;';
                    }
                    if($cro->semana5==1){
                        $ba5='background: #70ad47;';
                    }else if($cro->semana5==2){
                        $ba5='background: #e31a1a;';
                    }else{
                        $ba5='background: #a7a7a4;';
                    }
                    $html.='<tr>
                      <td><div class="padding1">'.$prioridad_txt.'</td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5)).'</div></div></td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.$cro->preg2.'</div></div></td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_s_1_'.$cro->id.'" onchange="resp_actividad_s('.$cro->id.',1)" style="'.$ba1.' color: white; border-radius: 30px; width: auto; ">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana1==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana1==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_s_2_'.$cro->id.'" onchange="resp_actividad_s('.$cro->id.',2)" style="'.$ba2.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana2==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana2==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_s_3_'.$cro->id.'" onchange="resp_actividad_s('.$cro->id.',3)" style="'.$ba3.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana3==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana3==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_s_4_'.$cro->id.'" onchange="resp_actividad_s('.$cro->id.',4)" style="'.$ba4.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana4==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana4==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_s_5_'.$cro->id.'" onchange="resp_actividad_s('.$cro->id.',5)" style="'.$ba5.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana5==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana5==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                    </tr>';
                }
                
                $result4=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_actividad($this->idcliente);
                foreach ($result4 as $cro){
                    $ba1='';
                    $ba2='';
                    $ba3='';
                    $ba4='';
                    $ba5='';

                    if($cro->prioridad==1){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_red"><div class="padding1"><div class="padding2">Alta</div></div></div></div>';
                    }else if($cro->prioridad==2){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></div>';
                    }else if($cro->prioridad==3){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></div></div>';
                    }

                    if($cro->semana1==1){
                        $ba1='background: #70ad47;';
                    }else if($cro->semana1==2){
                        $ba1='background: #e31a1a;';
                    }else{
                        $ba1='background: #a7a7a4;';
                    }
                    if($cro->semana2==1){
                        $ba2='background: #70ad47;';
                    }else if($cro->semana2==2){
                        $ba2='background: #e31a1a;';
                    }else{
                        $ba2='background: #a7a7a4;';
                    }
                    if($cro->semana3==1){
                        $ba3='background: #70ad47;';
                    }else if($cro->semana3==2){
                        $ba3='background: #e31a1a;';
                    }else{
                        $ba3='background: #a7a7a4;';
                    }
                    if($cro->semana4==1){
                        $ba4='background: #70ad47;';
                    }else if($cro->semana4==2){
                        $ba4='background: #e31a1a;';
                    }else{
                        $ba4='background: #a7a7a4;';
                    }
                    if($cro->semana5==1){
                        $ba5='background: #70ad47;';
                    }else if($cro->semana5==2){
                        $ba5='background: #e31a1a;';
                    }else{
                        $ba5='background: #a7a7a4;';
                    }
                    $html.='<tr>
                      <td><div class="padding1">'.$prioridad_txt.'</td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5)).'</div></div></td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.$cro->preg2.'</div></div></td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_n_1_'.$cro->id.'" onchange="resp_actividad_n('.$cro->id.',1)" style="'.$ba1.' color: white; border-radius: 30px; width: auto; ">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana1==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana1==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_n_2_'.$cro->id.'" onchange="resp_actividad_n('.$cro->id.',2)" style="'.$ba2.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana2==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana2==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_n_3_'.$cro->id.'" onchange="resp_actividad_n('.$cro->id.',3)" style="'.$ba3.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana3==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana3==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_n_4_'.$cro->id.'" onchange="resp_actividad_n('.$cro->id.',4)" style="'.$ba4.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana4==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana4==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_n_5_'.$cro->id.'" onchange="resp_actividad_n('.$cro->id.',5)" style="'.$ba5.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana5==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana5==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                    </tr>';
                }

                $result5=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_actividad($this->idcliente);
                foreach ($result5 as $cro){
                    $ba1='';
                    $ba2='';
                    $ba3='';
                    $ba4='';
                    $ba5='';

                    if($cro->prioridad==1){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_red"><div class="padding1"><div class="padding2">Alta</div></div></div></div>';
                    }else if($cro->prioridad==2){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_amarillo"><div class="padding1"><div class="padding2">Media</div></div></div></div>';
                    }else if($cro->prioridad==3){
                        $prioridad_txt='<div class="padding1"><div class="th_c bac_verde"><div class="padding1"><div class="padding2">Baja</div></div></div></div>';
                    }

                    if($cro->semana1==1){
                        $ba1='background: #70ad47;';
                    }else if($cro->semana1==2){
                        $ba1='background: #e31a1a;';
                    }else{
                        $ba1='background: #a7a7a4;';
                    }
                    if($cro->semana2==1){
                        $ba2='background: #70ad47;';
                    }else if($cro->semana2==2){
                        $ba2='background: #e31a1a;';
                    }else{
                        $ba2='background: #a7a7a4;';
                    }
                    if($cro->semana3==1){
                        $ba3='background: #70ad47;';
                    }else if($cro->semana3==2){
                        $ba3='background: #e31a1a;';
                    }else{
                        $ba3='background: #a7a7a4;';
                    }
                    if($cro->semana4==1){
                        $ba4='background: #70ad47;';
                    }else if($cro->semana4==2){
                        $ba4='background: #e31a1a;';
                    }else{
                        $ba4='background: #a7a7a4;';
                    }
                    if($cro->semana5==1){
                        $ba5='background: #70ad47;';
                    }else if($cro->semana5==2){
                        $ba5='background: #e31a1a;';
                    }else{
                        $ba5='background: #a7a7a4;';
                    }
                    $html.='<tr>
                      <td><div class="padding1">'.$prioridad_txt.'</td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.date('d/m/Y',strtotime($cro->preg4)).'-'.date('d/m/Y',strtotime($cro->preg5)).'</div></div></td>
                      <td><div class="padding1"><div class="th_c" style="background:#a7a7a4 !important; color: #636578 !important; font-size: 16px; max-width: none; padding: 8px; text-align: center;">'.$cro->preg2.'</div></div></td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_nd_1_'.$cro->id.'" onchange="resp_actividad_nd('.$cro->id.',1)" style="'.$ba1.' color: white; border-radius: 30px; width: auto; ">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana1==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana1==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_nd_2_'.$cro->id.'" onchange="resp_actividad_nd('.$cro->id.',2)" style="'.$ba2.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana2==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana2==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_nd_3_'.$cro->id.'" onchange="resp_actividad_nd('.$cro->id.',3)" style="'.$ba3.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana3==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana3==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_nd_4_'.$cro->id.'" onchange="resp_actividad_nd('.$cro->id.',4)" style="'.$ba4.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana4==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana4==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                      <td>
                        <div class="padding1">
                          <select class="form-select" id="idact_nd_5_'.$cro->id.'" onchange="resp_actividad_nd('.$cro->id.',5)" style="'.$ba5.' color: white; border-radius: 30px; width: auto;">
                          <option value="0">Selecciona una opción</option>
                          <option value="1"'; if($cro->semana5==1){$html.='selected';} $html.='>Si</option>
                          <option value="2"'; if($cro->semana5==2){$html.='selected';} $html.='>No</option>
                        </select></div>
                      </td>
                    </tr>';
                }

              $html.='</tbody>
            </table>';
        echo $html;
    }

    public function edit_respuesta_actividad(){
        $id=$this->input->post('id');
        //$mes=$this->input->post('mes');
        $respuesta=$this->input->post('respuesta');
        $sem=$this->input->post('sem');

        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles_cronograma_actividad',array('idguia_seleccion_preguntas_respuesta_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente));
        $html='';

        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }

        if($id_rg==0){
            $data = array('idguia_seleccion_preguntas_respuesta_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente,'semana'.$sem=>$respuesta);
            $this->General_model->add_record('guia_seleccion_preguntas_respuesta_detalles_cronograma_actividad ',$data);
        }else{
            $data = array('semana'.$sem=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_detalles_cronograma_actividad ');    
        }

    }

    public function edit_respuesta_actividad_i(){
        $id=$this->input->post('id');
        $mes=$this->input->post('mes');
        $respuesta=$this->input->post('respuesta');
        $sem=$this->input->post('sem');

        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_ps_re_detalles_cronograma_actividad',array('idguia_seleccion_fortalezas_ps_re_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente));
        $html='';

        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }

        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_ps_re_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente,'semana'.$sem=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_ps_re_detalles_cronograma_actividad ',$data);
        }else{
            $data = array('semana'.$sem=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_ps_re_detalles_cronograma_actividad ');    
        }

    }

    public function edit_respuesta_actividad_s(){
        $id=$this->input->post('id');
        $mes=$this->input->post('mes');
        $respuesta=$this->input->post('respuesta');
        $sem=$this->input->post('sem');

        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_r_r_d_cron_act',array('idguia_seleccion_fortalezas_estado_r_r_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente));
        $html='';

        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }

        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_estado_r_r_detalles_cronograma'=>$id,'idcliente'=>$this->idcliente,'semana'.$sem=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_estado_r_r_d_cron_act ',$data);
        }else{
            $data = array('semana'.$sem=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_r_r_d_cron_act ');    
        }

    }

    public function edit_respuesta_actividad_n(){
        $id=$this->input->post('id');
        $mes=$this->input->post('mes');
        $respuesta=$this->input->post('respuesta');
        $sem=$this->input->post('sem');
        $result=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza_actividad',array('idcronograma_debilidad_fortaleza'=>$id,'idcliente'=>$this->idcliente));
        $html='';
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idcronograma_debilidad_fortaleza'=>$id,'idcliente'=>$this->idcliente,'semana'.$sem=>$respuesta);
            $this->General_model->add_record('cronograma_debilidad_fortaleza_actividad ',$data);
        }else{
            $data = array('semana'.$sem=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza_actividad ');    
        }

    }

    public function edit_respuesta_actividad_nd(){
        $id=$this->input->post('id');
        $mes=$this->input->post('mes');
        $respuesta=$this->input->post('respuesta');
        $sem=$this->input->post('sem');
        $result=$this->General_model->getselectwheren('cronograma_debilidad_fortaleza_detalle_actividad',array('idcronograma_debilidad_fortaleza_detalle'=>$id,'idcliente'=>$this->idcliente));
        $html='';
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idcronograma_debilidad_fortaleza_detalle'=>$id,'idcliente'=>$this->idcliente,'semana'.$sem=>$respuesta);
            $this->General_model->add_record('cronograma_debilidad_fortaleza_detalle_actividad',$data);
        }else{
            $data = array('semana'.$sem=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'cronograma_debilidad_fortaleza_detalle_actividad');    
        }

    }

    /*function get_tabla_cronograma_resumen_anio()
    {   
        $html='<div class="btn-group">';
            $result_res=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_resumen_anio($this->idcliente);
            $tipo=0;
            foreach ($result_res as $x){    
                $html.='<button type="button" class="btn btn-primary btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="get_tablacronograma_resumen('.date('Y',strtotime($x->preg5)).','.$tipo.')">Resumen del año '.date('Y',strtotime($x->preg5)).'</button>';
                $tipo++;
            }
        $html.='</div>';
        echo $html;
    }*/

    function get_tabla_cronograma_resumen()
    {   
        //$anio=$this->input->post('anio');
        $html='';
        
        // 1
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente);
                        setlocale(LC_ALL,"es_ES@euro","es_ES","esp");
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        /*    
        //  2
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 2 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,2);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,2);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,2);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,2);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,2);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  3
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 3 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,3);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,3);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,3);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,3);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,3);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  4
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 4 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,4);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,4);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,4);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,4);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,4);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  5
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 5 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,5);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,5);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,5);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,5);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,5);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  6
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 6 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,6);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,6);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,6);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,6);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,6);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  7
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 7 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,7);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,7);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,7);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,7);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,7);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  8
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 8 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,8);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,8);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,8);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,8);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,8);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  9
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 9 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,9);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,9);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,9);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,9);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,9);
                        foreach ($result_cronn as $xnn){
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  10
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 10 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,10);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,10);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,10);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,10);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,10);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  11
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 11 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,11);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,11);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,11);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,11);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,11);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        //  12
        $html.='<br>
            <div style="padding-bottom: 8px;">
                <div class="text_act">Actividades mes 12 de trabajo</div>
            </div>
            <br>
            <div class="table-responsive text-nowrap" style="background-color: #2a1f2f; padding: 16px; border-radius: 14px;">
                <table style="white-space: normal !important;">
                  <tbody>';
                    $html.='<tr>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Debilidad a cambiar Fortaleza a mejorar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Factor a manejar</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo general</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" style="width: 255px;">Plan de trabajo especifico</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Responsable</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de inicio</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Fecha de termino</div></div></td>
                      <td style="text-align: center;"><div class="padding1"><div class="th_c" >Tiempo estimado para concluir el objetivo especifico</div></div></td>
                    </tr>';
                        $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles_anio_mes($this->idcliente,$anio,12);
                        foreach ($result_merc as $x){
                            $fecha4 = strftime("%d de %B %Y", strtotime($x->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($x->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$x->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$x->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$x->preg6.'</div></div>
                                </div></td>
                            </tr>';                      
                        }

                        $result_inter=$this->Model_guia_seleccion->get_pri_internosultados_detalles_anio_mes($this->idcliente,$anio,12);
                        foreach ($result_inter as $int){
                            $fecha4 = strftime("%d de %B %Y", strtotime($int->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($int->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu análisis interno / '.$int->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$int->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$int->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_est=$this->Model_guia_seleccion->get_pri_estadosultados_detalles_anio_mes($this->idcliente,$anio,12);
                        foreach ($result_est as $est){
                            $fecha4 = strftime("%d de %B %Y", strtotime($est->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($est->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades de tu estado de resultados / '.$est->titulo.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_white"><div class="padding2" style="color:white">'.$est->pregunta.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c bac_red" ><div class="padding1"><div class="padding2">Alta</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$est->preg6.'</div></div></td>
                            </tr>';
                        }

                        $result_cron=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_anio_mes($this->idcliente,$anio,12);
                        foreach ($result_cron as $xn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }

                        $result_cronn=$this->Model_guia_seleccion->get_cronograma_debilidad_fortaleza_detalle_anio_mes($this->idcliente,$anio,12);
                        foreach ($result_cronn as $xnn){
                            $fecha4 = strftime("%d de %B %Y", strtotime($xnn->preg4));
                            $fecha5 = strftime("%d de %B %Y", strtotime($xnn->preg5));
                            $html.='<tr>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">Debilidades del mercado / '.$xnn->debilidad_cambiar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="th_c"><div class="padding2" style="color:white">'.$xnn->factor_manejar.'</div></div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg1.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg2.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg3.'</div></div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha4.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$fecha5.'</div></td>
                              <td ><div class="padding1"><div class="div_g">'.$xnn->preg6.'</div></div>
                                </div></td>
                            </tr>';    
                        }
                
                     $html.='</tbody>
                </table>
            </div>';
        */ 
        echo $html;
    }

    function reporte_excel_actividades()
    {   
        $data['idcliente']=$this->idcliente;
        $data['fecha']=$this->fechahora;
        /*$data['mes']=$mes;
        $data['laboratorio']=$laboratorio;
        $data['get_pedidos']=$this->ModeloPedido->pedidodetalle_excel($mes,$anio,$laboratoriov,$laboratorio);*/
        $this->load->view('analisis/excel_actividades',$data);
    }

    function reporte_excel_cumplimiento()
    {   
        $data['idcliente']=$this->idcliente;
        $data['fecha']=$this->fechahora;
        /*$data['mes']=$mes;
        $data['laboratorio']=$laboratorio;
        $data['get_pedidos']=$this->ModeloPedido->pedidodetalle_excel($mes,$anio,$laboratoriov,$laboratorio);*/
        $this->load->view('analisis/excel_cumplimiento',$data);
    }

}
