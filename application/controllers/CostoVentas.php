<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CostoVentas extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_periodo');
        $this->load->model('Model_cliente');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloCostosventas');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');

        $this->anio_actual=date('Y');
        
        $this->anio_actual=$this->anio_actual-1;//verificar después si se queda asi con un año atras como inicial si no se comenta

        $this->anio_prev_1=$this->anio_actual-1;
        $this->anio_prev_2=$this->anio_actual-2;
        $this->anio_prev_3=$this->anio_actual-3;
        $this->anio_prev_4=$this->anio_actual-4;
        $this->anio_prev_5=$this->anio_actual-5;

        $this->anio_next_1=$this->anio_actual+1;
        $this->anio_next_2=$this->anio_actual+2;
        $this->anio_next_3=$this->anio_actual+3;

        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,10);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=2;
        $data['btn_active_sub']=3;
        
        $result_vh=$this->General_model->getselectwheren('ventashistoricas',array('idcliente'=>$this->idcliente,'activo'=>1));
        $data['result_vh']=$result_vh;

        

        $data['anio_actual']=$this->anio_actual;
        $data['anio_next_1']=$this->anio_next_1;
        $data['anio_next_2']=$this->anio_next_2;
        $data['anio_next_3']=$this->anio_next_3;

        $data['anio_prev_1']=$this->anio_prev_1;
        $data['anio_prev_2']=$this->anio_prev_2;
        $data['anio_prev_3']=$this->anio_prev_3;
        $data['anio_prev_4']=$this->anio_prev_4;
        $data['anio_prev_5']=$this->anio_prev_5;

        $this->v_e_costosventas();
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('costosventas/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('costosventas/indexjs');
    }
    function v_e_costosventas(){
        $result_vh=$this->General_model->getselectwheren('ventashistoricas',array('idcliente'=>$this->idcliente,'activo'=>1));
        $this->verificar_exist_costosventas($result_vh);
    }

    function verificar_exist_costosventas($result_vh){
        $datainsert=array();

        foreach ($result_vh->result() as $item){ 
            $id_servicio=$item->id;
            
            

            $result_aa = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_actual,'activo'=>1));
            if($result_aa->num_rows()==0){
                
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_actual,'mes'=>$mesrow);
                }
            }

            $result_a1 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_1,'activo'=>1));
            if($result_a1->num_rows()==0){
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_1,'mes'=>$mesrow);
                }

            }

            $result_a2 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_2,'activo'=>1));
            if($result_a2->num_rows()==0){
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_2,'mes'=>$mesrow);
                }
                

            }

            $result_a3 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_3,'activo'=>1));
            if($result_a3->num_rows()==0){
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_3,'mes'=>$mesrow);
                }

            }

            $result_a4 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_4,'activo'=>1));
            if($result_a4->num_rows()==0){
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_4,'mes'=>$mesrow);
                }  
            }

            $result_a5 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_5,'activo'=>1));
            if($result_a5->num_rows()==0){
                for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                    $datainsert[]=array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_5,'mes'=>$mesrow);
                }  
            }

        }
        if(count($datainsert)>0){
            $this->ModeloCatalogos->insert_batch('costosventas',$datainsert);
        }
        //=============================================================================
            $datainsert2=array();
            foreach ($result_vh->result() as $item){
                $id_servicio=$item->id;
                //log_message('error'," id_servicio : $id_servicio");
                //log_message('error'," costosventas_pro ->id_linea_servicio: $id_servicio");
                $result_cvp = $this->ModeloCatalogos->getselectwheren('costosventas_pro',array('id_linea_servicio'=>$id_servicio,'activo'=>1));
                foreach ($result_cvp->result() as $item_cvp){
                    $idpro=$item_cvp->id;
                    //log_message('error'," costosventas_pro_dll ->idser: $id_servicio ->idpro: $idpro -> anio: $this->anio_actual");
                    $result_cvpdll = $this->ModeloCatalogos->getselectwheren('costosventas_pro_dll',array('idser'=>$id_servicio,'idpro'=>$idpro,'anio'=>$this->anio_actual,'activo'=>1));
                    if($result_cvpdll->num_rows()==0){
                        for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                            $datainsert2[]=array('idser'=>$id_servicio,'idpro'=>$idpro,'anio'=>$this->anio_actual,'mes'=>$mesrow);
                        }
                    }

                }

            }
            if(count($datainsert2)>0){
                $this->ModeloCatalogos->insert_batch('costosventas_pro_dll',$datainsert2);
            }
        //=====================================================================================
            $this->proyectado_inservalue($result_vh,$this->anio_next_1);
            $this->proyectado_inservalue($result_vh,$this->anio_next_2);
            $this->proyectado_inservalue($result_vh,$this->anio_next_3);
        //=====================================================================================
    }
    function proyectado_inservalue($result_vh,$anios_next){
        $datainsert3=array();
            foreach ($result_vh->result() as $item){
                $id_servicio=$item->id;
                //===================================================================
                    $result_cvpdll0 = $this->ModeloCatalogos->getselectwheren('costosventas_proyectados',array('idser'=>$id_servicio,'idpro'=>0,'anio'=>$anios_next,'activo'=>1));
                    if($result_cvpdll0->num_rows()==0){
                        for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                            $datainsert3[]=array('idser'=>$id_servicio,'idpro'=>0,'anio'=>$anios_next,'mes'=>$mesrow);
                        }
                    }
                //===================================================================
                $result_cvp = $this->ModeloCatalogos->getselectwheren('costosventas_pro',array('id_linea_servicio'=>$id_servicio,'activo'=>1));
                foreach ($result_cvp->result() as $item_cvp){
                    $idpro=$item_cvp->id;
                    //log_message('error'," costosventas_pro_dll ->idser: $id_servicio ->idpro: $idpro -> anio: $this->anio_actual");
                    $result_cvpdll = $this->ModeloCatalogos->getselectwheren('costosventas_proyectados',array('idser'=>$id_servicio,'idpro'=>$idpro,'anio'=>$anios_next,'activo'=>1));
                    if($result_cvpdll->num_rows()==0){
                        for ($mesrow = 1; $mesrow <= 12; $mesrow++) {
                            $datainsert3[]=array('idser'=>$id_servicio,'idpro'=>$idpro,'anio'=>$anios_next,'mes'=>$mesrow);
                        }
                    }

                }

            }
            if(count($datainsert3)>0){
                $this->ModeloCatalogos->insert_batch('costosventas_proyectados',$datainsert3);
            }
    }
    function obtener_info_tables(){
        $params = $this->input->post();
        $tipo = $params['tipo'];
        $id_servicio = $params['idser'];
        $id_servicio = $params['idser'];
        $anio = $params['anio'];
        $html='<input type="hidden" class="form-control" id="id_servicio" value="'.$id_servicio.'" >';
        $result_meses = $this->ModeloCatalogos->getselect_tabla('meses');
            //================================================================
                //=====================================================================
                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_prev_5);
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                    }

                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_prev_4);
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                    }
                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_prev_3);
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                    }
                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_prev_2);
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                    }
                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_prev_1);
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                    }
                    $result_vi = $this->ModeloCostosventas->get_ventasingresos($this->idcliente,$this->anio_actual);
                    $totalg_anio_actual=0;
                    foreach ($result_vi->result() as $item_vi) {
                        ${'datapro_'.$item_vi->anio.'_'.$item_vi->mes}=$item_vi->cantidad;
                        $totalg_anio_actual=$totalg_anio_actual+$item_vi->cantidad;
                    }
                //=====================================================================
                    
                //=====================================================================
            //================================================================
        if($tipo==0){
            $res_pro_dll = $this->ModeloCostosventas->obtener_info_costosventas_sum($id_servicio,$this->anio_actual);
            $totalgeneral_anioactual=0;
            foreach ($res_pro_dll->result() as $item_pd) {
                ${'vc_'.$item_pd->anio.'_'.$item_pd->mes}=$item_pd->valor;
                $totalgeneral_anioactual+=$item_pd->valor;
            }
            //=============================================================


                $result_aa = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_actual,'activo'=>1));
                $result_a1 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_1,'activo'=>1));
                $result_a2 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_2,'activo'=>1));
                $result_a3 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_3,'activo'=>1));
                $result_a4 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_4,'activo'=>1));
                $result_a5 = $this->ModeloCatalogos->getselectwheren('costosventas',array('id_linea_servicio'=>$id_servicio,'anio'=>$this->anio_prev_5,'activo'=>1));
                foreach ($result_a5->result() as $item) {
                    ${'v_'.$item->anio.'_'.$item->mes}='<input type="number" class="form-control col_a_'.$item->anio.' col_m_'.$item->mes.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->valor.'" onchange="update_v_table0('.$item->id.','.$item->anio.','.$item->mes.')">';
                    
                }
                foreach ($result_a4->result() as $item) {
                    ${'v_'.$item->anio.'_'.$item->mes}='<input type="number" class="form-control col_a_'.$item->anio.' col_m_'.$item->mes.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->valor.'" onchange="update_v_table0('.$item->id.','.$item->anio.','.$item->mes.')">';
                    
                }
                foreach ($result_a3->result() as $item) {
                    ${'v_'.$item->anio.'_'.$item->mes}='<input type="number" class="form-control col_a_'.$item->anio.' col_m_'.$item->mes.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->valor.'" onchange="update_v_table0('.$item->id.','.$item->anio.','.$item->mes.')">';
                    
                }
                foreach ($result_a2->result() as $item) {
                    ${'v_'.$item->anio.'_'.$item->mes}='<input type="number" class="form-control col_a_'.$item->anio.' col_m_'.$item->mes.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->valor.'" onchange="update_v_table0('.$item->id.','.$item->anio.','.$item->mes.')">';
                    
                }
                foreach ($result_a1->result() as $item) {
                    ${'v_'.$item->anio.'_'.$item->mes}='<input type="number" class="form-control col_a_'.$item->anio.' col_m_'.$item->mes.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->valor.'" onchange="update_v_table0('.$item->id.','.$item->anio.','.$item->mes.')">';

                }
            //=====================================================================
                ${'sp_'.$this->anio_next_1.'_1'}=0;${'sp_'.$this->anio_next_1.'_2'}=0;${'sp_'.$this->anio_next_1.'_3'}=0;${'sp_'.$this->anio_next_1.'_4'}=0;${'sp_'.$this->anio_next_1.'_5'}=0;${'sp_'.$this->anio_next_1.'_6'}=0;${'sp_'.$this->anio_next_1.'_7'}=0;${'sp_'.$this->anio_next_1.'_8'}=0;${'sp_'.$this->anio_next_1.'_9'}=0;${'sp_'.$this->anio_next_1.'_10'}=0;${'sp_'.$this->anio_next_1.'_11'}=0;${'sp_'.$this->anio_next_1.'_12'}=0;

                ${'sp_'.$this->anio_next_2.'_1'}=0;${'sp_'.$this->anio_next_2.'_2'}=0;${'sp_'.$this->anio_next_2.'_3'}=0;${'sp_'.$this->anio_next_2.'_4'}=0;${'sp_'.$this->anio_next_2.'_5'}=0;${'sp_'.$this->anio_next_2.'_6'}=0;${'sp_'.$this->anio_next_2.'_7'}=0;${'sp_'.$this->anio_next_2.'_8'}=0;${'sp_'.$this->anio_next_2.'_9'}=0;${'sp_'.$this->anio_next_2.'_10'}=0;${'sp_'.$this->anio_next_2.'_11'}=0;${'sp_'.$this->anio_next_2.'_12'}=0;

                ${'sp_'.$this->anio_next_3.'_1'}=0;${'sp_'.$this->anio_next_3.'_2'}=0;${'sp_'.$this->anio_next_3.'_3'}=0;${'sp_'.$this->anio_next_3.'_4'}=0;${'sp_'.$this->anio_next_3.'_5'}=0;${'sp_'.$this->anio_next_3.'_6'}=0;${'sp_'.$this->anio_next_3.'_7'}=0;${'sp_'.$this->anio_next_3.'_8'}=0;${'sp_'.$this->anio_next_3.'_9'}=0;${'sp_'.$this->anio_next_3.'_10'}=0;${'sp_'.$this->anio_next_3.'_11'}=0;${'sp_'.$this->anio_next_3.'_12'}=0;

                $result_sp = $this->ModeloCatalogos->sum_proyeccion($id_servicio,$this->anio_next_1);
                foreach ($result_sp->result() as $itemsp) {
                    ${'sp_'.$itemsp->anio.'_'.$itemsp->mes}=$itemsp->valor;
                }
                $result_sp = $this->ModeloCatalogos->sum_proyeccion($id_servicio,$this->anio_next_2);
                foreach ($result_sp->result() as $itemsp) {
                    ${'sp_'.$itemsp->anio.'_'.$itemsp->mes}=$itemsp->valor;
                }
                $result_sp = $this->ModeloCatalogos->sum_proyeccion($id_servicio,$this->anio_next_3);
                foreach ($result_sp->result() as $itemsp) {
                    ${'sp_'.$itemsp->anio.'_'.$itemsp->mes}=$itemsp->valor;
                }
            //======================================================================
                    $sum_v_proye1=0;$sum_v_proye2=0;$sum_v_proye3=0;
                    ${'proye1_'.$this->anio_next_1}=0;${'proye2_'.$this->anio_next_1}=0;${'proye3_'.$this->anio_next_1}=0;${'proye4_'.$this->anio_next_1}=0;${'proye5_'.$this->anio_next_1}=0;${'proye6_'.$this->anio_next_1}=0;${'proye7_'.$this->anio_next_1}=0;${'proye8_'.$this->anio_next_1}=0;${'proye9_'.$this->anio_next_1}=0;${'proye10_'.$this->anio_next_1}=0;${'proye11_'.$this->anio_next_1}=0;${'proye12_'.$this->anio_next_1}=0;${'proye13_'.$this->anio_next_1}=0;
                    $sum_v_proye1=0;
                    $result_vpvd = $this->ModeloCatalogos->v_proyeccion($id_servicio,$this->anio_next_1);
                    foreach ($result_vpvd->result() as $itemvpv) {
                        ${'proye'.$itemvpv->mes.'_'.$itemvpv->anio}=$itemvpv->opcion_seleccionada;
                        $sum_v_proye1=$sum_v_proye1+$itemvpv->opcion_seleccionada;
                    }
                    ${'proye1_'.$this->anio_next_2}=0;${'proye2_'.$this->anio_next_2}=0;${'proye3_'.$this->anio_next_2}=0;${'proye4_'.$this->anio_next_2}=0;${'proye5_'.$this->anio_next_2}=0;${'proye6_'.$this->anio_next_2}=0;${'proye7_'.$this->anio_next_2}=0;${'proye8_'.$this->anio_next_2}=0;${'proye9_'.$this->anio_next_2}=0;${'proye10_'.$this->anio_next_2}=0;${'proye11_'.$this->anio_next_2}=0;${'proye12_'.$this->anio_next_2}=0;${'proye13_'.$this->anio_next_2}=0;
                    $result_vpvd2 = $this->ModeloCatalogos->v_proyeccion($id_servicio,$this->anio_next_2);
                    $num2=$result_vpvd2->num_rows();
                    log_message('error',"result_vpvd2 $num2 ");
                    foreach ($result_vpvd2->result() as $itemvpv2) {
                        ${'proye'.$itemvpv2->mes.'_'.$itemvpv2->anio}=$itemvpv2->opcion_seleccionada;
                        $sum_v_proye2=$sum_v_proye2+$itemvpv2->opcion_seleccionada;
                    }
                    ${'proye1_'.$this->anio_next_3}=0;${'proye2_'.$this->anio_next_3}=0;${'proye3_'.$this->anio_next_3}=0;${'proye4_'.$this->anio_next_3}=0;${'proye5_'.$this->anio_next_3}=0;${'proye6_'.$this->anio_next_3}=0;${'proye7_'.$this->anio_next_3}=0;${'proye8_'.$this->anio_next_3}=0;${'proye9_'.$this->anio_next_3}=0;${'proye10_'.$this->anio_next_3}=0;${'proye11_'.$this->anio_next_3}=0;${'proye12_'.$this->anio_next_3}=0;${'proye13_'.$this->anio_next_3}=0;
                    $result_vpvd3 = $this->ModeloCatalogos->v_proyeccion($id_servicio,$this->anio_next_3);
                    foreach ($result_vpvd3->result() as $itemvpv3) {
                        ${'proye'.$itemvpv3->mes.'_'.$itemvpv3->anio}=$itemvpv3->opcion_seleccionada;
                        $sum_v_proye3=$sum_v_proye3+$itemvpv3->opcion_seleccionada;
                    }
            //======================================================================
            

            $html.='<table class="table_oit_0 table table-bordered" border="1">
                    <thead>
                        <tr><th colspan="13"></th><th colspan="6">Presupuesto</th></tr>
                        <tr>
                            <th>Mes / Año</th>
                            <th class="anio_prev_5" data-anio="'.$this->anio_prev_5.'">'.$this->anio_prev_5.'</th>
                            <th>%  s/vtas</th>
                            <th class="anio_prev_4" data-anio="'.$this->anio_prev_4.'">'.$this->anio_prev_4.'</th>
                            <th>%  s/vtas</th>
                            <th class="anio_prev_3" data-anio="'.$this->anio_prev_3.'">'.$this->anio_prev_3.'</th>
                            <th>%  s/vtas</th>
                            <th class="anio_prev_2" data-anio="'.$this->anio_prev_2.'">'.$this->anio_prev_2.'</th>
                            <th>%  s/vtas</th>
                            <th class="anio_prev_1" data-anio="'.$this->anio_prev_1.'">'.$this->anio_prev_1.'</th>
                            <th>%  s/vtas</th>
                            <th class="anio_actual" data-anio="'.$this->anio_actual.'">'.$this->anio_actual.'</th>
                            <th>%  s/vtas</th>
                            <th>Promedio</th>
                            <th>%  s/vtas</th>
                            <th>'.$this->anio_next_1.'</th>
                            <th>%  s/vtas</th>
                            <th>'.$this->anio_next_2.'</th>
                            <th>%  s/vtas</th>
                            <th>'.$this->anio_next_3.'</th>
                            <th>%  s/vtas</th>
                        </tr>
                    </thead>
                    <tbody>';
                        foreach ($result_meses->result() as $item_m) {
                            $idmes = $item_m->idmes;
                            $mesname = $item_m->mesname;
                            $html.='<tr>';
                                $html.='<td class="mesestd" data-meses="'.$idmes.'" >'.$mesname.'</td>';
                                $html.='<td class="td_c_input">'.${'v_'.$this->anio_prev_5.'_'.$idmes}.'</td>';
                                $html.='<td class="cal_pro prom_'.$this->anio_prev_5.' prom_'.$this->anio_prev_5.'_'.$idmes.'" data-mes="'.$idmes.'" data-anio="'.$this->anio_prev_5.'" data-datapro="'.${'datapro_'.$this->anio_prev_5.'_'.$idmes}.'" ></td>';

                                $html.='<td class="td_c_input">'.${'v_'.$this->anio_prev_4.'_'.$idmes}.'</td>';
                                $html.='<td class="cal_pro prom_'.$this->anio_prev_4.' prom_'.$this->anio_prev_4.'_'.$idmes.'" data-mes="'.$idmes.'" data-anio="'.$this->anio_prev_4.'" data-datapro="'.${'datapro_'.$this->anio_prev_4.'_'.$idmes}.'"></td>';
                                
                                $html.='<td class="td_c_input">'.${'v_'.$this->anio_prev_3.'_'.$idmes}.'</td>';
                                $html.='<td class="cal_pro prom_'.$this->anio_prev_3.' prom_'.$this->anio_prev_3.'_'.$idmes.'" data-mes="'.$idmes.'" data-anio="'.$this->anio_prev_3.'" data-datapro="'.${'datapro_'.$this->anio_prev_3.'_'.$idmes}.'"></td>';
                                
                                $html.='<td class="td_c_input">'.${'v_'.$this->anio_prev_2.'_'.$idmes}.'</td>';
                                $html.='<td class="cal_pro prom_'.$this->anio_prev_2.' prom_'.$this->anio_prev_2.'_'.$idmes.'" data-mes="'.$idmes.'" data-anio="'.$this->anio_prev_2.'" data-datapro="'.${'datapro_'.$this->anio_prev_2.'_'.$idmes}.'"></td>';
                                
                                $html.='<td class="td_c_input">'.${'v_'.$this->anio_prev_1.'_'.$idmes}.'</td>';
                                $html.='<td class="cal_pro prom_'.$this->anio_prev_1.' prom_'.$this->anio_prev_1.'_'.$idmes.'" data-mes="'.$idmes.'" data-anio="'.$this->anio_prev_1.'" data-datapro="'.${'datapro_'.$this->anio_prev_1.'_'.$idmes}.'"></td>';
                                if(isset(${'vc_'.$this->anio_actual.'_'.$idmes})){
                                    $procentaje11=${'vc_'.$this->anio_actual.'_'.$idmes};
                                }else{
                                    $procentaje11=0;
                                }
                                $html.='<td class="td_c_input v_'.$this->anio_actual.'_'.$idmes.'" ><input class="form-control col_m_'.$idmes.'"  value="'.$procentaje11.'" readonly></td>';
                                $prom_anioactual=$procentaje11/${'datapro_'.$this->anio_actual.'_'.$idmes};
                                $prom_anioactual_l=round($prom_anioactual*100);
                                $html.='<td class=" prom_'.$this->anio_prev_5.' prom_'.$this->anio_prev_5.'_'.$idmes.' '.$prom_anioactual_l.' " data-mes="'.$idmes.'" data-anio="'.$this->anio_actual.'" data-datapro="'.${'datapro_'.$this->anio_actual.'_'.$idmes}.'" data-vprom="'.$prom_anioactual.'">'.$prom_anioactual_l.' %</td>';
                                
                                $html.='<td class="cal_prom cal_promedio_'.$idmes.'"></td>';
                                $html.='<td class="cal_porc cal_porc_'.$idmes.'"data-anioacvalor="'.${'datapro_'.$this->anio_actual.'_'.$idmes}.'"></td>';
                                //==================================
                                    



                                    if(isset(${'sp_'.$this->anio_next_1.'_'.$idmes})){
                                        $v1=${'sp_'.$this->anio_next_1.'_'.$idmes};

                                        if(isset(${'proye'.$idmes.'_'.$this->anio_next_1})){
                                            if(${'sp_'.$this->anio_next_1.'_'.$idmes}>0 and ${'proye'.$idmes.'_'.$this->anio_next_1}>0){
                                                $p1=${'sp_'.$this->anio_next_1.'_'.$idmes}/${'proye'.$idmes.'_'.$this->anio_next_1};
                                            }else{
                                                $p1=0;
                                            }
                                        }else{
                                            $p1=0;
                                        }
                                        
                                    }else{
                                        $v1=0;
                                        $p1=0;
                                    }
                                    $v2=${'sp_'.$this->anio_next_2.'_'.$idmes};
                                    if(isset(${'sp_'.$this->anio_next_2.'_'.$idmes})){
                                        $v2=${'sp_'.$this->anio_next_2.'_'.$idmes};

                                        if(isset(${'proye'.$idmes.'_'.$this->anio_next_2})){
                                            if(${'sp_'.$this->anio_next_2.'_'.$idmes}>0 and ${'proye'.$idmes.'_'.$this->anio_next_2}>0){
                                                $p2=${'sp_'.$this->anio_next_2.'_'.$idmes}/${'proye'.$idmes.'_'.$this->anio_next_2};
                                            }else{
                                                $p2=0;
                                            }
                                        }else{
                                            $p2=0;
                                        }
                                    }else{
                                        $v2=0;
                                        $p2=0;
                                    }

                                    if(isset(${'sp_'.$this->anio_next_3.'_'.$idmes})){
                                        $v3=${'sp_'.$this->anio_next_3.'_'.$idmes};

                                        if(isset(${'proye'.$idmes.'_'.$this->anio_next_3})){
                                            if(${'sp_'.$this->anio_next_3.'_'.$idmes}>0 and ${'proye'.$idmes.'_'.$this->anio_next_3}>0){
                                                $p3=${'sp_'.$this->anio_next_3.'_'.$idmes}/${'proye'.$idmes.'_'.$this->anio_next_3};
                                            }else{
                                                $p3=0;
                                            }
                                        }else{
                                            $p3=0;
                                        }
                                    }else{
                                        $v3=0;
                                         $p3=0;
                                    }

                                //==================================
                                $html.='<td class="tg_v1 v1_'.$idmes.'" data-valor="'.$v1.'">'.number_format($v1,2,'.',',').'</td>';
                                $html.='<td class="p1_'.$idmes.'" data-valor="'.$p1.'">'.round($p1*100).' %</td>';
                                $html.='<td class="tg_v2 v2_'.$idmes.'" data-valor="'.$v2.'">'.number_format($v2,2,'.',',').'</td>';
                                $html.='<td class="p2_'.$idmes.'" data-valor="'.$p2.'">'.round($p2*100).' %</td>';
                                $html.='<td class="tg_v3 v3_'.$idmes.'" data-valor="'.$v3.'">'.number_format($v3,2,'.',',').'</td>';
                                $html.='<td class="p3_'.$idmes.'" data-valor="'.$p3.'">'.round($p3*100).' %</td>';
                            $html.='</tr>';
                        }
                            $html.='<tr>';
                                $html.='<td>Total General</td>';
                                $html.='<td class="totalg tg_'.$this->anio_prev_5.'"></td>';
                                $html.='<td class="prom_g pg_'.$this->anio_prev_5.'"></td>';
                                $html.='<td class="totalg tg_'.$this->anio_prev_4.'"></td>';
                                $html.='<td class="prom_g pg_'.$this->anio_prev_4.'"></td>';
                                $html.='<td class="totalg tg_'.$this->anio_prev_3.'"></td>';
                                $html.='<td class="prom_g pg_'.$this->anio_prev_3.'"></td>';
                                $html.='<td class="totalg tg_'.$this->anio_prev_2.'"></td>';
                                $html.='<td class="prom_g pg_'.$this->anio_prev_2.'"></td>';
                                $html.='<td class="totalg tg_'.$this->anio_prev_1.'"></td>';
                                $html.='<td class="prom_g pg_'.$this->anio_prev_1.'"></td>';
                                $html.='<td class="totalg tg_'.$this->anio_actual.' " data-valor_pg="'.$totalgeneral_anioactual.'">'.number_format($totalgeneral_anioactual,2,'.',',').'</td>';
                                $pro_anio_actual=$totalgeneral_anioactual/$totalg_anio_actual;
                                $pro_anio_actual_l=round($pro_anio_actual*100);
                                $html.='<td class="prom_g pg_'.$this->anio_actual.'" data-datapro="'.$pro_anio_actual.'">'.$pro_anio_actual_l.' %</td>';

                                
                                $html.='<td class="cal_promedio_t"></td>';
                                $html.='<td class="cal_promedio_porc" data-valor="'.$totalg_anio_actual.'"></td>';
                                $html.='<td class="r_tg_v1"></td>';
                                $html.='<td class="r_tg_p1" data-value="'.$sum_v_proye1.'"></td>';
                                $html.='<td class="r_tg_v2"></td>';
                                $html.='<td class="r_tg_p2" data-value="'.$sum_v_proye2.'"></td>';
                                $html.='<td class="r_tg_v3"></td>';
                                $html.='<td class="r_tg_p3" data-value="'.$sum_v_proye3.'"></td>';
                            $html.='</tr>';
                        
                        
                $html.='</tbody>
            </table>';
        }
        if($tipo==2){
            $result_pro = $this->ModeloCatalogos->getselectwheren('costosventas_pro',array('id_linea_servicio'=>$id_servicio,'activo'=>1));
            $res_pro_dll = $this->ModeloCostosventas->obtener_info_costosventas($id_servicio,$this->anio_actual);
            $html.='<table class="table_oit_0 table table-bordered" border="1">
                    <thead>
                        <tr>
                            <th rowspan="2">Ventas<br>Costo de Ventas Real</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_1 vr_t_1" data-valor="'.${'datapro_'.$this->anio_actual.'_1'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_1'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_2 vr_t_2" data-valor="'.${'datapro_'.$this->anio_actual.'_2'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_2'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_3 vr_t_3" data-valor="'.${'datapro_'.$this->anio_actual.'_3'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_3'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_4 vr_t_4" data-valor="'.${'datapro_'.$this->anio_actual.'_4'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_4'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_5 vr_t_5" data-valor="'.${'datapro_'.$this->anio_actual.'_5'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_5'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_6 vr_t_6" data-valor="'.${'datapro_'.$this->anio_actual.'_6'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_6'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_7 vr_t_7" data-valor="'.${'datapro_'.$this->anio_actual.'_7'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_7'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_8 vr_t_8" data-valor="'.${'datapro_'.$this->anio_actual.'_8'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_8'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_9 vr_t_9" data-valor="'.${'datapro_'.$this->anio_actual.'_9'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_9'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_10 vr_t_10" data-valor="'.${'datapro_'.$this->anio_actual.'_10'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_10'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_11 vr_t_11" data-valor="'.${'datapro_'.$this->anio_actual.'_11'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_11'},2,'.',',').'</th>
                            <th colspan="2" class="vr_'.$this->anio_actual.'_12 vr_t_12" data-valor="'.${'datapro_'.$this->anio_actual.'_12'}.'">'.number_format(${'datapro_'.$this->anio_actual.'_12'},2,'.',',').'</th>
                            <th rowspan="2"></th>
                        </tr>
                        <tr>
                            <th colspan="2">ene-'.$this->anio_actual.'</th>
                            <th colspan="2">feb-'.$this->anio_actual.'</th>
                            <th colspan="2">mar-'.$this->anio_actual.'</th>
                            <th colspan="2">abr-'.$this->anio_actual.'</th>
                            <th colspan="2">may-'.$this->anio_actual.'</th>
                            <th colspan="2">jun-'.$this->anio_actual.'</th>
                            <th colspan="2">jul-'.$this->anio_actual.'</th>
                            <th colspan="2">ago-'.$this->anio_actual.'</th>
                            <th colspan="2">sep-'.$this->anio_actual.'</th>
                            <th colspan="2">oct-'.$this->anio_actual.'</th>
                            <th colspan="2">nov-'.$this->anio_actual.'</th>
                            <th colspan="2">dic-'.$this->anio_actual.'</th>
                        </tr>
                    </thead><tbody>';
                    foreach ($res_pro_dll->result() as $item_pd) {
                        ${'vc_'.$item_pd->anio.'_'.$item_pd->mes.'_'.$item_pd->idpro}='<input type="number" class="form-control calprom col_a_'.$item_pd->anio.' col_m_'.$item_pd->mes.' c_am_'.$item_pd->anio.'_'.$item_pd->mes.'" id="vc_'.$item_pd->id.'" value="'.$item_pd->valor.'" onchange="update_v_table2('.$item_pd->id.','.$item_pd->anio.','.$item_pd->mes.')" data-anio="'.$item_pd->anio.'" data-mes="'.$item_pd->mes.'" data-idpro="'.$item_pd->idpro.'" >';
                    }

                    foreach ($result_pro->result() as $itemp) {
                        $html.='<tr>';
                            $html.='<td>'.$itemp->namepro.'</td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_1_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_1_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_2_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_2_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_3_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_3_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_4_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_4_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_5_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_5_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_6_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_6_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_7_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_7_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_8_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_8_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_9_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_9_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_10_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_10_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_11_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_11_'.$itemp->id.'"></td>';
                            $html.='<td class="td_c_input" >'.${'vc_'.$this->anio_actual.'_12_'.$itemp->id}.'</td><td class="promc promc_'.$this->anio_actual.'_12_'.$itemp->id.'"></td>';
                        $html.='</tr>';
                    }
                    $html.='</tbody><tfoot>';
                        $html.='<tr>';
                            $html.='<td>Total costos Ventas</td>';
                            $html.='<td class="t_c_v t_c_v_1" ></td><td class="t_c_prom_1"></td>';
                            $html.='<td class="t_c_v t_c_v_2" ></td><td class="t_c_prom_2"></td>';
                            $html.='<td class="t_c_v t_c_v_3" ></td><td class="t_c_prom_3"></td>';
                            $html.='<td class="t_c_v t_c_v_4" ></td><td class="t_c_prom_4"></td>';
                            $html.='<td class="t_c_v t_c_v_5" ></td><td class="t_c_prom_5"></td>';
                            $html.='<td class="t_c_v t_c_v_6" ></td><td class="t_c_prom_6"></td>';
                            $html.='<td class="t_c_v t_c_v_7" ></td><td class="t_c_prom_7"></td>';
                            $html.='<td class="t_c_v t_c_v_8" ></td><td class="t_c_prom_8"></td>';
                            $html.='<td class="t_c_v t_c_v_9" ></td><td class="t_c_prom_9"></td>';
                            $html.='<td class="t_c_v t_c_v_10" ></td><td class="t_c_prom_10"></td>';
                            $html.='<td class="t_c_v t_c_v_11" ></td><td class="t_c_prom_11"></td>';
                            $html.='<td class="t_c_v t_c_v_12" ></td><td class="t_c_prom_12"></td>';
                            $html.='<td class="totalesgenerales"></td>';
                        $html.='</tr>';
                        $html.='<tr>';
                            $html.='<td>Costo / Ventas</td>';
                            $html.='<td class="co_ve_1" colspan="2"></td>';
                            $html.='<td class="co_ve_2" colspan="2"></td>';
                            $html.='<td class="co_ve_3" colspan="2"></td>';
                            $html.='<td class="co_ve_4" colspan="2"></td>';
                            $html.='<td class="co_ve_5" colspan="2"></td>';
                            $html.='<td class="co_ve_6" colspan="2"></td>';
                            $html.='<td class="co_ve_7" colspan="2"></td>';
                            $html.='<td class="co_ve_8" colspan="2"></td>';
                            $html.='<td class="co_ve_9" colspan="2"></td>';
                            $html.='<td class="co_ve_10" colspan="2"></td>';
                            $html.='<td class="co_ve_11" colspan="2"></td>';
                            $html.='<td class="co_ve_12" colspan="2"></td>';
                        $html.='</tr>';
            $html.='</tfoot></table>';
        }
        if($tipo==3){
                /*--------valores en duro quitar cuando se jalen los reales---------*/
                    /*
                    $proye1=84000;
                    $proye2=1224845;
                    $proye3=1717124;
                    $proye4=1255113;
                    $proye5=1343877;
                    $proye6=1210881;
                    $proye7=1338696;
                    $proye8=1400763;
                    $proye9=1411397;
                    $proye10=1510008;
                    $proye11=1655938;
                    $proye12=1970579;
                    */
                    $proye1=0;
                    $proye2=0;
                    $proye3=0;
                    $proye4=0;
                    $proye5=0;
                    $proye6=0;
                    $proye7=0;
                    $proye8=0;
                    $proye9=0;
                    $proye10=0;
                    $proye11=0;
                    $proye12=0;
                    $result_vpvd = $this->ModeloCatalogos->v_proyeccion($id_servicio,$anio);
                    foreach ($result_vpvd->result() as $itemvpv) {
                        ${'proye'.$itemvpv->mes}=$itemvpv->opcion_seleccionada;
                        
                    }
                /*-------------------------------------------------------------------*/
                $result_pro = $this->ModeloCatalogos->getselectwheren('costosventas_pro',array('id_linea_servicio'=>$id_servicio,'activo'=>1));
                $result_cp = $this->ModeloCatalogos->getselectwheren('costosventas_proyectados',array('idser'=>$id_servicio,'anio'=>$anio,'activo'=>1));
                //log_message('error','costosventas_proyectados idser: '.$id_servicio.' anio '.$anio);
                //log_message('error','Num costosventas_proyectados '.$result_cp->num_rows());
                foreach ($result_cp->result() as $item) {
                    $variableauto='v_'.$item->anio.'_'.$item->mes.'_'.$item->idpro;
                    //log_message('error',$variableauto);
                    
                    ${$variableauto}='<input type="number" class="form-control class_idpro class_idpro_'.$item->mes.' col_a_'.$item->anio.' col_m_'.$item->mes.' idpro_'.$item->idpro.' col_m_'.$item->mes.'_idpro_'.$item->idpro.' c_am_'.$item->anio.'_'.$item->mes.'" id="v_'.$item->id.'" value="'.$item->porcentaje.'" 
                        data-idpro="'.$item->idpro.'" onchange="update_v_table3('.$item->id.','.$item->anio.','.$item->mes.','.$item->idpro.')">';
                    
                }
            $html.='<table class="table_oit_0 table3 table table-bordered">
                        <thead>
                            <tr>
                                <th>Ventas Proyectadas</th>
                                <th colspan="2" class="ven_proy_1" data-value="'.$proye1.'">'.number_format($proye1,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_2" data-value="'.$proye2.'">'.number_format($proye2,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_3" data-value="'.$proye3.'">'.number_format($proye3,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_4" data-value="'.$proye4.'">'.number_format($proye4,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_5" data-value="'.$proye5.'">'.number_format($proye5,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_6" data-value="'.$proye6.'">'.number_format($proye6,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_7" data-value="'.$proye7.'">'.number_format($proye7,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_8" data-value="'.$proye8.'">'.number_format($proye8,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_9" data-value="'.$proye9.'">'.number_format($proye9,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_10" data-value="'.$proye10.'">'.number_format($proye10,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_11" data-value="'.$proye11.'">'.number_format($proye11,2,'.',',').'</th>
                                <th colspan="2" class="ven_proy_12" data-value="'.$proye12.'">'.number_format($proye12,2,'.',',').'</th>
                                <th rowspan="3"></th>
                            </tr>
                            <tr>
                                <th>Costo de Ventas Proyectadas</th>
                                <th colspan="2">ene-'.$anio.'</th>
                                <th colspan="2">feb-'.$anio.'</th>
                                <th colspan="2">mar-'.$anio.'</th>
                                <th colspan="2">abr-'.$anio.'</th>
                                <th colspan="2">may-'.$anio.'</th>
                                <th colspan="2">jun-'.$anio.'</th>
                                <th colspan="2">jul-'.$anio.'</th>
                                <th colspan="2">ago-'.$anio.'</th>
                                <th colspan="2">sep-'.$anio.'</th>
                                <th colspan="2">oct-'.$anio.'</th>
                                <th colspan="2">nov-'.$anio.'</th>
                                <th colspan="2">dic-'.$anio.'</th>
                            </tr>
                            <tr>
                                <th colspan="25"></th>
                            </tr>
                            <tr>
                                <th>Costo total proyectado</th>';
                                $html.='<th class="c_t_p v_1_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_1_0'}.'</th>';

                                $html.='<th class="c_t_p v_2_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_2_0'}.'</th>';

                                $html.='<th class="c_t_p v_3_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_3_0'}.'</th>';

                                $html.='<th class="c_t_p v_4_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_4_0'}.'</th>';

                                $html.='<th class="c_t_p v_5_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_5_0'}.'</th>';

                                $html.='<th class="c_t_p v_6_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_6_0'}.'</th>';

                                $html.='<th class="c_t_p v_7_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_7_0'}.'</th>';

                                $html.='<th class="c_t_p v_8_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_8_0'}.'</th>';

                                $html.='<th class="c_t_p v_9_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_9_0'}.'</th>';

                                $html.='<th class="c_t_p v_10_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_10_0'}.'</th>';

                                $html.='<th class="c_t_p v_11_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_11_0'}.'</th>';

                                $html.='<th class="c_t_p v_12_0"></th>';
                                $html.='<th class="td_c_input" >'.${'v_'.$anio.'_12_0'}.'</th>';
                                $html.='<th class="c_t_pt"></th>';
                    $html.='</tr>
                            <tr>
                                <th colspan="25"></th>
                            </tr>
                        </thead>
                        <tbody>';
                        foreach ($result_pro->result() as $itemp) {
                            $html.='<tr>';
                                $html.='<td>'.$itemp->namepro.'</td>';
                                $html.='<td class="v_1_'.$itemp->id.'" monto_valor_'.$itemp->id.'_1" >a</td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_1_'.$itemp->id}.'</td>';
                                $html.='<td class="v_2_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_2_'.$itemp->id}.'</td>';
                                $html.='<td class="v_3_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_3_'.$itemp->id}.'</td>';
                                $html.='<td class="v_4_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_4_'.$itemp->id}.'</td>';
                                $html.='<td class="v_5_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_5_'.$itemp->id}.'</td>';
                                $html.='<td class="v_6_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_6_'.$itemp->id}.'</td>';
                                $html.='<td class="v_7_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_7_'.$itemp->id}.'</td>';
                                $html.='<td class="v_8_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_8_'.$itemp->id}.'</td>';
                                $html.='<td class="v_9_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_9_'.$itemp->id}.'</td>';
                                $html.='<td class="v_10_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_10_'.$itemp->id}.'</td>';
                                $html.='<td class="v_11_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_11_'.$itemp->id}.'</td>';
                                $html.='<td class="v_12_'.$itemp->id.'" ></td>';
                                $html.='<td class="td_c_input">'.${'v_'.$anio.'_12_'.$itemp->id}.'</td>';
                            $html.='</tr>';
                        }
                $html.='</tbody>';
                $html.='<tfoot>';
                        $html.='<tr>';
                                $html.='<td>Total Costo de ventas</td>';
                                $html.='<td class="tot_vent tot_vent_1"></td>';
                                $html.='<td class="tot_porc tot_porc_1"></td>';
                                $html.='<td class="tot_vent tot_vent_2"></td>';
                                $html.='<td class="tot_porc tot_porc_2"></td>';
                                $html.='<td class="tot_vent tot_vent_3"></td>';
                                $html.='<td class="tot_porc tot_porc_3"></td>';
                                $html.='<td class="tot_vent tot_vent_4"></td>';
                                $html.='<td class="tot_porc tot_porc_4"></td>';
                                $html.='<td class="tot_vent tot_vent_5"></td>';
                                $html.='<td class="tot_porc tot_porc_5"></td>';
                                $html.='<td class="tot_vent tot_vent_6"></td>';
                                $html.='<td class="tot_porc tot_porc_6"></td>';
                                $html.='<td class="tot_vent tot_vent_7"></td>';
                                $html.='<td class="tot_porc tot_porc_7"></td>';
                                $html.='<td class="tot_vent tot_vent_8"></td>';
                                $html.='<td class="tot_porc tot_porc_8"></td>';
                                $html.='<td class="tot_vent tot_vent_9"></td>';
                                $html.='<td class="tot_porc tot_porc_9"></td>';
                                $html.='<td class="tot_vent tot_vent_10"></td>';
                                $html.='<td class="tot_porc tot_porc_10"></td>';
                                $html.='<td class="tot_vent tot_vent_11"></td>';
                                $html.='<td class="tot_porc tot_porc_11"></td>';
                                $html.='<td class="tot_vent tot_vent_12"></td>';
                                $html.='<td class="tot_porc tot_porc_12"></td>';
                                $html.='<td class="tot_vent_general"></td>';
                            $html.='</tr>';
                            $html.='<tr class="tdred">';
                                $html.='<td>Alerta</td>';
                                $html.='<td colspan="2" class="rest_m_1"></td>';
                                $html.='<td colspan="2" class="rest_m_2"></td>';
                                $html.='<td colspan="2" class="rest_m_3"></td>';
                                $html.='<td colspan="2" class="rest_m_4"></td>';
                                $html.='<td colspan="2" class="rest_m_5"></td>';
                                $html.='<td colspan="2" class="rest_m_6"></td>';
                                $html.='<td colspan="2" class="rest_m_7"></td>';
                                $html.='<td colspan="2" class="rest_m_8"></td>';
                                $html.='<td colspan="2" class="rest_m_9"></td>';
                                $html.='<td colspan="2" class="rest_m_10"></td>';
                                $html.='<td colspan="2" class="rest_m_11"></td>';
                                $html.='<td colspan="2" class="rest_m_12"></td>';
                            $html.='</tr>';
                $html.='</tfoot>';
            $html.='</table>';
        }

        echo $html; 
    }
    function update_v_table0(){
        $params = $this->input->post();
        $id = $params['id'];
        $valor = $params['valor'];
        if($valor==''){
            $valor=null;
        }
        $this->ModeloCatalogos->updateCatalogo('costosventas',array('valor'=>$valor),array('id'=>$id));
    }
    function adddescript(){
        $params = $this->input->post();
        $idpro = $params['idpro'];
        $idser_pro = $params['idser_pro'];
        $descrit = $params['descrit'];
        if($idpro>0){
            $this->ModeloCatalogos->updateCatalogo('costosventas_pro',array('namepro'=>$descrit),array('id'=>$idpro));
        }else{
            $this->ModeloCatalogos->Insert('costosventas_pro',array('id_linea_servicio'=>$idser_pro,'namepro'=>$descrit));
            $this->v_e_costosventas();
        }
    }
    function viewdescript(){
        $params = $this->input->post();
        $idser = $params['idser'];
        $result_aa = $this->ModeloCatalogos->getselectwheren('costosventas_pro',array('id_linea_servicio'=>$idser,'activo'=>1));
        $html='';
        foreach ($result_aa->result() as $item) {
            $btns='<button type="button" class="btn btn-icon btn-primary waves-effect waves-light edit_descrit_'.$item->id.'" data-namepro="'.$item->namepro.'" onclick="edit_descrit('.$item->id.')">
              <span class="mdi mdi-pencil"></span>
            </button> ';
            $btns.='<button type="button" class="btn btn-icon btn-danger waves-effect waves-light" onclick="delete_descrit('.$item->id.')">
              <span class="mdi mdi-trash-can"></span>
            </button> ';
            $html.='<tr><td>'.$item->namepro.'</td><td>'.$btns.'</td></tr>';
        }
        echo $html;
    }
    function delete_descrit(){
        $params = $this->input->post();
        $id = $params['id'];
        $this->ModeloCatalogos->updateCatalogo('costosventas_pro',array('activo'=>0),array('id'=>$id));
    }
    function update_v_table2(){
        $params = $this->input->post();
        $id = $params['id'];
        $valor = $params['valor'];
        $this->ModeloCatalogos->updateCatalogo('costosventas_pro_dll',array('valor'=>$valor),array('id'=>$id));
    }
    function update_v_table3(){
        $params = $this->input->post();
        $id = $params['id'];
        $porcentaje = $params['porcentaje'];

        $valor = $params['valor'];
        
        $this->ModeloCatalogos->updateCatalogo('costosventas_proyectados',array('porcentaje'=>$porcentaje,'valor'=>$valor),array('id'=>$id));
    }

}
