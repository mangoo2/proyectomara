<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periodos extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_periodo');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,5);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['btn_active']=4;
        $data['btn_active_sub']=5;
        $data['perfil']=$this->perfilid;        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('periodo/index');
        $this->load->view('templates/footer');
        $this->load->view('periodo/indexjs');
    }

    public function add_data(){
        $data=$this->input->post();
        $data['reg']=$this->fechahoy;
        $data['idpersonal']=$this->idpersonal; 
        $id=$this->General_model->add_record('periodo',$data);
        echo $id;
    }



    public function getlist_data(){
        $params = $this->input->post();
        $getdata = $this->Model_periodo->get_list($params);
        $totaldata= $this->Model_periodo->total_list($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function get_form_periodo()
    {
        $html='<form class="form" method="post" role="form" id="form_registro" novalidate>
          <div class="row">
            <div class="col-md-3">
              <select class="form-select" id="anio" name="anio" required>
                <option selected value="">Seleccionar año</option>';
                    $ano = date("Y");
                    for ($i=2024;$i<=$ano;$i++){
                        $html.='<option value="'.$i.'">'.$i.'</option>';
                    }
              $html.='</select>
            </div>
            <div class="col-md-3">
              <select class="form-select" id="mes" name="mes" required>
                <option selected value="">Seleccionar mes</option>
                <option value="1">ENERO</option>
                <option value="2">FEBRERO</option>
                <option value="3">MARZO</option>
                <option value="4">ABRIL</option>
                <option value="5">MAYO</option>
                <option value="6">JUNIO</option>
                <option value="7">JULIO</option>
                <option value="8">AGOSTO</option>
                <option value="9">SEPTIEMBRE</option>
                <option value="10">OCTUBRE</option>
                <option value="11">NOVIEMBRE</option>
                <option value="12">DICIEMBRE</option>
              </select>
            </div>
            <div class="col-md-3">
              <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre del curso/programa" required />
            </div>
            <div class="col-md-3">
              <button type="submit" class="btn btn-outline-primary waves-effect btn_registro">
                <span class="tf-icons mdi mdi-checkbox-marked-circle-outline me-1"></span>Agregar periodo
              </button>
            </div>  
          </div>  
        </form>';
        echo $html;
    }

}    