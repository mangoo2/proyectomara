<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guia_seleccion_factores_criticos_exito extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_guia_seleccion');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahora = date('Y-m-d G:i:s');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idcliente=$this->session->userdata('idcliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,22);// perfil y id del submenu
            if ($permiso==0) {
                //redirect('Login');
            }
        }else{
            //redirect('/Login');
        }
    }

    public function index(){        
        $data['btn_active']=7;
        $data['btn_active_sub']=22;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('guia_seleccion_factores/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('guia_seleccion_factores/indexjs');
    }

    function get_tabla_mercado()
    {   
        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_tipo',array('activo'=>1));
        $resultx=$this->Model_guia_seleccion->get_total_mercados();
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }

        // Instrucciones: Selecciona las fortalezas que vas a mantener y las debilidades que deseas trabajar

        /*<tr>
            <th colspan="10" style="color:white"></th>
        </tr>*/
        $html='<div class="table-responsive text-nowrap tabla_mer">
            <table class="table table-bordered table_info" id="tabla_datos1" style="white-space: normal !important;">
              <tbody>';

                $html.='<tr style="vertical-align: top;" class="encabezado_m  tr_enca_fix" id="encabezado_m">
                    <td><div style="color: white; font-size: 22px; text-transform: none; width: 165px;">El mercado</div></td>';
                    foreach ($result->result() as $x){
                        $html.='<td class="td_input"><div style="background: #91928f; border-radius: 35px; padding:12px; text-transform: none; text-align: -webkit-center; color: #342E37; width: 225px;">'.$x->nombre.'</div></td>';
                    }
                    $html.='<td class="td_input"><div style="background: #91928f; border-radius: 35px; padding: 12px; text-transform: none; color:#342E37; text-align: -webkit-center; width: 225px;">Gestiones a implementar</div></td>
                      <td class="td_input" colspan="3" ><div style="background: #91928f; border-radius: 35px; padding: 12px;; text-transform: none; color:#342E37; text-align: -webkit-center; width: 662px;">Impacto esperado en el negocio</div></td>
                      <td class="td_input"><div style="background: #91928f; border-radius: 35px; padding-left: 18px; padding-right: 18px; text-transform: none; color:#342E37; text-align: -webkit-center; width: 228px;">¿Qué recursos requieres para su ejecución?</div></td>';
                $html.='</tr>';
                $result_p=$this->General_model->getselectwheren('guia_seleccion_preguntas',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td style="text-align: center;" colspan="12"><div class="div-linea"></div></td>
                    </tr>';
                    $html.='<tr>
                      <td><div class="th_c" style="width: 165px;">'.$p->titulo.'</div></td>
                      <td colspan="'.$total_r.'"></td>
                      <td></td>
                      <td colspan="3"></td>
                      <td></td>
                    </tr>';

                    $result_p=$this->General_model->getselectwheren('guia_seleccion_preguntas_detalles',array('idpregunta'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;
                        
                        
                        
                        $html.='<tr style="vertical-align: text-top;">
                            <td style="width: 40%; text-align: center;" ><div class="th_cx" style="padding: 4px;">'.$pd->pregunta.'</div></td>';

                            foreach ($result->result() as $x){
                                $idtipo=$x->id;
                                $pr1=0;
                                $pr2=0;
                                $pr3=0;
                                $pr4=0;
                                $pr5=0;
                                $pr6=0;
                                $pr7='';
                                $pr8=0;
                                $pr9=0;
                                $pr10=0;
                                $pr11='';

                                $result_f=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$idpd,'idcliente'=>$this->idcliente));
                                foreach ($result_f->result() as $f){
                                    $pr1=$f->resp1;
                                    $pr2=$f->resp2;
                                    $pr3=$f->resp3;
                                    $pr4=$f->resp4;
                                    $pr5=$f->resp5;
                                    $pr6=$f->resp6;
                                    $pr7=$f->resp7;
                                    $pr8=$f->resp8;
                                    $pr9=$f->resp9;
                                    $pr10=$f->resp10;
                                    $pr11=$f->resp11;
                                }

                                $html.='<td style="width: 20%;" class="td_input">';
                                    $style_p=''; 
                                    if($idtipo==1){
                                        if($pr1=='1'){
                                            $style_p='style="background: #70ad47; color:#342e37; border-radius: 30px; width: 225px;"';
                                        }else if($pr1=='2'){
                                            $style_p='style="background: #f5bd00; color:#342e37; border-radius: 30px; width: 225px;"';
                                        }else{
                                            $style_p='style="color:#342e37; border-radius: 30px; width: 225px;"';
                                        }
                                    }else if($idtipo==2){
                                        if($pr2=='1'){
                                            $style_p='style="background: #70ad47; color:#342e37; border-radius: 30px; width: 225px;"';
                                        }else if($pr2=='2'){
                                            $style_p='style="background: #f5bd00; color:#342e37; border-radius: 30px; width: 225px;"';
                                        }else{
                                            $style_p='style="background: #a7a7a4; color:#342e37; border-radius: 30px; width: 225px;"';
                                        }
                                    }else{
                                        $style_p='style="background: #a7a7a4; color:#342e37; border-radius: 30px; width: 225px;"';
                                    }
                                    $html.='<select class="form-select" id="pre_'.$x->id.'_'.$idpd.'" onchange="pregunta_mercado('.$x->id.','.$idpd.')" '.$style_p.' ><option valor="0">Selecciona una opción</option>';
                                        $result_pdo=$this->General_model->getselectwheren('guia_seleccion_preguntas_detalles_opciones',array('idguia'=>$idpd,'idguia_tipo'=>$x->id,'activo'=>1));
                                        foreach ($result_pdo->result() as $pdo){
                                            $selected_p='';  
                                            if($idtipo==1){ if($pr1==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==2){ if($pr2==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==3){ if($pr3==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==4){ if($pr4==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==5){ if($pr5==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==6){ if($pr6==$pdo->valor){ $selected_p='selected'; }
                                            }
                                            $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                        }
                                    $html.='</select>';
                                $html.='</td>';
                            }


                            //$html.='<td class="td_input"><input type="text" class="form-control" id="pre_7_'.$idpd.'"  oninput="pregunta_mercado_input(7,'.$idpd.')" value="'.$pr7.'" /></td>';
                            $html.='<td style="width: 20%;" class="td_input"><textarea rows="1" style="width: 240px;" class="form-control js-auto-size" id="pre_7_'.$idpd.'" oninput="pregunta_mercado_input(7,'.$idpd.')">'.$pr7.'</textarea></td>';
                            $html.='<td style="width: 20%;" class="td_input">';
                                    $html.='<select style="width: 200px; color:#342e37; border-radius: 30px;" class="form-select" id="pre_8_'.$idpd.'"  onchange="pregunta_mercado(8,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
                                            $selected_p='';   
                                            $html.='<option value="1" '; if($pr8==1) $html.='selected'; $html.='>Crecimiento en ventas</option>';
                                            $html.='<option value="2" '; if($pr8==2) $html.='selected'; $html.='>Crecimiento en rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr8==3) $html.='selected'; $html.='>Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr8==4) $html.='selected'; $html.='>Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr8==5) $html.='selected'; $html.='>Atraer clientes</option>';
                                    $html.='</select>';
                                    $html.='</td>';
                            $html.='<td style="width: 20%;" class="td_input">';
                                    $html.='<select style="width: 200px; color:#342e37; border-radius: 30px;" class="form-select" id="pre_9_'.$idpd.'" onchange="pregunta_mercado(9,'.$idpd.')" ><option valor="0">Selecciona una opción</option>';
                                            $selected_p='';  
                                             $html.='<option value="1" '; if($pr9==1) $html.='selected'; $html.=' >Crecimiento en ventas</option>';
                                            $html.='<option value="2" '; if($pr9==2) $html.='selected'; $html.=' >Crecimiento en rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr9==3) $html.='selected'; $html.=' >Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr9==4) $html.='selected'; $html.=' >Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr9==5) $html.='selected'; $html.=' >Atraer clientes</option>';
                                    $html.='</select>';
                                    $html.='</td>';
                            $html.='<td style="width: 20%;" class="td_input">';
                                    $html.='<select style="width: 200px; color:#342e37; border-radius: 30px;" class="form-select" id="pre_10_'.$idpd.'" onchange="pregunta_mercado(10,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
                                            $selected_p='';  
                                            $html.='<option value="1" '; if($pr10==1) $html.='selected'; $html.=' >Crecimiento en ventas</option>';
                                            $html.='<option value="2" '; if($pr10==2) $html.='selected'; $html.=' >Crecimiento en rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr10==3) $html.='selected'; $html.=' >Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr10==4) $html.='selected'; $html.=' >Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr10==5) $html.='selected'; $html.=' >Atraer clientes</option>';
                                        
                                    $html.='</select>';
                                    $html.='</td>';
                            //$html.='<td class="td_input"><input type="text" class="form-control"  id="pre_11_'.$idpd.'"  oninput="pregunta_mercado_input(11,'.$idpd.')" value="'.$pr11.'" /></td>';
                            $html.='<td class="td_input"><textarea rows="1" style="width: 240px;" class="form-control js-auto-size" id="pre_11_'.$idpd.'" oninput="pregunta_mercado_input(11,'.$idpd.')">'.$pr11.'</textarea></td>';
                        $html.='</tr>';
                    }
                }
              $html.='</tbody>
            </table>';
            //$html.='<div class="clonetr"><table class="table clonetr_data" id="clonetr_data1"></table></div>';
            $html.='<br>
            <br>';
            $html.='<div class="div-linea"></div>
            <table class="table table-bordered table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>
                <tr>
                    <th colspan="10" style="color:white">Instrucciones: De las fortalezas que seleccionaste, analiza ¿qué es lo que debes de mantener?</th>
                </tr>
                <tr>';
                    $html.='<th class="td_input" style="text-align-last: center;"><div class="th_c" style="background: #70ad47 !important;">Oportunidades  a mantener</div></th>';
                    $html.='<th class="td_input" style="text-align-last: center;"><div class="th_c" style="background: #70ad47 !important;">Factores a mantener</div></th>';
                    $html.='<th class="td_input" style="text-align-last: center;"><div class="th_c" style="background: #70ad47 !important;">¿Realizaré alguna acción?</div></th>';
                    $html.='<th class="td_input" style="text-align-last: center;"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light" onclick="add_oportunidades()">
                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                      </button></th>';
                $html.='</tr>
              </thead>
              <tbody id="tbody_opo">';
                /*$result_p=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_instrucciones',array('idcliente'=>$this->idcliente));
                $res1='';
                $res2='';
                $res3='';
                foreach ($result_p->result() as $p){
                    $res1=$p->resp1;
                    $res2=$p->resp2;
                    $res3=$p->resp3;
                }

                $html.='<tr><td class="td_input"><textarea rows="1" type="text" id="pre_cfi_1" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones(1)">'.$res1.'</textarea></td>
                    <td class="td_input"><textarea rows="1" type="text" id="pre_cfi_2" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones(2)">'.$res2.'</textarea></td>
                    <td class="td_input"><textarea rows="1" type="text" id="pre_cfi_3" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones(3)">'.$res3.'</textarea></td>
                </tr>';*/

                $result=$this->General_model->getselectwheren('foda_oportunidad',array('idcliente'=>$this->idcliente,'activo'=>1,'tipo'=>1));
                foreach ($result->result() as $x){
                    $html.='<tr style="vertical-align: text-top;" class="tr_opo_'.$x->id.'"><td class="td_input"><textarea rows="1" type="text" id="opo_resp1_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',1)">'.$x->concepto.'</textarea></td>
                        <td class="td_input"><textarea rows="1" type="text" id="opo_resp2_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',2)">'.$x->resp2.'</textarea></td>
                        <td class="td_input"><textarea rows="1" type="text" id="opo_resp3_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',3)">'.$x->resp3.'</textarea></td>
                        <td class="td_input"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_ope('.$x->id.')">
                                    <span class="tf-icons mdi mdi-delete-empty"></span>
                                </button>  </td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';

          $html.='</div>';
          $html.='<br>
            <div style="padding-left: 20px; padding-right: 20px;">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3" style="text-align:center">
                        <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(1)"><span style="font-size: 20px;">Tus competidores</span></a>
                    </div>
                    <div class="col-md-3" style="text-align:center"> 
                        <a style="cursor:pointer" onclick="siguiente_competidores_ocultar(2)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Tu posición en el mercado</span></a>
                    </div>
                    <div class="col-md-3" style="text-align:center">
                         &nbsp; &nbsp;<a style="cursor:pointer" onclick="siguiente_competidores_ocultar(3)"><span style="font-size: 20px;">Tu entorno externo</span></a><!--&nbsp;&nbsp; <span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                    </div>
                    <div class="col-md-2" style="text-align: right;">
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_competidores_ocultar(3)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                    </div>
                </div>    
            </div><br>';   
        echo $html;
    }

    function get_oportunidades_tabla()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_oportunidad',array('idcliente'=>$this->idcliente,'activo'=>1,'tipo'=>1));
        foreach ($result->result() as $x){
            $html.='<tr class="tr_opo_'.$x->id.'"><td class="td_input"><textarea rows="1" type="text" id="opo_resp1_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',1)">'.$x->concepto.'</textarea></td>
                <td class="td_input"><textarea rows="1" type="text" id="opo_resp2_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',2)">'.$x->resp2.'</textarea></td>
                <td class="td_input"><textarea rows="1" type="text" id="opo_resp3_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_mercado_instrucciones('.$x->id.',3)">'.$x->resp3.'</textarea></td>
                <td class="td_input"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_ope('.$x->id.')">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                        </button>  </td>
            </tr>';
        }
        echo $html;
    }

    public function edit_pregunta_seleccion(){
        $id=$this->input->post('id');
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$id=>$respuesta);
            $this->General_model->add_record('guia_seleccion_preguntas_respuesta',$data);
        }else{
            $data = array('resp'.$id=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta');    
        }
        
    }

    public function edit_pregunta_seleccion_instrucciones(){
        $id=$this->input->post('id');
        $tipo=$this->input->post('tipo');
        $respuesta=$this->input->post('respuesta');
        if($tipo==1){
            $data = array('idcliente'=>$this->idcliente,'concepto'=>$respuesta);
        }else{
            $data = array('idcliente'=>$this->idcliente,'resp'.$tipo=>$respuesta);
        }
        $this->General_model->edit_record('id',$id,$data,'foda_oportunidad');    
        
        //$result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_instrucciones',array('idcliente'=>$this->idcliente));
        /*$id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){*/
        
            /*$this->General_model->add_record('guia_seleccion_preguntas_respuesta_instrucciones',$data);
        }else{
            $data = array('resp'.$id=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta_instrucciones');    
        }   */
    }

    function get_tabla_guia_fortalezas()
    {   
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_tipo',array('activo'=>1));
        $resultx=$this->Model_guia_seleccion->get_total_factores();
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }
        /*
        <tr>
                    <th colspan="10">Instrucciones: Selecciona las fortalezas que vas a mantener y las debilidades que deseas trabajar</th>
                </tr>
                */
        $html='<div class="table-responsive text-nowrap">
            <table class="table table-bordered table_info" id="tabla_datos2" style="white-space: normal !important;">
              <thead>';
                
                /*$html.='<tr>
                    <td style="width:205px" width="205"><h3 style="color:red;">Fortalezas</h3></td>';
                    foreach ($result->result() as $x){
                        $html.='<th class="th_cxy" style="width:127px">'.$x->nombre.'</th>';
                    }
                $html.='</tr>';*/

              $html.='</thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td style="text-align: center;" colspan="10"><div class="div-linea"></div></td>
                    </tr>';
                    //<div class="th_c" style="background: #a7a7a4 !important;">'.$p->titulo.'</div>
                    $html.='<tr>
                      <td style="width:205px" width="205"><div class="th_c">'.$p->titulo.'</div></td>
                      <td><div class="th_c" style="background: #a7a7a4 !important; width: 230px;">Selecciona las fortalezas a mantener y las debilidades a gestionar</div></td>
                      <td style="width:127px"><div class="th_c" style="background: #a7a7a4 !important; width: 255px;">Gestiones a implementar</div></td>
                      <td colspan="3"><div class="th_c" style="background: #a7a7a4 !important;">Impacto esperado en el negocio</div></td>
                      <td style="text-align: center;"><div class="th_c" style="background: #a7a7a4 !important; width: 255px;">¿Qué recursos requieres para su ejecución?</div></td>
                      <td ><div class="th_c" style="background: #a7a7a4 !important;">Plazo Implementación</div></td>
                    </tr>';
                    // <td><div class="th_c" style="background: #a7a7a4 !important;">Debilidades<br>Voy a cambiar</div></td>
  /*                  $html.='<tr>
                      <td></td>
                      <td><div class="th_c" style="background: #a7a7a4 !important;">Fortalezas<br>Voy a mantener</div></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>';*/

                    $result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('idpregunta'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;
        
                        $html.='<tr style="vertical-align: text-top;">
                            <td style="width: 40%; text-align: center;" class="">'.$pd->pregunta.'</td>';

                            foreach ($result->result() as $x){
                                $idtipo=$x->id;
                                $pr1=0;
                                $pr2=0;
                                $pr3='';
                                $pr4=0;
                                $pr5=0;
                                $pr6=0;
                                $pr7='';
                                $pr8=0;

                                $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpd,'idcliente'=>$this->idcliente));
                                foreach ($result_f->result() as $f){
                                    $pr1=$f->resp1;
                                    $pr2=$f->resp2;
                                    $pr3=$f->resp3;
                                    $pr4=$f->resp4;
                                    $pr5=$f->resp5;
                                    $pr6=$f->resp6;
                                    $pr7=$f->resp7;
                                    $pr8=$f->resp8;
                                }

                                $html.='<td class="td_input">';
                                    $style_p=''; 
                                    if($idtipo==1){
                                        if($pr1=='1'){
                                            $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                        }else if($pr1=='2'){
                                            $style_p='style="background: #f5bd00; color:white; border-radius: 30px;"';
                                        }else{
                                            $style_p='style="border-radius: 30px;"';
                                        }
                                    }else if($idtipo==2){
                                        if($pr2=='1'){
                                            $style_p='style="background: #70ad47; color:white; border-radius: 30px;"';
                                        }else if($pr2=='2'){
                                            $style_p='style="background: #f5bd00; color:white; border-radius: 30px;"';
                                        }else{
                                            $style_p='style="border-radius: 30px;"';
                                        }
                                    }
                                    $html.='<select class="form-select" id="pre_f_'.$x->id.'_'.$idpd.'" onchange="pregunta_factor('.$x->id.','.$idpd.')" '.$style_p.'><option valor="0">Selecciona una opción</option>';
                                        $result_pdo=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles_opciones',array('idguia'=>$idpd,'idguia_tipo'=>$x->id,'activo'=>1));
                                        foreach ($result_pdo->result() as $pdo){
                                            $selected_p='';  
                                            if($idtipo==1){ if($pr1==$pdo->valor){ $selected_p='selected'; }
                                            }else if($idtipo==2){ if($pr2==$pdo->valor){ $selected_p='selected'; }
                                            }
                                            $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                        }
                                    $html.='</select>';
                                $html.='</td>';
                            }


                            /*$html.='<td class="td_input"><input type="text" class="form-control" id="pre_f_3_'.$idpd.'"  oninput="pregunta_factor_input(3,'.$idpd.')" value="'.$pr3.'" /></td>';*/
                            $html.='<td class="td_input"><textarea style="width: 300px;" rows="1" type="text" class="form-control colorlabel_white recordable rinited js-auto-size" id="pre_f_3_'.$idpd.'" oninput="pregunta_factor_input(3,'.$idpd.')">'.$pr3.'</textarea></td>';
                            
                            $html.='<td class="td_input">';
                                    $html.='<select style="width: 200px; border-radius: 30px;" class="form-select" id="pre_f_4_'.$idpd.'"  onchange="pregunta_factor(4,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
            
                                            $html.='<option value="1" '; if($pr4==1) $html.='selected'; $html.='>Crecer la ventas</option>';
                                            $html.='<option value="2" '; if($pr4==2) $html.='selected'; $html.='>Crecer la rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr4==3) $html.='selected'; $html.='>Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr4==4) $html.='selected'; $html.='>Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr4==5) $html.='selected'; $html.='>Atraer clientes</option>';
                                            $html.='<option value="6" '; if($pr4==6) $html.='selected'; $html.='>Mejorar la liquidez</option>';
                                            $html.='<option value="7" '; if($pr4==7) $html.='selected'; $html.='>Formar talento</option>';
                                            $html.='<option value="8" '; if($pr4==8) $html.='selected'; $html.='>Retener talento</option>';
                                            $html.='<option value="9" '; if($pr4==9) $html.='selected'; $html.='>Tomar mejores decisiones</option>';
                                            $html.='<option value="10" '; if($pr4==10) $html.='selected'; $html.='>Mejorar el producto/servicio</option>';
                                            $html.='<option value="11" '; if($pr4==11) $html.='selected'; $html.='>Disminuir mi vulnerabilidad</option>';
                                    $html.='</select>';
                                    $html.='</td>';
                            $html.='<td class="td_input">';
                                    $html.='<select style="width: 200px; border-radius: 30px;" class="form-select" id="pre_f_5_'.$idpd.'" onchange="pregunta_factor(5,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
                         
                                            $html.='<option value="1" '; if($pr5==1) $html.='selected'; $html.='>Crecer la ventas</option>';
                                            $html.='<option value="2" '; if($pr5==2) $html.='selected'; $html.='>Crecer la rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr5==3) $html.='selected'; $html.='>Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr5==4) $html.='selected'; $html.='>Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr5==5) $html.='selected'; $html.='>Atraer clientes</option>';
                                            $html.='<option value="6" '; if($pr5==6) $html.='selected'; $html.='>Mejorar la liquidez</option>';
                                            $html.='<option value="7" '; if($pr5==7) $html.='selected'; $html.='>Formar talento</option>';
                                            $html.='<option value="8" '; if($pr5==8) $html.='selected'; $html.='>Retener talento</option>';
                                            $html.='<option value="9" '; if($pr5==9) $html.='selected'; $html.='>Tomar mejores decisiones</option>';
                                            $html.='<option value="10" '; if($pr5==10) $html.='selected'; $html.='>Mejorar el producto/servicio</option>';
                                            $html.='<option value="11" '; if($pr5==11) $html.='selected'; $html.='>Disminuir mi vulnerabilidad</option>';
                                    $html.='</select>';
                                    $html.='</td>';
                            $html.='<td class="td_input">';
                                    $html.='<select style="width: 200px; border-radius: 30px;" class="form-select" id="pre_f_6_'.$idpd.'" onchange="pregunta_factor(6,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
                                
                                            $html.='<option value="1" '; if($pr6==1) $html.='selected'; $html.='>Crecer la ventas</option>';
                                            $html.='<option value="2" '; if($pr6==2) $html.='selected'; $html.='>Crecer la rentabilidad</option>';
                                            $html.='<option value="3" '; if($pr6==3) $html.='selected'; $html.='>Mejorar mi reputación</option>';
                                            $html.='<option value="4" '; if($pr6==4) $html.='selected'; $html.='>Retener clientes</option>';
                                            $html.='<option value="5" '; if($pr6==5) $html.='selected'; $html.='>Atraer clientes</option>';
                                            $html.='<option value="6" '; if($pr6==6) $html.='selected'; $html.='>Mejorar la liquidez</option>';
                                            $html.='<option value="7" '; if($pr6==7) $html.='selected'; $html.='>Formar talento</option>';
                                            $html.='<option value="8" '; if($pr6==8) $html.='selected'; $html.='>Retener talento</option>';
                                            $html.='<option value="9" '; if($pr6==9) $html.='selected'; $html.='>Tomar mejores decisiones</option>';
                                            $html.='<option value="10" '; if($pr6==10) $html.='selected'; $html.='>Mejorar el producto/servicio</option>';
                                            $html.='<option value="11" '; if($pr6==11) $html.='selected'; $html.='>Disminuir mi vulnerabilidad</option>';

                                    $html.='</select>';
                                    $html.='</td>';

                            //$html.='<td class="td_input"><input type="text" class="form-control" id="pre_f_7_'.$idpd.'"  oninput="pregunta_factor_input(7,'.$idpd.')" value="'.$pr7.'" /></td>';
                            $html.='<td class="td_input"><textarea style="width: 300px;" rows="1" type="text" class="form-control colorlabel_white recordable rinited js-auto-size" id="pre_f_7_'.$idpd.'"  oninput="pregunta_factor_input(7,'.$idpd.')">'.$pr7.'</textarea></td>';
                                    $html.='<td class="td_input">';
                                    $html.='<select style="width: 200px; border-radius: 30px;" class="form-select" id="pre_f_8_'.$idpd.'"  onchange="pregunta_factor(8,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
                                            $selected_p='';   
                                            $html.='<option value="1" '; if($pr8==1) $html.='selected'; $html.='>Trimestre 1</option>';
                                            $html.='<option value="2" '; if($pr8==2) $html.='selected'; $html.='>Trimestre 2</option>';
                                            $html.='<option value="3" '; if($pr8==3) $html.='selected'; $html.='>Trimestre 3</option>';
                                            $html.='<option value="4" '; if($pr8==4) $html.='selected'; $html.='>Trimestre 4</option>';

                                            $anio=$this->anio;
                                            $anio_1=$anio+1;
                                            $anio_2=$anio_1+1;
                                            $anio_3=$anio_2+1;
                                            $html.='<option value="5" '; if($pr8==5) $html.='selected'; $html.='>Año '.$anio_1.'</option>';
                                            $html.='<option value="6" '; if($pr8==6) $html.='selected'; $html.='>Año '.$anio_2.'</option>';
                                            $html.='<option value="7" '; if($pr8==7) $html.='selected'; $html.='>Año '.$anio_3.'</option>';                                    
                                            $html.='</select>';
                                    $html.='</td>';
                        $html.='</tr>';
                    }
                }
              $html.='</tbody>
            </table>';
            $html.='<div class="clonetr"><table class="table" id="clonetr_data2"></table></div>';
            $html.='<div class="div-linea"></div><br>
            <br>
            <table class="table table-bordered table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>
                <tr>
                    <th colspan="10">Instrucciones: De las fortalezas que seleccionaste, analiza ¿qué es lo que debes de mantener?</th>
                </tr>
                <tr>';
                    $html.='<th class="td_input"><div class="th_c" style="background: #70ad47 !important;">Fortalezas a mantener</div></th>';
                    $html.='<th class="td_input"><div class="th_c" style="background: #70ad47 !important;">Factores a mantener</div></th>';
                    $html.='<th class="td_input"><button type="button" class="btn rounded-pill btn-icon btn-primary btn-fab demo waves-effect waves-light" onclick="add_fortaleza_mejorar()">
                        <span class="tf-icons mdi mdi-plus-thick mdi-24px"></span>
                      </button></th>';
                $html.='</tr>
              </thead>
              <tbody id="tbody_deb">';


                /*$result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta_instrucciones',array('idcliente'=>$this->idcliente));
                $res1='';
                $res2='';
                foreach ($result_p->result() as $p){
                    $res1=$p->resp1;
                    $res2=$p->resp2;
                }*/
                $result=$this->General_model->getselectwheren('foda_fortaleza',array('idcliente'=>$this->idcliente,'activo'=>1,'tipo'=>1));
                foreach ($result->result() as $x){
                    $html.='<tr style="vertical-align: text-top;" class="tr_d_'.$x->id.'"><td class="td_input"><textarea rows="1" type="text" id="d_resp1_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_factor_instrucciones('.$x->id.',1)">'.$x->concepto.'</textarea></td>
                        <td class="td_input"><textarea rows="1" type="text" id="d_resp2_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_factor_instrucciones('.$x->id.',2)">'.$x->resp2.'</textarea></td>
                        <td class="td_input"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_deb('.$x->id.')">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                        </button>  </td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';
          $html.='</div><br><br>
          <div style="padding-left: 20px; padding-right: 20px;">
                <div class="row">
                    <div class="col-md-1" style="text-align: right;">
                        <!--<span style="background: #ffbd59; padding: 12px; padding-top: 0px; padding-bottom: 0px; border-radius: 12px; font-size: 20px;"></span>-->
                    </diV>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(1)"><span style="font-size: 20px;">Apalancamiento interno</span></a>
                    </div>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(2)"><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Factores internos</span></a>
                    </div>
                    <div class="col-md-3">
                        <a style="cursor:pointer" onclick="siguiente_analisis_ocultar(3)"><span style="font-size: 20px;">Tu entorno interno</span></a>
                    </div>
                    <div class="col-md-2" style="text-align: right;">
                      <button type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" onclick="siguiente_analisis_ocultar(3)">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></button>
                    </div>
                </div>    
          </div>';
    
        echo $html;
    }

    function get_fortaleza_mejorar_tabla()
    {
        $html='';
        $result=$this->General_model->getselectwheren('foda_fortaleza',array('idcliente'=>$this->idcliente,'activo'=>1,'tipo'=>1));
        foreach ($result->result() as $x){
            $html.='<tr class="tr_d_'.$x->id.'"><td class="td_input"><textarea rows="1" type="text" id="d_resp1_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_factor_instrucciones('.$x->id.',1)">'.$x->concepto.'</textarea></td>
                <td class="td_input"><textarea rows="1" type="text" id="d_resp2_'.$x->id.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_factor_instrucciones('.$x->id.',2)">'.$x->resp2.'</textarea></td>
                <td class="td_input"><button type="button" class="btn rounded-pill btn-icon btn-outline-secondary" onclick="eliminar_registro_deb('.$x->id.')">
                            <span class="tf-icons mdi mdi-delete-empty"></span>
                        </button>  </td>
            </tr>';
        }
        echo $html;
    }
    

    public function edit_fortaleza_pregunta_seleccion(){
        $id=$this->input->post('id');
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$id=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_preguntas_respuesta',$data);
        }else{
            $data = array('resp'.$id=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_preguntas_respuesta');    
        }
        
    }

    public function edit_fortaleza_pregunta_seleccion_instrucciones(){
        $id=$this->input->post('id');
        $respuesta=$this->input->post('respuesta');
        $tipo=$this->input->post('tipo');
        if($tipo==1){
            $data = array('idcliente'=>$this->idcliente,'concepto'=>$respuesta);
        }else{
            $data = array('idcliente'=>$this->idcliente,'resp'.$tipo=>$respuesta);
        }
        $this->General_model->edit_record('id',$id,$data,'foda_fortaleza');    
    }

    function get_tabla_fortaleza_pregunta_seleccion_cliente()
    {   
        $html='<div class="table-responsive text-nowrap">
            <table class="table table_info" id="tabla_datos" style="white-space: normal !important;">
              <thead>';
                /*
                $html.='<tr>
                  <th colspan="3">Estos son los insights que requieres conocer de tu cliente potencial y que te servirán de base para tu comunicación publicitaria</th>';
                    $html.='';
                $html.='</tr>';
                */

              $html.='</thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_cliente_preguntas',array('activo'=>1));
                foreach ($result_p->result() as $pd){
                    $idpd=$pd->id;
                    $result_c=$this->General_model->getselectwheren('guia_seleccion_fortalezas_cliente_preguntas_respuesta',array('idguia_seleccion_fortalezas_cliente_preguntas_respuesta'=>$idpd,'idcliente'=>$this->idcliente));
                    $respuesta='';
                    foreach ($result_c->result() as $f){
                        $respuesta=$f->respuesta;
                    }
                    
                    $html.='<tr>
                        <td style="width: 100%;"><div class="th_c" style="width: fit-content;">'.$pd->pregunta.'</div><br>';
                                $html.='<textarea rows="1" type="text" id="pre_sfc_'.$idpd.'" class="form-control colorlabel_white recordable rinited js-auto-size" oninput="pregunta_fortaleza_pregunta_seleccioncliente('.$idpd.')">'.$respuesta.'</textarea>';
                        $html.='</td>';
                    $html.='</tr>';
                }
              $html.='</tbody>
            </table>
          </div>';
    
        echo $html;
    }
    
    public function edit_fortaleza_pregunta_seleccion_cliente(){
        $id=$this->input->post('id');
        $respuesta=$this->input->post('respuesta');
        $data = array('idguia_seleccion_fortalezas_cliente_preguntas_respuesta'=>$id,'idcliente'=>$this->idcliente,'respuesta'=>$respuesta);
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_cliente_preguntas_respuesta',array('idguia_seleccion_fortalezas_cliente_preguntas_respuesta'=>$id,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $this->General_model->add_record('guia_seleccion_fortalezas_cliente_preguntas_respuesta',$data);
        }else{
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_cliente_preguntas_respuesta');    
        }
        
    }

    function get_tabla_estado_resultados()
    {   
        $resultx=$this->Model_guia_seleccion->get_total_factores();
        $total_r=0;
        foreach ($resultx as $g){
            $total_r=$g->total;
        }
        $html='<div class="table-responsive text-nowrap">
            <table class="table table-bordered table_info" id="tabla_datos4" style="white-space: normal !important;">
              <thead>';
          
                /*$html.='<tr>
                    <th colspan="10">Anota tus estrategias para cada uno de los conceptos</th>
                </tr>
                <tr>
                    <td><h3 style="color:red;">Estado de Resultados</h3></td>';
                    $html.='<th class="th_cxy">Debilidades Voy a gestionar</th>';
                $html.='</tr>';*/
              //<td ><div class="th_c" style="background: #a7a7a4 !important;">¿Este factor se puede eliminar?</td>
              $html.='</thead>
              <tbody>';
                $result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados',array('activo'=>1));
                foreach ($result_p->result() as $p){
                    $html.='<tr>
                      <td style="text-align: center;" colspan="5" ><div class="div-linea"></div></td>
                    </tr>';
                    $html.='<tr style="vertical-align: text-top;">
                      <td style="width:205px"><div class="th_c">'.$p->titulo.'<br>'.$p->pregunta.'</div></td>
                      <td ><div class="th_c" style="width:318px">'.$p->titulo.'</div></td>
                      <td ><div class="th_c" style="background: #a7a7a4 !important; width: 255px;">Gestiones</td>
                      
                      <td style="text-align: center"><div class="th_c" style="background: #a7a7a4 !important; width: 255px;">¿Qué recursos requieres para su ejecución?</div></td>
                      <td ><div class="th_c" style="background: #a7a7a4 !important;">Plazo Implementación</div></td>
                    </tr>';

                    $result_p=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_detalles',array('idpregunta'=>$p->id,'activo'=>1));
                    foreach ($result_p->result() as $pd){
                        $idpd=$pd->id;
                        $tipo=$pd->tipo;
        
                        $html.='<tr style="vertical-align: text-top;">
                            <td style="width: 40%; text-align: center;">'.$pd->pregunta.'</td>';

  
                                $pr1='';
                                $pr2='';
                                $pr3='';
                                $pr4=0;
                                //$pr5='';

                                $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpd,'idcliente'=>$this->idcliente));
                                foreach ($result_f->result() as $f){
                                    $pr1=$f->resp1;
                                    $pr2=$f->resp2;
                                    $pr3=$f->resp3;
                                    $pr4=$f->resp4;
                                    //$pr5=$f->resp5;
                                }

                                $html.='<td class="">';
                                    $style_p=''; 
                                    if($pr1=='1'){
                                        $style_p='style="background: #f5bd00; border-radius: 30px;"';
                                    }else if($pr1=='2'){
                                        $style_p='style="background: #f5bd00; border-radius: 30px;"';
                                    }else{
                                        $style_p='style="background: #a7a7a4; border-radius: 30px;"';
                                    }
                                    if($tipo==1){ 
                                    $html.='<select class="form-select" id="pre_fer_1_'.$idpd.'" onchange="pregunta_seleccionestado_resultados(1,'.$idpd.')" '.$style_p.'><option valor="0">Selecciona si es una debilidad actual</option>';
                                        $result_pdo=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_detalles_opciones',array('idguia'=>$idpd,'activo'=>1));
                                        foreach ($result_pdo->result() as $pdo){
                                            $selected_p='';  
                                            if($pr1==$pdo->valor){ $selected_p='selected'; }
                                            $html.='<option value="'.$pdo->valor.'" '.$selected_p.'>'.$pdo->respuesta.'</option>';
                                        }
                                    $html.='</select>';
                                    }else{
                                        $html.='<textarea style="width: 300px;" rows="1" class="form-control js-auto-size" id="pre_fer_1_'.$idpd.'" oninput="pregunta_seleccionestado_resultados_input(1,'.$idpd.')">'.$pr1.'</textarea>';
                                    }
                                $html.='</td>';
                            /**/
                            /*$html.='<td class="td_input"><input type="text" class="form-control" id="pre_fer_2_'.$idpd.'"  oninput="pregunta_seleccionestado_resultados_input(2,'.$idpd.')" value="'.$pr2.'" /></td>';
                            $html.='<td class="td_input"><input type="text" class="form-control" id="pre_fer_3_'.$idpd.'"  oninput="pregunta_seleccionestado_resultados_input(3,'.$idpd.')" value="'.$pr3.'" /></td>';*/
                            /**/ 
                            $html.='<td class="td_input"><textarea style="width: 300px;" rows="1" class="form-control js-auto-size" id="pre_fer_2_'.$idpd.'" oninput="pregunta_seleccionestado_resultados_input(2,'.$idpd.')">'.$pr2.'</textarea></td>';

                            /*$html.='<td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_fer_5_'.$idpd.'" oninput="pregunta_seleccionestado_resultados_input(5,'.$idpd.')">'.$pr5.'</textarea></td>';*/

                            $html.='<td class="td_input"><textarea style="width: 300px;" rows="1" class="form-control js-auto-size" id="pre_fer_3_'.$idpd.'" oninput="pregunta_seleccionestado_resultados_input(3,'.$idpd.')">'.$pr3.'</textarea></td>';
                            /**/
                            $html.='<td class="td_input">';
                                    $html.='<select style="width: 100%; border-radius: 30px;" class="form-select" id="pre_fer_4_'.$idpd.'"  onchange="pregunta_seleccionestado_resultados(4,'.$idpd.')"><option valor="0">Selecciona una opción</option>';
            
                                            $html.='<option value="1" '; if($pr4==1) $html.='selected'; $html.='>Trimestre 1</option>';
                                            $html.='<option value="2" '; if($pr4==2) $html.='selected'; $html.='>Trimestre 2</option>';
                                            $html.='<option value="3" '; if($pr4==3) $html.='selected'; $html.='>Trimestre 3</option>';
                                            $html.='<option value="4" '; if($pr4==4) $html.='selected'; $html.='>Trimestre 4</option>';
                                            $anio=$this->anio;
                                            $anio_1=$anio+1;
                                            $anio_2=$anio_1+1;
                                            $anio_3=$anio_2+1;
                                            $html.='<option value="5" '; if($pr4==5) $html.='selected'; $html.='>Año '.$anio_1.'</option>';
                                            $html.='<option value="6" '; if($pr4==6) $html.='selected'; $html.='>Año '.$anio_2.'</option>';
                                            $html.='<option value="7" '; if($pr4==7) $html.='selected'; $html.='>Año '.$anio_3.'</option>';
                                    $html.='</select>';
                                    $html.='</td>';
                    }
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
          //$html.='<div class="clonetr"><table class="table" id="clonetr_data4"></table></div>';
    
        echo $html;
    }
    
    public function edit_seleccion_estado_resultados(){
        $id=$this->input->post('id');
        $idpregunta=$this->input->post('idpregunta');
        $respuesta=$this->input->post('respuesta');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'resp'.$id=>$respuesta);
            $this->General_model->add_record('guia_seleccion_fortalezas_estado_resultados_respuesta',$data);
        }else{
            $data = array('resp'.$id=>$respuesta);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta');    
        }
        
    }

    function get_tabla_resumen_estrategias()
    {   
        
        $html='<div>
        <div class="table-responsive text-nowrap">
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">

              <tbody style="vertical-align: -webkit-baseline-middle; text-align: -webkit-center;">';
                $result_tm=$this->Model_guia_seleccion->get_mercadoresultados_total($this->idcliente);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }
                if($totalm!=0){
                    $result_merc=$this->Model_guia_seleccion->get_mercadoresultados($this->idcliente);
                    $cont_m=0;
                    foreach ($result_merc as $g){
                        $cont_m++;
                    }
                    $html.='<tr>
                      <td colspan="'.$cont_m.'"><div style="padding-bottom: 8px; text-align: -webkit-center;"><div class="th_c">Resumen de fortalezas con respecto del análisis del mercado</div></div></td>
                    </tr>';
                    
                    $html.='<tr>';
                        $result_merc=$this->Model_guia_seleccion->get_mercadoresultados($this->idcliente);
                        foreach ($result_merc as $g){
                            $html.='<td width="25%">';
                            $html.='<div style="padding-bottom: 8px;"></div><div class="th_c">'.$g->titulo.'</div>';
                            $html.='</td>';
                        }
                    $html.='</tr>';
                    $html.='<tr>';
                        $result_merc=$this->Model_guia_seleccion->get_mercadoresultados($this->idcliente);
                        foreach ($result_merc as $g){
                            $html.='<td width="25%">';
                            $result_merc=$this->Model_guia_seleccion->get_mercadoresultados_detalles($this->idcliente,$g->idpregunta);
                            foreach ($result_merc as $x){
                                $html.='<div class="th_v" style="width: fit-content; margin: 4px; padding-left: 12px; padding-right: 12px;">'.$x->pregunta.'</div>';
                            }
                            $html.='</td>';
                        }
                    $html.='</tr>';
                    /*$html.='<tr>
                        <td>';
                            $result_merc=$this->Model_guia_seleccion->get_mercadoresultados($this->idcliente);
                            foreach ($result_merc as $g){
                                $html.='<div style="padding-bottom: 8px;"></div><div class="th_c">'.$g->titulo.'</div><div style="padding-top: 15px; padding-bottom: 15px; width: 94%; display: flex; flex-wrap: wrap;">';
                                $result_merc=$this->Model_guia_seleccion->get_mercadoresultados_detalles($this->idcliente,$g->idpregunta);
                                foreach ($result_merc as $x){
                                    $html.='<div class="th_v" style="width: fit-content; margin: 4px; padding-left: 12px; padding-right: 12px;">'.$x->pregunta.'</div>';
                                }
                                $html.='</div><div class="div-linea"></div>';
                            }
                        $html.='</td>
                    </tr>';*/
                }
            $html.='</tbody>
            </table></div>';
            $html.='<div class="div-linea"></div>';
            $html.='
            <div class="table-responsive text-nowrap">
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">

              <tbody style="vertical-align: -webkit-baseline-middle; text-align: -webkit-center;">';
                $result_ti=$this->Model_guia_seleccion->get_internosultados_total($this->idcliente);
                $totali=0;
                foreach ($result_ti as $i){
                    $totali=$i->total;
                }
                if($totali!=0){
                    $result_merc=$this->Model_guia_seleccion->get_internosultados($this->idcliente);
                    $cont_in=0;
                    foreach ($result_merc as $g){
                        $cont_in++;
                    }
                    $html.='<tr>
                      <td colspan="'.$cont_in.'"><br><br><div style="padding-bottom: 8px; text-align: -webkit-center;"><div class="th_c">Resumen de fortalezas con respecto del análisis interno</div></div></td>
                    </tr>';

                    $html.='<tr>';
                        $result_merc=$this->Model_guia_seleccion->get_internosultados($this->idcliente);
                        foreach ($result_merc as $g){
                        $html.='<td>';
                            $html.='<div style="padding-bottom: 8px;"></div><div class="th_c" style="width: 280px;">'.$g->titulo.'</div>';
                            $result_merc=$this->Model_guia_seleccion->get_internosultados_detalles($this->idcliente,$g->idpregunta);   
                        $html.='</td>';
                        }
                    $html.='</tr>';
                    $html.='<tr>';
                        $result_merc=$this->Model_guia_seleccion->get_internosultados($this->idcliente);
                        foreach ($result_merc as $g){
                        $html.='<td>';
                            $result_merc=$this->Model_guia_seleccion->get_internosultados_detalles($this->idcliente,$g->idpregunta);
                            foreach ($result_merc as $x){
                                $html.='<div class="th_v" style="width: fit-content; margin: 4px; padding-left: 12px; padding-right: 12px; width: 280px;">'.$x->pregunta.'</div>';
                            }
                            
                        $html.='</td>';
                        }
                    $html.='</tr>';
                }
              $html.='</tbody>
            </table></div>';
          $html.='</div>';
    
        echo $html;
    }

    function get_tabla_resumen_estrategias_prioriza()
    {   
        
        $html='<div>
            <table class="" id="tabla_datos" style="white-space: normal !important; width: 100%;">

              <tbody>';
                $result_tm=$this->Model_guia_seleccion->get_pri_mercadoresultados_total($this->idcliente);
                $totalm=0;
                foreach ($result_tm as $x){
                    $totalm=$x->total;
                }
                if($totalm!=0){
                    $html.='<tr>
                      <td ><div style="padding-bottom: 8px; text-align: -webkit-center;"><div class="th_c">Debilidades del mercado</div></div></td>
                    </tr>';

                    $html.='<tr>
                        <td>';
                            $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados($this->idcliente);
                            foreach ($result_merc as $g){
                                $html.='<div style="padding-bottom: 8px;"></div><div class="th_c">'.$g->titulo.'</div><div style="padding-bottom: 15px;"><div class="row">';
                                $result_merc=$this->Model_guia_seleccion->get_pri_mercadoresultados_detalles($this->idcliente,$g->idpregunta);
                                foreach ($result_merc as $x){
                                    $st1=0;
                                    $st2=0;
                                    $st3=0;
                                    $st4=0;
                                    $st5=0;
                                    $result_f=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles',array('idguia_seleccion_preguntas_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                    $pre_r=$x->id;
                                    foreach ($result_f->result() as $f){
                                        $st1=$f->resp1;
                                        $st2=$f->resp2;
                                        $st3=$f->resp3;
                                        $st4=$f->resp4;
                                        //$pr5=$f->resp5;
                                    }
                                     
                                    if($st1=='1'){
                                        $style_p1='background: #70ad47; color:white';
                                    }else if($st1=='2'){
                                        $style_p1='background: #f5bd00; color:black';
                                    }else if($st1=='3'){
                                        $style_p1='background: #e31b1b; color:black';
                                    }else{
                                        $style_p1='';
                                    }


                                    if($st2=='1'){
                                        $style_p2='background: #70ad47; color:white';
                                    }else if($st2=='2'){
                                        $style_p2='background: #f5bd00; color:black';
                                    }else if($st2=='3'){
                                        $style_p2='background: #e31b1b; color:black';
                                    }else{
                                        $style_p2='';
                                    }

                                    if($st3=='1'){
                                        $style_p3='background: #70ad47; color:white';
                                    }else if($st3=='2'){
                                        $style_p3='background: #f5bd00; color:black';
                                    }else if($st3=='3'){
                                        $style_p3='background: #e31b1b; color:black';
                                    }else{
                                        $style_p3='';
                                    }

                                    if($st4=='1'){
                                        $style_p4='background: #70ad47; color:white';
                                    }else if($st4=='2'){
                                        $style_p4='background: #f5bd00; color:black';
                                    }else if($st4=='3'){
                                        $style_p4='background: #e31b1b; color:black';
                                    }else{
                                        $style_p4='';
                                    }
                                    $html.='<div class="col-md-3"><br>
                                        <div style="padding: 12px; background: #2a1f2f; border-radius: 36px; height: 95%; text-align: -webkit-center;">
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; color: #030303 !important;  text-align: -webkit-center; max-width: none !important;">'.$x->pregunta.'</div>
                                            <div>
                                                <div>Costo de implementación</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p1.'" class="form-select" id="est_costo_imple'.$x->id.'_1"  onchange="est_costo_imple('.$x->id.',1)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st1==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st1==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st1==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='<div>Riesgos (probabilidad de fracasar)</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p2.'" class="form-select" id="est_costo_imple'.$x->id.'_2"  onchange="est_costo_imple('.$x->id.',2)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st2==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st2==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st2==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Complejidad de implementar</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p3.'" class="form-select" id="est_costo_imple'.$x->id.'_3"  onchange="est_costo_imple('.$x->id.',3)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st3==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st3==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st3==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Beneficios</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p4.'" class="form-select" id="est_costo_imple'.$x->id.'_4"  onchange="est_costo_imple('.$x->id.',4)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st4==1) $html.='selected'; $html.='>Ingresos</option>';
                                                        $html.='<option value="2" '; if($st4==2) $html.='selected'; $html.='>Ahorros</option>';
                                                $html.='</select>';
                                                $html.='<div>Prioridad</div>';
                                                $poner='';
                                                $result_poner=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta_detalles',array('idguia_seleccion_preguntas_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                                foreach ($result_poner->result() as $x){
                                                    $pr1=$x->resp1;
                                                    $pr2=$x->resp2;
                                                    $pr3=$x->resp3;
                                                    $pr4=$x->resp4;
                                                    $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
                                                    $costo=0;
                                                    $riesgo=0;
                                                    $complejidad=0;
                                                    $beneficios=0;
                                                    foreach ($result_c->result() as $c){
                                                        $costo=$c->costo;
                                                        $riesgo=$c->riesgo;
                                                        $complejidad=$c->complejidad;
                                                        $beneficios=$c->beneficios;
                                                    }
                                                    
                                                    $mult1=$pr1*$costo;
                                                    $mult2=$pr2*$riesgo;
                                                    $mult3=$pr3*$complejidad;
                                                    $mult4=$pr4*$beneficios;
                                                    $div1=$mult1/100;
                                                    $div2=$mult2/100;
                                                    $div3=$mult3/100;
                                                    $div4=$mult4/100;
                                                    $suma=$div1+$div2+$div3+$div4;

                                                    if($suma>=1 && $suma<=1.39){
                                                        $poner='<div class="resum_c3 ">Alta</div>';
                                                    }else if($suma>=1.4 && $suma<=2.34){
                                                        $poner='<div class="resum_c2">Media</div>';  
                                                    }else if($suma>=2.34 && $suma<=3){
                                                        $poner='<div class="resum_c1">Baja</div>';
                                                    }
                                                }
                                                $html.='<div class="txt_prioridad_1_'.$pre_r.'">'.$poner.'</div>
                                            </div>
                                        </div>
                                        
                                    </div>';
                                }
                                $html.='</div></div><div class="div-linea"></div>';
                            }
                        $html.='</td>
                    </tr>';
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_internosultados_total($this->idcliente);
                $totali=0;
                foreach ($result_ti as $i){
                    $totali=$i->total;
                }
                if($totali!=0){
                    $html.='<tr>
                      <td ><br><br><div style="padding-bottom: 8px; text-align: -webkit-center;"><div class="th_c">Debilidades de tu análisis interno</div></div></td>
                    </tr>';
                    $html.='<tr>
                        <td>';
                            $result_merc=$this->Model_guia_seleccion->get_pri_internosultados($this->idcliente);
                            foreach ($result_merc as $g){
                                $html.='<div style="padding-bottom: 8px;"></div><div class="th_c">'.$g->titulo.'</div><div style="padding-bottom: 15px;"><div class="row">';
                                $result_merc=$this->Model_guia_seleccion->get_pri_internosultados_detalles($this->idcliente,$g->idpregunta);
                                foreach ($result_merc as $x){
                                    $st1=0;
                                    $st2=0;
                                    $st3=0;
                                    $st4=0;
                                    $st5=0;
                                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta_detalles',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                    $pre_r=$x->id;
                                    foreach ($result_f->result() as $f){
                                        $st1=$f->resp1;
                                        $st2=$f->resp2;
                                        $st3=$f->resp3;
                                        $st4=$f->resp4;
                                        //$pr5=$f->resp5;
                                    }
                                     
                                    if($st1=='1'){
                                        $style_p1='background: #70ad47; color:white';
                                    }else if($st1=='2'){
                                        $style_p1='background: #f5bd00; color:black';
                                    }else if($st1=='3'){
                                        $style_p1='background: #e31b1b; color:black';
                                    }else{
                                        $style_p1='';
                                    }


                                    if($st2=='1'){
                                        $style_p2='background: #70ad47; color:white';
                                    }else if($st2=='2'){
                                        $style_p2='background: #f5bd00; color:black';
                                    }else if($st2=='3'){
                                        $style_p2='background: #e31b1b; color:black';
                                    }else{
                                        $style_p2='';
                                    }

                                    if($st3=='1'){
                                        $style_p3='background: #70ad47; color:white';
                                    }else if($st3=='2'){
                                        $style_p3='background: #f5bd00; color:black';
                                    }else if($st3=='3'){
                                        $style_p3='background: #e31b1b; color:black';
                                    }else{
                                        $style_p3='';
                                    }

                                    if($st4=='1'){
                                        $style_p4='background: #70ad47; color:white';
                                    }else if($st4=='2'){
                                        $style_p4='background: #f5bd00; color:black';
                                    }else if($st4=='3'){
                                        $style_p4='background: #e31b1b; color:black';
                                    }else{
                                        $style_p4='';
                                    }
                                    $html.='<div class="col-md-3"><br>
                                        <div style="padding: 12px; background: #2a1f2f; border-radius: 36px; height: 95%; text-align: -webkit-center;">
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; color: #030303 !important; text-align: -webkit-center; max-width: none !important;">'.$x->pregunta.'</div>
                                            <div>
                                                <div>Costo de implementación</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p1.'" class="form-select" id="est_riesgo'.$x->id.'_1"  onchange="est_riesgo('.$x->id.',1)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st1==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st1==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st1==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='<div>Riesgos (probabilidad de fracasar)</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p2.'" class="form-select" id="est_riesgo'.$x->id.'_2"  onchange="est_riesgo('.$x->id.',2)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st2==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st2==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st2==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Complejidad de implementar</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p3.'" class="form-select" id="est_riesgo'.$x->id.'_3"  onchange="est_riesgo('.$x->id.',3)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st3==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st3==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st3==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Beneficios</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p4.'" class="form-select" id="est_riesgo'.$x->id.'_4"  onchange="est_riesgo('.$x->id.',4)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st4==1) $html.='selected'; $html.='>Ingresos</option>';
                                                        $html.='<option value="2" '; if($st4==2) $html.='selected'; $html.='>Ahorros</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Prioridad</div>';
                                                $poner='';
                                                $result_poner=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta_detalles',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                                foreach ($result_poner->result() as $x){
                                                    $pr1=$x->resp1;
                                                    $pr2=$x->resp2;
                                                    $pr3=$x->resp3;
                                                    $pr4=$x->resp4;
                                                    $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
                                                    $costo=0;
                                                    $riesgo=0;
                                                    $complejidad=0;
                                                    $beneficios=0;
                                                    foreach ($result_c->result() as $c){
                                                        $costo=$c->costo;
                                                        $riesgo=$c->riesgo;
                                                        $complejidad=$c->complejidad;
                                                        $beneficios=$c->beneficios;
                                                    }
                                                    
                                                    $mult1=$pr1*$costo;
                                                    $mult2=$pr2*$riesgo;
                                                    $mult3=$pr3*$complejidad;
                                                    $mult4=$pr4*$beneficios;
                                                    $div1=$mult1/100;
                                                    $div2=$mult2/100;
                                                    $div3=$mult3/100;
                                                    $div4=$mult4/100;
                                                    $suma=$div1+$div2+$div3+$div4;

                                                    if($suma>=1 && $suma<=1.39){
                                                        $poner='<div class="resum_c3 ">Alta</div>';
                                                    }else if($suma>=1.4 && $suma<=2.34){
                                                        $poner='<div class="resum_c2">Media</div>';  
                                                    }else if($suma>=2.34 && $suma<=3){
                                                        $poner='<div class="resum_c1">Baja</div>';
                                                    }
                                                }
                                                $html.='<div class="txt_prioridad_2_'.$pre_r.'">'.$poner.'</div>
                                            </div>
                                        </div>
                                    </div>';
                                }
                                $html.='</div></div><div class="div-linea"></div>';
                            }
                        $html.='</td>
                    </tr>';
                }

                $result_ti=$this->Model_guia_seleccion->get_pri_estadosultados_total($this->idcliente);
                $totali=0;
                foreach ($result_ti as $i){
                    $totali=$i->total;
                }
                if($totali!=0){
                    $html.='<tr>
                      <td ><br><br><div style="padding-bottom: 8px; text-align: -webkit-center;"><div class="th_c">Debilidades de tu estado de resultados</div></div></td>
                    </tr>';
                    $html.='<tr>
                        <td>';
                            $result_merc=$this->Model_guia_seleccion->get_pri_estadosultados($this->idcliente);
                            foreach ($result_merc as $g){
                                $html.='<div style="padding-bottom: 8px;"></div><div class="th_c">'.$g->titulo.'</div><div style="padding-bottom: 15px;"><div class="row">';
                                $result_merc=$this->Model_guia_seleccion->get_pri_estadosultados_detalles($this->idcliente,$g->idpregunta);
                                foreach ($result_merc as $x){
                                    $st1=0;
                                    $st2=0;
                                    $st3=0;
                                    $st4=0;
                                    $st5=0;
                                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta_detalles',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                    $pre_r=$x->id;
                                    foreach ($result_f->result() as $f){
                                        $st1=$f->resp1;
                                        $st2=$f->resp2;
                                        $st3=$f->resp3;
                                        $st4=$f->resp4;
                                        //$pr5=$f->resp5;
                                    }
                                    
                                    if($st1=='1'){
                                        $style_p1='background: #70ad47; color:white';
                                    }else if($st1=='2'){
                                        $style_p1='background: #f5bd00; color:black';
                                    }else if($st1=='3'){
                                        $style_p1='background: #e31b1b; color:black';
                                    }else{
                                        $style_p1='';
                                    }


                                    if($st2=='1'){
                                        $style_p2='background: #70ad47; color:white';
                                    }else if($st2=='2'){
                                        $style_p2='background: #f5bd00; color:black';
                                    }else if($st2=='3'){
                                        $style_p2='background: #e31b1b; color:black';
                                    }else{
                                        $style_p2='';
                                    }

                                    if($st3=='1'){
                                        $style_p3='background: #70ad47; color:white';
                                    }else if($st3=='2'){
                                        $style_p3='background: #f5bd00; color:black';
                                    }else if($st3=='3'){
                                        $style_p3='background: #e31b1b; color:black';
                                    }else{
                                        $style_p3='';
                                    }

                                    if($st4=='1'){
                                        $style_p4='background: #70ad47; color:white';
                                    }else if($st4=='2'){
                                        $style_p4='background: #f5bd00; color:black';
                                    }else if($st4=='3'){
                                        $style_p4='background: #e31b1b; color:black';
                                    }else{
                                        $style_p4='';
                                    }
                                    $html.='<div class="col-md-3"><br>
                                        <div style="padding: 12px; background: #2a1f2f; border-radius: 36px; height: 95%; text-align: -webkit-center;">
                                            <div class="th_v" style="padding-left: 12px; padding-right: 12px;  background: #f5bd00 !important; color: #030303 !important; text-align: -webkit-center; max-width: none !important;">'.$x->pregunta.'</div>
                                            <div>
                                                <div>Costo de implementación</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p1.'" class="form-select" id="est_fortaleza'.$x->id.'_1"  onchange="est_fortaleza('.$x->id.',1)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st1==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st1==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st1==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='<div>Riesgos (probabilidad de fracasar)</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p2.'" class="form-select" id="est_fortaleza'.$x->id.'_2"  onchange="est_fortaleza('.$x->id.',2)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st2==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st2==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st2==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Complejidad de implementar</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p3.'" class="form-select" id="est_fortaleza'.$x->id.'_3"  onchange="est_fortaleza('.$x->id.',3)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st3==1) $html.='selected'; $html.='>Bajo</option>';
                                                        $html.='<option value="2" '; if($st3==2) $html.='selected'; $html.='>Medio</option>';
                                                        $html.='<option value="3" '; if($st3==3) $html.='selected'; $html.='>Alto</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Beneficios</div>';
                                                $html.='<select style="width: auto; border-radius: 30px; '.$style_p4.'" class="form-select" id="est_fortaleza'.$x->id.'_4"  onchange="est_fortaleza('.$x->id.',4)"><option valor="0">Selecciona una opción</option>';
                                                        $html.='<option value="1" '; if($st4==1) $html.='selected'; $html.='>Ingresos</option>';
                                                        $html.='<option value="2" '; if($st4==2) $html.='selected'; $html.='>Ahorros</option>';
                                                $html.='</select>';
                                                $html.='
                                                <div>Prioridad</div>';
                                                $poner='';
                                                $result_poner=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta_detalles',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$x->id,'idcliente'=>$this->idcliente));
                                                foreach ($result_poner->result() as $x){
                                                    $pr1=$x->resp1;
                                                    $pr2=$x->resp2;
                                                    $pr3=$x->resp3;
                                                    $pr4=$x->resp4;
                                                    $result_c=$this->General_model->getselect_tabla('matriz_calificacion');
                                                    $costo=0;
                                                    $riesgo=0;
                                                    $complejidad=0;
                                                    $beneficios=0;
                                                    foreach ($result_c->result() as $c){
                                                        $costo=$c->costo;
                                                        $riesgo=$c->riesgo;
                                                        $complejidad=$c->complejidad;
                                                        $beneficios=$c->beneficios;
                                                    }
                                                    
                                                    $mult1=$pr1*$costo;
                                                    $mult2=$pr2*$riesgo;
                                                    $mult3=$pr3*$complejidad;
                                                    $mult4=$pr4*$beneficios;
                                                    $div1=$mult1/100;
                                                    $div2=$mult2/100;
                                                    $div3=$mult3/100;
                                                    $div4=$mult4/100;
                                                    $suma=$div1+$div2+$div3+$div4;

                                                    if($suma>=1 && $suma<=1.39){
                                                        $poner='<div class="resum_c3 ">Alta</div>';
                                                    }else if($suma>=1.4 && $suma<=2.34){
                                                        $poner='<div class="resum_c2">Media</div>';  
                                                    }else if($suma>=2.34 && $suma<=3){
                                                        $poner='<div class="resum_c1">Baja</div>';
                                                    }
                                                }
                                                $html.='<div class="txt_prioridad_3_'.$pre_r.'">'.$poner.'</div>
                                            </div>
                                        </div>
                                    </div>';
                                }
                                $html.='</div></div><div class="div-linea"></div>';
                            }
                        $html.='</td>
                    </tr>';
                }
              $html.='</tbody>
            </table>';
          $html.='</div>';
    
        echo $html;
    }


    function get_tabla_resumen_estrategias_demo()
    {   
        $suma1=0;
        $resultfort=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(1);
        $total_fort=0;
        foreach ($resultfort as $f1){
            $total_fort=$f1->total+1;
        }

        $resultfort2=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(2);
        $total_fort2=0;
        foreach ($resultfort2 as $f2){
            $total_fort2=$f2->total;
        }

        $resultmerc=$this->Model_guia_seleccion->get_total_seleccion_mercado(1);
        $total_merc=0;
        foreach ($resultmerc as $c1){
            $total_merc=$c1->total;
        }
        
        $suma1=$total_fort+$total_merc+$total_fort2;

        $resultfort3=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(3);
        $total_fort3=0;
        foreach ($resultfort3 as $f3){
            $total_fort3=$f3->total+1;
        }

        $resultfort4=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(4);
        $total_fort4=0;
        foreach ($resultfort4 as $f4){
            $total_fort4=$f4->total;
        }

        $resultmerc=$this->Model_guia_seleccion->get_total_seleccion_mercado(2);
        $total_merc2=0;
        foreach ($resultmerc as $m2){
            $total_merc2=$m2->total;
        }
        
        $suma2=$total_fort3+$total_merc2+$total_fort4;
        
        $resultmerc3=$this->Model_guia_seleccion->get_total_seleccion_mercado(3);
        $total_merc3=0;
        foreach ($resultmerc3 as $m3){
            $total_merc3=$m3->total+1;
        }

        $suma3=$total_merc3;
        
        $result_resx1=$this->Model_guia_seleccion->get_total_seleccion_resumen_estado(1);
        $total_res1=0;
        foreach ($result_resx1 as $resx1){
            $total_res1=$resx1->total+1;
        } 
        $suma4=$total_res1;

        $resultfort5=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(5);
        $total_fort5=0;
        foreach ($resultfort5 as $f5){
            $total_fort5=$f5->total+2;
        }

        $result_resx2=$this->Model_guia_seleccion->get_total_seleccion_resumen_estado(2);
        $total_res2=0;
        foreach ($result_resx2 as $resx2){
            $total_res2=$resx2->total;
        } 

        $suma5=$total_fort5+$total_res2;

        $resultfort6=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(6);
        $total_fort6=0;
        foreach ($resultfort6 as $f6){
            $total_fort6=$f6->total+2;
        }

        $result_resx3=$this->Model_guia_seleccion->get_total_seleccion_resumen_estado(3);
        $total_res3=0;
        foreach ($result_resx3 as $resx3){
            $total_res3=$resx3->total;
        } 

        $suma6=$total_fort6+$total_res3;

        $resultfort7=$this->Model_guia_seleccion->get_total_seleccion_fortalezas(7);
        $total_fort7=0;
        foreach ($resultfort7 as $f7){
            $total_fort7=$f7->total+2;
        } 

        $suma7=$total_fort6+$total_res3;
        $html='<div class="">
            <table class="" id="tabla_datos" style="white-space: normal !important;">

              <tbody>';
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;"><div class="th_c">Ventas / Ingresos</div></td>
                  <td style="text-align: -webkit-center;"><div class="th_g"> Estrategias</div></td>
                  <td  colspan="2" style="text-align: -webkit-center; width: 11%;"><div class="th_g">A trabajar</div></td>
                  <td ><div class="th_g">Plazo implementación</div></td>
                  <td ><div class="th_g">Gestiones</div></td>
                </tr>';
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td rowspan="'.$suma1.'" style="text-align: -webkit-center;"><div class="th_c" >Captación de valor</div></td>';
                $html.='  
                </tr>';

                $result_fortx=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>1));
                foreach ($result_fortx->result() as $f1x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f1x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }

                    /*$html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f1x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f1x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f1x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f1x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f1x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/
                    
                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f1x->pregunta.'</div></td>
                      <td style="text-align: center;">'.$pr1x.'</td>
                      <td style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f1x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f1x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f1x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f1x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }

                $result_merx=$this->General_model->getselectwheren('guia_seleccion_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>1));
                foreach ($result_merx->result() as $m1x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$m1x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$m1x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_p_'.$m1x->id.'"  oninput="pregunta_mercado_resumen_input('.$m1x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_g_'.$m1x->id.'"  oninput="pregunta_mercado_resumen_input('.$m1x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/
                    
                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$m1x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_p_'.$m1x->id.'" oninput="pregunta_mercado_resumen_input('.$m1x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_g_'.$m1x->id.'" oninput="pregunta_mercado_resumen_input('.$m1x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }

                $result_fortx=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>2));
                foreach ($result_fortx->result() as $f2x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f2x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }

                    /*$html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f2x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f2x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f2x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f2x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f2x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f2x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f2x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f2x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f2x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f2x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td rowspan="'.$suma2.'" style="text-align: -webkit-center;"><div class="th_c">Redes Sociales, responsabilidad social y audiencia</div></td>
                </tr>';

                $result_fortx=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>3));
                foreach ($result_fortx->result() as $f3x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f3x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /* $html.='<tr>
                      <td class="th_mer" style="text-align: center;">'.$f3x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f3x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f3x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f3x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f3x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f3x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f3x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f3x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f3x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f3x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }

                $result_merx=$this->General_model->getselectwheren('guia_seleccion_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>2));
                foreach ($result_merx->result() as $m2x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$m2x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_mer" style="text-align: center;">'.$m2x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_p_'.$m2x->id.'"  oninput="pregunta_mercado_resumen_input('.$m2x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_g_'.$m2x->id.'"  oninput="pregunta_mercado_resumen_input('.$m2x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$m2x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_p_'.$m2x->id.'" oninput="pregunta_mercado_resumen_input('.$m2x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_g_'.$m2x->id.'" oninput="pregunta_mercado_resumen_input('.$m2x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }

                $result_fortx4=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>4));
                foreach ($result_fortx4->result() as $f4x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f4x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_mer" style="text-align: center;">'.$f4x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f4x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f4x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f4x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f4x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f4x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f4x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f4x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f4x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f4x->id.')" >'.$gestiones.'</textarea></td>
                    </tr>';

                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;" rowspan="'.$suma3.'" style="text-align: center;"><div class="th_c">Entrega</div></td>
                </tr>';
            
                $result_merx3=$this->General_model->getselectwheren('guia_seleccion_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>3));
                foreach ($result_merx3->result() as $m3x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$m3x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_ent" style="text-align: center;">'.$m3x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_p_'.$m3x->id.'"  oninput="pregunta_mercado_resumen_input('.$m3x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_mer_g_'.$m3x->id.'"  oninput="pregunta_mercado_resumen_input('.$m3x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$m3x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_p_'.$m3x->id.'" oninput="pregunta_mercado_resumen_input('.$m3x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_mer_g_'.$m3x->id.'" oninput="pregunta_mercado_resumen_input('.$m3x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;" rowspan="'.$suma4.'" style="text-align: center;"><div class="th_c">Estrategias de crecimiento y rentabilidad</div></td>
                </tr>';

                $result_res1=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_detalles',array('activo'=>1,'resumen'=>1,'orden'=>1));
                foreach ($result_res1->result() as $res1){

                    $pr1=0;
                    $plazo='';
                    $gestiones='';
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$res1->id,'idcliente'=>$this->idcliente));
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }

                    if($pr1==1){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    /*$html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$res1->pregunta.</div>'</td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_p_'.$res1->id.'"  oninput="pregunta_estado_resumen_input('.$res1->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_g_'.$res1->id.'"  oninput="pregunta_estado_resumen_input('.$res1->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$res1->pregunta.'</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_p_'.$res1->id.'" oninput="pregunta_estado_resumen_input('.$res1->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_g_'.$res1->id.'" oninput="pregunta_estado_resumen_input('.$res1->id.')">'.$gestiones.'</textarea></td>
                    </tr>';

                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;" rowspan="'.$suma5.'" style="text-align: center;"><div class="th_c">Palancas que impactan la rentabilidad</div></td>
                </tr>';

                $result_fortx5=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>5));

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_g">Estrategias</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                    </tr>';
                foreach ($result_fortx5->result() as $f5x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f5x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_mer2" style="text-align: center;">'.$f5x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f5x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f5x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f5x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f5x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f5x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f5x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f5x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f5x->id.'" oninput="pregunta_fortaleza_resumen_input('.$f5x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';
                }

                $result_res2=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_detalles',array('activo'=>1,'resumen'=>1,'orden'=>2));
                foreach ($result_res2->result() as $res2){
                    $pr1=0;
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$res2->id,'idcliente'=>$this->idcliente));
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                    }

                    if($pr1==1){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }
                    /*$html.='<tr>
                      <td class="th_mer2" style="text-align: center;">'.$res2->pregunta.'</td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_p_'.$res2->id.'"  oninput="pregunta_estado_resumen_input('.$res2->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_g_'.$res2->id.'"  oninput="pregunta_estado_resumen_input('.$res2->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$res2->pregunta.'</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_p_'.$res2->id.'"  oninput="pregunta_estado_resumen_input('.$res2->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_g_'.$res2->id.'"  oninput="pregunta_estado_resumen_input('.$res2->id.')">'.$gestiones.'</textarea></td>
                    </tr>';
                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;" rowspan="'.$suma6.'" style="text-align: center;"><div class="th_c">Palancas que impactan en la gestión empresarial</div></td>
                </tr>';

                $result_fortx6=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>6));
                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_g">Estrategias</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                    </tr>';
                foreach ($result_fortx6->result() as $f6x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f6x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_ges2" style="text-align: center;">'.$f6x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f6x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f6x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f6x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f6x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f6x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f6x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f6x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f6x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f6x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';
                }

                $result_res3=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_detalles',array('activo'=>1,'resumen'=>1,'orden'=>3));
                foreach ($result_res3->result() as $res3){
                    $pr1=0;
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$res3->id,'idcliente'=>$this->idcliente));
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                    }

                    if($pr1==1){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }
                    /*$html.='<tr>
                      <td class="th_ges2" style="text-align: center;">'.$res3->pregunta.'</td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_p_'.$res3->id.'"  oninput="pregunta_estado_resumen_input('.$res3->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_est_g_'.$res3->id.'"  oninput="pregunta_estado_resumen_input('.$res3->id.')" value="'.$gestiones.'"></td>
                    </tr>'*/;

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$res3->pregunta.'</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_p_'.$res3->id.'"  oninput="pregunta_estado_resumen_input('.$res3->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_est_g_'.$res3->id.'"  oninput="pregunta_estado_resumen_input('.$res3->id.')">'.$gestiones.'</textarea></td>
                    </tr>';
                }
                $html.='<tr>
                      <td style="text-align: center;" colspan="6" ><div class="div-linea"></div></td>
                    </tr>';
                $html.='<tr>
                  <td style="text-align: -webkit-center;" rowspan="'.$suma7.'" style="text-align: center;"><div class="th_c">Palancas que impactan en el pilar del capital humano</div></td>
                </tr>';

                $result_fortx7=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_detalles',array('activo'=>1,'resumen'=>1,'orden'=>7));

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_g">Estrategias</div></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                      <td class="" style="text-align: center;"></td>
                    </tr>';
                foreach ($result_fortx7->result() as $f7x){
                    $result_f=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$f7x->id,'idcliente'=>$this->idcliente));
                    $pr1=0;
                    $pr2=0;
                    $plazo='';
                    $gestiones='';
                    foreach ($result_f->result() as $f){
                        $pr1=$f->resp1;
                        $pr2=$f->resp2;
                        $plazo=$f->plazo;
                        $gestiones=$f->gestiones;
                    }
                    if($pr1==1){
                        $pr1x='F';
                    }else if($pr1==2){
                        $pr1x='D';
                    }else{
                        $pr1x=''; 
                    }

                    if($pr2==1){
                        $pr2x='F';
                    }else if($pr2==2){
                        $pr2x='D';
                    }else{
                        $pr2x='';
                    }
                    /*$html.='<tr>
                      <td class="th_pil2" style="text-align: center;">'.$f7x->pregunta.'</td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_p_'.$f7x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f7x->id.')" value="'.$plazo.'"></td>
                      <td class="td_input"><input type="text" class="form-control" id="pre_for_g_'.$f7x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f7x->id.')" value="'.$gestiones.'"></td>
                    </tr>';*/

                    $html.='<tr>
                      <td style="text-align: -webkit-center;"><div class="th_v">'.$f7x->pregunta.'</div></td>
                      <td class="" style="text-align: center;">'.$pr1x.'</td>
                      <td class="" style="text-align: center;">'.$pr2x.'</td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_p_'.$f7x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f7x->id.')">'.$plazo.'</textarea></td>
                      <td class="td_input"><textarea rows="1" class="form-control js-auto-size" id="pre_for_g_'.$f7x->id.'"  oninput="pregunta_fortaleza_resumen_input('.$f7x->id.')">'.$gestiones.'</textarea></td>
                    </tr>';
                }
               
              $html.='</tbody>
            </table>';
          $html.='</div>
          <br><br>
            <div style="padding-left: 20px; padding-right: 20px;">
              <div class="row">
                  <div class="col-md-2"></div>
                  <div class="col-md-4">
                      <a style="cursor:pointer" href="'.base_url().'Analisisfoda?con=2" ><span style="border-bottom: 3px solid #bd9250; font-size: 20px;">Resumen de estrategias</span></a>
                  </div>
                  <div class="col-md-2" style="text-align: right;">
                    <a type="button" class="btn btn-primary me-sm-3 me-1 btn_registro waves-effect waves-light" style="text-transform: math-auto;" href="'.base_url().'Analisisfoda?con=3">Siguiente <span class="tf-icons mdi mdi-chevron-right"></span></a>
                  </div>
              </div>    
            </div><br>';
    
        echo $html;
    }

    public function edit_fortaleza_pregunta_seleccion_resumen(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta1=$this->input->post('respuesta1');
        $respuesta2=$this->input->post('respuesta2');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_preguntas_respuesta',array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->add_record('guia_seleccion_fortalezas_preguntas_respuesta',$data);
        }else{
            $data = array('plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_preguntas_respuesta');    
        }
        
    }

    public function edit_pregunta_seleccion_resumen(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta1=$this->input->post('respuesta1');
        $respuesta2=$this->input->post('respuesta2');
        $result=$this->General_model->getselectwheren('guia_seleccion_preguntas_respuesta',array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_preguntas_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->add_record('guia_seleccion_preguntas_respuesta',$data);
        }else{
            $data = array('plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_preguntas_respuesta');    
        }
        
    }

    public function edit_seleccion_estado_resultados_resumen(){
        $idpregunta=$this->input->post('idpregunta');
        $respuesta1=$this->input->post('respuesta1');
        $respuesta2=$this->input->post('respuesta2');
        $result=$this->General_model->getselectwheren('guia_seleccion_fortalezas_estado_resultados_respuesta',array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente));
        $id_rg=0;
        foreach ($result->result() as $x){
            $id_rg=$x->id;
        }
        if($id_rg==0){
            $data = array('idguia_seleccion_fortalezas_estado_resultados_detalles'=>$idpregunta,'idcliente'=>$this->idcliente,'plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->add_record('guia_seleccion_fortalezas_estado_resultados_respuesta',$data);
        }else{
            $data = array('plazo'=>$respuesta1,'gestiones'=>$respuesta2);
            $this->General_model->edit_record('id',$id_rg,$data,'guia_seleccion_fortalezas_estado_resultados_respuesta');    
        }
        
    }
}