<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BibliotecaRecursos extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_periodo');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){    
        $data['btn_active']=3;
        $data['btn_active_sub']=4;         
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('biblioteca/index');
        $this->load->view('templates/footer');
        $this->load->view('biblioteca/indexjs');
    }

    public function registro(){        
        $data['btn_active']=3;
        $data['btn_active_sub']=4;     
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('biblioteca/form');
        $this->load->view('templates/footer');
        $this->load->view('biblioteca/formjs');
    }

}