<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas_reales extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('Model_periodo');
        $this->load->model('ModeloCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->anio = date('Y');
        $this->mes = date('m');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,19);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $idcliente=$this->idpersonal;
        $anio=$this->anio;
        //==============================================
            $meses=$this->getmeses();
            
            $r_v_a=$this->General_model->getselectwheren('ventas_reales_anio',array('idcliente'=>$idcliente,'anio'=>$anio));
            if($r_v_a->num_rows()==0){
                $datainser=array();
                foreach ($meses as $itemm) {
                    //$array_dias =$this->optenerdiasdelmes($anio,$itemm['mes_num']);
                    //=================================================================
                        $year = $anio;
                        $month = $itemm['mes_num']; // Enero

                        // Obtener el número de días en el mes
                        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

                        // Crear un array para almacenar los días
                        $daysArray = [];

                        // Llenar el array con cada día del mes
                        for ($day = 1; $day <= $daysInMonth; $day++) {
                            $daysArray[] = $day;
                        }
                        for ($day = 1; $day <= $daysInMonth; $day++) {
                            // Formatear cada día como "YYYY-MM-DD"
                            //$date = sprintf('%4d-%02d-%02d', $year, $month, $day);
                            //$date=$day;
                            //$daysArray[] = $date;

                            $datainser[]=array('idcliente'=>$idcliente,'anio'=>$anio,'mes'=>$month,'dia'=>$day);
                        }
                    //=================================================================
                }
                if(count($datainser)>0){
                    $this->General_model->insert_batch('ventas_reales_anio',$datainser);
                }
                
            }
        //============================================== 
        $data['btn_active']=3;
        $data['btn_active_sub']=4;
        $data['meses']=$meses;  
        $data['anio']=$anio;
        $data['idcliente']=$idcliente;         
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('ventasreales/index');
        $this->load->view('templates/footer');
        $this->load->view('ventasreales/indexjs');
    }
    function optenerdiasdelmes($year,$month){
        //$year = 2023; // Año
        //$month = 1; // Enero

        // Obtener el número de días en el mes
        $daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        // Crear un array para almacenar los días
        $daysArray = [];

        // Llenar el array con cada día del mes
        for ($day = 1; $day <= $daysInMonth; $day++) {
            $daysArray[] = $day;
        }

        // Mostrar el array
        print_r($daysArray);
        return $daysArray;
    }
    function obtener_info_mes(){
        $params = $this->input->post();
        $mes = $params['mes'];
        $mesn = $params['mesn'];
        $cli = $params['cli'];
        $anio = $params['anio'];

        $r_v_m=$this->General_model->getselectwheren('ventas_reales_anio',array('idcliente'=>$cli,'anio'=>$anio,'mes'=>$mes));
        $html_tr='';
        $tota_montos=0;
        foreach ($r_v_m->result() as $item) {
            $tota_montos=$tota_montos+$item->monto;
            $t_inpu='<input type="number" class="input_monto input_monto_'.$item->id.' form-control" value="'.$item->monto.'" onchange="updatemontos('.$item->id.')">';
            $html_tr.='<tr><td>'.$item->dia.'</td><td class="td_input">'.$t_inpu.'</td></tr>';
        }
        $html='';



        $html.='<table class="table table-dark table-dar table-bordered" style="width: 300px;"><tbody>';
                $html.='<tr><td><span style="width:90px">Total '.$mesn.'</span> </td><td class="tota_montos">$ '.number_format($tota_montos,2,'.',',').'</td></tr>';
        $html.='</tbody></table>';

        $html.='<table class="tablepagos table table-bordered">
                <thead class="table-light">
                    <tr><th>Dia</th><th>Ingresos</th></tr>
                </thead><tbody>';
                $html.=$html_tr;
                    $html.='<tr><td><span style="width:90px">Total '.$mesn.'</span> </td><td class="tota_montos">$ '.number_format($tota_montos,2,'.',',').'</td></tr>';

        $html.='</tbody></table>';
        echo $html;
    }
    function updatemontos(){
        $params = $this->input->post();
        $id = $params['id'];
        $monto = $params['monto'];
        $this->ModeloCatalogos->updateCatalogo('ventas_reales_anio',array('monto'=>$monto),array('id'=>$id));
    }
    function infogeneralmeses(){
        $params = $this->input->post();
        $cli = $params['cli'];
        $anio = $params['anio'];

        $meses=$this->getmeses();
        $html='';
        $html='<table class="table table-dark table_meses_pagos">';


        $result_mes_g = $this->ModeloCatalogos->obtenermontostotalespormes($cli,$anio);

        foreach ($result_mes_g->result() as $itemm) {
            $idmes=$itemm->idmes;
            $idmes_menos=$idmes-1;
            if($itemm->monto>0){
                $monto_del_mes=$itemm->monto;
            }else{
                $monto_del_mes=0;
            }
            ${'monto_del_mes_'.$idmes}=$monto_del_mes;
            $stilo='';
            if($idmes>1){
                if(${'monto_del_mes_'.$idmes_menos}>0){
                    $porcentaje=round(((${'monto_del_mes_'.$idmes}-${'monto_del_mes_'.$idmes_menos})/${'monto_del_mes_'.$idmes_menos})*100,0);
                    if($porcentaje>0){
                        $stilo='green';
                    }
                    if($porcentaje<0){
                        $stilo='red';
                    }
                    $porcentaje.=' %';
                }else{
                    $porcentaje='';
                }
                
            }else{
                $porcentaje='';
            }
            $html.='<tr> <td>'.$itemm->mesname.'</td> <td>$ '.number_format($monto_del_mes,2,'.',',').'</td> <td class="'.$stilo.'">'.$porcentaje.'</td></tr>';

        }

        
        $html.='</table>';
        echo $html;

    }
    function getmeses(){
        $meses[]=array('mes_num'=>1,'mes_name'=>'ENERO');
        $meses[]=array('mes_num'=>2,'mes_name'=>'FEBRERO');
        $meses[]=array('mes_num'=>3,'mes_name'=>'MARZO');
        $meses[]=array('mes_num'=>4,'mes_name'=>'ABRIL');
        $meses[]=array('mes_num'=>5,'mes_name'=>'MAYO');
        $meses[]=array('mes_num'=>6,'mes_name'=>'JUNIO');
        $meses[]=array('mes_num'=>7,'mes_name'=>'JULIO');
        $meses[]=array('mes_num'=>8,'mes_name'=>'AGOSTO');
        $meses[]=array('mes_num'=>9,'mes_name'=>'SEPTIEMBRE');
        $meses[]=array('mes_num'=>10,'mes_name'=>'OCTUBRE');
        $meses[]=array('mes_num'=>11,'mes_name'=>'NOVIEMBRE');
        $meses[]=array('mes_num'=>12,'mes_name'=>'DICIEMBRE');
        return $meses;
    }



}