INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`, `visible`) VALUES (NULL, '6', 'Ventas reales del año', 'Ventas_reales', 'x', '2', '0', '1', '9', '1');
/*******************/
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '2', '19');
/*******************/
CREATE TABLE `ventas_reales_anio` (
  `id` bigint(20) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `dia` int(11) DEFAULT NULL,
  `monto` decimal(14,2) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/* no asta que se defina las secciones del cliente
ALTER TABLE `ventas_reales_anio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ventas_reales_fk_cliente` (`idcliente`);
*/
/*******************/
ALTER TABLE `ventas_reales_anio` CHANGE `monto` `monto` DECIMAL(14,2) NULL;
/***********************************/
CREATE TABLE `meses` (
  `idmes` int(11) NOT NULL,
  `mesname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `meses` (`idmes`, `mesname`) VALUES
(1, 'ENERO'),
(2, 'FEBRERO'),
(3, 'MARZO'),
(4, 'ABRIL'),
(5, 'MAYO'),
(6, 'JUNIO'),
(7, 'JULIO'),
(8, 'AGOSTO'),
(9, 'SEPTIEMBRE'),
(10, 'OCTUBRE'),
(11, 'NOVIEMBRE'),
(12, 'DICIEMBRE');


ALTER TABLE `meses`
  ADD PRIMARY KEY (`idmes`);


ALTER TABLE `meses`
  MODIFY `idmes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/******************2024-04-17*****************/
    CREATE TABLE `costosventas` (
      `id` bigint(20) NOT NULL,
      `id_linea_servicio` bigint(20) NOT NULL,
      `anio` int(4) NOT NULL,
      `mes` tinyint(4) NOT NULL,
      `valor` decimal(14,2) NOT NULL,
      `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
      `activo` tinyint(1) NOT NULL DEFAULT '1'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    --
    -- Índices para tablas volcadas
    --

    --
    -- Indices de la tabla `costosventas`
    --
    ALTER TABLE `costosventas`
      ADD PRIMARY KEY (`id`),
      ADD KEY `costoventas_fk_vh` (`id_linea_servicio`);

    --
    -- AUTO_INCREMENT de las tablas volcadas
    --

    --
    -- AUTO_INCREMENT de la tabla `costosventas`
    --
    ALTER TABLE `costosventas`
      MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

    --
    -- Restricciones para tablas volcadas
    --

    --
    -- Filtros para la tabla `costosventas`
    --
    ALTER TABLE `costosventas`
      ADD CONSTRAINT `costoventas_fk_vh` FOREIGN KEY (`id_linea_servicio`) REFERENCES `ventashistoricas` (`id`) ON UPDATE NO ACTION;
/*********************2024-04-17************************/
  ALTER TABLE `costosventas` CHANGE `valor` `valor` DECIMAL(14,2) NULL DEFAULT NULL;
/*********************************/
/*****************2024-04-18*******************/
  CREATE TABLE `costosventas_pro_dll` (
    `id` bigint(20) NOT NULL,
    `idpro` bigint(20) NOT NULL,
    `anio` int(4) NOT NULL,
    `mes` tinyint(2) NOT NULL,
    `valor` date NOT NULL,
    `activo` tinyint(1) NOT NULL DEFAULT '1',
    `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  --
  -- Índices para tablas volcadas
  --

  --
  -- Indices de la tabla `costosventas_pro_dll`
  --
  ALTER TABLE `costosventas_pro_dll`
    ADD PRIMARY KEY (`id`),
    ADD KEY `cvpd_fk_pro` (`idpro`);

  --
  -- AUTO_INCREMENT de las tablas volcadas
  --

  --
  -- AUTO_INCREMENT de la tabla `costosventas_pro_dll`
  --
  ALTER TABLE `costosventas_pro_dll`
    MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

  --
  -- Restricciones para tablas volcadas
  --

  --
  -- Filtros para la tabla `costosventas_pro_dll`
  --
  ALTER TABLE `costosventas_pro_dll`
    ADD CONSTRAINT `cvpd_fk_pro` FOREIGN KEY (`idpro`) REFERENCES `costosventas_pro` (`id`) ON UPDATE NO ACTION;

/**********************************************************************/
ALTER TABLE `costosventas_pro_dll` CHANGE `valor` `valor` DATE NULL DEFAULT NULL;
/*****************/
/*****************2024-04-22*******************/
CREATE TABLE `costosventas_proyectados` (
  `id` bigint(20) NOT NULL,
  `idser` bigint(20) NOT NULL,
  `idpro` bigint(20) NOT NULL,
  `anio` int(4) NOT NULL,
  `mes` int(2) NOT NULL,
  `porcentaje` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `costosventas_proyectados`
--
ALTER TABLE `costosventas_proyectados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proyec_fk_ser` (`idser`),
  ADD KEY `proyec_fk_pro` (`idpro`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `costosventas_proyectados`
--
ALTER TABLE `costosventas_proyectados`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `costosventas_proyectados`
--
ALTER TABLE `costosventas_proyectados`
  ADD CONSTRAINT `proyec_fk_pro` FOREIGN KEY (`idpro`) REFERENCES `costosventas_pro` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `proyec_fk_ser` FOREIGN KEY (`idser`) REFERENCES `ventashistoricas` (`id`) ON UPDATE NO ACTION;

/************************/