ALTER TABLE `ventashistoricas_detalles` ADD `promedio` FLOAT NOT NULL AFTER `cantidad_1`;
ALTER TABLE `ventashistoricas_detalles` CHANGE `promedio` `promedio` DECIMAL(10,2) NOT NULL;