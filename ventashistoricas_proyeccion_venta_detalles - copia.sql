
DROP TABLE IF EXISTS `ventashistoricas_proyeccion_venta_detalles`;
CREATE TABLE IF NOT EXISTS `ventashistoricas_proyeccion_venta_detalles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idventashistoricas_proyecion_venta` bigint(20) NOT NULL,
  `idventashistoricas` bigint(20) NOT NULL,
  `opcion_a` decimal(10,2) NOT NULL,
  `opcion_b` decimal(10,2) NOT NULL,
  `opcion_porcentaje_c` decimal(10,2) NOT NULL,
  `opcion_c` decimal(10,2) NOT NULL,
  `opcion_seleccionada` decimal(10,2) NOT NULL,
  `num_tabla` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

