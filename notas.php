10/09/2024

ALTER TABLE `guia_seleccion_preguntas_respuesta_detalles` ADD `anio` INT(5) NOT NULL AFTER `resultado`, ADD `semestre` TINYINT NOT NULL AFTER `anio`;

ALTER TABLE `guia_seleccion_fortalezas_preguntas_respuesta_detalles` ADD `anio` INT(5) NOT NULL AFTER `resultado`, ADD `semestre` TINYINT NOT NULL AFTER `anio`;

ALTER TABLE `guia_seleccion_fortalezas_estado_resultados_respuesta_detalles` ADD `anio` INT(5) NOT NULL AFTER `resultado`, ADD `semestre` TINYINT NOT NULL AFTER `anio`;


/////////////////////////
guia_seleccion_preguntas_respuesta_detalles_cronograma

23/10/2024
UPDATE `guia_preguntas_detalles_opciones` SET `respuesta` = 'Menos' WHERE `guia_preguntas_detalles_opciones`.`id` = 44;

UPDATE `guia_seleccion_preguntas_tipo` SET `activo` = '0' WHERE `guia_seleccion_preguntas_tipo`.`id` = 2;
UPDATE `guia_seleccion_fortalezas_preguntas_tipo` SET `activo` = '0' WHERE `guia_seleccion_fortalezas_preguntas_tipo`.`id` = 2;
UPDATE `guia_seleccion_fortalezas_preguntas_tipo` SET `nombre` = 'Selecciona las fortalezas a mantener y las debilidades a gestionar' WHERE `guia_seleccion_fortalezas_preguntas_tipo`.`id` = 1;
UPDATE `guia_seleccion_preguntas_tipo` SET `nombre` = 'Selecciona las fortalezas a mantener y las debilidades a gestionar' WHERE `guia_seleccion_preguntas_tipo`.`id` = 1;


UPDATE `guia_seleccion_preguntas_tipo` SET `nombre` = 'Selecciona las fortalezas a mantener y las debilidades a gestionar' WHERE `guia_seleccion_preguntas_tipo`.`id` = 1;
UPDATE `guia_seleccion_fortalezas_preguntas_tipo` SET `nombre` = 'Selecciona las fortalezas a mantener y las debilidades a gestionar' WHERE `guia_seleccion_fortalezas_preguntas_tipo`.`id` = 1;


28/10/2024
UPDATE `guia_preguntas_cliente_detalles` SET `pregunta` = '¿Cuáles son las metas funcionales que resuelve tu producto/servicio de tu cliente? Meta Funcional es ¿a dónde quiere llegar con tu producto/servicio? ¿lo que quiere hacer con tu producto/servicio?' WHERE `guia_preguntas_cliente_detalles`.`id` = 15;
UPDATE `guia_preguntas_cliente_detalles` SET `activo` = '0' WHERE `guia_preguntas_cliente_detalles`.`id` = 16;

UPDATE `guia_seleccion_fortalezas_preguntas` SET `titulo` = 'Palanca Capital Financiero' WHERE `guia_seleccion_fortalezas_preguntas`.`id` = 4;

//scroll(0, 100);


06/11/2024
ALTER TABLE `foda_oportunidad` ADD `resp2` TEXT NOT NULL AFTER `reg`;
ALTER TABLE `foda_oportunidad` ADD `resp3` TEXT NOT NULL AFTER `resp2`;
ALTER TABLE `foda_debilidades` ADD `resp2` TEXT NOT NULL AFTER `reg`;


02/11/2024
ALTER TABLE `foda_fortaleza` ADD `resp2` TEXT NOT NULL AFTER `reg`;

========================================================================================================================================

Analisisfoda
Guia_seleccion_factores_criticos_exito

Model_guia_seleccion

Views 
Templates